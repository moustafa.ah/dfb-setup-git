# from collections import deque
from .rollbuffer import RollingBuffer

class DataProducer(object):

	def __init__(self,maxlength=1000,numcol=2):
		self.consumers = []
		self.maxlength = maxlength
		self.buf = RollingBuffer(maxlength,numcol)
		# self.deque = deque([],maxlength)

	def register_consumer(self,consumer):
		if consumer not in self.consumers:
			self.consumers.append(consumer)

	def unregister_consumer(self,consumer):
		if consumer in self.consumers:
			self.consumers.remove(consumer)
			print('Removed consumer {} from list of producer {} ({} consumers remaining)'.format(str(consumer) , str(self), len(self.consumers) ))

	def send_to_consumers(self,data):
		for consumer in self.consumers:
			consumer.data_queue.put(data)

	def send_multiple_to_consumers(self,array):
		for consumer in self.consumers:
			map(consumer.data_queue.put,array)