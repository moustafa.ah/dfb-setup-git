from .driver import Driver
from pyqtgraph.Qt import QtGui, QtCore
import yaml

class DeviceSet(object):

	def get_config(self):
		# devices = [dev for key, dev in self.__dict__.items() if isinstance(dev,Driver)]
		print("Getting devices configuration")
		devices = [dev for key, dev in self.__dict__.items() if hasattr(dev,"get_config")]
		configs = []
		for dev in devices: configs.append(dev.get_config())
		print("Done")
		return configs

	def save_config(self,filename):
		config = self.get_config()
		with open(filename, 'w') as outfile:
			yaml.safe_dump(config, outfile, default_flow_style=False)

	def save_config_ask_filename(self):
		filename = QtGui.QFileDialog.getSaveFileName(None, "Save File", "./config.yaml", "YAML file (*.yaml)")
		self.save_config(filename)