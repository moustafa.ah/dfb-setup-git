import numpy as np
import collections

class RollingBuffer():
    "A 2D ring buffer using numpy arrays"
    def __init__(self, length, num_col):
        self.num_col = num_col
        self.length = length
        self.data = np.zeros((length,num_col))
        self.idxdeque = collections.deque(maxlen=length)
        self.index = 0

    def append(self,col):
        newIndex = self.index % self.length
        self.data[newIndex] = col
        self.idxdeque.append(newIndex)
        self.index += 1

    def extend(self,cols):
        for col in cols:
            newIndex = self.index % self.length
            self.data[newIndex] = col
            self.idxdeque.append(newIndex)
            self.index += 1

    def get(self):
        return self.data[self.idxdeque]
        # idx = (np.arange(max([0,self.index-self.length]),self.index)) % self.length
        # return self.data[idx]

    def clear(self):
        self.idxdeque.clear()
        self.index = 0