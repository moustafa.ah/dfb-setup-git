# Set high priority execution on the system process 
import psutil, os
p = psutil.Process(os.getpid())
p.nice(psutil.HIGH_PRIORITY_CLASS)

import time, sys
sys.coinit_flags = 2
from base.deviceset import DeviceSet
from datamanager.datalogger import DataLogger
from datamanager.datagetter_ui import DataGetter_UI
from tasks.autotaskpanel import AutoTaskPanel
from utils.adev_graph_ui import Adev_graph_UI
from storage.shelved_param_ui import Shelved_param_UI

print("Loading pyqtgraph")
import pyqtgraph as pg
from pyqtgraph.dockarea import *
from pyqtgraph.Qt import QtGui, QtCore
import pyqtgraph.parametertree.parameterTypes as pTypes
from pyqtgraph.parametertree import Parameter, ParameterTree

app = QtGui.QApplication([])

print("Loading drivers")
from drivers.temperatureCtrl.cypidreg_ui import CYPidReg_UI
from drivers.lockBox.lockBox_ui import LockBox_UI
from drivers.powerMeter.pm16dotnet import PM16
#from drivers.powerControllerAOM_ui import PowerControllerAOM_UI
from drivers.freqSynth.smb100a_ui import SMB100A_UI, SMB100A_AM_UI
from drivers.lockInAmp.sr830_ui import SR830_UI
from drivers.nidaq.syncAIAO_ui_ErrSig import SyncAIAO_UI
from drivers.nidaq.syncAIAO_pulsed3_ui import SyncAIAO_pulsed_UI
#from drivers.nidaq.syncAIAOlock_ui import SyncAIAOlock_UI
#from drivers.nidaq.asserv_ui_CES import Asserv_UI
#from drivers.nidaq.asserv_CW_CES_rb1_ui import Asserv_UI
#from drivers.nidaq.asserv_CW_CES_rb1_m_ui import Asserv_UI
#from drivers.nidaq.asserv_CW_CES_rb1_m_2AIs_symm_ui import Asserv_UI
from drivers.nidaq.asserv_CW_CES_rb1_m_2AIs_symm_viewer_ui import Asserv_UI
from drivers.nidaq.asserv_pulsed6_UI import Asserv_pulsed_UI
from drivers.nidaq.analogOutputs import AnalogOutputs
from drivers.multimeter.hp34401a import HP34401A
from drivers.tempController import TempController
from drivers.laserDiodeDrv.np560b_ui import NP560B_UI
from drivers.pressure_sensor.bme280_ui import BME280_UI



from ui.graphs.cw_freq_scanner_ErrSig import CWFreqScanner
from ui.graphs.laser_freq_scanner import LaserFreqScanner
from ui.graphs.pulsed_freq_scanner import PulseFreqScanner


ds = DeviceSet()

#ds.shelved_params = Shelved_param_UI(filename='new_vcsel_params')

ds.regulation = CYPidReg_UI(comPort='com4')
ds.lock_box = LockBox_UI(comPort='com5')
ds.pow_meter = PM16(dev_ref='USB0::0x1313::0x807B::15110239::INSTR')
# ds.daq = NIUSB6259(devID='Dev1')
ds.fs = SMB100A_UI(pow_amp_lim=12,
					usbDev="USB0::0x0AAD::0x0054::178059::INSTR",
					name="RF Driver")
ds.aom_fs = SMB100A_AM_UI(pow_amp_lim = -1,
					usbDev= "USB0::0x0AAD::0x0054::176227::INSTR",
					name="AOM Driver")
ds.li_las = SR830_UI(gpibAdress=9,name="Laser DC lock-in")
# ds.syncAiAo = SyncAIAO_UI(device="Dev1",
# 					inChanList=["ai0","ai1"],
# 					outChan="ao3",
# 					inRange=(-3.,3.),
# 					outRange=(-1.,1.))
ds.syncAiAo = SyncAIAO_UI(device="Dev1",
					dds_device=ds.fs,
					inChanList=["ai0","ai1"],
					outChan="ao3",
					inRange=(-3.,3.),
					outRange=(-1.,1.))
ds.laserSyncAiAo = SyncAIAO_UI(device="Dev1",inChanList=["ai0","ai1"],outChan="ao2",inRange=(-10.,10.),outRange=(-10.,10.)) 
ds.laserSyncAiAo.vpp=18
ds.laserSyncAiAo.numSamp=4000
ds.laserSyncAiAo.nbSampCropped=2000
ds.pulsedsyncAiAo = SyncAIAO_pulsed_UI(device="Dev1",outChan="ao3", DOChan = 'port0/line0',inChan="ai0",auxChan='ai1',inRange=(-3.,3.),outRange=(-1.,1.))
ds.pulsedAsserv = Asserv_pulsed_UI(device="Dev1",outChan="ao3",inChan="ai0", doChan = "port0/line0", dds_device=ds.fs, power_lock_device = None)

#ds.asserv = Asserv_UI(device="Dev1",rf_fm_out_chan="ao3",inChan="ai0",dds_device=ds.fs)
#ds.asserv = Asserv_UI(device="Dev1", rf_fm_out_chan="ao3", inChan="ai0", dds_device=ds.fs)
ds.asserv = Asserv_UI(device="Dev1", rf_am_aom_out_chan="ao1", lo_fm_out_chan="ao3", pdcpt_inChan="ai0", pdincell_inChan="ai2", dds_device=ds.fs)
#ds.pow_ctrl = PowerControllerAOM_UI(ds.pow_meter,ds.aom_fs,'Dev1/ao1',beam_splitter_factor=5.2,asserv = ds.asserv)
# ds.laserCtrl = LaserController_UI(ds.daq,laserModChan="ao2",signalChan="ai0",lockBox=ds.lock_box)
ds.analogOuts = AnalogOutputs(devID='Dev1')
ds.analogOuts.add_channel('Laser DC offset','ao2')
# ds.analogOuts.add_channel('AOM offset','ao1')
ds.analogOuts.add_channel('Vcsel Temp tune','ao0',step=0.0001,daq_range=(-1,1))
# ds.multimeter2 = HP34401A(gpibAdress=16)
ds.multimeter3 = HP34401A(gpibAdress=10)
ds.multimeter3.configure('RES',10000,1e-2)
ds.multimeter4 = HP34401A(gpibAdress=24)
ds.multimeter4.configure('RES',10000,1e-2)
ds.multimeter5 = HP34401A(gpibAdress=13)
ds.multimeter5.configure('RES',10000,1e-2)
ds.multimeter6 = HP34401A(gpibAdress=15)
ds.multimeter6.configure('VOLT:DC',1,1e-6)
ds.multimeter7 = HP34401A(gpibAdress=23)
ds.multimeter7.configure('VOLT:DC',2,1e-5)

ds.bme280 = BME280_UI(comPort='com6')
#ds.mag_ldd = NP560B_UI(dev_key='501B 10114')



def convertor(voltage):
	resistance = voltage/4.94287e-4
	return resistance

#ds.tempCtrlVcsel = TempController(ds.multimeter6,name="VCSEL thermometer",char="ULMBETHA",convertorVtoRes=convertor)
ds.pow_mtr_tempCtrl = TempController(ds.multimeter5,name="Pow mtr thermometer",char="BETHAT10K")
ds.pd_tempCtrl = TempController(ds.multimeter3,name="PD thermometer",char="BETHAT10K")
ds.tempCtrlBox = TempController(ds.multimeter4,name="Box thermometer",char="BETHAT10K")



		# Rows must have the form:    [ "Label", function ]
		# 					       or [ "Label", (object,'property') ]
columns = [["Time",time.time],
				["Cell temperature (degC)",(ds.regulation,'temperature')],
				["Power meter temp (degC)",(ds.pow_mtr_tempCtrl,'temperature')],
				["Photodiode temp (degC)",(ds.pd_tempCtrl,'temperature')],
				["Box temperature (degC)",(ds.tempCtrlBox,'temperature')],
				["Heater current Image",(ds.regulation,'output')],
				#["Laser current (V)",ds.multimeter5.fetch_and_trig],
				# ["Optical Power Output Cell (V)",a.multimeter2.read],
				["Vcsel Temperature (V)",ds.multimeter6.fetch_and_trig],
				["Laser mod signal (V)",ds.multimeter7.fetch_and_trig],
				["Input power (uW)",(ds.pow_meter, 'power')],
				# ["RF power (dBm)",(ds.fs,'power_amplitude')],
				# ["FM dev (Hz)",(ds.fs,'fm_dev')],
				]
ds.data_producer = DataGetter_UI(columns,delay=0.0,maxlength=1000,name='DataGetter')

ds.dl = DataLogger(ds,data_folder="G:/Utilisateurs/tfmanips/Desktop/Pilotage/Python Data")
ds.dl.register_device(ds.asserv, "asserv")
ds.dl.register_device(ds.pulsedAsserv, "pulsedasserv")
ds.dl.register_device(ds.data_producer, "data_producer")
#ds.dl.register_device(ds.pow_ctrl, "pow_ctrl")
ds.dl.register_device(ds.bme280, "bme280")

autotaskpanel = AutoTaskPanel()

win = QtGui.QMainWindow()
area = DockArea()
win.setCentralWidget(area)
win.resize(1600,900)
win.setWindowTitle('New VCSEL main')

autotaskpanel.window = win
autotaskpanel.deviceset = ds

cw_freq_scanner = CWFreqScanner(ds.syncAiAo,ds.fs)
laser_freq_scanner = LaserFreqScanner(ds.laserSyncAiAo)
pulsed_freq_scanner = PulseFreqScanner(ds.pulsedsyncAiAo,ds.fs)

# Create docks
d1 = Dock("Controls", size=(2, 1))
d2 = Dock("Data Logger", size = (4,1))
d2b = Dock("Data Logger 2", size = (4,1))
d3 = Dock("Absorption signal", size = (4,1))
d4 = Dock("CW CPT signal", size=(4, 1))
d5 = Dock("Allan deviation", size=(4, 1))
d6 = Dock("Pulsed CPT signal", size=(4, 1))
area.addDock(d1, 'left')
area.addDock(d2,'right')
area.addDock(d2b,'below',d2)
area.addDock(d3, 'right', d1)
area.addDock(d4, 'bottom', d3)
area.addDock(d5, 'below', d3)
area.addDock(d6, 'below', d3)


param_tree = ParameterTree()
param_group = Parameter.create(name='params', type='group')
param_group.addChildren([	ds.dl.param_group,
							#ds.shelved_params.param_group,
							ds.asserv.param_group,
							ds.pulsedAsserv.param_group,
							cw_freq_scanner.param_group,
							ds.pulsedsyncAiAo.param_group,
							laser_freq_scanner.param_group,
							ds.lock_box.param_group,
							ds.analogOuts.param_group,
							# ds.laserCtrl.param_group,
							ds.li_las.param_group,
							#ds.pow_ctrl.param_group,
							ds.fs.param_group,
							ds.aom_fs.param_group,
							ds.regulation.param_group,
							# ds.pow_ctrl.param_group,
							#ds.mag_ldd.param_group,
							autotaskpanel.param_group,
							])
param_tree.setParameters(param_group, showTop=False)

layout = pg.LayoutWidget()		
layout.addWidget(laser_freq_scanner.startBtn,row=0,col=0)
layout.addWidget(cw_freq_scanner.startBtn,row=1,col=0)
layout.addWidget(pulsed_freq_scanner.startBtn,row=2,col=0)
layout.addWidget(ds.asserv.startBtn,row=3,col=0)
layout.addWidget(ds.pulsedAsserv.startBtn,row=4,col=0)
layout.addWidget(param_tree,row=5,col=0)

dl_graph_layout = pg.GraphicsLayoutWidget()
dl_graph_layout2 = pg.GraphicsLayoutWidget()
ds.asserv.make_plots(dl_graph_layout)
ds.data_producer.make_plots(dl_graph_layout)
#ds.pow_ctrl.make_plots(dl_graph_layout)
ds.pulsedAsserv.make_plots(dl_graph_layout2)
ds.bme280.make_plots(dl_graph_layout2)

adev = Adev_graph_UI(ds)
adev.start()

d1.addWidget(layout)
d2.addWidget(dl_graph_layout)
d2b.addWidget(dl_graph_layout2)
d3.addWidget(laser_freq_scanner.gw)
d4.addWidget(cw_freq_scanner.gw)
d5.addWidget(adev.graph_layout)
d6.addWidget(pulsed_freq_scanner.gw)

def update():
	ds.asserv.update()
	ds.pulsedAsserv.update()
	ds.data_producer.update()
	#ds.pow_ctrl.update()
	ds.bme280.update()

ds.data_producer.start()
#ds.pow_ctrl.start()
ds.bme280.start()

timer = pg.QtCore.QTimer()
timer.timeout.connect(update)
timer.start(200)

win.show()

if __name__ == "__main__":
	if (sys.flags.interactive != 1) or not hasattr(QtCore, 'PYQT_VERSION'):
		ret = QtGui.QApplication.instance().exec_()
		ds.dl.stop()
		ds.data_producer.stop()
		#ds.pow_ctrl.stop()
		ds.bme280.stop()
		timer.stop()
		# ds.pow_ctrl.stop()
		adev.stop()
		sys.exit(ret)
		