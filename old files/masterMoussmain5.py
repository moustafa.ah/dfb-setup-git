import sys, datetime
sys.path.append('../PyLotage')

import numpy as np
from tasks.datalog import *
from tasks.fitter import *
from tasks.scheduledjob import ScheduledJob

print "Loading pyqtgraph",
import pyqtgraph as pg
from pyqtgraph.dockarea import *
from pyqtgraph.Qt import QtGui, QtCore
import pyqtgraph.parametertree.parameterTypes as pTypes
from ui.pyqtgraphaddon import MyFloatParameter
from pyqtgraph.parametertree import Parameter, ParameterTree
print " [DONE]"

print "Loading drivers",
from drivers import *
from drivers.nidaq.asserv import Asserv
from drivers.nidaq.asserv_pulsed import Asserv_pulsed
from drivers.nidaq.syncAIAO import *
from drivers.nidaq.syncAIAO_pulsed5 import *
print " [DONE]"

data_folder_path = "D:\Rodolphe.Boudot\Python Sandbox 2\data"

# Modification de la fonction optsChanged pour pouvoir cacher les GroupParameters

def optsChanged_Fix(self, param, opts):
    pg.parametertree.ParameterItem.optsChanged(self, param, opts)
    if 'addList' in opts:
        self.updateAddList()

pTypes.GroupParameterItem.optsChanged = optsChanged_Fix


class DeviceSet(object):
    def __init__(s):
        # #Setup equipments
        print "Connecting to devices",
        s.daq = NIUSB6259(devID='Dev1')

        s.dds = AG33250A(gpibAdress='usb')
        s.dds.fm_source = "ext"

        s.multimeter1 = HP34401A(gpibAdress = 22)
        s.multimeter2 = HP34401A(gpibAdress = 2)
        s.multimeter3 = HP34401A(gpibAdress = 14)
        s.multimeter1.reset()
        s.multimeter1.configure(mode=s.multimeter1.mode_resistance,range=10000,resolution=1e-2)
        s.multimeter1.trig()
        s.multimeter2.reset()
        s.multimeter2.configure(mode=s.multimeter2.mode_dc_voltage,range=15,resolution=1e-3)
        s.multimeter2.trig()
        s.multimeter3.reset()
        s.multimeter3.configure(mode=s.multimeter3.mode_resistance,range=10000,resolution=1e-2)

        s.syntheAOM = SMB100A(gpibAdress='usb')

        s.powerAsserv = MoussX(comPort = 'com3')
        
        time.sleep(3)

        # #Setup compound equipments
        
        s.asserv = Asserv(device="Dev1",outChan="ao3",inChan="ai7",dds_device=s.dds, power_lock_device =s.powerAsserv)
        s.asserv.compatibility_mode = 1
        s.asserv.initial_dds_frequency = 7360710 # Frequence centrale (Hz)
        s.asserv.get_init_freq_from_dds = False
        s.asserv.gain = 1050
        s.asserv.cycle_number = 1 # Number of cycles between fc correction
        s.asserv.sampling_rate = 1e6
        s.asserv.default_freq_mod = s.asserv.sampling_rate/(2*int(s.asserv.sampling_rate/(2*39))) # Hz
        s.asserv.freq_mod = s.asserv.default_freq_mod
        s.asserv.amplitude = 5.
        s.asserv.inRange = (-10.,10.)
        s.asserv.outRange = (-5,5)
        s.asserv.n_samp_per_cycle = 2*int(s.asserv.sampling_rate/(2*s.asserv.default_freq_mod))
        # s.asserv.discarded_samples_factor = 0.15
        s.asserv.log_ratio = 1

        s.asserv_pulsed = Asserv_pulsed(device="Dev1",outChan="ao3",inChan="ai7",dds_device=s.dds, power_lock_device =s.powerAsserv)
        s.asserv_pulsed.compatibility_mode = 1
        s.asserv_pulsed.initial_dds_frequency = 7360710 # Frequence centrale (Hz)
        s.asserv_pulsed.get_init_freq_from_dds = False
        s.asserv_pulsed.gain = 1050
        s.asserv_pulsed.n_cycle = 3 # Number of cycles per period
        s.asserv_pulsed.sampling_rate = 1e6
        s.asserv_pulsed.mod_frequency = s.asserv_pulsed.sampling_rate/s.asserv_pulsed.n_samp_per_period
        s.asserv_pulsed.amplitude = 5.
        s.asserv_pulsed.inRange = (-10,10)
        s.asserv_pulsed.outRange = (-5,5)
        s.asserv.n_samp_per_period = 2*int(s.asserv.sampling_rate/(2*s.asserv.default_freq_mod))
        # s.asserv.discarded_samples_factor = 0.15
        s.asserv_pulsed.log_ratio = 1

        s.syncAiAoCPT = SyncAIAO(device="Dev1",inChanList=["ai7"],outChan="ao3")
        s.syncAiAoCPT.compatibility_mode =1
        s.syncAiAoCPT.vpp = 10.
        s.syncAiAoCPT.mean = 8
        s.syncAiAoCPT.sampling_rate = 2e3

        s.syncAiAoCPT_pulsed = SyncAIAO_pulsed(device = "Dev1", outChan="ao3", DOChan = 'port0/line6',inChan="ai7",inRange=(-10.,10.),outRange=(-10.,10.))
        s.syncAiAoCPT_pulsed.compatibility_mode =1
        s.syncAiAoCPT.vpp = 10.

        s.syncAiAoFP = SyncAIAO(device="Dev1",inChanList=["ai20"],outChan="ao2")
        s.syncAiAoFP.compatibility_mode =1
        s.syncAiAoFP.vpp = 10.
        s.syncAiAoFP.offset =2.
        s.syncAiAoFP.numSamp = 20000
        # s.syncAiAo.sampling_rate = 20000

        
        print " [DONE]"

class DataLogger(object):

     def __init__(self):
    
         self.consThread = None
         self.asservConsThread = None
         self.running = False
    
     def __del__(self):
         self.stop()

     def multimeter1Temperature (self) :
         resistance = a.multimeter1.fetch()
         return 1/(0.001125308852122+(0.000234711863267*(np.log(resistance)))+0.000000085663516*(np.log(resistance))**3)-273.15

     def multimeter3Temperature (self) :
         resistance = a.multimeter3.fetch()
         return 1/(0.001125308852122+(0.000234711863267*(np.log(resistance)))+0.000000085663516*(np.log(resistance))**3)-273.15

     def timeFunction (self) :
        return time.clock() - self.t0

     def start(self):
         assert not self.running
         self.running = True
         self.t0 = a.asserv.t0 = a.asserv_pulsed.t0 = time.clock()

         self.multimeter1init = AutomatedInitiation(gpibAdress = 22, delay = 5)
         self.multimeter2init = AutomatedInitiation(gpibAdress = 2, delay = 5)
         self.multimeter3init = AutomatedInitiation(gpibAdress = 14, delay = 5)
         timeStr = time.strftime("%Y-%m-%d %H%M")
         self.f1 = open("{}\datalog {} A.txt".format(data_folder_path,timeStr),'w')
         self.f2 = open("{}\datalog {} B.txt".format(data_folder_path,timeStr),'w')
        
         # Init producer thread
         self.multimeter1init.start()
         self.multimeter2init.start()
         self.multimeter3init.start()
         time.sleep(5)
         a.asserv.initTime = time.time()
         a.asserv_pulsed.initTime = time.time()
         
         aux_columns = [["Time",self.timeFunction],
         ["Cell temperature (degC)",self.multimeter1Temperature],
         ["Laser power before cell (V)",a.multimeter2.fetch],
         ["Table temperature (degC)",self.multimeter3Temperature]]

         aux_headers = [item[0] for item in aux_columns]
         aux_getters = [item[1] for item in aux_columns]
         self.prodThread = DataProducer(aux_headers, aux_getters, delay = 5)
        
         
         if win.control_widget.pulseContinuousBtn.isChecked() :
            # Init and start consumer threads
            self.consThread = DataConsumer(self.f1,self.prodThread.header)
            self.asservConsThread = DataConsumer(self.f2,a.asserv_pulsed.header)
            self.consThread.start()
            self.asservConsThread.start()

            # Start producer threads
            self.prodThread.start()
            a.asserv_pulsed.dataQueue = self.asservConsThread.data.queue
            self.prodThread.queue = self.consThread.data.queue
         
         else :
            # Init and start consumer threads
            self.consThread = DataConsumer(self.f1, self.prodThread.header)
            self.asservConsThread = DataConsumer(self.f2,a.asserv.header)
            self.consThread.start()
            self.asservConsThread.start()

            # Start producer threads
            self.prodThread.start()
            a.asserv.dataQueue = self.asservConsThread.data.queue
            self.prodThread.queue = self.consThread.data.queue

         def slowPowerAsserv() :
         #     if self.consThread.is_full() :
         #        a.asserv.power_error = a.asserv.initial_CPT_transmission - self.consThread.data.array[-1][2]
         #     else :
         #        a.asserv.power_error = a.asserv.initial_CPT_transmission - self.consThread.data.array[self.consThread.data.ptr - 1][2]
            
         #     if a.asserv.powerLock :
         #        a.asserv.power_correction += a.asserv.powergain * a.asserv.power_error
         #        a.asserv.powerlockbox.correction = "%.9f" %a.asserv.power_correction

         # self.slowPowerAsservJob = ScheduledJob (interval = 10, job = slowPowerAsserv)
         # self.slowPowerAsservJob.start()
             if self.consThread.is_full() :
                a.asserv_pulsed.power_error = a.asserv_pulsed.initial_CPT_transmission - self.consThread.data.array[-1][2]
             else :
                a.asserv_pulsed.power_error = a.asserv_pulsed.initial_CPT_transmission - self.consThread.data.array[self.consThread.data.ptr - 1][2]
             a.asserv_pulsed.initial_synthe_power = a.syntheAOM.power
             if a.asserv_pulsed.powerLock :
                a.asserv_pulsed.power_correction += a.asserv_pulsed.powergain * a.asserv_pulsed.power_error
                a.syntheAOM.power = "%.9f" %(a.asserv_pulsed.power_correction + a.asserv_pulsed.initial_synthe_power)


         self.slowPowerAsservJob = ScheduledJob (interval = 10, job = slowPowerAsserv)
         self.slowPowerAsservJob.start()
              
    
     def stop(self):
         if self.running:
             self.running = False
             self.consThread.stop()
             self.asservConsThread.stop()
             self.prodThread.stop()
             self.slowPowerAsservJob.stop()
             self.multimeter1init.stop()
             self.multimeter2init.stop()
             self.multimeter3init.stop()
             a.asserv.dataQueue = None
             self.f1.close()
             self.f2.close()

    
     def empty(self):
         self.consThread.empty()
         self.asservConsThread.empty()

class ControlWidget(pg.LayoutWidget):

     task = None

     def __init__(self, parent=None):
         super(ControlWidget, self).__init__(parent)
         self.par = parent
         self.windowStatus = 'FreqScan'
         self.startFreqScanWindowBtn = QtGui.QPushButton()
         self.startFreqScanWindowBtn.setText("Frequency Scan")
         self.startFreqScanWindowBtn.setCheckable(False)
         self.startAsservWindowBtn = QtGui.QPushButton()
         self.startAsservWindowBtn.setText("Asserv")
         self.startAsservWindowBtn.setCheckable(False)
         # self.startDlBtn = QtGui.QPushButton()
         # self.startDlBtn.setText("Start Data Logging")
         # self.startDlBtn.setCheckable(True)
         # self.startDlBtn = QtGui.QPushButton()
         # self.startDlBtn.setText("Start Data Logging")
         # self.startDlBtn.setCheckable(True)
         self.startFPScanWindowBtn = QtGui.QPushButton()
         self.startFPScanWindowBtn.setText("Fabry Perot")
         self.startFPScanWindowBtn.setCheckable(False)
         self.pulseContinuousBtn = QtGui.QPushButton()
         self.pulseContinuousBtn.setText("Continuous Mode Set")
         self.pulseContinuousBtn.setCheckable(True)
               

         self.startFreqScanWindowBtn.clicked.connect(self.on_startFreqScanWindowBtn)
         self.startAsservWindowBtn.clicked.connect(self.on_startAsservWindowBtn)
         # self.startDlBtn.clicked.connect(self.on_startDlBtn)
         self.startFPScanWindowBtn.clicked.connect(self.on_startFPScanWindowBtn)
         self.pulseContinuousBtn.clicked.connect(self.on_pulseContinuousBtn)
       
        

         self.gp1 = pTypes.GroupParameter(name='Equipments')
        
         self.gp1_1 = pTypes.GroupParameter(name='DDS')
         self.dds_freq_param = MyFloatParameter.create(name='Frequency', type='float2', value=a.dds.frequency, step = 1000, suffix='Hz', limits=(7000000, 7800000))
         self.dds_pow_amp_param = Parameter.create(name='Power', type='float', value=a.dds.power, step = 0.1, suffix='dBm')
         self.dds_fm_status_param = Parameter.create(name='FM stat', type='bool', value=a.dds.fm_status)
         self.dds_fm_dev_param = Parameter.create(name='FM dev', type='float', value=a.dds.fm_dev, step = 100, suffix='Hz', siPrefix=True,limits=(0,5e5))
         self.gp1_1.addChildren([self.dds_freq_param, self.dds_fm_status_param, self.dds_pow_amp_param, self.dds_fm_dev_param])
        
         self.gp1_2 = pTypes.GroupParameter(name='AOM driver + Power asserv')
         self.RF_power_param = Parameter.create(name='RF power', type='float', value=a.syntheAOM.power, step= 0.1, suffix='dBm', limits=(-50,-2))
         self.RF_freq_param = Parameter.create(name='RF frequency', type='float', value=a.syntheAOM.frequency, step= 1000000, siPrefix=True, suffix='Hz')
         self.RF_outStatus_param = Parameter.create(name='Output Enabled', type='bool', value=a.syntheAOM.outStatus)
         self.RF_modStatus_param = Parameter.create(name='Modulation Enabled', type='bool', value=a.syntheAOM.modStatus)
         self.RF_AMStatus_param = Parameter.create(name='AM Enabled', type='bool', value=a.syntheAOM.AMStatus)
         self.RF_AMSource_param = Parameter.create(name='AM Source', type='list', values=["INT", "EXTAC", "EXTALC", "EXTDC"], value="EXTDC")
         self.RF_AMDepth_param = Parameter.create(name='AM Depth', type='float', value=a.syntheAOM.AMDepth, step= 1, suffix = '%', limits=(0,100))
         # self.RF_AMFreq_param = Parameter.create(name='AM frequency', type='float', value=a.syntheAOM.AMFreq, step= 1, siPrefix=True, suffix = 'Hz')
         self.laserPowerSetpoint_param = Parameter.create(name='Laser Power Setpoint', type='float', value=a.powerAsserv.setpoint, step= 0.001, siPrefix=True, suffix = 'V', limits = (0, 10))
         self.laserPowerSetpointCorrection_param = Parameter.create(name='Laser Power Setpoint Correction', type='float', value=a.powerAsserv.correction, step= 0.001, siPrefix=True, suffix = 'V', limits = (-100, 100))
         self.gp1_2.addChildren([self.RF_power_param, self.RF_freq_param, self.RF_outStatus_param, self.RF_modStatus_param, self.RF_AMStatus_param, self.RF_AMSource_param, self.RF_AMDepth_param, self.laserPowerSetpoint_param, self.laserPowerSetpointCorrection_param])
        
         self.gp1.addChildren([self.gp1_1, self.gp1_2])


         self.gp2 = pTypes.GroupParameter(name='Parameters')

         self.gp2_1 = pTypes.GroupParameter(name = 'Shared parameters')
         self.Tbright_param = Parameter.create(name = 'Tbright', type = 'float', value = a.syncAiAoCPT_pulsed.TbrightLength/a.syncAiAoCPT_pulsed.sampling_rate, step = 0.001, siPrefix=True, suffix= 's')
         self.Tdark_param = Parameter.create(name = 'Tdark', type = 'float', value = a.syncAiAoCPT_pulsed.TdarkLength/a.syncAiAoCPT_pulsed.sampling_rate, step = 0.001, siPrefix=True, suffix= 's')
         self.Tdead_param = Parameter.create(name = 'Dead time between 2 cycles', type = 'float', value = a.syncAiAoCPT_pulsed.TdeadLength/a.syncAiAoCPT_pulsed.sampling_rate, step = 0.000001, siPrefix=True, suffix= 's')
         self.Tcycle_param = Parameter.create(name = 'Tcycle', type = 'float', value = a.syncAiAoCPT_pulsed.TcycleLength/a.syncAiAoCPT_pulsed.sampling_rate, siPrefix = True, suffix = 's', readonly = True)
         self.Tdiscarded_param = Parameter.create(name = 'Dead time before averaging', type = 'float', value = a.syncAiAoCPT_pulsed.TdiscardedLength/a.syncAiAoCPT_pulsed.sampling_rate, step = 0.000001, siPrefix=True, suffix= 's')
         self.Taverage_param = Parameter.create(name = 'Averaging time', type = 'float', value = a.syncAiAoCPT_pulsed.TaverageLength/a.syncAiAoCPT_pulsed.sampling_rate, step = 0.000001, siPrefix=True, suffix= 's')
         self.n_cycle_param = Parameter.create(name = 'Number of cycles per point', type = 'int', value = a.syncAiAoCPT_pulsed.n_cycle, step = 1, limits = (1,1000))
         self.gp2_1.addChildren([self.Tbright_param, self.Tdark_param, self.Tdead_param, self.Tcycle_param, self.Tdiscarded_param, self.Taverage_param, self.n_cycle_param])

         self.gp2_2 = pTypes.GroupParameter(name = 'CPT Scan parameters')
         self.scan_Btn_param = Parameter.create(name = 'Start/Stop Scan', type = 'action')
         self.scan_ramp_frequency_param = Parameter.create(name='Ramp frequency', type='float', value=a.syncAiAoCPT.sampling_rate/a.syncAiAoCPT.numSamp, step= 1, siPrefix=True, suffix='Hz')
         self.scan_sample_number = Parameter.create(name='Sample number', type='int', value=a.syncAiAoCPT.numSamp, step= 1000)
         self.amplitude_param = Parameter.create(name = 'Ramp amplitude pp', type = 'float', value = a.syncAiAoCPT.vpp, step = 0.1, siPrefix=True, suffix= 'V')
         self.offset_param = Parameter.create(name = 'Ramp offset', type = 'float', value = a.syncAiAoCPT.offset, step = 0.1, siPrefix = True, suffix = 'V')
         self.fit_action = Parameter.create(name='Fit', type='action')
         self.save_action = Parameter.create(name = 'Save', type = 'action')
         self.show_data_curve = Parameter.create(name='Show data curve', type='bool', value=True)
         self.show_mean_curve = Parameter.create(name='Show mean curve', type='bool', value=True)
         self.show_fit_curve = Parameter.create(name='Show fitting curve', type='bool', value=True)
         self.scan_numfreqs_param = Parameter.create(name='Number of frequency points', type='int', value=a.syncAiAoCPT_pulsed.numfreqs, step= 10)
         self.scan_duration_param = Parameter.create(name='Scan duration', type='float', value=a.syncAiAoCPT_pulsed.scan_duration, readonly = True, siPrefix=True, suffix='s')

         self.gp2_2.addChildren([self.scan_Btn_param, self.scan_ramp_frequency_param, self.scan_duration_param, self.scan_sample_number, self.scan_numfreqs_param, self.amplitude_param, self.fit_action, self.save_action, self.show_data_curve, self.show_mean_curve, self.show_fit_curve])


         self.gp2_3 = pTypes.GroupParameter(name = 'CPT Lock parameters')
         self.asserv_Btn_param = Parameter.create(name = 'Start/Stop Asserv', type = 'action')
         self.dl_Btn_param = Parameter.create(name = 'Start/Stop DataLogger', type = 'action')
         self.waveform_param = Parameter.create(name = 'Lock-in Waveform', type = 'list', values = ['square', 'sine'], value = 'square')
         self.phase_param = Parameter.create(name = 'Phase', type = 'float', value = a.asserv.phase, step = 1, suffix = 'deg', limits = (-180, 180))
         self.dds_freq_offset_action = Parameter.create(name='Frequency step', type='action')
         self.dds_freq_offset_value = Parameter.create(name='Offset', type='float', value=4, step = 1, suffix='Hz')
         self.dds_freq_offset_action.addChild(self.dds_freq_offset_value)
         self.cpt_lock_param = Parameter.create(name='Frequency Lock', type='bool', value=a.asserv.frequencyLock)
         self.power_lock_param = Parameter.create(name='Power Lock', type='bool', value=a.asserv.powerLock)
         self.power_gain_param = Parameter.create(name='Power Gain', type='float', value=a.asserv.powergain)
         self.gain_param = Parameter.create(name='Frequency Gain', type='float', value=a.asserv.gain, step= 100)
         # self.sampling_rate_param = Parameter.create(name='Sampling rate', type='int', value=a.asserv.sampling_rate, step= 100000, siPrefix=True, suffix='Samp/s')
         self.freq_mod_param = Parameter.create(name='Mod frequency', type='float', value=a.asserv.freq_mod, step= 1, siPrefix=True, suffix='Hz')
         self.mod_frequency_param = Parameter.create(name='Modulation frequency', type='float', value=a.asserv_pulsed.mod_frequency, siPrefix=True, suffix='Hz', readonly=True)
         self.cycle_number_param = Parameter.create(name='N cycle', type='int', value=a.asserv.cycle_number, step=1)
         self.log_ratio_param = Parameter.create(name='Logged points ratio', type='int', value=a.asserv.log_ratio, step=1)
         # self.discarded_samples_factor_param = Parameter.create(name='Ratio discarded samp.', type='float', value= a.asserv.discarded_samples_factor, step=0.05)
         self.n_samp_per_cycle_param = Parameter.create(name='N samp/cycle', type='int', value=a.asserv.n_samp_per_cycle, readonly=True)
         self.n_samp_per_period_param= Parameter.create(name='N samp/period', type='int', value=a.asserv_pulsed.n_samp_per_period, readonly=True)
         self.act_freq_mod_param = Parameter.create(name='Actual Mod frequency', type='float', value=a.asserv.freq_mod, siPrefix=True, suffix='Hz', readonly=True)
         self.update_interval_param = Parameter.create(name='Update interval', type='float', value=a.asserv.update_interval, siPrefix=True, suffix='s', readonly=True)
         self.initial_CPT_transmission_param = Parameter.create(name = 'initial CPT transmission', type = 'float', value = a.asserv.initial_CPT_transmission, siPrefix = True, suffix ='V', readonly = True)
         self.clear_plot_action = Parameter.create(name='Clear plot', type='action')
         self.gp2_3.addChildren([self.asserv_Btn_param, self.dl_Btn_param, self.cpt_lock_param, self.dds_freq_offset_action, self.gain_param, self.power_lock_param, self.power_gain_param, self.waveform_param, self.phase_param, self.freq_mod_param, self.mod_frequency_param, self.cycle_number_param, self.log_ratio_param, self.n_samp_per_cycle_param, self.n_samp_per_period_param, self.act_freq_mod_param, self.update_interval_param, self.initial_CPT_transmission_param, self.clear_plot_action])

         
         self.gp2.addChildren([self.gp2_1, self.gp2_2, self.gp2_3])

         self.removable_params_list = self.gp2_1.children() + self.gp2_2.children() + self.gp2_3.children()
         self.removable_group_parameters = [self.gp2_1, self.gp2_2, self.gp2_3]
         self.params_list_scan_continuous = self.gp2_2.children()
         self.params_list_scan_continuous.remove(self.scan_numfreqs_param)
         self.params_list_scan_continuous.remove(self.scan_duration_param)
         self.params_list_scan_pulsed = self.gp2_1.children() + self.gp2_2.children()
         for param in [self.show_data_curve, self.show_mean_curve, self.show_fit_curve] : self.params_list_scan_pulsed.remove(param)
         self.params_list_asserv_continuous = self.gp2_3.children()
         self.params_list_asserv_continuous.remove(self.mod_frequency_param)
         self.params_list_asserv_pulsed = self.gp2_1.children() + self.gp2_3.children()
         self.params_list_asserv_pulsed.remove(self.freq_mod_param)

         # self.params_list = [self.dds_freq_param, self.dds_fm_status_param, self.dds_pow_amp_param, self.dds_fm_dev_param,self.dds_freq_offset_action, self.RF_power_param, self.RF_freq_param, self.RF_outStatus_param, self.RF_modStatus_param, self.RF_AMStatus_param, self.RF_AMSource_param, self.RF_AMDepth_param, self.RF_AMFreq_param, self.laserPowerSetpoint_param, self.laserPowerSetpointCorrection_param, self.Tbright_param, self.Tdark_param, self.Tcycle_param, self.Tdiscarded_param, self.Taverage_param, self.n_cycle_param, self.scan_Btn_param, self.scan_ramp_frequency_param, self.scan_duration, self.scan_sample_number, self.scan_numfreqs_param, self.amplitude_param, self.fit_action, self.save_action, self.show_data_curve, self.show_mean_curve, self.show_fit_curve, self.asserv_Btn_param, self.dl_Btn_param, self.cpt_lock_param, self.gain_param, self.power_lock_param, self.power_gain_param, self.waveform_param, self.phase_param, self.freq_mod_param, self.mod_frequency_param, self.cycle_number_param, self.log_ratio_param, self.n_samp_per_cycle_param, self.n_samp_per_period_param, self.act_freq_mod_param, self.update_interval_param, self.initial_CPT_transmission_param, self.clear_plot_action])
         # self.params_list_always_visible =          
         params = [self.gp1, self.gp2]
         self.p = Parameter.create(name='params', type='group', children=params)
         self.t = ParameterTree()
         self.t.setParameters(self.p, showTop=False)
         self.p.sigTreeStateChanged.connect(self.change)
         self.dds_freq_offset_action.sigActivated.connect(self.freq_jump)
         self.clear_plot_action.sigActivated.connect(a.dl.empty)
         self.fit_action.sigActivated.connect(self.par.live_graph_widgetCPT.fit)
         self.save_action.sigActivated.connect(self.par.live_graph_widgetCPT.save)
         self.scan_Btn_param.sigActivated.connect(self.on_scan_Btn)
         self.asserv_Btn_param.sigActivated.connect(self.on_asserv_Btn)
         self.dl_Btn_param.sigActivated.connect(self.on_dl_Btn)
        
         self.addWidget(self.pulseContinuousBtn, row=0, col=0, colspan = 3)
         self.addWidget(self.startAsservWindowBtn,row=1, col=0)
         self.addWidget(self.startFreqScanWindowBtn,row=1, col=1)
         # self.addWidget(self.startDlBtn,row=2, col=1)
         self.addWidget(self.startFPScanWindowBtn, row = 1, col = 2)
         self.addWidget(self.t,row=4, col=0, colspan = 3)
    
     def change(self,param,changes):
         for param, change, data in changes:
             if param is self.dds_pow_amp_param: a.dds.power_amplitude = param.value()
             elif param is self.dds_fm_dev_param:
                a.dds.fm_dev = param.value()
                self.par.live_graph_widgetCPT.fm_dev = a.dds.fm_dev
             elif param is self.dds_fm_status_param : a.dds.fm_status = param.value()
             elif param is self.cpt_lock_param: a.asserv.frequencyLock = a.asserv_pulsed.frequencyLock = param.value()
             elif param is self.gain_param: a.asserv.gain = a.asserv_pulsed.gain = param.value()
             elif param is self.dds_freq_param:
                 a.dds.frequency = param.value()
                 self.par.live_graph_widgetCPT.update_label()
             elif param is self.freq_mod_param:
                 a.asserv.freq_mod = param.value()
                 self.update_readonly_params()
             elif param is self.cycle_number_param:
                 a.asserv.cycle_number = param.value()
                 self.update_readonly_params()
             elif param is self.log_ratio_param : a.asserv.log_ratio = a.asserv_pulsed.log_ratio = param.value()
             # elif param is self.discarded_samples_factor_param: a.asserv.discarded_samples_factor = param.value()
             elif param is self.scan_ramp_frequency_param:
                 running = a.syncAiAoCPT.running
                 a.syncAiAoCPT.stop()
                 a.syncAiAoCPT.sampling_rate = a.syncAiAoCPT.numSamp * param.value()
                 if running == True :
                    a.syncAiAoCPT.start()
             elif param is self.scan_sample_number:
                 running = a.syncAiAoCPT.running
                 a.syncAiAoCPT.stop()
                 a.syncAiAoCPT.numSamp = param.value()
                 a.syncAiAoCPT.sampling_rate = param.value() * self.scan_ramp_frequency_param.value()
                 if running == True :
                    a.syncAiAoCPT.start()    
             elif param is self.show_data_curve: 
                self.par.live_graph_widgetCPT.curve.setVisible(param.value())
                self.par.live_graph_widgetFP.curve.setVisible(param.value())
             elif param is self.show_mean_curve: 
                self.par.live_graph_widgetCPT.meanCurve.setVisible(param.value())
                self.par.live_graph_widgetFP.meanCurve.setVisible(param.value())
             elif param is self.show_fit_curve: 
                self.par.live_graph_widgetCPT.fitCurve.setVisible(param.value())
                self.par.live_graph_widgetFP.fitCurve.setVisible(param.value())
             elif param is self.offset_param : 
                if self.windowStatus == FreqScan :
                    if self.pulseContinuousBtn.isChecked() :
                        running = a.syncAiAoCPT_pulsed.running
                        a.syncAiAoCPT_pulsed.stop()
                        a.syncAiAoCPT_pulsed.offset = param.value()
                        if running == True :
                            a.syncAiAoCPT_pulsed.start()
                    else :
                        running = a.syncAiAoCPT.running
                        a.syncAiAoCPT.stop()
                        a.syncAiAoCPT.offset = param.value()
                        if running == True :
                            a.syncAiAoCP.start()
                elif self.windowStatus == 'FPScan' :
                    running = a.syncAiAoFP.running
                    a.syncAiAoFP.stop()
                    a.syncAiAoFP.offset = param.value()
                    if running == True :
                        a.syncAiAoFP.start()
             elif param is self.amplitude_param :
                if self.windowStatus == 'FreqScan' :
                    if self.pulseContinuousBtn.isChecked() :
                        running = a.syncAiAoCPT_pulsed.running
                        a.syncAiAoCPT_pulsed.stop()
                        a.syncAiAoCPT_pulsed.vpp = param.value()
                        if running == True :
                            a.syncAiAoCPT_pulsed.start()
                    else :
                        running = a.syncAiAoCPT.running
                        a.syncAiAoCPT.stop()
                        a.syncAiAoCPT.vpp = param.value()
                        if running == True :
                            a.syncAiAoFP.start()
                elif self.windowStatus == 'FPScan' :
                    running = a.syncAiAoFP.running
                    a.syncAiAoFP.stop()
                    a.syncAiAoFP.vpp = param.value()
                    if running == True :
                        a.syncAiAoFP.start()
             elif param is self.RF_freq_param: a.syntheAOM.frequency = param.value()
             elif param is self.RF_power_param: a.syntheAOM.power = param.value()
             elif param is self.RF_outStatus_param: a.syntheAOM.outStatus = param.value()
             elif param is self.RF_modStatus_param: a.syntheAOM.modStatus = param.value()
             elif param is self.RF_AMStatus_param: a.syntheAOM.AMStatus = param.value()
             elif param is self.RF_AMSource_param: a.syntheAOM.AMSource = param.value()
             elif param is self.RF_AMDepth_param: a.syntheAOM.AMDepth = param.value()
             # elif param is self.RF_AMFreq_param: a.syntheAOM.AMFreq = param.value()
             elif param is self.laserPowerSetpoint_param : a.powerAsserv.setpoint = param.value()
             elif param is self.laserPowerSetpointCorrection_param : a.powerAsserv.correction = param.value()
             elif param is self.power_lock_param :
                if self.pulseContinuousBtn.isChecked :
                    a.asserv_pulsed.powerLock = param.value()
                    if a.asserv_pulsed.powerLock : a.asserv_pulsed.initial_CPT_transmission = -a.asserv_pulsed.power_error
                    else : a.asserv_pulsed.initial_CPT_transmission = 0
                    self.update_readonly_params()
                else :
                    a.asserv.powerLock = param.value()
                    if a.asserv.powerLock : a.asserv.initial_CPT_transmission = -a.asserv.power_error
                    else : a.asserv.initial_CPT_transmission = 0
                    self.update_readonly_params()
             elif param is self.power_gain_param : a.asserv.powergain = a.asserv_pulsed.powergain = param.value()
             elif param is self.waveform_param : a.asserv.waveform = a.asserv_pulsed.waveform = param.value()
             elif param is self.phase_param : a.asserv.phase = a.asserv_pulsed.phase = param.value()
             elif param in [self.Tbright_param, self.Tdark_param, self.Tdead_param, self.Tdiscarded_param, self.Taverage_param, self.n_cycle_param, self.scan_numfreqs_param] :
                running = a.syncAiAoCPT_pulsed.running
                a.syncAiAoCPT_pulsed.stop()
                if param is self.Tbright_param : 
                    a.asserv_pulsed.Tbright = a.syncAiAoCPT_pulsed.Tbright = param.value()
                elif param is self.Tdark_param :
                    a.asserv_pulsed.Tdark = a.syncAiAoCPT_pulsed.Tdark = param.value()
                elif param is self.Tdead_param :
                    a.syncAiAoCPT_pulsed.Tdead = param.value()
                elif param is self.Tdiscarded_param :
                    a.asserv_pulsed.Tdiscarded = a.syncAiAoCPT_pulsed.Tdiscarded = param.value()
                elif param is self.Taverage_param :
                    a.asserv_pulsed.Taverage = a.syncAiAoCPT_pulsed.Taverage = param.value()
                elif param is self.n_cycle_param :
                    a.asserv_pulsed.n_cycle = a.syncAiAoCPT_pulsed.n_cycle = param.value()
                elif param is self.scan_numfreqs_param :
                    a.asserv.numfreqs = a.syncAiAoCPT_pulsed.numfreqs = param.value()
                self.update_pulsed_params()
                if running == True :
                    a.syncAiAoCPT_pulsed.start()
    
     def update_readonly_params(self):
         self.act_freq_mod_param.setValue(a.asserv.freq_mod)
         self.n_samp_per_cycle_param.setValue(a.asserv.n_samp_per_cycle)
         self.update_interval_param.setValue(a.asserv.update_interval)
         self.initial_CPT_transmission_param.setValue(a.asserv.initial_CPT_transmission)

     def update_pulsed_params(self) :
        self.p.sigTreeStateChanged.disconnect(self.change)
        a.asserv_pulsed.calculate_params()
        a.syncAiAoCPT_pulsed.calculate_params()
        self.Tbright_param.setValue(a.asserv_pulsed.TbrightLength/a.asserv_pulsed.sampling_rate)
        self.Tdark_param.setValue(a.asserv_pulsed.TdarkLength/a.asserv_pulsed.sampling_rate)
        self.Tcycle_param.setValue(a.asserv_pulsed.TcycleLength/a.asserv_pulsed.sampling_rate)
        self.Tdiscarded_param.setValue(a.asserv_pulsed.TdiscardedLength/a.asserv_pulsed.sampling_rate)
        self.Taverage_param.setValue(a.asserv_pulsed.TaverageLength/a.asserv_pulsed.sampling_rate)
        self.scan_numfreqs_param.setValue(a.syncAiAoCPT_pulsed.numfreqs)
        self.scan_duration_param.setValue(a.syncAiAoCPT_pulsed.scan_duration)
        self.n_samp_per_period_param.setValue(a.asserv_pulsed.n_samp_per_period)
        self.mod_frequency_param.setValue(a.asserv_pulsed.mod_frequency)
        self.update_interval_param.setValue(a.asserv_pulsed.update_interval)
        self.p.sigTreeStateChanged.connect(self.change)
    
     def on_startFreqScanWindowBtn(self):
        win.d3.raiseDock()
        self.windowStatus = 'FreqScan'
        for param in self.removable_params_list : param.hide()
        for group in self.removable_group_parameters : group.hide()
        self.gp2_2.show()
        if self.pulseContinuousBtn.isChecked() :
            self.gp2_1.show()
            for param in self.params_list_scan_pulsed : param.show()
            self.amplitude_param.setValue(a.syncAiAoCPT_pulsed.vpp)
            self.offset_param.setValue(a.syncAiAoCPT_pulsed.offset)
        else :
            for param in self.params_list_scan_continuous : param.show()
            self.amplitude_param.setValue(a.syncAiAoCPT.vpp)
            self.offset_param.setValue(a.syncAiAoCPT.offset)

     def on_scan_Btn(self):
        if self.windowStatus == 'FreqScan' :
             if self.pulseContinuousBtn.isChecked() :
                if a.syncAiAoCPT_pulsed.running == False :
                    a.syncAiAoCPT_pulsed.start()
                    self.par.live_graph_widgetCPT.start_timer()
                    self.disable_window_Btns()
                else :
                    a.syncAiAoCPT_pulsed.stop()
                    self.par.live_graph_widgetCPT.stop_timer()
                    self.enable_window_Btns()
             else :
                if a.syncAiAoCPT.running == False :
                    a.syncAiAoCPT.start()
                    self.par.live_graph_widgetCPT.start_timer()
                    self.disable_window_Btns()
                else :
                    a.syncAiAoCPT.stop()
                    self.par.live_graph_widgetCPT.stop_timer()
                    self.enable_window_Btns()

        elif self.windowStatus == 'FPScan' :
             if a.syncAiAoFP.running == False :
                a.syncAiAoFP.start()
                self.par.live_graph_widgetFP.start_timer()
                self.disable_window_Btns()
             else :
                a.syncAiAoFP.stop()
                self.par.live_graph_widgetFP.start_timer()
                self.enable_window_Btns()


                 
     def on_startAsservWindowBtn(self):
        win.d2.raiseDock()
        for param in self.removable_params_list : param.hide()
        for group in self.removable_group_parameters : group.hide()
        self.gp2_3.show()
        if self.pulseContinuousBtn.isChecked() :
            self.gp2_1.show()
            for param in self.params_list_asserv_pulsed : param.show()
        else :
            for param in self.params_list_asserv_continuous : param.show()
        self.windowStatus = 'Asserv'

     def on_asserv_Btn(self) :
         if self.pulseContinuousBtn.isChecked() :
            if a.asserv_pulsed.running == False :
                a.asserv_pulsed.start()
                self.disable_window_Btns()
            else :
                a.asserv_pulsed.stop()
                self.enable_window_Btns()
         else:
            if a.asserv.running == False :
                a.asserv.start()
                self.disable_window_Btns()
            else :
                a.asserv.stop()
                self.enable_window_Btns()

             
     def on_startFPScanWindowBtn(self) :
        win.d5.raiseDock()
        for param in self.removable_params_list : param.hide()
        for group in self.removable_group_parameters : group.hide()
        self.gp2_2.show()
        for param in self.params_list_scan_continuous : param.show()
        self.windowStatus = 'FPScan'
        self.amplitude_param.setValue(a.syncAiAoFP.vpp)
        self.offset_param.setValue(a.syncAiAoFP.offset)
       
    
     def on_dl_Btn(self):
         if a.dl.running == False:
             a.dl.start()
             self.disable_window_Btns()
         else:
             a.dl.stop()
             self.enable_window_Btns()

     def on_pulseContinuousBtn(self):
         for param in self.removable_params_list : param.hide()
         self.gp2_1.hide()
         if self.pulseContinuousBtn.isChecked():
             self.gp2_1.show()
             if self.windowStatus == 'FreqScan' :
                 for param in self.params_list_scan_pulsed : param.show()
             elif self.windowStatus == 'FPScan' :
                 for param in self.params_list_scan_continuous : param.show()
             elif self.windowStatus == 'Asserv' :
                 for param in self.params_list_asserv_pulsed : param.show()
             self.pulseContinuousBtn.setText('Pulsed Mode Set')
         else :
             if self.windowStatus == 'Asserv' :
                 for param in self.params_list_asserv_continuous : param.show()
             else :
                 for param in self.params_list_scan_continuous : param.show()
             self.pulseContinuousBtn.setText('Continuous Mode Set')

     def disable_window_Btns(self):
         self.pulseContinuousBtn.setEnabled(False)
         self.startFPScanWindowBtn.setEnabled(False)
         self.startFreqScanWindowBtn.setEnabled(False)
         self.startAsservWindowBtn.setEnabled(False)

     def enable_window_Btns(self) :
         self.pulseContinuousBtn.setEnabled(True)
         self.startFPScanWindowBtn.setEnabled(True)
         self.startFreqScanWindowBtn.setEnabled(True)
         self.startAsservWindowBtn.setEnabled(True)
    
     def freq_jump(self):
         init_f = a.dds.frequency
         for f in np.linspace(-1, 1, 100) :
            a.dds.frequency = init_f+f*self.dds_freq_offset_value.value()
            time.sleep(a.asserv.update_interval)

         a.dds.frequency = init_f

         # a.dds.frequency = a.dds.frequency + self.dds_freq_offset_value.value()


     def on_refresh_task_list(self):
         reload(tasks.automation)
         dic = {}
         for name, obj in inspect.getmembers(tasks.automation):
             if inspect.isclass(obj):
                 print name, obj
                 dic.update({name:obj})
         self.task_list.setLimits(dic)
    
     def on_run_task(self):
         if self.task:
             self.task.stop()
         selectedTaskClass = self.task_list.value()
         self.task = selectedTaskClass(a,self.parent)
         self.task.start()
    
     def on_stop_task(self):
         if self.task:
             self.task.stop()

class DataGraphWidget(pg.GraphicsLayoutWidget):
    def __init__(self, parent=None):
        super(DataGraphWidget, self).__init__(parent)
    
        def addLabelPlot(pen="w",label1=" ",label2=" ",xlinkp=None,hideXAxis=True):
            self.addLabel(label1, col=0)
            l2 = self.addLabel(label2, col=1)
            self.nextRow()
            p = self.addPlot(colspan=2)
            c = p.plot(pen = pen)
            if xlinkp: p.setXLink(xlinkp)
            p.setDownsampling(mode='peak')
            p.setClipToView(True)
            p.showGrid(x=True, y=True, alpha=1)
            if hideXAxis:
                p.hideAxis('bottom')
            self.nextRow()
            return p, c, l2
    
        p1, self.c1, self.c1_label  = addLabelPlot('m',"Frequency (Hz)")
        p2, self.c2, _  = addLabelPlot('c',"Error",xlinkp=p1)
        p3, self.c3, _  = addLabelPlot('r',"Mean signal (V)",xlinkp=p1, hideXAxis=False)
        p5, self.c5, _  = addLabelPlot('w',"Cell temperature (degC)",xlinkp=p1, hideXAxis=False)

        
        self.timer = QtCore.QTimer()
        self.timer.timeout.connect(self.update1)
        self.timer.start(200)
        
        self.timer2 = QtCore.QTimer()
        self.timer2.timeout.connect(self.update2)
        self.timer2.start(1000)
        

    
    def update(self):
        self.update1()
        self.update2()
    
    def update1(self):
        if a.dl.asservConsThread and  a.dl.asservConsThread.data.ptr -1 > 2:
            currentDataPtr = a.dl.asservConsThread.data.ptr -1
            pcTimes = a.dl.asservConsThread.data.array[0:currentDataPtr,0]
            self.c1.setData(x=pcTimes, y=a.dl.asservConsThread.data.array[0:currentDataPtr,3])
            self.c2.setData(x=pcTimes, y=a.dl.asservConsThread.data.array[0:currentDataPtr,4])
            self.c3.setData(x=pcTimes, y=a.dl.asservConsThread.data.array[0:currentDataPtr,5])
    
    def update2(self):
        if  a.dl.consThread and a.dl.consThread.data.ptr - 1 > 2:
            currentDataPtr = a.dl.consThread.data.ptr - 1
            pcTimes = a.dl.consThread.data.array[0:currentDataPtr,0]
            self.c5.setData(x=pcTimes, y=a.dl.consThread.data.array[0:currentDataPtr,1])


class LiveGraphWidget(pg.GraphicsLayoutWidget):
    def __init__(self, parent=None):
        super(LiveGraphWidget, self).__init__(parent)
        self.p = self.addPlot()
        self.p.showGrid(x=True, y=True, alpha=0.8)
        self.curve = self.p.plot(pen="c")
        self.meanCurve = self.p.plot(pen="m")
        self.fitCurve = self.p.plot(pen="r")
        self.point = self.p.plot(pen=(200,200,200), symbolBrush = (255,0,0), symbolPen ='w')
        self.timer = QtCore.QTimer()
        self.timer.timeout.connect(self.update)

    def start_timer(self):
        self.timer.start(50)
    
    def stop_timer(self):
        self.timer.stop()


class LiveGraphWidget_CPT(LiveGraphWidget):

    def __init__(self,parent=None):
        super(LiveGraphWidget_CPT, self).__init__(parent)
        self.p.setLabel('left', "Signal", units='V')
        self.p.setLabel('bottom', "Detuning", units='Hz')
        self.isText = False
    
    def update(self):
        if win.control_widget.pulseContinuousBtn.isChecked() :
            x, y = a.syncAiAoCPT_pulsed.AOdata, a.syncAiAoCPT_pulsed.AImean
            self.curve.setData(x=x * self.fm_dev*2/a.syncAiAoCPT_pulsed.vpp,y=y)
            self.meanCurve.clear()
            self.fitCurve.clear()

            xpoint, ypoint = np.array([a.syncAiAoCPT_pulsed.AOdata[a.syncAiAoCPT_pulsed.ptr-1]]), np.array([a.syncAiAoCPT_pulsed.AImean[a.syncAiAoCPT_pulsed.ptr-1]])
            self.point.setData(x=xpoint* self.fm_dev*2/a.syncAiAoCPT_pulsed.vpp, y=ypoint)    

        else:
            x, y = a.syncAiAoCPT.getNthChanAIdata(0)

            _, y2 = a.syncAiAoCPT.getNthChanAImean(0)
            self.point.clear()
            self.curve.setData(x=x * self.fm_dev*2/a.syncAiAoCPT.vpp,y=y)
            self.meanCurve.setData(x=x * self.fm_dev*2/a.syncAiAoCPT.vpp,y=y2)
            if a.syncAiAoCPT.counter == 1:
                self.p.enableAutoRange('xy', False)

    def update_label(self):
        self.frequency = a.dds.frequency
        self.p.setLabel('bottom', "Frequency shift from {:.2f}".format(self.frequency), units='Hz')
    
    def fit(self):
        x, y = a.syncAiAoCPT.getNthChanAImean(0)
        if self.fm_dev:
            x = x*self.fm_dev*2/a.syncAiAoCPT.vpp
        self.fitter = Fitter(x,y)
        self.fitter.newData.connect(self.update_fit_curve)
        self.fitter.start()

    def save(self) :
        fc = a.dds.frequency
        if win.control_widget.pulseContinuousBtn.isChecked() :
            x, f, y = a.syncAiAoCPT_pulsed.AOdata, np.zeros(np.size(a.syncAiAoCPT_pulsed.AOdata)), a.syncAiAoCPT_pulsed.last_AImean
            if self.fm_dev :
                f = 9200000000-fc-x*self.fm_dev*2/a.syncAiAoCPT_pulsed.vpp
                x = x*self.fm_dev*2/a.syncAiAoCPT_pulsed.vpp
        else :
            x, y = a.syncAiAoCPT.getNthChanAImean(0)
            if self.fm_dev :
                f = 9200000000-fc-x*self.fm_dev*2/a.syncAiAoCPT.vpp
                x = x*self.fm_dev*2/a.syncAiAoCPT_pulsed.vpp
        filename = str(QtGui.QFileDialog.getSaveFileName(None, 'Save file as','./data/'+ datetime.datetime.now().strftime('%Y-%m-%d_%H-%M.txt'), filter="txt(*.txt)"))
        header = 'Detuning, LO frequency, Data\nHz, Hz, V'
        if filename != '' :
            np.savetxt(filename, np.transpose(np.vstack((x, f, y))), delimiter = ', ', newline = '\n', header = header, comments = '')
            print('saved to %s' %(filename))


    def update_fit_curve(self,x,fitData):
        self.fitCurve.setData(x=x,y=fitData)
        parameters = ['signal', 'contrast', 'background', 'fwhm']
        texte = ''
        paramTuples = []
        for parameter in parameters :
            texte= texte + parameter + ' : '+ str(self.fitter.fitOut.params[parameter].value) + '\n'
        texte = texte + 'contrast/fwhm : ' + str(1000*self.fitter.fitOut.params['contrast'].value/self.fitter.fitOut.params['fwhm'].value)
        if self.isText :
            self.text.setText(texte)
        else :
            self.isText = True
            self.text = pg.TextItem(texte, color = (200,200,200), anchor=(-0.3,1.3), border='w', fill=(0, 0, 255, 100))
            self.p.addItem(self.text)
        self.text.setPos(0, (fitData.max()+fitData.min())/2)

    def start_timer(self):
        self.fm_dev = a.dds.fm_dev
        super(LiveGraphWidget_CPT, self).start_timer()

class LiveGraphWidget_FP(LiveGraphWidget):

    def __init__(self,parent=None):
        super(LiveGraphWidget_FP, self).__init__(parent)
        self.p.setLabel('left', "Signal", units='V')
        self.p.setLabel('bottom', "FP piezzo position", units='V')

    def update(self):
        x, y = a.syncAiAoFP.getNthChanAIdata(0)
        _, y2 = a.syncAiAoFP.getNthChanAImean(0)
        self.curve.setData(x=x,y=y)
        self.meanCurve.setData(x=x,y=y2)
        if a.syncAiAoFP.counter == 1:
            self.p.enableAutoRange('xy', False)        


class MyWindow(QtGui.QMainWindow):
    def __init__(self):
        QtGui.QMainWindow.__init__(self)
        self.setWindowTitle("qtMain")
        self.resize(1400,900)
        
        self.data_graph_widget = DataGraphWidget(self)
        self.live_graph_widgetCPT = LiveGraphWidget_CPT(self)
        self.live_graph_widgetFP = LiveGraphWidget_FP(self)

        # self.abs_graph_widget = AbsGraphWidget(self)
        self.control_widget = ControlWidget(self)
        
        area = DockArea()
        self.setCentralWidget(area)
        self.d1 = Dock("Controls", size=(300, 1))
        self.d2 = Dock("Data logger", size=(900, 1))
        self.d3 = Dock("CPT signal")
        # d4 = Dock("Abs. signal", size=(1000, 1))
        self.d5 = Dock("Fabry Perot")
        
        area.addDock(self.d1, 'left')
        area.addDock(self.d2, 'right', self.d1)
        # area.addDock(d4, 'above', d3)
        area.addDock(self.d5, 'above', self.d2)
        area.addDock(self.d3, 'above', self.d2)
        
        self.d1.addWidget(self.control_widget)
        self.d2.addWidget(self.data_graph_widget)
        # d4.addWidget(self.abs_graph_widget)
        self.d5.addWidget(self.live_graph_widgetFP)
        self.d3.addWidget(self.live_graph_widgetCPT)


a = DeviceSet()
a.dl = DataLogger()

app = QtGui.QApplication([])
win = MyWindow()
win.control_widget.on_startFreqScanWindowBtn()
win.show()

if __name__ == "__main__":
    if (sys.flags.interactive != 1) or not hasattr(QtCore, 'PYQT_VERSION'):
        ret = QtGui.QApplication.instance().exec_()
        a.syncAiAoCPT.stop()
        win.live_graph_widgetCPT.stop_timer()
        a.syncAiAoCPT.stop()
        a.syncAiAoCPT_pulsed.stop()
        win.live_graph_widgetCPT.stop_timer()
        a.asserv.stop()
        a.asserv_pulsed.stop()
        a.dl.stop()
        sys.exit(ret)