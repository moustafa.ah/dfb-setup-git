import pyqtgraph as pg
from pyqtgraph.Qt import QtCore, QtGui
import pyqtgraph.parametertree.parameterTypes as pTypes
from pyqtgraph.parametertree import Parameter
import allantools, threading, time
import numpy as np


class Adev_graph_UI(QtCore.QObject):

	delay = 3 # Seconds
	newAdev = QtCore.Signal(object,object,object,object)

	def __init__(self,ds):
		super(Adev_graph_UI, self).__init__()
		self.ds = ds
		self.graph_layout = pg.GraphicsLayoutWidget()
		self.plot = self.graph_layout.addPlot(title = "")
		self.plot.setLabel('left', "Allan deviation")
		self.plot.setLabel('bottom', "Integration time", units='s')
		self.curve = self.plot.plot(pen="g")
		self.err_bar = pg.ErrorBarItem(x=0,y=0,beam=0.05,pen=(0,255,0,100))
		self.plot.setLogMode(x=True, y=True)
		self.plot.addItem(self.err_bar)
		self.plot.showGrid(x=True, y=True, alpha=1)

		self.param_group = pTypes.GroupParameter(name = "Allan deviation")
		self.autorefresh_param = Parameter.create(name='Auto-refresh',
												type='bool',
												value=True)
		self.param_group.addChildren([self.autorefresh_param])
		self.param_group.sigTreeStateChanged.connect(self.change)
		self.thread = None
		self.newAdev.connect(self.update)

	def compute_adev(self):
		print("Starting Adev computer")
		self.running = True
		while self.running:
			if self.ds.asserv.buf.index > 4:
				data_freq = self.ds.asserv.buf.get()[:,1]
				data_time = self.ds.asserv.buf.get()[:,0]
				rate = 1./np.mean(np.diff(data_time))
				(tau_used, ad , ade, adn) =  allantools.oadev(data_freq/4.596e9, rate=rate, taus='decade', data_type="freq")
				top_error = np.log10(ad + ade/2) - np.log10(ad)
				bot_error = np.log10(ad) - np.log10(ad - ade/2)
				self.newAdev.emit(tau_used,ad,top_error,bot_error)
			time.sleep(self.delay)
		print("Adev computer stopped")	

	def __del__(self):
		self.stop()

	def change(self,param,changes):
		for param, change, data in changes:
			if param is self.autorefresh_param:
				if param.value(): self.start()
				else: self.stop()

	def update(self,x,y,top_error,bot_error):
			self.err_bar.setData(x=np.log10(x), y=np.log10(y), top=top_error, bottom=bot_error)
			self.curve.setData(x=x,y=y)

	def start(self):
		self.thread = threading.Thread(target=self.compute_adev)
		self.thread.start()

	def stop(self):
		if self.thread is not None:
			self.running = False
			self.thread = None