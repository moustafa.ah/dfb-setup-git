from phase_jumps import PhaseJumps
import pyqtgraph as pg
import pyqtgraph.parametertree.parameterTypes as pTypes
from pyqtgraph.parametertree import Parameter, ParameterTree
from pyqtgraph.Qt import QtGui, QtCore
import numpy as np
from pyqtgraph.graphicsItems.PlotItem import PlotItem
from pyqtgraph import LabelItem
from pyqtgraph.dockarea import *

pens = [(82, 9, 184), (255, 0, 133), (255, 226, 8), (20, 183, 204), "r"]

class PhaseJumps_UI(PhaseJumps):

	def __init__(self,**kwargs):
		super(PhaseJumps_UI, self).__init__(**kwargs)

		self.sequence_parameter_group = pTypes.GroupParameter(name = 'Sequence Parameters')

		self.phase_modulation_amp_param = Parameter.create(name = 'Phase Modulation Amp',
											type = 'float',
											value = self.phase_modulation_amp,
											step = 0.05,
											siPrefix=False,
											suffix='V',
											decimals=2	)
		self.cooling_duration_param = Parameter.create(name = 'Cooling time', 
											  type = 'float',
											  value = self.cooling_duration, 
											  step = 0.001,
											  siPrefix=True,
											  suffix= 's',
											  decimals=5)

		self.dark_duration_param = Parameter.create(name = 'Dark time', 
											  type = 'float',
											  value = self.dark_duration, 
											  step = 0.001,
											  siPrefix=True,
											  suffix= 's')

		self.sequence_parameter_group.addChildren([self.phase_modulation_amp_param,
													self.cooling_duration_param,
													self.dark_duration_param])

		self.clock_parameter_group = pTypes.GroupParameter(name = 'Clock Parameters')

		self.initial_offset_param = Parameter.create(name = 'Initial Offset',
												type = 'float',
												value = self.initial_offset,
												step = 1,
												siPrefix=True,
												suffix= 'Hz')

		self.freq_lock_param = Parameter.create(name = 'Freq. lock',
											type = 'bool',
											value = self._freq_lock)

		self.p_gain_param = Parameter.create(name = 'P gain', 
											  type = 'float',
											  value = self._freq_p_gain, 
											  step = 1e3)

		self.autorestart_param = Parameter.create(name = 'Autorestart',
											type = 'bool',
											value = self.autorestart)

		self.clock_parameter_group.addChildren([self.initial_offset_param,
												self.freq_lock_param,
												self.p_gain_param,
												self.autorestart_param])

		self.param_group = pTypes.GroupParameter(name = "")

		self.param_group.addChildren([self.sequence_parameter_group,self.clock_parameter_group])

		self.param_group.sigTreeStateChanged.connect(self.change)

		##### Create start/stop button #####
		self.startButton = QtGui.QPushButton("Start Pulse")
		self.startButton.setCheckable(True)
		self.startButton.clicked.connect(self.on_button_clicked)

		##### Create sequence plot #####
		self.sequence_dock = Dock("Sequence signal", size=(800, 1))

		self.sequence_graph_wgt = pg.GraphicsLayoutWidget()
		self.cooling_sequence_plot = self.sequence_graph_wgt.addPlot()
		self.sequence_graph_wgt.nextRow()
		self.cpt_sequence_plot= self.sequence_graph_wgt.addPlot()
		self.sequence_graph_wgt.nextRow()
		self.phase_mod_plot = self.sequence_graph_wgt.addPlot()
		self.sequence_dock.addWidget(self.sequence_graph_wgt)

		#Link the x-axis of the plots
		self.cpt_sequence_plot.getViewBox().setXLink(self.cooling_sequence_plot)
		self.cooling_sequence_plot.getViewBox().setXLink(self.phase_mod_plot)

		#Legends
		self.cooling_sequence_plot.addLegend()
		self.cpt_sequence_plot.addLegend()
		self.phase_mod_plot.addLegend()

		#Grids and axis labels
		self.cooling_sequence_plot.showGrid(x=True,y=True,alpha=0.8)	
		self.cpt_sequence_plot.showGrid(x=True,y=True,alpha=0.8)
		self.phase_mod_plot.showGrid(x=True,y=True,alpha=0.8)			

		self.cooling_sequence_plot.setLabel('left', "Cooling Sequence", units='V')
		self.cooling_sequence_plot.setLabel('bottom', "Time (ms)")

		self.cpt_sequence_plot.setLabel('left', "CPT Light Sequence", units='V')
		self.cpt_sequence_plot.setLabel('bottom', "Time (ms)")

		self.phase_mod_plot.setLabel('left', "Phase Modulation Sequence", units='V')
		self.phase_mod_plot.setLabel('bottom', "Time (ms)")

		#Add curves to plots

		#Cooling sequence
		self.cooling_light_switch_curve = self.cooling_sequence_plot.plot(pen="g", name= 'Cooling light switch')
		self.cooling_light_freq_curve =  self.cooling_sequence_plot.plot(pen="b", name = 'Cooling light freq')
		self.mag_field_switch_curve = self.cooling_sequence_plot.plot(pen="m", name = 'B MOT switch')

		#CPT Sequence
		self.CPT_light_switch_curve = self.cpt_sequence_plot.plot(pen="r", name='CPT light switch', fillLevel=0.,brush=(200,50,50,100))
		self.signal_curve = self.cpt_sequence_plot.plot(pen="c", name = "Signal")
		self.normal_curve = self.cpt_sequence_plot.plot(pen="w", name = "Normalization")

		#Phase Modulation Sequence
		self.phase_modulation_curve = self.phase_mod_plot.plot(pen="y", name='LO phase mod')

		self.draw_waveforms()

		self.timer = QtCore.QTimer()
		self.timer.timeout.connect(self.update_cycle_plot)
		
	def change(self,param,changes):
		for param, change, data in changes:
			if param is self.dark_duration_param: self.dark_duration = param.value()
			elif param is self.phase_modulation_amp_param: self.phase_modulation_amp = param.value()
			elif param is self.cooling_duration_param: self.cooling_duration = param.value()
			elif param is self.freq_lock_param: self._freq_lock = param.value()
			elif param is self.p_gain_param: self._freq_p_gain = param.value()
			elif param is self.autorestart_param: self.autorestart = param.value()
		self.redraw_waveform = True
		if not self.running:
			self.generate_waveforms()
			self.draw_waveforms()

	def on_button_clicked(self,object):
		if self.startButton.isChecked():
			self.start()
			self.startButton.setText("Stop Pulse")
			self.timer.start(50)
		else:
			self.stop()
			self.startButton.setText("Start Pulse")
			self.timer.stop()

	def update_cycle_plot(self):
		if self.counter > 0:
			self.signal_curve.setData(x = self.time_data, y = self.signal_data)
			self.normal_curve.setData(x = self.time_data, y = self.normal_data)
			
		if self.redraw_waveform:
			self.draw_waveforms()

	def draw_waveforms(self):
		self.CPT_light_switch_curve.setData(x = self.time_data, y = self.CPT_light_switch_data)
		self.cooling_light_switch_curve.setData(x = self.time_data, y = self.cooling_light_switch_data)
		self.cooling_light_freq_curve.setData(x = self.time_data, y = self.cooling_light_freq_data)
		self.mag_field_switch_curve.setData(x = self.time_data, y = self.mag_field_switch_data)
		self.phase_modulation_curve.setData(x = self.time_data, y = self.phase_modulation_data)
		self.redraw_waveform = False

	def make_plots(self, graph_layout=None,title_font_size=3):
		self.plots = []
		self.curves = []
		for idx, header in enumerate(self.header[1:]):
			l = LabelItem("<font size={:d}>{}</font>".format(title_font_size,header))
			# p = PlotItem(title = "<font size={:d}>{}</font>".format(title_font_size,header))
			p = PlotItem()
			c = p.plot( title = header, 
						pen = pens[idx % len(pens)])
			if idx > 1:
				c.getViewBox().setXLink(self.plots[idx-1])
				self.curves[0].getViewBox().setXLink(p)
			self.plots.append(p)
			self.curves.append(c)
			if graph_layout is not None:
				graph_layout.addItem(l)
				graph_layout.addItem(p)
				graph_layout.nextRow()

	def update(self):
		if self.buf.index > 0:
			data = self.buf.get()
			t0 = data[0,0]
			for idx, curve in enumerate(self.curves):
				curve.setData(x = data[:,0] - t0 , y = data[:,idx+1])