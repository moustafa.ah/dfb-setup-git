# Installation de Python et des pilotes pour le banc VCSEL

- Installer un éditeur de texte : Sublime Text 3 ou autre (Atom.io, ...).

- Installer Anaconda:
	Python 3.7+ version 64-bit
	https://www.anaconda.com/download/
	- Installer pour tous les utilisateurs dans C:\Anaconda3
	- Cocher "Register Anaconda as the system Python 3.7"
	- Skip Microsoft Visual Code installation

- Ajouter Python au "path" système (pour que les commandes python/ipython/pip/jupyter soient accessibles de n'importe où):
	- Panneau de configuration\Système et sécurité\Système
	- "Paramètres système avancés"
	- "Variables d'environnement"
	- Sous "Variables système" vérifier que la valeur de la variable PATH contient les chemins:
		C:\Anaconda3;C:\Anaconda3\Scripts;C:\Anaconda3\Library\bin
	- Les ajouter s'ils sont absents

- Pour faire fonctionner Jupyter Notebook, il peut être nécessaire de bypasser le proxy ENSMM pour les adresses locales:
	- Panneau de configuration/Options Internet Onglet Connexions -> Paramètres réseau
	- Décocher "Détection automatique"
	- Utiliser adresse: "proxy-www.ens2m.fr", port: 3128
	- Cocher "Ne pas utiliser de serveur proxy  pour les adresses locales"

- Installation des modules
	Pour installer des modules Python derrière le proxy ensmm, il faut parfois utiliser la commande:
	pip install --proxy proxy-www.ens2m.fr:3128 nom_du_module
	Les modules à installer sont:
	- quantities (conversion d'unités)
	- colour (générer des gradients de couleurs pour les graphes)
	- lmfit (fitting de courbe)
	- pyqt5 (nécessaire pour pyqtgraph)
	- pyserial (communications série par exemple pour parler avec cartes Arduino)
	- allantools

- Le module PyQtGraph (interface graphique) doit être installé via l’exécutable:
	- Télécharger "pyqtgraph-0.10.0.win-amd64.exe" à l'adresse http://www.pyqtgraph.org/ et installer dans C:\Anaconda3
	- Vérifier le fonctionnement en lançant les exemples: python -m pyqtgraph.examples

- Installer le module pythonnet:
	- Télécharger le fichier wheel "pythonnet‑2.4.0.dev0‑cp37‑cp37m‑win_amd64.whl" https://www.lfd.uci.edu/~gohlke/pythonlibs/#pythonnet
	- Exécuter la commande: "pip install pythonnet‑2.4.0.dev0‑cp37‑cp37m‑win_amd64.whl"

# Installation des pilotes

- Installation des drivers National Instruments
	Le boîtier GPIB-USB-HS et la carte d'acquisition NiDAQ nécessitent des drivers spécifiques:
	- NI-488.2 18.5.0 (700 Mo) http://www.ni.com/download/ni-488.2-18.5/7816/en/
	- NI-VISA 18.0 (730 Mo) à télécharger depuis http://www.ni.com/download/ni-visa-18.0/7597/en/ (nécessite un compte gratuit) et à installer
	- NI-DAQmx 18.5.0 (2 Go) à télécharger (http://www.ni.com/download/ni-daqmx-18.5/7829/en/) et à installer
	
- Installer les modules Python qui communique avec les drivers NI : PyVisa et PyDAQmx
	- pip install pyvisa
	- pip install PyDAQmx

- Installer Arduino https://www.arduino.cc/en/Main/Software
- Installer manuellement le driver du device FT232R via le gestionnaire de périphériques (C:\Program Files (x86)\Arduino\drivers)
	Identifier les ports COM correspondant à la régulation de température et la "lock box"
	Modifier adéquatement les ports COM dans le programme "main" lors de l'instanciation:
		ds.regulation = CYPidReg_UI(comPort='com4')
		ds.lock_box = LockBox_UI(comPort='com5')

- Installer le pilote du puissance-mètre Thorlabs PM16:
	-Télécharger la dernière version i.e. Thorlabs.OpticalPowerMonitor.1.1.2317.143.zip (https://www.thorlabs.com/software_pages/viewsoftwarepage.cfm?code)
	-Vérifier ensuite la présence du fichier "Thorlabs.TLPM_64.Interop.dll" à l'adresse:
	"C:\Program Files\IVI Foundation\VISA\VisaCom64\Primary Interop Assemblies"
	-Si ce fichier a été installer ailleurs, il faut changer le chemin d'accès dans le fichier "pm16dotnet.py": "clr.AddReference("C:\\Program Files\\IVI Foundation\\VISA\\VisaCom64\\Primary Interop Assemblies\\Thorlabs.TLPM_64.Interop")"
	-Utiliser le Power Meter Driver Switcher installé précédemment pour affecter le pilote TLPM (libusb) au puissance mètre PM160.

- Installer le pilote pour la source de courant Newport 560B:
	Télécharger le fichier M530Software_CD_v3.30.zip sur le serveur FTP Newport:
	ftp://download.newport.com/Photonics/Laser%20Diode%20Control%20Instruments/500B_300B%20Software/
	Installer le logiciel (Setup.exe dans le dossier racine du fichier zip)
	Vérifier ensuite la présence du fichier "M530USBWrap.dll" à l'adresse:
	"C:\Program Files (x86)\Newport\Newport USB Driver M530\Bin"
	Si ce fichier a été installer ailleurs, il faut changer le chemin d'accès dans le fichier "np560.py": "clr.AddReference("C:\\Program Files (x86)\\Newport\\Newport USB Driver M530\\Bin\\M530USBWrap")"



A suivre...