import pyqtgraph.parametertree.parameterTypes as pTypes
from pyqtgraph.parametertree import Parameter, ParameterTree
import shelve

class Shelved_param_UI(object):

	def __init__(self,filename='myparams'):

		self.param_group = pTypes.GroupParameter(name = 'Shelved parameters')
		self.data_shelve = shelve.open(filename)
			
		if "cell_id" not in self.data_shelve.keys(): self.data_shelve["cell_id"] = "Unknown"

		for key in self.data_shelve.keys():
			try:
				param = Parameter.create(name=key, type='str',value=self.data_shelve[key])
				self.param_group.addChild(param)
			except EOFError:
				print("Cound not read key : "+key)

		self.param_group.sigTreeStateChanged.connect(self.change)

	def change(self,param,changes):
		for param, change, data in changes:
			if param is not self.param_group:
				self.data_shelve[param.name()] = param.value()

	def __del__(self):
		self.data_shelve.close()

	def close(self):
		self.data_shelve.close()

	def get_config(self):
		return {"Shelved params":dict(self.data_shelve)}

if __name__ == '__main__':
	import pyqtgraph as pg
	from pyqtgraph.Qt import QtCore, QtGui

	app = QtGui.QApplication([])
	win = QtGui.QMainWindow()

	shelf = Shelved_param_UI()

	t = ParameterTree()
	t.setParameters(shelf.param_group, showTop=False)

	win.setCentralWidget(t)
	win.show()
	import sys
	if (sys.flags.interactive != 1) or not hasattr(QtCore, 'PYQT_VERSION'):
		QtGui.QApplication.instance().exec_()
		shelf.close()
