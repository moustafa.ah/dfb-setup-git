import wx.lib.activex
import threading
import comtypes.client as cc
import comtypes.automation as ca
from ctypes import byref, pointer, c_long, c_float, c_bool

##cc.GetModule( ('{9460A175-8618-4753-B337-61D9771C4C14}', 1, 0) )
##progID_system = 'MG17SYSTEM.MG17SystemCtrl.1'
##import comtypes.gen.MG17SystemLib as MGsys

cc.GetModule( ('{2A833923-9AA7-4C45-90AC-DA4F19DC24D1}', 1, 0) )
progID_motor = 'MGMOTOR.MGMotorCtrl.1'
import comtypes.gen.MG17MotorLib as APTMotorLib
channel1 = APTMotorLib.CHAN1_ID
channel2 = APTMotorLib.CHAN2_ID
break_type_switch = APTMotorLib.HWLIMSW_BREAKS
make_type_switch = APTMotorLib.HWLIMSW_MAKES
units_mm = APTMotorLib.UNITS_MM
home_rev = APTMotorLib.HOME_REV
homelimsw_rev = APTMotorLib.HOMELIMSW_REV_HW
motor_moving_bits =  -2147478512
motor_stopped_not_homed_bits = -2147479552
motor_stopped_and_homed_bits = -2147478528

class MGMotor( wx.lib.activex.ActiveXCtrl ):
	"""The Motor class derives from wx.lib.activex.ActiveXCtrl, which
	   is where all the heavy lifting with COM gets done."""
	
	def __init__( self, parent, HWSerialNum, id=wx.ID_ANY, pos=wx.DefaultPosition,
				 size=wx.DefaultSize, style=0, name='Stepper Motor' ):
		wx.lib.activex.ActiveXCtrl.__init__(self, parent, progID_motor,
											id, pos, size, style, name)
		self.ctrl.HWSerialNum = HWSerialNum
		print "Starting Motor Driver: "+str(HWSerialNum)
		self.ctrl.StartCtrl()
		returnCode = self.ctrl.EnableHWChannel(channel1)
		# assert returnCode!=0, "Connection Error"
		print "Connection OK"
		self.enabled = True
		self.SetJogVelParams()
		self.SetVelParams()
		self.SetHomeParams()
	
	def __del__(self):
		self.ctrl.StopCtrl()

	def MotorIsNotMoving( self, channel=channel1 ):
		return self.GetStatusBits_Bits( channel ) in [ motor_stopped_not_homed_bits, motor_stopped_and_homed_bits]
		
	def GetHWLimSwitches( self, channel=channel1 ):
		plRevLimSwitch = c_long()
		plFwdLimSwitch = c_long()
		self.ctrl.GetHWLimSwitches( channel, byref(plRevLimSwitch),byref(plFwdLimSwitch) )
		return plRevLimSwitch.value, plFwdLimSwitch.value
		
	def GetJogStepSize( self, channel=channel1,jogStepSize=0 ):
		stepsize = c_float(jogStepSize)
		self.ctrl.GetJogStepSize( channel, byref( stepsize ) )
		return stepsize.value
	
	def GetPosition( self, channel=channel1 ):
		position = c_float()
		self.ctrl.GetPosition(channel, byref(position))
		return position.value
	
	def GetStageAxisInfo( self, channel=channel1 ):
		min_position = c_float()
		max_position = c_float()
		units = c_long()
		pitch = c_float()
		direction = c_long()
		self.ctrl.GetStageAxisInfo(channel, min_position,max_position, units,pitch, direction)
		return min_position.value, max_position.value, units.value, pitch.value, direction.value

	def GetStageAxisInfo_MaxPos( self, channel=channel1 ):
		return self.ctrl.GetStageAxisInfo_MaxPos( channel )

	def GetStageAxisInfo_MinPos( self, channel=channel1 ):
		return self.ctrl.GetStageAxisInfo_MinPos( channel )

	def GetStatusBits_Bits( self, channel=channel1 ):
		return self.ctrl.GetStatusBits_Bits( channel )

	def MoveHome(self,channel=channel1,wait=True):
		return self.ctrl.MoveHome(channel,wait)
	
	def MoveJog(self,channel=channel1,jogDir=APTMotorLib.JOG_FWD):
		return self.ctrl.MoveJog(channel,jogDir)
	
	def SetBLashDist( self, channel=channel1, backlash=0.01 ):
		return self.ctrl.SetBLashDist( channel, backlash )
	
	def SetHomeParams( self, channel=channel1, direction=home_rev, switch=homelimsw_rev,
					   velocity=3, zero_offset=0.1 ):
		return self.ctrl.SetHomeParams( channel, direction, switch, velocity, zero_offset )
	
	def SetHWLimSwitches( self, channel=channel1, lRevLimSwitch=make_type_switch ,lFwdLimSwitch=make_type_switch ):
		return self.ctrl.SetHWLimSwitches(channel,lRevLimSwitch,lFwdLimSwitch)
	
	def SetSWPosLimits( self, channel=channel1, minpos=-10.0, maxpos=12.0, limitmode=break_type_switch ):
		return self.ctrl.SetSWPosLimits( channel, minpos, maxpos, limitmode )
		
	def SetStageAxisInfo( self, channel=channel1, minpos=-10.0, maxpos=12.0, pitch=1.0, units=units_mm ):
		return self.ctrl.SetStageAxisInfo( channel, minpos, maxpos, units, pitch, 1 )

	def SetJogStepSize( self, channel=channel1, stepsize=1 ):
		if self.enabled: return self.ctrl.SetJogStepSize(channel,stepsize)
	
	def SetJogMode( self, channel=channel1, mode=APTMotorLib.JOG_SINGLESTEP, stopMode=APTMotorLib.STOP_IMMEDIATE ):
		return self.ctrl.SetJogMode(channel,mode,stopMode)
	
	def SetJogVelParams(self,channel=channel1,minvelocity=0.0,maxvelocity=2,acceleration=1):
		return self.ctrl.SetJogVelParams(channel,minvelocity,acceleration,maxvelocity)
	
	def SetVelParams( self, channel=channel1, minvelocity=0.0, maxvelocity=2, acceleration=1 ):
		return self.ctrl.SetVelParams( channel, minvelocity,acceleration,maxvelocity)
		
	def StopImmediate( self, channel=channel1 ):
		return self.ctrl.StopImmediate( channel )
		
	def StopProfiled( self, channel=channel1 ):
		return self.ctrl.StopProfiled( channel )

class StageApp( wx.App ): 
	def __init__( self, redirect=False, filename=None ):
		wx.App.__init__( self, redirect, filename )
		self.frame = wx.Frame( None, wx.ID_ANY, title='MG17MotorControl' )
		self.panel = wx.Panel( self.frame, wx.ID_ANY )

class Stage():
	def __init__( self):
		self.app=StageApp()
		self.motor1 = MGMotor( self.app.panel, HWSerialNum=90861957, style=wx.SUNKEN_BORDER )
		self.motor2 = MGMotor( self.app.panel, HWSerialNum=90861958, style=wx.SUNKEN_BORDER )
		if self.motor1.enabled: self.motor1.SetHWLimSwitches()
		if self.motor2.enabled: self.motor2.SetHWLimSwitches()

	def Close(self):
		if self.motor1.enabled: self.motor1.ctrl.DisableHWChannel( channel1 )
		if self.motor2.enabled: self.motor2.ctrl.DisableHWChannel( channel1 )
		self.motor1.ctrl.StopCtrl()
		self.motor2.ctrl.StopCtrl()
	
	def GetPosition(self):
		x=self.motor1.GetPosition()
		y=self.motor2.GetPosition()
		return x,y
	
	def MoveHome(self):
		self.motor1.MoveHome(wait=False)
		self.motor2.MoveHome(wait=False)
	
	def StopImmediate(self):
		self.motor1.StopImmediate()
		self.motor2.StopImmediate()

	def MoveAbsolute(self,(x,y)):
		self.motor1.ctrl.SetAbsMovePos(channel1,x)
		self.motor2.ctrl.SetAbsMovePos(channel1,y)
		def thread():
			self.motor1.ctrl.MoveAbsolute(channel1,True)			
		t = threading.Thread(name="thread",target=thread)
		t.start()
		self.motor2.ctrl.MoveAbsolute(channel1,True)	
		t.join()
	
	def HomeAndCenter(self):
		self.motor1.ctrl.SetAbsMovePos(channel1,60)
		self.motor2.ctrl.SetAbsMovePos(channel1,60)
		def thread():
			self.motor1.MoveHome(wait=True)
			self.motor1.ctrl.MoveAbsolute(channel1,True)			
		t = threading.Thread(name="thread",target=thread)
		t.start()
		self.motor2.MoveHome(wait=True)
		self.motor2.ctrl.MoveAbsolute(channel1,True)	
		t.join()

	def SetJogStepSize(self,stepSize):
		self.motor1.SetJogStepSize(stepsize=stepSize)
		self.motor2.SetJogStepSize(stepsize=stepSize)

	def SetJogMode(self):
		self.motor1.SetJogMode()
		self.motor2.SetJogMode()

if __name__=='__main__':
	stage = Stage()