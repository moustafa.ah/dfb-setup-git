from .sr830 import SR830
import pyqtgraph.parametertree.parameterTypes as pTypes
from pyqtgraph.parametertree import Parameter, ParameterTree

class SR830_UI(SR830):

	def __init__(self,**kwargs):
		super(SR830_UI, self).__init__(**kwargs)
		self.param_group = pTypes.GroupParameter(name = self._name+' (SR830)')
		self.amplitude_param = Parameter.create(name='Amplitude',
												type='float',
												value=self.amplitude,
												step = 0.01,
												suffix='V',
												siPrefix=True)
		self.phase_param = Parameter.create(name='Phase',
											type='float',
											value=self.phase,
											step= 1,
											suffix='deg')
		self.frequency_param = Parameter.create(name='Frequency',
												type='float',
												value=self.ref_freq,
												step= 1,
												suffix='Hz')
		self.offset_param = Parameter.create(name='Offset',
												type='float',
												value=self.offset_x,
												step= 0.01,
												suffix='%')
		self.time_constant_param = Parameter.create(name='Time constant',
												type='int',
												value=self.time_constant,
												limits= [0,19])
		self.param_group.addChildren([self.amplitude_param,
									self.phase_param,
									self.frequency_param,
									self.offset_param,
									self.time_constant_param])
		self.param_group.sigTreeStateChanged.connect(self.change)

	def change(self,param,changes):
		for param, change, data in changes:
			if param is self.amplitude_param: self.amplitude = param.value()
			elif param is self.phase_param: self.phase = param.value()
			elif param is self.frequency_param:	self.ref_freq = param.value()
			elif param is self.offset_param: self.offset_x = param.value()
			elif param is self.time_constant_param: self.time_constant = param.value()
