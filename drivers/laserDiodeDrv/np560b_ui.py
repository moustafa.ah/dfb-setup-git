from .np560b import NP560B
import pyqtgraph.parametertree.parameterTypes as pTypes
from pyqtgraph.parametertree import Parameter, ParameterTree

class NP560B_UI(NP560B):

	def __init__(self,*args,**kwargs):
		super(NP560B_UI, self).__init__(*args,**kwargs)
		self.param_group = pTypes.GroupParameter(name = self._name)
		self.current_setpoint_param = Parameter.create(name='Current setpoint (mA)',
												type='float',
												step=0.1,
												value=self.current_set_point)
		self.output_state_param = Parameter.create(name='Output state',
												type='bool',
												value=self.output)
		self.param_group.addChildren([self.current_setpoint_param,
									self.output_state_param,
									])
		self.param_group.sigTreeStateChanged.connect(self.change)

	def change(self,param,changes):
		for param, change, data in changes:
			if param is self.current_setpoint_param: self.current_set_point = param.value()
			elif param is self.output_state_param: self.output = param.value()