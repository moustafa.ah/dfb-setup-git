from .syncAIAO_pulsed3 import SyncAIAO_pulsed
import pyqtgraph.parametertree.parameterTypes as pTypes
from pyqtgraph.parametertree import Parameter, ParameterTree

class SyncAIAO_pulsed_UI(SyncAIAO_pulsed):

	param_group_name = 'Freq. Scan Pulsed'

	def __init__(self,**kwargs):
		super(SyncAIAO_pulsed_UI, self).__init__(**kwargs)
		self.param_group = pTypes.GroupParameter(name = self.param_group_name)
		self.Tbright_param = Parameter.create(name = 'Tbright',
											type = 'float',
											value = self.TbrightLength/self.sampling_rate,
											step = 0.0001,
											siPrefix=True,
											suffix= 's')
		self.Tdark_param = Parameter.create(name = 'Tdark',
											type = 'float',
											value = self.TdarkLength/self.sampling_rate,
											step = 0.0001,
											siPrefix=True,
											suffix= 's')
		self.Tdiscarded_param = Parameter.create(name = 'Dead time before averaging',
											type = 'float',
											value = self.TdiscardedLength/self.sampling_rate,
											step = 0.000001,
											siPrefix=True,
											suffix= 's')
		self.Taverage_param = Parameter.create(name = 'Averaging time',
											type = 'float',
											value = self.TaverageLength/self.sampling_rate,
											step = 0.000001,
											siPrefix=True,
											suffix= 's')
		self.n_cycle_param = Parameter.create(name = 'Number of cycles per point',
											type = 'int',
											value = self.n_cycle,
											step = 1,
											limits = (1,1000))
		self.Tdead_param = Parameter.create(name = 'Dead time between 2 cycles',
											type = 'float',
											value = self.TdeadLength/self.sampling_rate,
											step = 0.000001,
											siPrefix=True,
											suffix= 's')
		self.Tcycle_param = Parameter.create(name = 'Tcycle',
											type = 'float',
											value = self.TcycleLength/self.sampling_rate,
											siPrefix = True,
											suffix = 's',
											readonly = True)
		self.scan_duration_param = Parameter.create(name='Scan duration',
											type='float',
											value=self.scan_duration,
											readonly = True,
											siPrefix=True,
											suffix='s')
		self.scan_numfreqs_param = Parameter.create(name='Number of frequency points',
											type='int',
											value=self.numfreqs,
											step= 10)		
		self.param_group.addChildren([self.Tbright_param,
								self.Tdark_param,
								self.Tdiscarded_param,
								self.Taverage_param,
								self.n_cycle_param,
								self.Tdead_param,
								self.Tcycle_param,
								self.scan_duration_param,
								self.scan_numfreqs_param])
		self.param_group.sigTreeStateChanged.connect(self.change)

	def change(self,param,changes):
		for param, change, data in changes:
			running = self.running
			super(SyncAIAO_pulsed_UI, self).stop()
			if param is self.Tbright_param: self.Tbright = param.value()
			elif param is self.Tdark_param: self.Tdark = param.value()
			elif param is self.Tdead_param: self.Tdead = param.value()
			elif param is self.Tdiscarded_param : self.Tdiscarded = param.value()
			elif param is self.Taverage_param: self.Taverage = param.value()
			elif param is self.n_cycle_param: self.n_cycle = param.value()
			elif param is self.scan_numfreqs_param : self.numfreqs = param.value()
			self.update_pulsed_params()
			if running:
				super(SyncAIAO_pulsed_UI, self).start()

	def update_pulsed_params(self) :
		self.param_group.sigTreeStateChanged.disconnect(self.change)
		self.calculate_params()
		self.scan_numfreqs_param.setValue(self.numfreqs)
		self.scan_duration_param.setValue(self.scan_duration)
		self.param_group.sigTreeStateChanged.connect(self.change)

	def start(self):
		self.param_group.setName(self.param_group_name+" (Active)")
		super(SyncAIAO_pulsed_UI, self).start()

	def stop(self):
		self.param_group.setName(self.param_group_name)
		super(SyncAIAO_pulsed_UI, self).stop()