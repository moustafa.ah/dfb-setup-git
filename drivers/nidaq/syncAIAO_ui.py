from .syncAIAO import SyncAIAO
# from .syncAIAO_ErrSig import SyncAIAO
import pyqtgraph.parametertree.parameterTypes as pTypes
from pyqtgraph.parametertree import Parameter, ParameterTree

class SyncAIAO_UI(SyncAIAO):

    def __init__(self,**kwargs):
        super(SyncAIAO_UI, self).__init__(**kwargs)
        self.param_group = pTypes.GroupParameter(name = 'Scan')
        self.scan_sampling_rate_param = Parameter.create(name='Sampling rate',
                                                        type='int',
                                                        value=self.sampling_rate,
                                                        step= 100000,
                                                        siPrefix=True,
                                                        suffix='Samp/s',
                                                        limits=[100,1000000])
        self.scan_sample_number = Parameter.create(name='Sample number',
                                                    type='int',
                                                    value=self.numSamp,
                                                    step= 1000,
                                                    limits=[100,1000000])
        self.scan_duration_param = Parameter.create(name='Scan duration',
                                            type='float',
                                            value=self.scan_duration,
                                            readonly = True,
                                            siPrefix=True,
                                            suffix='s')
        self.scan_direction_param = Parameter.create(name = 'Direction',
                                                type = 'list',
                                                values = self.scan_directions,
                                                value = self.scan_direction)
        self.mean_param = Parameter.create(name = 'Averaging',
                                                type = 'int',
                                                value = self.mean,
                                                step = 1)
        self.param_group.addChildren([self.scan_sampling_rate_param,
                                        self.scan_sample_number,
                                        self.scan_duration_param,
                                        self.scan_direction_param,
                                        self.mean_param])

        self.param_group.sigTreeStateChanged.connect(self.change)

    def change(self,param,changes):
        restart_flag=False
        for param, change, data in changes:
            if param is self.scan_sampling_rate_param:
                self.sampling_rate = param.value()
                restart_flag= True
            elif param is self.scan_sample_number:
                self.numSamp = param.value()
                restart_flag= True
            elif param is self.scan_direction_param:
                self.scan_direction = param.value()
                restart_flag = True
            elif param is self.mean_param:
                self.mean = param.value()
                restart_flag = True
        if self.running and restart_flag:
            self.stop()
            self.start()
        self.scan_duration_param.setValue(self.scan_duration)