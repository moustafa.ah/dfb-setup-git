# TODO Mouss: P1 and P2 are currenty not averaged over thecycle_number

from PyDAQmx import *
import numpy as np
from collections import deque
import ctypes, time
from base.dataproducer import DataProducer

class Asserv(DataProducer):

    # UI accessible settings
    lock = True
    gain_f0 = 40000
    gain_ls_coeff = 40000
    max_error = 0.0005
    cycle_number = 20
    cycle_number2 = 100
    default_freq_mod = 1450 # LO FM frequency in Hz 
    lo_fm_period = 1 / default_freq_mod # (T_FMLO = 1/1450 = 690 us)
    plaser_mod_period = 2*lo_fm_period # (T_mod Plaser = 1.38 ms)
    sampling_rate = 5e5 # Maximum for two analog input channels
    n_samp_per_cycle = 4*2*int(sampling_rate/(2*default_freq_mod)) #[1376 ech if 500kSamp/s] over a whole symmetrized cycle
    discarded_samples_factor = 0.2
    NIntegrator2 = 1000
    gainI2_f0 = 0.
    gainI2_ls_coeff = 0.

    # Fixed settings
    compatibility_mode = 0          # Set this to 1 on some PC (Mouss)
    trigName = "ai/StartTrigger"
    timeout = 10.0
    initial_dds_frequency = 7358910
    get_init_freq_from_dds = True   # Set to False to use initial_dds_frequency as first frequency
    amplitude = 0.5
    inRange = (-2.,2.)
    outRange = (-0.5,0.5)
    running = False
    header = ["Time","Ls-free frequency (Hz)", "fsynth1 (Hz)", "fsynth2 (Hz)", "Frequency correction (Hz)", "Ls coefficient (Hz/V)", "Error A (V)", "Error B (V)", "Error C (V)", "Error D (V)", "Error + (V)", "Error - (V)", "Mean signal (V)", "P incell 1 (V)", "P incell 2 (V)", "2nd-order integral correction f0 (Hz)", "2nd-order integral correction ls coeff (Hz/V)"]

    rf_am_aom_out_chan          = "/Dev1/ao1"
    lo_fm_out_chan              = "/Dev1/ao3"
    pdcpt_inChan                = "/Dev1/ai0"
    pdincell_inChan             = "/Dev1/ai2"
    switch_chan                 = '/port0/line6'
    gate_clock_out              = '/PFI1'
    gate_clock_in               = '/Ctr0'
    #mag_field_switch_chan       = "/Dev1/port0/line0"
   
    AOChannels = [
                rf_am_aom_out_chan,
                lo_fm_out_chan
                ]

    AIChannels = [
                pdcpt_inChan,
                pdincell_inChan
                ]

    _numAIChan = len(AIChannels)




    def __init__(self, device="Dev1", rf_am_aom_out_chan="ao1", lo_fm_out_chan="ao3", pdcpt_inChan="ai0", pdincell_inChan="ai2", dds_devices=None):
        """This method is called when an object is created from a class and it allows the class to initialize the attributes of the class
        """
        DataProducer.__init__(self,maxlength=3000,numcol=len(self.header))
        self.device = device
        self.rf_am_aom_out_chan = rf_am_aom_out_chan
        self.lo_fm_out_chan = lo_fm_out_chan
        self.pdcpt_inChan = pdcpt_inChan
        self.pdincell_inChan = pdincell_inChan
        self.dds1, self.dds2 = dds_devices
        self.freq_mod = self.default_freq_mod
        
    def initialize(self):
        #Generation of AO waveforms.
        #Generate AO waveform for AOM driver (SMB100A) AM modulation. Here P2>P1.
        self.rf_am_aom_out_data = np.tile(np.repeat(self.amplitude*np.array([-1,1]), 2), 2) # --++--++

        #Generate AO waveform for LO frequency modulation 
        self.lo_fm_out_data = np.hstack((np.tile(self.amplitude *np.array([-1,1]),2),np.tile(self.amplitude *np.array([1,-1]),2))) # -+-++-+-


        #Generate DO waveform for switching
        self.switch_data = np.tile(np.repeat(np.array([0,1], dtype = np.uint8), 2), 2) #  00110011

        ##### Generate data arrays for the read data #####
        self.pdcpt_data     = np.zeros(self.n_samp_per_cycle,dtype=np.float64)
        self.pdincell_data  = np.zeros(self.n_samp_per_cycle,dtype=np.float64)
        self.AI_data = np.zeros(self.n_samp_per_cycle*self._numAIChan,dtype=np.float64)

        self.detectTaskHandle = None
        self.AOtaskHandle = None
        self.ptr = 0
        self.ptr2 = 0
        self.ptrN2 = 0

        if self.get_init_freq_from_dds:         # Overide initial frequency
            self.initial_dds_frequency = self.dds1.frequency
            self.dds2.frequency = self.initial_dds_frequency
        self.bufferLength = 100*self.n_samp_per_cycle
        # self.bufferLength = 2*self.cycle_number*self.n_samp_per_cycle*int(1e8/(2*self.cycle_number*self.n_samp_per_cycle)) #1e8
        self.error_sum_Tab = np.zeros(self.cycle_number,dtype=np.float64) 
        self.error_diff_Tab = np.zeros(self.cycle_number,dtype=np.float64) 
        self.powerTab = np.zeros(self.cycle_number,dtype=np.float64)
        self.nbSampCropped = int(self.n_samp_per_cycle//8*self.discarded_samples_factor) #(2760/8)*0.2 = 69 #number of removed samples at the beginning of each windows
        self.f0 = self.initial_dds_frequency
        self.ls_coeff = 0.

        #### Generate tables for errorSum and errorDiff
        self.tableErrorSum = np.zeros(self.NIntegrator2)
        self.tableErrorDiff = np.zeros(self.NIntegrator2)
        

    @property 
    def update_interval(self):
        return self.cycle_number/float(self.freq_mod/4)

    @property
    def freq_mod(self):
        return 4*self.sampling_rate/float(self.n_samp_per_cycle)

    @freq_mod.setter
    def freq_mod(self,freq_mod):
        self.n_samp_per_cycle = 4*2*int(self.sampling_rate/(2*freq_mod)) # 1376 samples @ 500kSamp/s

    def start(self):
        readAI = ctypes.c_int32()
        P1 = 0.
        P2 = 0.
        self.int2ErrorSum = 0.
        self.int2ErrorDiff = 0.
        # c_cubic = 0.
        # c_quadratic = 0.
        c_cubic = 128.76607699
        c_quadratic = -324.8092184

        AIp1_incell = np.zeros(self.n_samp_per_cycle//2)
        AIp2_incell = np.zeros(self.n_samp_per_cycle//2)

        self.f0_int2 = 0.
        self.ls_coeff_int2 = 0.
        assert not self.running
        self.running = True
        self.initialize()

        print("Starting...")
        print ("LO Modulation frequency: {}".format(self.freq_mod)) 
        print("DDS Update Intervall: {}".format(self.update_interval))
        print("Number of samples per cycle: {}".format(self.n_samp_per_cycle))
        print("N samples removed at each new detection window: {}".format(self.nbSampCropped))
        print("Samples per new LO frequency",self.n_samp_per_cycle/8)
        
        #---------------------------------------------------------------------------#
        def EveryNCallback(taskHandle, everyNsamplesEventType, nSamples, callbackData):
            DAQmxReadAnalogF64(self.detectTaskHandle,self.n_samp_per_cycle,self.timeout,DAQmx_Val_GroupByChannel,self.AI_data,self.n_samp_per_cycle*self._numAIChan,ctypes.byref(readAI),None)

            self.ptr = (self.ptr + 1) % self.cycle_number
            #self.pdcpt_data = np.roll(self.pdcpt_data, -1)
            self.pdcpt_data, self.pdincell_data = self.AI_data.reshape(self._numAIChan,self.n_samp_per_cycle)

            #Process signal data
            #Mesure AI in cell
            AIp1_incell = (np.mean(self.pdincell_data[self.nbSampCropped+1:(self.n_samp_per_cycle//4)+1]) + np.mean(self.pdincell_data[(self.n_samp_per_cycle//2) + self.nbSampCropped+1:(3*self.n_samp_per_cycle//4)+1]))*0.5
            AIp2_incell = (np.mean(self.pdincell_data[(self.n_samp_per_cycle//4)+ self.nbSampCropped+1:(self.n_samp_per_cycle//2)+1]) + np.mean(self.pdincell_data[(3*self.n_samp_per_cycle//4) + self.nbSampCropped+1:]))*0.5

            #Mesure AI cpt

            self.eps_A = self.pdcpt_data[self.n_samp_per_cycle//8+self.nbSampCropped+1: \
                            self.n_samp_per_cycle//4+1] - \
                            self.pdcpt_data[self.nbSampCropped+1: \
                            self.n_samp_per_cycle//8+1]
            self.eps_B = self.pdcpt_data[3*self.n_samp_per_cycle//8+self.nbSampCropped+1: \
                            self.n_samp_per_cycle//2+1] - \
                            self.pdcpt_data[self.n_samp_per_cycle//4+self.nbSampCropped+1: \
                            3*self.n_samp_per_cycle//8+1]
            self.eps_C = self.pdcpt_data[self.n_samp_per_cycle//2+self.nbSampCropped+1: \
                            5*self.n_samp_per_cycle//8+1] - \
                            self.pdcpt_data[5*self.n_samp_per_cycle//8+self.nbSampCropped+1: \
                            6*self.n_samp_per_cycle//8+1]
            self.eps_D = self.pdcpt_data[6*self.n_samp_per_cycle//8+self.nbSampCropped+1: \
                            7*self.n_samp_per_cycle//8+1] - \
                            np.hstack((self.pdcpt_data[7*self.n_samp_per_cycle//8+self.nbSampCropped+1:],
                             self.pdcpt_data[0]))

            mean_eps_A = np.mean(self.eps_A)
            mean_eps_B = np.mean(self.eps_B)
            mean_eps_C = np.mean(self.eps_C)
            mean_eps_D = np.mean(self.eps_D)

            eps_sum = mean_eps_A + mean_eps_B + mean_eps_C + mean_eps_D
            eps_diff = mean_eps_A - mean_eps_B + mean_eps_C - mean_eps_D
            
            P1 = AIp1_incell - 0.01341
            P2 = AIp2_incell - 0.01341
            # P1 = AIp1_incell - 0.0137
            # P2 = AIp2_incell - 0.026
            
            self.error_sum_Tab[self.ptr] = min(max(eps_sum,-self.max_error),self.max_error)
            self.error_diff_Tab[self.ptr] = min(max(eps_diff,-self.max_error),self.max_error)
            self.powerTab[self.ptr] = np.mean(self.pdcpt_data) # Here we decided to measure an average power!
            if self.ptr == 0: # First order integrator cycle
                self.ptrN2 = (self.ptrN2 + 1) % self.NIntegrator2
                self.ptr2 = (self.ptr2 + 1) % self.cycle_number2
                meanErrorSum = np.mean(self.error_sum_Tab)
                meanErrorDiff = np.mean(self.error_diff_Tab)
                self.tableErrorDiff[self.ptr2] = meanErrorSum
                self.tableErrorSum[self.ptr2] = meanErrorDiff
                if self.ptrN2 == 0: # updates value of the integrated error
                    self.int2ErrorSum = np.mean(self.tableErrorSum)
                    self.int2ErrorDiff = np.mean(self.tableErrorDiff)
                mean_power_cpt = np.mean(self.powerTab)
                correction_f0 = self.gain_f0*meanErrorSum
                correction_ls_coeff = self.gain_ls_coeff*meanErrorDiff
                if self.ptr2 ==0: # applies drift correction
                    correction_f0_int2 = self.gainI2_f0*self.int2ErrorSum
                    correction_ls_coeff_int2 = self.gainI2_ls_coeff*self.int2ErrorDiff
                    if self.lock:
                        self.f0_int2 += correction_f0_int2
                        self.ls_coeff_int2 +=correction_ls_coeff_int2
                        self.f0 += correction_f0_int2
                        self.ls_coeff += correction_ls_coeff_int2
                if self.lock:
                    self.f0 += correction_f0
                    self.ls_coeff += correction_ls_coeff
                    self.dds1.frequency = self.f0+P1*self.ls_coeff+P1**2*c_quadratic+P1**3*c_cubic
                    self.dds2.frequency = self.f0+P2*self.ls_coeff+P2**2*c_quadratic+P2**3*c_cubic
                    
                data = [time.time(),self.f0,self.f0+P1*self.ls_coeff,self.f0+P2*self.ls_coeff,self.f0-self.initial_dds_frequency,self.ls_coeff,mean_eps_A,mean_eps_B,mean_eps_C,mean_eps_D,eps_sum,eps_diff,mean_power_cpt, P1, P2,self.f0_int2,self.ls_coeff_int2] 
                self.buf.append(data)
                self.send_to_consumers(data)
            
            return int(0)       
        #---------------------------------------------------------------------------#

        #---------------------------------------------------------------------------#
        def DoneCallback(taskHandle, status, callbackData):
            self.clearTasks()
            return int(0)
        #---------------------------------------------------------------------------#
        
        #---------------------------------------------------------------------------#
        ##### Create task for clock timer #####
        self.COtaskHandle = TaskHandle()
        DAQmxCreateTask(None, ctypes.byref(self.COtaskHandle))

        ##### Create task for digital outputs #####     
        self.DOtaskHandle = TaskHandle()   
        DAQmxCreateTask(None, ctypes.byref(self.DOtaskHandle))  

        ##### Create task for analog outputs #####     
        self.AOtaskHandle = TaskHandle() 
        DAQmxCreateTask(None, ctypes.byref(self.AOtaskHandle))  

        ##### Create task for detection #####
        self.detectTaskHandle = TaskHandle()
        DAQmxCreateTask(None, ctypes.byref(self.detectTaskHandle))

        ##### Create chans #####
        DAQmxCreateCOPulseChanTime(self.COtaskHandle, self.device+self.gate_clock_in,None,DAQmx_Val_Seconds, DAQmx_Val_Low, 0., self.n_samp_per_cycle/(8*2*self.sampling_rate), self.n_samp_per_cycle/(8*2*self.sampling_rate)) # Genrerate a pulse each time the frequency or power changes
        DAQmxCreateDOChan(self.DOtaskHandle, self.device+self.switch_chan, None, DAQmx_Val_ChanPerLine)
        DAQmxCreateAOVoltageChan(self.AOtaskHandle, ",".join(self.AOChannels), None, self.outRange[0],self.outRange[1], DAQmx_Val_Volts, None)
        DAQmxCreateAIVoltageChan(self.detectTaskHandle, ",".join(self.AIChannels), None, DAQmx_Val_Cfg_Default, self.inRange[0],self.inRange[1], DAQmx_Val_Volts, None)
        
        ##### Config clock timing #####
        DAQmxCfgImplicitTiming(self.COtaskHandle,DAQmx_Val_ContSamps, self.n_samp_per_cycle) # buffer length set to cycle length
        DAQmxCfgSampClkTiming(self.DOtaskHandle,'/'+self.device+self.gate_clock_out, 8*self.sampling_rate/self.n_samp_per_cycle, DAQmx_Val_Rising, DAQmx_Val_ContSamps, 8)
        DAQmxCfgSampClkTiming(self.AOtaskHandle,'/'+self.device+self.gate_clock_out, 8*self.sampling_rate/self.n_samp_per_cycle, DAQmx_Val_Rising, DAQmx_Val_ContSamps, 8)
        DAQmxCfgSampClkTiming(self.detectTaskHandle,None, self.sampling_rate, DAQmx_Val_Rising, DAQmx_Val_ContSamps, self.n_samp_per_cycle)
       
        
        ##### Config start trigger #####
        DAQmxCfgDigEdgeStartTrig(self.AOtaskHandle,self.trigName,DAQmx_Val_Rising)
        DAQmxCfgDigEdgeStartTrig(self.COtaskHandle, self.trigName, DAQmx_Val_Rising)

        ##### Define write arrays #####
        DAQmxWriteDigitalLines(self.DOtaskHandle,8,0,0.0,DAQmx_Val_GroupByChannel,self.switch_data,None,None)
        DAQmxWriteAnalogF64(self.AOtaskHandle, 8, 0, 10, DAQmx_Val_GroupByChannel, 
                                np.hstack([
                                    self.rf_am_aom_out_data,
                                    self.lo_fm_out_data]),
                                None, None)

        
        DAQmxCfgInputBuffer(self.detectTaskHandle, ctypes.c_uint32(self.bufferLength*self._numAIChan))

        #---------------------------------------------------------------------------#
              
        
        #---------------------------------------------------------------------------#
        if self.compatibility_mode == 0:
            EveryNCallbackCWRAPPER = ctypes.CFUNCTYPE(ctypes.c_int32,ctypes.c_void_p,ctypes.c_int32,ctypes.c_uint32,ctypes.c_void_p)
        else:
            EveryNCallbackCWRAPPER = ctypes.CFUNCTYPE(ctypes.c_int32,ctypes.c_ulong,ctypes.c_int32,ctypes.c_uint32,ctypes.c_void_p)
        self.everyNCallbackWrapped = EveryNCallbackCWRAPPER(EveryNCallback)
        DAQmxRegisterEveryNSamplesEvent(self.detectTaskHandle,DAQmx_Val_Acquired_Into_Buffer,self.n_samp_per_cycle,0,self.everyNCallbackWrapped,None)
       
        if self.compatibility_mode == 0:
            DoneCallbackCWRAPPER = ctypes.CFUNCTYPE(ctypes.c_int32,ctypes.c_void_p,ctypes.c_int32,ctypes.c_void_p)
        else:
            DoneCallbackCWRAPPER = ctypes.CFUNCTYPE(ctypes.c_int32,ctypes.c_ulong,ctypes.c_int32,ctypes.c_void_p)
        self.doneCallbackWrapped = DoneCallbackCWRAPPER(DoneCallback)
        DAQmxRegisterDoneEvent(self.detectTaskHandle,0,self.doneCallbackWrapped,None)

        #---------------------------------------------------------------------------#
        # Gestion des exceptions
        # Charge à essayer en premier
        #try:
        #    DAQmxWriteAnalogF64(self.AOtaskHandle, self.n_samp_per_cycle, 0, 10, DAQmx_Val_GroupByChannel, np.hstack([self.rf_am_aom_out_data,self.lo_fm_out_data]), None, None)
        # Things to execute if error.
        #except PALResourceReservedError as e:
        #    print(e)
        #    self.running = False
        #    return False
        #---------------------------------------------------------------------------#

        DAQmxStartTask(self.AOtaskHandle)
        DAQmxStartTask(self.DOtaskHandle)
        DAQmxStartTask(self.COtaskHandle)
        DAQmxStartTask(self.detectTaskHandle)
        print("Starting asserv")
        return True

    def stop(self):
        if self.running:
            time.sleep(0.2)
            self.clearTasks()
            self.setZero()
            self.running = False

    def clearTasks(self):
        if self.detectTaskHandle:
            DAQmxStopTask(self.detectTaskHandle)
            DAQmxClearTask(self.detectTaskHandle)
            self.detectTaskHandle = None
        if self.AOtaskHandle:
            DAQmxStopTask(self.AOtaskHandle)
            DAQmxClearTask(self.AOtaskHandle)
            self.AOtaskHandle = None
        if self.DOtaskHandle:
            DAQmxStopTask(self.DOtaskHandle)
            DAQmxClearTask(self.DOtaskHandle)
            self.DOtaskHandle = None
        if self.COtaskHandle:
            DAQmxStopTask(self.COtaskHandle)
            DAQmxClearTask(self.COtaskHandle)

    def setZero(self):
        print("Setting output to 0 V")
        clearAOtaskHandle = TaskHandle()
        DAQmxCreateTask("", ctypes.byref(clearAOtaskHandle))
        DAQmxCreateAOVoltageChan(clearAOtaskHandle, ",".join(self.AOChannels), None, self.outRange[0],self.outRange[1], DAQmx_Val_Volts, None)
        try:
            DAQmxWriteAnalogF64(clearAOtaskHandle,1,1,self.timeout,DAQmx_Val_GroupByChannel,np.array([0.]),None,None)
        except PALResourceReservedError as e:
            print(e)
            return       
        DAQmxStartTask(clearAOtaskHandle)
        DAQmxClearTask(clearAOtaskHandle)

        clearDOtaskHandle = TaskHandle()
        DAQmxCreateTask("", ctypes.byref(clearDOtaskHandle))
        DAQmxCreateDOChan(clearDOtaskHandle, self.device+self.switch_chan, None, DAQmx_Val_ChanPerLine)
        try:
            DAQmxStartTask(clearDOtaskHandle)
            DAQmxWriteDigitalLines(clearDOtaskHandle, 1, 1, 10, DAQmx_Val_GroupByChannel, np.array([0.], np.uint8), None, None)
        except PALResourceReservedError as e:
            print(e)
            return       
        DAQmxClearTask(clearDOtaskHandle)
    


    def __del__(self):
        self.stop()