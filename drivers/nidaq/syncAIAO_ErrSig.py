from PyDAQmx import *
import numpy as np
import ctypes, time
from pyqtgraph.Qt import QtGui, QtCore
import pyqtgraph as pg
from collections import deque
# from drivers.freqSynth.smb100a import SMB100A

# synth = SMB100A()

class SyncAIAO(object):

    compatibility_mode = 0          # Set this to 1 on some PC (Mouss)
    trigName = "ai/StartTrigger"
    timeout = 10.0
    mean = 64
    sampling_rate = 1e5
    numSamp=2000
    nbSampCroppedFactor=0.5
    vpp=1.
    offset = 0.
    scan_directions = ["upward","downward"]
    scan_direction = "upward"

    def __init__(self,device="Dev2",dds_device=None,outChan="ao1",inChanList=["ai0"],inRange=(-10.,10.),outRange=(-10.,10.)):
        self.device = device
        self.synth = dds_device
        self.outChan = outChan
        self.inChanList = inChanList
        self.inRange = inRange
        self.outRange = outRange
        self.running = False
        # self.initialize()

    def update_fm_dev(self,object=None):
        # self.curve.clear()
        self.FMDEVscan = self.synth.fm_dev
        self.ratioFMDev = self.FMDEVlock/self.FMDEVscan
        # On s'assure que le nombre d'échantillons sur une rampe est multiple du ratio des FM DEV(/2)
        # Si ce n'est pas le cas, on arrondit au multiple le plus proche 
        if abs(2/self.ratioFMDev - (0.5*self.numSamp)%(2/self.ratioFMDev)) <= abs((0.5*self.numSamp)%(2/self.ratioFMDev)) :
            self._numSamp = int(2*(0.5*self.numSamp + 2/self.ratioFMDev - (0.5*self.numSamp)%(2/self.ratioFMDev)))
        else :
            self._numSamp = 2*int((0.5*self.numSamp - (0.5*self.numSamp)%(2/self.ratioFMDev)))

    def initialize(self):
        self._sampling_rate = self.sampling_rate
        self.FMDEVlock = 1600.
        self.update_fm_dev()

        self.nbSampCropped = int(self.nbSampCroppedFactor * self._numSamp)
        self.AImean = np.zeros(self._numSamp*len(self.inChanList),dtype=np.float64)
        self.AIdata = np.zeros((self.mean,self._numSamp*len(self.inChanList)),dtype=np.float64)
        self.AIdata_plus = np.zeros((self.mean,self._numSamp*len(self.inChanList)),dtype=np.float64)
        self.sigErrmean = np.zeros(self._numSamp*len(self.inChanList),dtype=np.float64)
        self.sigErrdata = np.zeros((self.mean,self._numSamp*len(self.inChanList)),dtype=np.float64)
        self.ptr = 0
        self.deque = deque([],self.mean)
        self.AOdata = self.offset + np.hstack([np.linspace(-self.vpp/2.,self.vpp/2.,self._numSamp//2,dtype=np.float64,endpoint=False),
            np.linspace(self.vpp/2.,-self.vpp/2.,self._numSamp//2,dtype=np.float64,endpoint=False)])
        self.counter=0
        self.totalAI=0
        self.AItaskHandle = None
        self.AOtaskHandle = None

    @property
    def scan_duration(self):
        return self.numSamp/2./float(self.sampling_rate)

    def makeInputStr(self):
        return ",".join([self.device+"/"+inChan for inChan in self.inChanList])
    def makeOutputStr(self):
        return self.device+"/"+self.outChan

    def getNthFullChanName(self,index):
        return self.device+"/"+self.inChanList[index]

    def getNthChanAIdata(self,index):
        if self.scan_direction == "upward":
            return self.AOdata[:self._numSamp-self.nbSampCropped], \
                    self.AIdata[self.ptr,index*self._numSamp:(index+1)*self._numSamp-self.nbSampCropped]
        else:
            return self.AOdata[self._numSamp-self.nbSampCropped:], \
                    self.AIdata[self.ptr,index*self._numSamp+self.nbSampCropped:(index+1)*self._numSamp]

    def getNthChanAImean(self,index):
        if self.scan_direction == "upward":
            return self.AOdata[:self._numSamp-self.nbSampCropped], \
                    self.AImean[index*self._numSamp:(index+1)*self._numSamp-self.nbSampCropped]
        else:
            return self.AOdata[self._numSamp-self.nbSampCropped:], \
                    self.AImean[index*self._numSamp+self.nbSampCropped:(index+1)*self._numSamp]

    def getNthChanSigErrdata(self):
        if self.scan_direction == "upward":
            return self.AOdata[int((self._numSamp-self.nbSampCropped)*(self.ratioFMDev)/2):int((self._numSamp-self.nbSampCropped)*(self.ratioFMDev)/2)+int((1-self.ratioFMDev)*(self._numSamp-self.nbSampCropped))], \
                    self.sigErrdata[self.ptr,:int((1-self.ratioFMDev)*(self._numSamp-self.nbSampCropped))]
    
    def getNthChanSigErrmean(self):
        if self.scan_direction == "upward":
            return self.AOdata[int((self._numSamp-self.nbSampCropped)*(self.ratioFMDev)/2):int((self._numSamp-self.nbSampCropped)*(self.ratioFMDev)/2)+int((1-self.ratioFMDev)*(self._numSamp-self.nbSampCropped))], \
                    self.sigErrmean[:int((1-self.ratioFMDev)*(self._numSamp-self.nbSampCropped))]

    def start(self):
        assert not self.running
        self.running = True
        self.initialize()
        self.synth.fm_dev_param.sigValueChanged.connect(self.update_fm_dev)
        def EveryNCallback(taskHandle, everyNsamplesEventType, nSamples, callbackData):
            # global AItaskHandle, totalAI, AIdata, ptr
            readAI = ctypes.c_int32()
            self.ptr=(self.ptr+1)%self.mean
            self.deque.append(self.ptr)
            DAQmxReadAnalogF64(self.AItaskHandle,self._numSamp,self.timeout,DAQmx_Val_GroupByChannel,self.AIdata[self.ptr],self._numSamp*len(self.inChanList),ctypes.byref(readAI),None)
            self.AImean=np.mean(self.AIdata[self.deque],axis=0)
            self.AIdata_plus[self.ptr] = np.roll(self.AIdata[self.ptr],-int((self._numSamp-self.nbSampCropped)*self.ratioFMDev))
            self.sigErrdata[self.ptr] = self.AIdata_plus[self.ptr]-self.AIdata[self.ptr]
            self.sigErrmean = np.mean(self.sigErrdata[self.deque], axis=0)
            self.totalAI = self.totalAI + readAI.value
            self.counter=self.counter+1
            # print self.totalAI
            return int(0)

        def DoneCallback(taskHandle, status, callbackData):
            self.clearTasks()
            return int(0)

        self.AItaskHandle = TaskHandle()
        self.AOtaskHandle = TaskHandle()
        self.totalAI=0

        DAQmxCreateTask(None,ctypes.byref(self.AItaskHandle))
        DAQmxCreateAIVoltageChan(self.AItaskHandle,self.makeInputStr(), None, DAQmx_Val_Cfg_Default, self.inRange[0],self.inRange[1], DAQmx_Val_Volts, None)
        DAQmxCfgSampClkTiming(self.AItaskHandle,None, self._sampling_rate, DAQmx_Val_Rising, DAQmx_Val_ContSamps, self._numSamp)
        DAQmxCreateTask(None,ctypes.byref(self.AOtaskHandle))
        DAQmxCreateAOVoltageChan(self.AOtaskHandle,self.makeOutputStr(),None,self.outRange[0],self.outRange[1],DAQmx_Val_Volts,None)
        DAQmxCfgSampClkTiming(self.AOtaskHandle,None,self._sampling_rate,DAQmx_Val_Rising,DAQmx_Val_ContSamps,self._numSamp)
        DAQmxCfgDigEdgeStartTrig(self.AOtaskHandle,self.trigName,DAQmx_Val_Rising)

        if self.compatibility_mode == 0:
            EveryNCallbackCWRAPPER = ctypes.CFUNCTYPE(ctypes.c_int32,ctypes.c_void_p,ctypes.c_int32,ctypes.c_uint32,ctypes.c_void_p)
        else:
            EveryNCallbackCWRAPPER = ctypes.CFUNCTYPE(ctypes.c_int32,ctypes.c_ulong,ctypes.c_int32,ctypes.c_uint32,ctypes.c_void_p)
        self.everyNCallbackWrapped = EveryNCallbackCWRAPPER(EveryNCallback)
        DAQmxRegisterEveryNSamplesEvent(self.AItaskHandle,DAQmx_Val_Acquired_Into_Buffer,self._numSamp,0,self.everyNCallbackWrapped,None)

        if self.compatibility_mode == 0:
            DoneCallbackCWRAPPER = ctypes.CFUNCTYPE(ctypes.c_int32,ctypes.c_void_p,ctypes.c_int32,ctypes.c_void_p)
        else:
            DoneCallbackCWRAPPER = ctypes.CFUNCTYPE(ctypes.c_int32,ctypes.c_ulong,ctypes.c_int32,ctypes.c_void_p)
        self.doneCallbackWrapped = DoneCallbackCWRAPPER(DoneCallback)
        DAQmxRegisterDoneEvent(self.AItaskHandle,0,self.doneCallbackWrapped,None)

        try:
            DAQmxWriteAnalogF64(self.AOtaskHandle, self._numSamp, 0, self.timeout, DAQmx_Val_GroupByChannel, self.AOdata, None, None)
        except PALResourceReservedError as e:
            print(e)
            self.running = False
            return False

        try:
            DAQmxStartTask(self.AOtaskHandle)
            DAQmxStartTask(self.AItaskHandle)
        except Exception as e:
            print(e)
            self.stop()
            return False
        print("Starting acquisition")
        return True

    def clearTasks(self):
        if self.AItaskHandle:
            DAQmxStopTask(self.AItaskHandle)
            DAQmxClearTask(self.AItaskHandle)
            self.AItaskHandle = None
        if self.AOtaskHandle:
            DAQmxStopTask(self.AOtaskHandle)
            DAQmxClearTask(self.AOtaskHandle)
            self.AOtaskHandle = None

    def stop(self):
        if self.running:
            self.clearTasks()
            self.setZero()
            self.running = False

    def setZero(self):
        print("Setting output to 0 V")
        clearTaskHandle = TaskHandle()
        DAQmxCreateTask("", ctypes.byref(clearTaskHandle))
        DAQmxCreateAOVoltageChan(clearTaskHandle, self.makeOutputStr(), None, self.outRange[0],self.outRange[1], DAQmx_Val_Volts, None)
        try:
            DAQmxWriteAnalogF64(clearTaskHandle,1,1,self.timeout,DAQmx_Val_GroupByChannel,np.array([0.]),None,None)
        except PALResourceReservedError as e:
            print(e)
            return
        DAQmxStartTask(clearTaskHandle)
        DAQmxClearTask(clearTaskHandle)

    def __del__(self):
        self.stop()
