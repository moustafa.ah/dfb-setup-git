from .niusb6259 import NIUSB6259
import pyqtgraph.parametertree.parameterTypes as pTypes
from pyqtgraph.parametertree import Parameter
import numpy as np

class Channel(object):

	def __init__(self,parent, name, daq_chan, step = 0.1, daq_range = (-10,+10)):
		self.parent = parent
		self.daq_chan = daq_chan
		self.daq_range = daq_range
		self.param = Parameter.create(name=name,
								type='float',
								value=np.round(self.parent.read_voltage_on_output(daq_chan),4),
								step= step,
								suffix='V')
		self.parent.param_group.addChild(self.param)
		self.param.sigValueChanged.connect(self.change)

	def change(self,param):
		self.parent.setOutputVoltage(param.value(),self.daq_chan,self.daq_range)

class AnalogOutputs(NIUSB6259):

	def __init__(self,devID='Dev2'):
		NIUSB6259.__init__(self,devID)
		self.param_group = pTypes.GroupParameter(name = 'Analog Outputs')
		self.channels = []

	def add_channel(self,name,daq_chan,step = 0.1, daq_range = (-10, 10)):
		channel = Channel(self, name, daq_chan, step, daq_range)
		self.channels.append(channel)