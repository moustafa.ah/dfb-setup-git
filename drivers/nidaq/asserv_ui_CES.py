from .asserv_CES import Asserv
import pyqtgraph.parametertree.parameterTypes as pTypes
from pyqtgraph.parametertree import Parameter, ParameterTree
from ui.toggle_button import ToggleButton
from pyqtgraph.graphicsItems.PlotItem import PlotItem
from pyqtgraph import LabelItem
import numpy as np

symbolBrushes = [(130, 45, 255), (255, 0, 133), (255, 226, 8), (20, 183, 204), "r"]

class Asserv_UI(Asserv):

	param_group_name = "Asserv. CW"

	def __init__(self,**kwargs):
		super(Asserv_UI, self).__init__(**kwargs)
		self.param_group = pTypes.GroupParameter(name = self.param_group_name)
		self.freq_lock_param = Parameter.create(name='Lock',
											type='bool',
											value=self.lock)
		self.gain_param = Parameter.create(name='Gain',
											type='float',
											value=self.gain,
											step= 100)
		self.alpha_param = Parameter.create(name='alpha',
											type='float',
											value=self.alpha,
											step= 0.1)
		self.max_error_param = Parameter.create(name='Max error',
											type='float',
											value=self.max_error,
											step= 0.0001)
		self.sampling_rate_param = Parameter.create(name='Sampling rate',
											type='int',
											value=self.sampling_rate,
											step= 100000,
											siPrefix=True,
											suffix='Samp/s')
		self.freq_mod_param = Parameter.create(name='Mod. frequency',
											type='float',
											value=self.freq_mod,
											step= 1,
											siPrefix=True,
											suffix='Hz')
		self.cycle_number_param = Parameter.create(name='N cycle',
											type='int',
											value=self.cycle_number,
											step=1)
		self.discarded_samples_factor_param = Parameter.create(name='Ratio discarded samp.',
											type='float',
											value= self.discarded_samples_factor,
											step=0.05)
		# Read only parameters
		self.n_samp_per_cycle_param = Parameter.create(name='N samp/cycle',
											type='int',
											value=self.n_samp_per_cycle,
											readonly=True)
		self.act_freq_mod_param = Parameter.create(name='Actual Mod frequency',
											type='float',
											value=self.freq_mod,
											siPrefix=True,
											suffix='Hz',
											readonly=True)
		self.update_interval_param = Parameter.create(name='Update interval',
											type='float',
											value=self.update_interval,
											siPrefix=True,
											suffix='s',
											readonly=True)
		self.param_group.addChildren([self.freq_lock_param,
									self.gain_param,
									self.alpha_param,
									self.max_error_param,
									self.sampling_rate_param,
									self.freq_mod_param,
									self.cycle_number_param,
									self.discarded_samples_factor_param,
									self.n_samp_per_cycle_param,
									self.act_freq_mod_param,
									self.update_interval_param])

		self.param_group.sigTreeStateChanged.connect(self.change)

		self.startBtn = ToggleButton("Asserv", self)


	def change(self,param,changes):
		for param, change, data in changes:
			if param is self.freq_lock_param: self.lock = param.value()
			elif param is self.gain_param: self.gain = param.value()
			elif param is self.alpha_param: self.alpha = param.value()
			elif param is self.max_error_param: self.max_error = param.value()
			elif param is self.sampling_rate_param:	self.sampling_rate = param.value()				
			elif param is self.freq_mod_param: self.freq_mod = param.value()
			elif param is self.cycle_number_param: self.cycle_number = param.value()
			elif param is self.discarded_samples_factor_param: self.discarded_samples_factor = param.value()
			self.update_readonly_params()

	def update_readonly_params(self):
		self.act_freq_mod_param.setValue(self.freq_mod)
		self.n_samp_per_cycle_param.setValue(self.n_samp_per_cycle)
		self.update_interval_param.setValue(self.update_interval)

	def start(self):
		if super(Asserv_UI, self).start():
			self.param_group.setName(self.param_group_name+" (Active)")
			return True
		return False

	def stop(self):
		self.param_group.setName(self.param_group_name)
		super(Asserv_UI, self).stop()


	def make_plots(self, graph_layout=None,title_font_size=3):
		self.plots = []
		self.curves = []
		for idx, header in enumerate(self.header[1:]):
			l = LabelItem("<font size={:d}>{}</font>".format(title_font_size,header))
			# p = PlotItem(title = "<font size={:d}>{}</font>".format(title_font_size,header))
			p = PlotItem()
			c = p.plot( title = header, pen = symbolBrushes[idx % len(symbolBrushes)])
			self.plots.append(p)
			self.curves.append(c)
			if graph_layout is not None:
				graph_layout.addItem(l)
				graph_layout.addItem(p)
				graph_layout.nextRow()

	def update(self):
		if self.buf.index > 0:
			data = self.buf.get()
			t0 = data[0,0]
			for idx, curve in enumerate(self.curves):
				curve.setData(x = data[:,0] - t0 , y = data[:,idx+1])