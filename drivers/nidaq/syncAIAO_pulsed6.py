from PyDAQmx import *
import numpy as np
import ctypes, time
from pyqtgraph.Qt import QtGui, QtCore
import pyqtgraph as pg
from collections import deque

class SyncAIAO_pulsed(object):

	compatibility_mode = 0          # Set this to 1 on some PC (Mouss)
	trigName = "ai/StartTrigger"
	timeout = 10.0
	# mean = 64
	sampling_rate = 1e6
	Tbright = .0011 # s
	Tdark = .0027 # s
	Tdiscarded = .00002 # s
	Taverage = .000050 # s
	# ramp_frequency = 0.1 # Hz
	numfreqs=500
	vpp=2.
	offset = 0.
	n_cycle = 6
	TdeadLength = 10


	

	
	# numSamp = None
	# nbSampCroppedFactor=0.5
	
	# queue =

	def __init__(self,device="Dev2",outChan="ao1", DOChan = 'port0/line6',inChan="ai0",inRange=(-10.,10.),outRange=(-10.,10.)):
		self.device = device
		self.DOChan = DOChan
		self.COChan = 'PFI12'
		self.outChan = outChan
		self.inChan = inChan
		self.inRange = inRange
		self.outRange = outRange
		self.running = False
		self.calculate_params()
		self.initialize()

	def init5VEnabling(self):
		self.init5VtaskHandle = TaskHandle()
		DAQmxCreateTask(None, byref(self.init5VtaskHandle))
		DAQmxCreateDOChan(self.init5VtaskHandle, self.device+'/port0/line7', None, DAQmx_Val_ChanPerLine)
		DAQmxStartTask(self.init5VtaskHandle)
		DAQmxWriteDigitalLines(self.init5VtaskHandle, 1, 1, 10, DAQmx_Val_GroupByChannel, np.array([1.], np.uint8), None, None)
		DAQmxStopTask(self.init5VtaskHandle)
		DAQmxClearTask(self.init5VtaskHandle)

	def calculate_params(self) :
		
		self.TbrightLength = int(self.Tbright*self.sampling_rate)
		self.TdarkLength = int(self.Tdark*self.sampling_rate)
		self.TcycleLength = self.TbrightLength + self.TdarkLength
		self.TdiscardedLength = int(self.Tdiscarded * self.sampling_rate)
		self.TaverageLength = int(self.Taverage *self.sampling_rate)
		self.scan_duration = self.TcycleLength*self.n_cycle*self.numfreqs/self.sampling_rate

	def initialize(self):
		# self.numSamp = self.numfreqs*self.n_cycle*self.TcycleLength
		# Tableau final correspondant a une rampe en frequence
		self.AImean = np.zeros(self.numfreqs,dtype=np.float64) 
		self.last_AImean = np.zeros(self.numfreqs,dtype=np.float64) 
		self.AIdata = np.zeros(self.TcycleLength*self.n_cycle)
		# Waveform envoyee pour pulser la lumiere
		self.DOdata = np.zeros(self.TcycleLength/10, dtype = np.uint8)
		self.DOdata[(self.TbrightLength - self.TdiscardedLength - self.TaverageLength)/10 : (self.TcycleLength - self.TdiscardedLength - self.TaverageLength)/10] = 1
		# self.DOdata = np.tile(self.DOdata, self.n_cycle)
		self.DOdata2 = np.ones(self.TcycleLength/10, dtype = np.uint8)
		self.DOdata2[(self.TbrightLength - self.TdiscardedLength - self.TaverageLength)/10 : (self.TcycleLength - self.TdiscardedLength - self.TaverageLength)/10] = 0
		# self.DOdata2 = np.tile(self.DOdata2, self.n_cycle)


		
		# Waveform moyennage
		self.average_mask = np.zeros(self.TcycleLength, dtype =bool)
		self.average_mask[self.TcycleLength-self.TaverageLength:] = 1
		self.average_mask = np.tile(self.average_mask, self.n_cycle)
		
		# Waveform DDS
		self.AOdata = np.linspace(-self.vpp/2.,self.vpp/2.,self.numfreqs)
		


		# # self.nbSampCropped = int(self.nbSampCroppedFactor * self.numSamp)
		# self.AImean = np.zeros(self.numSamp,dtype=np.float64) # Tableau final correspondant a une rampe en frequence
		# self.DOdata = np.zeros(self.TcycleLength, dtype = np.uint8)
		# self.DOdata[0:self.TbrightLength] = 1 # Waveform envoyee pour pulser la lumiere
		# self.AIdata_N_cycles = np.zeros(self.TcycleLength * self.N_cycles, dtype = np.float64)
		# self.mask = np.zeros(self.TcycleLength * self.N_cycles, dtype = bool)
		# for i in np.arange(1, N_cycles) :
		# 	self.mask[i*TcycleLength+TdiscardedLength : i*TcycleLength+TdiscardedLength + TaverageLength] = True
		# # self.deque = deque([],self.mean)
		# self.AOdata = self.offset + np.hstack([np.linspace(-self.vpp/2.,self.vpp/2.,self.numSamp/2,dtype=np.float64,endpoint=False),
		#     np.linspace(self.vpp/2.,-self.vpp/2.,self.numSamp/2,dtype=np.float64,endpoint=False)])
		# self.totalAI=0
		self.COtaskHandle = None
		self.AItaskHandle = None
		self.AOtaskHandle = None
		self.DOtaskHandle = None
		self.DO2taskHandle = None


	def makeInputStr(self):			
		return self.device+"/"+self.inChan
	def makeOutputStr(self):
		return self.device+"/"+self.outChan
	def makeDOStr(self):
		return self.device+"/"+self.DOChan


	def start(self):	
		assert not self.running
		self.running = True
		self.initialize()
		self.ptr = 0
		def EveryNCallback(taskHandle, everyNsamplesEventType, nSamples, callbackData):
			# global AItaskHandle, totalAI, AIdata, ptr
			readAI = c_int32()
			# self.ptr=(self.ptr+1)%self.mean
			# self.deque.append(self.ptr)
			if self.ptr ==0 :
				self.last_AImean = self.AImean
			DAQmxReadAnalogF64(self.AItaskHandle,self.TcycleLength * self.n_cycle,self.timeout,DAQmx_Val_GroupByChannel,self.AIdata,self.TcycleLength * self.n_cycle,byref(readAI),None)

			self.AImean[self.ptr] = np.mean(self.AIdata[self.average_mask])
			# self.totalAI = self.totalAI + readAI.value
			# self.counter=self.counter+1
			# print self.totalAI
			self.ptr = (self.ptr+1)%self.numfreqs
			return int(0)
			

		def DoneCallback(taskHandle, status, callbackData):
			self.clearTasks()
			return int(0)

		self.COtaskHandle = TaskHandle()
		self.DOtaskHandle = TaskHandle()
		# self.DO2taskHandle = TaskHandle()
		self.AItaskHandle = TaskHandle()
		self.AOtaskHandle = TaskHandle()
		# self.totalAI=0

		# DAQmxCreateTask(None, byref(self.DO2taskHandle))
		DAQmxCreateTask(None,byref(self.AItaskHandle))
		DAQmxCreateTask(None, byref(self.COtaskHandle))
		DAQmxCreateTask(None, byref(self.DOtaskHandle))

		DAQmxCreateCOPulseChanTime(self.COtaskHandle, '/'+self.device+'/Ctr0',None,DAQmx_Val_Seconds, DAQmx_Val_High, 0, 5E-6,5E-6)
			# self.TbrightLength/self.sampling_rate,self.TdarkLength/self.sampling_rate)
		DAQmxCreateDOChan(self.DOtaskHandle, '/'+self.device+'/port0/line6:7', None, DAQmx_Val_ChanPerLine)
		# DAQmxCreateDOChan(self.DO2taskHandle, 'Dev1/port0/line7', None, DAQmx_Val_ChanPerLine)
		DAQmxCreateAIVoltageChan(self.AItaskHandle,self.makeInputStr(), None, DAQmx_Val_Cfg_Default, self.inRange[0],self.inRange[1], DAQmx_Val_Volts, None)
		DAQmxCfgInputBuffer(self.AItaskHandle, 100* self.TcycleLength * self.n_cycle)
		DAQmxCfgSampClkTiming(self.AItaskHandle,None, self.sampling_rate, DAQmx_Val_Rising, DAQmx_Val_ContSamps, self.TcycleLength*self.n_cycle)
		DAQmxCreateTask(None,byref(self.AOtaskHandle))
		DAQmxCreateAOVoltageChan(self.AOtaskHandle,self.makeOutputStr(),None,self.outRange[0],self.outRange[1],DAQmx_Val_Volts,None)
		# DAQmxCfgSampClkTiming(self.DO2taskHandle,'/Dev1/PFI1', 100000., DAQmx_Val_Rising, DAQmx_Val_ContSamps, self.TcycleLength)
		DAQmxCfgSampClkTiming(self.DOtaskHandle,'/'+self.device+'/PFI1', 100000., DAQmx_Val_Rising, DAQmx_Val_ContSamps, self.TcycleLength)
		DAQmxCfgSampClkTiming(self.AOtaskHandle, None,self.sampling_rate/(self.TcycleLength*self.n_cycle),DAQmx_Val_Rising,DAQmx_Val_ContSamps,self.numfreqs)
		DAQmxCfgImplicitTiming(self.COtaskHandle,DAQmx_Val_ContSamps,self.TcycleLength)
		DAQmxCfgDigEdgeStartTrig(self.AOtaskHandle,self.trigName,DAQmx_Val_Rising)
		DAQmxCfgDigEdgeStartTrig(self.COtaskHandle,self.trigName,DAQmx_Val_Rising)

		if self.compatibility_mode == 0:
			EveryNCallbackCWRAPPER = CFUNCTYPE(c_int32,c_void_p,c_int32,c_uint32,c_void_p)
		else:
			EveryNCallbackCWRAPPER = CFUNCTYPE(c_int32,c_ulong,c_int32,c_uint32,c_void_p)
		self.everyNCallbackWrapped = EveryNCallbackCWRAPPER(EveryNCallback)
		DAQmxRegisterEveryNSamplesEvent(self.AItaskHandle,DAQmx_Val_Acquired_Into_Buffer,self.TcycleLength*self.n_cycle,0,self.everyNCallbackWrapped,None)

		if self.compatibility_mode == 0:
			DoneCallbackCWRAPPER = CFUNCTYPE(c_int32,c_void_p,c_int32,c_void_p)
		else:
			DoneCallbackCWRAPPER = CFUNCTYPE(c_int32,c_ulong,c_int32,c_void_p)
		self.doneCallbackWrapped = DoneCallbackCWRAPPER(DoneCallback)
		DAQmxRegisterDoneEvent(self.AItaskHandle,0,self.doneCallbackWrapped,None)

		# DAQmxWriteDigitalLines(self.DOtaskHandle, self.n_cycle*self.TcycleLength, 0, self.timeout, DAQmx_Val_GroupByChannel, self.DOdata, None, None)
		# DAQmxWriteDigitalU32(self.DOtaskHandle,self.TcycleLength*self.n_cycle,0,0,DAQmx_Val_GroupByChannel,self.DOdata,None,None)
		DAQmxWriteDigitalLines(self.DOtaskHandle,self.TcycleLength/10,0,0.0,DAQmx_Val_GroupByChannel,np.hstack((self.DOdata, self.DOdata2)),None,None)
		# DAQmxWriteDigitalLines(self.DO2taskHandle,self.TcycleLength/10,0,0.0,DAQmx_Val_GroupByChannel,self.DOdata2,None,None)
		DAQmxWriteAnalogF64(self.AOtaskHandle, self.numfreqs, 0, self.timeout, DAQmx_Val_GroupByChannel, self.AOdata, None, None)

		DAQmxStartTask(self.COtaskHandle)
		DAQmxStartTask(self.DOtaskHandle)
		# DAQmxStartTask(self.DO2taskHandle)
		DAQmxStartTask(self.AOtaskHandle)
		DAQmxStartTask(self.AItaskHandle)
		print "Starting acquisition"

	def clearTasks(self):
		if self.AItaskHandle:
			DAQmxStopTask(self.AItaskHandle)
			DAQmxClearTask(self.AItaskHandle)
			self.AItaskHandle = None
		if self.AOtaskHandle:
			DAQmxStopTask(self.AOtaskHandle)
			DAQmxClearTask(self.AOtaskHandle)
			self.AOtaskHandle = None
		if self.COtaskHandle:
			DAQmxStopTask(self.COtaskHandle)
			DAQmxClearTask(self.COtaskHandle)
			self.COtaskHandle = None
		if self.DOtaskHandle:
			DAQmxStopTask(self.DOtaskHandle)
			DAQmxClearTask(self.DOtaskHandle)
			self.DOtaskHandle = None
		# if self.DO2taskHandle:
		# 	DAQmxStopTask(self.DO2taskHandle)
		# 	DAQmxClearTask(self.DO2taskHandle)
		# 	self.DO2taskHandle = None

	def stop(self):
		if self.running:
			self.clearTasks()
			self.setZero()
			self.running = False

	def setZero(self):
		print "Setting output to 0 V"
		clearAOTaskHandle = TaskHandle()
		DAQmxCreateTask("", byref(clearAOTaskHandle))
		DAQmxCreateAOVoltageChan(clearAOTaskHandle, self.makeOutputStr(), None, self.outRange[0],self.outRange[1], DAQmx_Val_Volts, None)
		DAQmxWriteAnalogF64(clearAOTaskHandle,1,1,self.timeout,DAQmx_Val_GroupByChannel,np.array([0.]),None,None)
		DAQmxStartTask(clearAOTaskHandle)
		DAQmxClearTask(clearAOTaskHandle)
		clearDOTaskHandle = TaskHandle()
		DAQmxCreateTask(None, byref(clearDOTaskHandle))
		DAQmxCreateDOChan(clearDOTaskHandle, self.makeDOStr(), None, DAQmx_Val_ChanPerLine)
		DAQmxStartTask(clearDOTaskHandle)
		DAQmxWriteDigitalLines(clearDOTaskHandle, 1, 1, 10, DAQmx_Val_GroupByChannel, np.array([0.], np.uint8), None, None)
		DAQmxClearTask(clearDOTaskHandle)
		self.init5VEnabling()


	def __del__(self):
		self.stop()

if __name__=="__main__":

	app = QtGui.QApplication([])
	win = pg.GraphicsWindow()
	win.resize(1000,600)
	win.setWindowTitle('Pyqtgraph : Live NIDAQmx data')
	pg.setConfigOptions(antialias=True)
	outChan="ao3"
	inChan="ai7"
	syncAiAo = SyncAIAO_pulsed(device = "Dev1", inChan=inChan,outChan=outChan)
	p = win.addPlot(title="Live plot")
	p.addLegend()

	curve = p.plot(pen='c',name='None')

	def update():
		x, y = syncAiAo.AOdata, syncAiAo.AImean
		curve.setData(x=x, y=y)

	timer = QtCore.QTimer()
	timer.timeout.connect(update)
	timer.start(100)

	syncAiAo.start()

	import sys 
	if (sys.flags.interactive != 1) or not hasattr(QtCore, 'PYQT_VERSION'):
		ret = QtGui.QApplication.instance().exec_()
		print "Closing"
		syncAiAo.stop()
		sys.exit(ret)
