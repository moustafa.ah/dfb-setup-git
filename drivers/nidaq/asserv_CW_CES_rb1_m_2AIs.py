from PyDAQmx import *
import numpy as np
import ctypes, time
from base.dataproducer import DataProducer
# from drivers.freqSynth.smb100a_ui import SMB100A_UI, SMB100A_AM_UI

class Asserv(DataProducer):

    # UI accessible settings
    lock = True
    gain = 40000
    alpha = 1.
    max_error = 0.0005
    cycle_number = 20
    default_freq_mod = 192 # LO FM frequency in Hz 
    lo_fm_period = 1 / default_freq_mod # (T_FMLO = 1/192 = 5.2 ms)
    plaser_mod_period = 2*lo_fm_period # (T_mod Plaser = 10.4 ms)
    sampling_rate = 5e5
    n_samp_per_cycle = sampling_rate * plaser_mod_period #on definit ici cycle comme P1+P2, soit 2 cycles d'horloge F1-F2-F1-F2, nombre d'échantillons sur une séquence P1-P2 (now 10400)
    discarded_samples_factor = 0.1

    # Fixed settings
    compatibility_mode = 0          # Set this to 1 on some PC (Mouss)
    trigName = "ai/StartTrigger"
    timeout = 10.0
    initial_dds_frequency = 7358910
    get_init_freq_from_dds = True   # Set to False to use initial_dds_frequency as first frequency
    amplitude = 0.5
    inRange = (-1.,1.)
    outRange = (-0.5,0.5)
    running = False
    header = ["Time","Frequency (Hz)","Correction (Hz)","Error","Mean signal (V)", "AIf1p1", "AIf2p1", "AIf1p2", "AIf2p2", "Epsilon1(V)", "Epsilon2(V)", "Pin1 (V)", "Pin2(V)"]

    rf_am_aom_out_chan          = "/Dev1/ao1"
    lo_fm_out_chan              = "/Dev1/ao3"
    pdcpt_inChan                = "/Dev1/ai0"
    pdincell_inChan             = "/Dev1/ai2"
    #mag_field_switch_chan       = "/Dev1/port0/line0"
   
    AOChannels = [
                rf_am_aom_out_chan,
                lo_fm_out_chan
                ]

    AIChannels = [
                pdcpt_inChan,
                pdincell_inChan
                ]

    _numAIChan = len(AIChannels)
    _chunk_size = 100 # [samples] - number of samples after which the data from the DAQ will be read
    #_num_cycles = 2   # The number of cycles per sequence (a cycle is cooling, two cpt pulses and dark time)
    

    #__init__: This method is called when an object is created from a class and it allows the class to initialize the attributes of the class
    def __init__(self, device="Dev1", rf_am_aom_out_chan="ao1", lo_fm_out_chan="ao3", pdcpt_inChan="ai0", pdincell_inChan="ai2", dds_device=None):
        DataProducer.__init__(self,maxlength=3000,numcol=len(self.header))
        self.device = device
        self.rf_am_aom_out_chan = rf_am_aom_out_chan
        self.lo_fm_out_chan = lo_fm_out_chan
        self.pdcpt_inChan = pdcpt_inChan
        self.pdincell_inChan = pdincell_inChan
        self.dds = dds_device
        self.freq_mod = self.default_freq_mod
        
    def initialize(self):
        #Generation des waveform AO.
        #Generate AO waveform for AOM driver (SMB100A) AM modulation. On prend dans notre cas P2>P1.
        self.rf_am_aom_out_data = np.hstack([-self.amplitude *np.ones(int(self.n_samp_per_cycle//2)),
                                    self.amplitude *np.ones(int(self.n_samp_per_cycle//2))])
        #Generate AO waveform for LO frequency modulation 
        self.lo_fm_out_data = np.tile(np.hstack([-self.amplitude *np.ones(int(self.n_samp_per_cycle//4)),
                                    self.amplitude *np.ones(int(self.n_samp_per_cycle//4))]), 2)
        

        ##### Generate time data #####
        self.time_data = np.arange(self.n_samp_per_cycle) / float(self.sampling_rate) * 1000

        ##### Generate data arrays for the read data #####
        
        #self.AI_data        = np.zeros(self._chunk_size*self._numAIChan,dtype=np.float64)
        self.pdcpt_data     = np.zeros(self.n_samp_per_cycle,dtype=np.float64)
        self.pdincell_data  = np.zeros(self.n_samp_per_cycle,dtype=np.float64)
        self.AI_data = np.zeros(self.n_samp_per_cycle*self._numAIChan,dtype=np.float64)

        self.detectTaskHandle = None
        self.AOtaskHandle = None
        self.ptr = 0

        if self.get_init_freq_from_dds:         # Overide initial frequency
            self.initial_dds_frequency = self.dds.frequency
        self.dds_frequency = self.initial_dds_frequency
        self.bufferLength = 2*self.cycle_number*self.n_samp_per_cycle*int(1e8/(2*self.cycle_number*self.n_samp_per_cycle)) #1e8
        self.errorTab = np.zeros(self.cycle_number,dtype=np.float64) #20 ech
        self.powerTab = np.zeros(self.cycle_number,dtype=np.float64) #20 ech
        self.nbSampCropped = int(self.n_samp_per_cycle//4*self.discarded_samples_factor) #(10400/4)*0.1 = 260 #number of removed samples at the beginning of each windows
        # self.P2_True = False
        self.alpha = 1

    @property 
    def update_interval(self):
        return self.cycle_number/float(self.freq_mod/2) #now 208 ms

    @property
    def freq_mod(self):
        return 2*self.sampling_rate/float(self.n_samp_per_cycle) # very confusing since lo-fm value becomes here power-fm value

    @freq_mod.setter
    def freq_mod(self,freq_mod):
        self.n_samp_per_cycle = 2*2*int(self.sampling_rate/(2*freq_mod)) # gives now 10416 samples

    #CES error signal calculation (we consider P2>P1)
    def eps_CES(self, eps_1, eps_2, P1, P2, alpha):
        beta_cal = (P1/P2)**(alpha+1)
        return eps_1 - beta_cal*eps_2

    def start(self):
        
        P1 = 0.
        P2 = 0.
        AIf1p1 = np.zeros(self.n_samp_per_cycle//4)
        AIf2p1 = np.zeros(self.n_samp_per_cycle//4)
        AIf1p2 = np.zeros(self.n_samp_per_cycle//4)
        AIf2p2 = np.zeros(self.n_samp_per_cycle//4)
        eps_1 = np.zeros(self.n_samp_per_cycle//2)
        eps_2 = np.zeros(self.n_samp_per_cycle//2)
        AIp1_incell = np.zeros(self.n_samp_per_cycle//2)
        AIp2_incell = np.zeros(self.n_samp_per_cycle//2)

        assert not self.running
        self.running = True
        self.initialize()

        print("Starting...")
        print ("LO Modulation frequency: {}".format(self.freq_mod)) 
        print("DDS Update Intervall: {}".format(self.update_interval))
        print("Number of samples per cycle: {}".format(self.n_samp_per_cycle))
        print("N samples removed at each new detection window: {}".format(self.nbSampCropped))
        print("Samples per new LO frequency",self.n_samp_per_cycle/4)
        
        #---------------------------------------------------------------------------#
        def EveryNCallback(taskHandle, everyNsamplesEventType, nSamples, callbackData):
            t1 = time.clock()
            readAI = ctypes.c_int32()
            DAQmxReadAnalogF64(self.detectTaskHandle,self.n_samp_per_cycle,self.timeout,DAQmx_Val_GroupByChannel,self.AI_data,self.n_samp_per_cycle*self._numAIChan,ctypes.byref(readAI),None)

            self.ptr = (self.ptr + 1) % self.cycle_number
            #self.pdcpt_data = np.roll(self.pdcpt_data, -1)
            self.pdcpt_data, self.pdincell_data = self.AI_data.reshape(self._numAIChan,self.n_samp_per_cycle)

            #Process signal data
            #Mesure AI in cell
            AIp1_incell = np.mean(self.pdincell_data[self.nbSampCropped:int(self.n_samp_per_cycle//2)])
            AIp2_incell = np.mean(self.pdincell_data[int(self.n_samp_per_cycle//2)+self.nbSampCropped:self.n_samp_per_cycle])

            #Mesure AI cpt
            AIf1p1 = np.mean(self.pdcpt_data[self.nbSampCropped:int(self.n_samp_per_cycle//4)])
            AIf2p1 = np.mean(self.pdcpt_data[int(self.n_samp_per_cycle//4)+self.nbSampCropped:int(self.n_samp_per_cycle//2)])
            AIf1p2 = np.mean(self.pdcpt_data[int(self.n_samp_per_cycle//2)+self.nbSampCropped:int(3*self.n_samp_per_cycle//4)])
            AIf2p2 = np.mean(self.pdcpt_data[int(3*self.n_samp_per_cycle//4)+self.nbSampCropped:self.n_samp_per_cycle])
            eps_1 = AIf2p1-AIf1p1 #error signal at P1
            eps_2 = AIf2p2-AIf1p2 #error signal at P2
            #P1 = (AIf1p1 + AIf2p1)/2 # mean voltage(power) at P1 (pd cpt)
            #P2 = (AIf1p2 + AIf2p2)/2 # mean voltage (power) at P2 (pd cpt)
            P1 = AIp1_incell
            P2 = AIp2_incell

            error_CES = np.mean(self.eps_CES(eps_1, eps_2, P1, P2, self.alpha))

            self.errorTab[self.ptr] = min(max(error_CES,-self.max_error),self.max_error)
            #self.powerTab[self.ptr] = np.mean(AIf1p2 + AIf2p2)/2 # Here we decided to measure only P2!
            self.powerTab[self.ptr] = np.mean(P1 + P2)/2 # Here we decided to measure only P2!
            if self.ptr == 0:
                meanError = np.mean(self.errorTab)
                mean_power_cpt = np.mean(self.powerTab)
                correction = self.gain*meanError
                if self.lock:
                    self.dds_frequency += correction 
                    self.dds.frequency = float(self.dds_frequency)

                data = [time.time(),self.dds_frequency,self.dds_frequency-self.initial_dds_frequency,meanError,mean_power_cpt, AIf1p1, AIf2p1, AIf1p2, AIf2p2, eps_1, eps_2, AIp1_incell, AIp2_incell] 
                self.buf.append(data)
                self.send_to_consumers(data)
            
            return int(0)       
        #---------------------------------------------------------------------------#

        #---------------------------------------------------------------------------#
        def DoneCallback(taskHandle, status, callbackData):
            self.clearTasks()
            return int(0)
        #---------------------------------------------------------------------------#
        
        #---------------------------------------------------------------------------#
        ##### Create task for analog outputs #####     
        self.detectTaskHandle = TaskHandle()
        self.AOtaskHandle = TaskHandle()   

        ##### Create task for detection #####
        #self.detectTaskHandle = TaskHandle()
        DAQmxCreateTask(None,ctypes.byref(self.detectTaskHandle))
        DAQmxCreateAIVoltageChan(self.detectTaskHandle, ",".join(self.AIChannels), None, DAQmx_Val_Cfg_Default, self.inRange[0],self.inRange[1], DAQmx_Val_Volts, None)
        DAQmxCfgSampClkTiming(self.detectTaskHandle,None, self.sampling_rate, DAQmx_Val_Rising, DAQmx_Val_ContSamps, self.n_samp_per_cycle)
        
        DAQmxCreateTask(None,ctypes.byref(self.AOtaskHandle))
        DAQmxCreateAOVoltageChan(self.AOtaskHandle, ",".join(self.AOChannels), None, self.outRange[0],self.outRange[1], DAQmx_Val_Volts, None)
        DAQmxCfgSampClkTiming(self.AOtaskHandle,None, self.sampling_rate, DAQmx_Val_Rising, DAQmx_Val_ContSamps, self.n_samp_per_cycle)
        DAQmxCfgDigEdgeStartTrig(self.AOtaskHandle,self.trigName,DAQmx_Val_Rising)
        DAQmxWriteAnalogF64(self.AOtaskHandle, self.n_samp_per_cycle, 0, 10, DAQmx_Val_GroupByChannel, 
                                np.hstack([
                                    self.rf_am_aom_out_data,
                                    self.lo_fm_out_data]),
                                None, None)

        
        DAQmxCfgInputBuffer(self.detectTaskHandle, ctypes.c_uint32(self.bufferLength*self._numAIChan))


        #DAQmxCfgInputBuffer(self.detectTaskHandle, ctypes.c_uint32(self.cycle_num_samp*self.numAIChan*80)) #10400*2*80 = 1.664e6

        #---------------------------------------------------------------------------#
              
        
        #---------------------------------------------------------------------------#
        if self.compatibility_mode == 0:
            EveryNCallbackCWRAPPER = ctypes.CFUNCTYPE(ctypes.c_int32,ctypes.c_void_p,ctypes.c_int32,ctypes.c_uint32,ctypes.c_void_p)
        else:
            EveryNCallbackCWRAPPER = ctypes.CFUNCTYPE(ctypes.c_int32,ctypes.c_ulong,ctypes.c_int32,ctypes.c_uint32,ctypes.c_void_p)
        self.everyNCallbackWrapped = EveryNCallbackCWRAPPER(EveryNCallback)
        DAQmxRegisterEveryNSamplesEvent(self.detectTaskHandle,DAQmx_Val_Acquired_Into_Buffer,self.n_samp_per_cycle,0,self.everyNCallbackWrapped,None)
       
        if self.compatibility_mode == 0:
            DoneCallbackCWRAPPER = ctypes.CFUNCTYPE(ctypes.c_int32,ctypes.c_void_p,ctypes.c_int32,ctypes.c_void_p)
        else:
            DoneCallbackCWRAPPER = ctypes.CFUNCTYPE(ctypes.c_int32,ctypes.c_ulong,ctypes.c_int32,ctypes.c_void_p)
        self.doneCallbackWrapped = DoneCallbackCWRAPPER(DoneCallback)
        DAQmxRegisterDoneEvent(self.detectTaskHandle,0,self.doneCallbackWrapped,None)
        #---------------------------------------------------------------------------#


        #---------------------------------------------------------------------------#
        # Gestion des exceptions
        # Charge à essayer en premier
        #try:
        #    DAQmxWriteAnalogF64(self.AOtaskHandle, self.n_samp_per_cycle, 0, 10, DAQmx_Val_GroupByChannel, np.hstack([self.rf_am_aom_out_data,self.lo_fm_out_data]), None, None)
        # Things to execute if error.
        #except PALResourceReservedError as e:
        #    print(e)
        #    self.running = False
        #    return False
        #---------------------------------------------------------------------------#

        #DAQmxWriteAnalogF64(self.AOtaskHandle, self.n_samp_per_cycle, 0, 10, DAQmx_Val_GroupByChannel, 
        #                        np.hstack([
        #                            self.rf_am_aom_out_data,
        #                            self.lo_fm_out_data]),
        #                        None, None)

        DAQmxStartTask(self.AOtaskHandle)
        DAQmxStartTask(self.detectTaskHandle)
        print("Starting asserv")
        return True

    def stop(self):
        if self.running:
            self.clearTasks()
            self.setZero()
            self.running = False

    def clearTasks(self):
        if self.detectTaskHandle:
            DAQmxStopTask(self.detectTaskHandle)
            DAQmxClearTask(self.detectTaskHandle)
            self.detectTaskHandle = None
        if self.AOtaskHandle:
            DAQmxStopTask(self.AOtaskHandle)
            DAQmxClearTask(self.AOtaskHandle)
            self.AOtaskHandle = None

    def setZero(self):
        print("Setting output to 0 V")
        clearAOtaskHandle = TaskHandle()
        DAQmxCreateTask("", ctypes.byref(clearAOtaskHandle))
        DAQmxCreateAOVoltageChan(clearAOtaskHandle, ",".join(self.AOChannels), None, self.outRange[0],self.outRange[1], DAQmx_Val_Volts, None)
        try:
            DAQmxWriteAnalogF64(clearAOtaskHandle,1,1,self.timeout,DAQmx_Val_GroupByChannel,np.array([0.]),None,None)
        except PALResourceReservedError as e:
            print(e)
            return       
        DAQmxStartTask(clearAOtaskHandle)
        DAQmxClearTask(clearAOtaskHandle)
        
        
    


    def __del__(self):
        self.stop()