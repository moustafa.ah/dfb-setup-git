from PyDAQmx import *
import numpy as np
import ctypes, time
from base.dataproducer import DataProducer
# from drivers.freqSynth.smb100a_ui import SMB100A_UI, SMB100A_AM_UI

class Asserv(DataProducer):

    # UI accessible settings
    lock = True
    gain_f0 = 40000
    gain_ls_coeff = 40000
    max_error = 0.0005
    cycle_number = 20
    default_freq_mod = 192 # LO FM frequency in Hz 
    lo_fm_period = 1 / default_freq_mod # (T_FMLO = 1/192 = 5.2 ms)
    plaser_mod_period = 2*lo_fm_period # (T_mod Plaser = 10.4 ms)
    sampling_rate = 5e5
    n_samp_per_cycle = sampling_rate * 4*lo_fm_period #[20800 ech if 1Ms/s] #on definit ici cycle comme P1+P2, soit 2 cycles d'horloge F1-F2-F1-F2, nombre d'échantillons sur une séquence P1-P2 (now 10400)
    discarded_samples_factor = 0.2

    # Fixed settings
    compatibility_mode = 0          # Set this to 1 on some PC (Mouss)
    trigName = "ai/StartTrigger"
    timeout = 10.0
    initial_dds_frequency = 7358910
    get_init_freq_from_dds = True   # Set to False to use initial_dds_frequency as first frequency
    amplitude = 0.5
    inRange = (-2.,2.)
    outRange = (-0.5,0.5)
    running = False
    header = ["Time","Ls-free frequency (Hz)", "fsynth1 (Hz)", "fsynth2 (Hz)", "Frequency correction (Hz)", "Ls coefficient (Hz/V)", "Error A (V)", "Error B (V)", "Error C (V)", "Error D (V)", "Error + (V)", "Error - (V)", "Mean signal (V)", "P incell 1 (V)", "P incell 2 (V)"]

    rf_am_aom_out_chan          = "/Dev1/ao1"
    lo_fm_out_chan              = "/Dev1/ao3"
    pdcpt_inChan                = "/Dev1/ai0"
    pdincell_inChan             = "/Dev1/ai2"
    switch_chan                 = '/port0/line6'
    gate_clock_out              = '/PFI1'
    gate_clock_in               = '/Ctr0'
    #mag_field_switch_chan       = "/Dev1/port0/line0"
   
    AOChannels = [
                rf_am_aom_out_chan,
                lo_fm_out_chan
                ]

    AIChannels = [
                pdcpt_inChan,
                pdincell_inChan
                ]

    _numAIChan = len(AIChannels)
    _chunk_size = 100 # [samples] - number of samples after which the data from the DAQ will be read
    #_num_cycles = 2   # The number of cycles per sequence (a cycle is cooling, two cpt pulses and dark time)
    

    #__init__: This method is called when an object is created from a class and it allows the class to initialize the attributes of the class
    def __init__(self, device="Dev1", rf_am_aom_out_chan="ao1", lo_fm_out_chan="ao3", pdcpt_inChan="ai0", pdincell_inChan="ai2", dds_devices=None):
        DataProducer.__init__(self,maxlength=3000,numcol=len(self.header))
        self.device = device
        self.rf_am_aom_out_chan = rf_am_aom_out_chan
        self.lo_fm_out_chan = lo_fm_out_chan
        self.pdcpt_inChan = pdcpt_inChan
        self.pdincell_inChan = pdincell_inChan
        self.dds1, self.dds2 = dds_devices
        self.freq_mod = self.default_freq_mod
        
    def initialize(self):
        #Generation des waveform AO.
        #Generate AO waveform for AOM driver (SMB100A) AM modulation. On prend dans notre cas P2>P1.
        # self.rf_am_aom_out_data = np.tile(np.hstack([-self.amplitude *np.ones(int(self.n_samp_per_cycle//4)),
        #                             +self.amplitude *np.ones(int(self.n_samp_per_cycle//4))]),2)
        self.rf_am_aom_out_data = np.tile(np.repeat(self.amplitude*np.array([-1,1]), 2), 2) # --++--++

        #Generate AO waveform for LO frequency modulation 
        #self.lo_fm_out_data = np.tile(np.hstack([-self.amplitude *np.ones(int(self.n_samp_per_cycle//4)),
        #                            self.amplitude *np.ones(int(self.n_samp_per_cycle//4))]), 2)
        # self.lo_fm_out_data = np.hstack([-self.amplitude *np.ones(int(self.n_samp_per_cycle//8)),
        #                             self.amplitude *np.ones(int(self.n_samp_per_cycle//8)),  
        #                             -self.amplitude *np.ones(int(self.n_samp_per_cycle//8)),
        #                             self.amplitude *np.ones(int(self.n_samp_per_cycle//8)),  
        #                             self.amplitude *np.ones(int(self.n_samp_per_cycle//8)),  
        #                             -self.amplitude *np.ones(int(self.n_samp_per_cycle//8)),
        #                             self.amplitude *np.ones(int(self.n_samp_per_cycle//8)), 
        #                             -self.amplitude *np.ones(int(self.n_samp_per_cycle//8))])
        self.lo_fm_out_data = np.hstack((np.tile(self.amplitude *np.array([-1,1]),2),np.tile(self.amplitude *np.array([1,-1]),2))) # -+-++-+-


        #Generate DO waveform for switching
        self.switch_data = np.tile(np.repeat(np.array([0,1], dtype = np.uint8), 2), 2)


        ##### Generate time data #####
        self.time_data = np.arange(self.n_samp_per_cycle) / float(self.sampling_rate) * 1000

        ##### Generate data arrays for the read data #####
        
        #self.AI_data        = np.zeros(self._chunk_size*self._numAIChan,dtype=np.float64)
        self.pdcpt_data     = np.zeros(self.n_samp_per_cycle,dtype=np.float64)
        self.pdincell_data  = np.zeros(self.n_samp_per_cycle,dtype=np.float64)
        self.AI_data = np.zeros(self.n_samp_per_cycle*self._numAIChan,dtype=np.float64)

        self.detectTaskHandle = None
        self.AOtaskHandle = None
        self.ptr = 0

        if self.get_init_freq_from_dds:         # Overide initial frequency
            self.initial_dds_frequency = self.dds1.frequency
            self.dds2.frequency = self.initial_dds_frequency
        self.dds_frequency = self.initial_dds_frequency
        self.bufferLength = 100*self.n_samp_per_cycle
        # self.bufferLength = 2*self.cycle_number*self.n_samp_per_cycle*int(1e8/(2*self.cycle_number*self.n_samp_per_cycle)) #1e8
        self.error_sum_Tab = np.zeros(self.cycle_number,dtype=np.float64) #20 ech
        self.error_diff_Tab = np.zeros(self.cycle_number,dtype=np.float64) #20 ech
        self.powerTab = np.zeros(self.cycle_number,dtype=np.float64) #20 ech
        self.nbSampCropped = int(self.n_samp_per_cycle//8*self.discarded_samples_factor) #(10400/4)*0.1 = 260 #number of removed samples at the beginning of each windows
        # self.P2_True = False
        self.f0 = self.initial_dds_frequency
        self.ls_coeff = 0.

    @property 
    def update_interval(self):
        return self.cycle_number/float(self.freq_mod/4) #

    @property
    def freq_mod(self):
        return 4*self.sampling_rate/float(self.n_samp_per_cycle) # very confusing since lo-fm value becomes here power-fm value

    @freq_mod.setter
    def freq_mod(self,freq_mod):
        self.n_samp_per_cycle = 4*2*int(self.sampling_rate/(2*freq_mod)) # gives now 10416 samples

    def start(self):
        
        P1 = 0.
        P2 = 0.
        # c_cubic = 0.
        # c_quadratic = 0.
        c_cubic = 128.76607699
        c_quadratic = -324.8092184
        AIf1p1ss1 = np.zeros(self.n_samp_per_cycle//8)
        AIf2p1ss1 = np.zeros(self.n_samp_per_cycle//8)
        AIf1p2ss1 = np.zeros(self.n_samp_per_cycle//8)
        AIf2p2ss1 = np.zeros(self.n_samp_per_cycle//8)
        AIf2p1ss2 = np.zeros(self.n_samp_per_cycle//8)
        AIf1p1ss2 = np.zeros(self.n_samp_per_cycle//8)
        AIf2p2ss2 = np.zeros(self.n_samp_per_cycle//8)
        AIf1p2ss2 = np.zeros(self.n_samp_per_cycle//8)

        AIp1_incell = np.zeros(self.n_samp_per_cycle//2)
        AIp2_incell = np.zeros(self.n_samp_per_cycle//2)

        assert not self.running
        self.running = True
        self.initialize()

        print("Starting...")
        print ("LO Modulation frequency: {}".format(self.freq_mod)) 
        print("DDS Update Intervall: {}".format(self.update_interval))
        print("Number of samples per cycle: {}".format(self.n_samp_per_cycle))
        print("N samples removed at each new detection window: {}".format(self.nbSampCropped))
        print("Samples per new LO frequency",self.n_samp_per_cycle/8)
        
        #---------------------------------------------------------------------------#
        def EveryNCallback(taskHandle, everyNsamplesEventType, nSamples, callbackData):
            t1 = time.clock()
            readAI = ctypes.c_int32()
            DAQmxReadAnalogF64(self.detectTaskHandle,self.n_samp_per_cycle,self.timeout,DAQmx_Val_GroupByChannel,self.AI_data,self.n_samp_per_cycle*self._numAIChan,ctypes.byref(readAI),None)

            self.ptr = (self.ptr + 1) % self.cycle_number
            #self.pdcpt_data = np.roll(self.pdcpt_data, -1)
            self.pdcpt_data, self.pdincell_data = self.AI_data.reshape(self._numAIChan,self.n_samp_per_cycle)

            #Process signal data
            #Mesure AI in cell
            AIp1_incell = (np.mean(self.pdincell_data[self.nbSampCropped+1:(self.n_samp_per_cycle//4)+1]) + np.mean(self.pdincell_data[(self.n_samp_per_cycle//2) + self.nbSampCropped+1:(3*self.n_samp_per_cycle//4)+1]))*0.5
            AIp2_incell = (np.mean(self.pdincell_data[(self.n_samp_per_cycle//4)+ self.nbSampCropped+1:(self.n_samp_per_cycle//2)+1]) + np.mean(self.pdincell_data[(3*self.n_samp_per_cycle//4) + self.nbSampCropped+1:]))*0.5

            #Mesure AI cpt
            AIf1p1ss1 = (self.pdcpt_data[self.nbSampCropped+1:(self.n_samp_per_cycle//8)+1])
            AIf2p1ss1 = (self.pdcpt_data[(self.n_samp_per_cycle//8)+self.nbSampCropped+1:(self.n_samp_per_cycle//4)+1])
            AIf1p2ss1 = (self.pdcpt_data[(self.n_samp_per_cycle//4)+self.nbSampCropped+1:(3*self.n_samp_per_cycle//8)+1])
            AIf2p2ss1 = (self.pdcpt_data[(3*self.n_samp_per_cycle//8)+self.nbSampCropped+1:self.n_samp_per_cycle//2+1])

            AIf2p1ss2 = (self.pdcpt_data[(self.n_samp_per_cycle//2)+self.nbSampCropped+1:(5*self.n_samp_per_cycle//8)+1])
            AIf1p1ss2 = (self.pdcpt_data[(5*self.n_samp_per_cycle//8)+self.nbSampCropped+1:(6*self.n_samp_per_cycle//8)+1])
            AIf2p2ss2 = (self.pdcpt_data[(6*self.n_samp_per_cycle//8)+self.nbSampCropped+1:(7*self.n_samp_per_cycle//8)+1])
            AIf1p2ss2 = np.hstack((self.pdcpt_data[(7*self.n_samp_per_cycle//8)+self.nbSampCropped+1:], self.pdcpt_data[0]))

            mean1 = np.mean(AIf1p1ss1)
            mean2 = np.mean(AIf2p1ss1)
            mean3 = np.mean(AIf1p2ss1)
            mean4 = np.mean(AIf2p2ss1)
            mean5 = np.mean(AIf2p1ss2)
            mean6 = np.mean(AIf1p1ss2)
            mean7 = np.mean(AIf2p2ss2)
            mean8 = np.mean(AIf1p2ss2)

            self.eps_A = AIf2p1ss1-AIf1p1ss1
            self.eps_B = AIf2p2ss1-AIf1p2ss1
            self.eps_C = AIf2p1ss2-AIf1p1ss2
            self.eps_D = AIf2p2ss2-AIf1p2ss2

            self.mean_eps_A = mean2-mean1 
            self.mean_eps_B = mean4-mean3
            self.mean_eps_C = mean5-mean6
            self.mean_eps_D = mean7-mean8

            eps_sum = self.mean_eps_A+self.mean_eps_B+self.mean_eps_C+self.mean_eps_D
            eps_diff = self.mean_eps_A-self.mean_eps_B+self.mean_eps_C-self.mean_eps_D
            
            P1 = AIp1_incell - 0.01341
            P2 = AIp2_incell - 0.01341
            # P1 = AIp1_incell - 0.0137
            # P2 = AIp2_incell - 0.026
            
            self.error_sum_Tab[self.ptr] = min(max(eps_sum,-self.max_error),self.max_error)
            self.error_diff_Tab[self.ptr] = min(max(eps_diff,-self.max_error),self.max_error)
            self.powerTab[self.ptr] = np.mean([mean1,mean2,mean3,mean4,mean5,mean6,mean7,mean8]) # Here we decided to measure an average power!
            if self.ptr == 0:
                meanErrorSum = np.mean(self.error_sum_Tab)
                meanErrorDiff = np.mean(self.error_diff_Tab)
                mean_power_cpt = np.mean(self.powerTab)
                correction_f0 = self.gain_f0*meanErrorSum
                correction_ls_coeff = self.gain_ls_coeff*meanErrorDiff 
                if self.lock:
                    self.f0 += correction_f0
                    self.ls_coeff += correction_ls_coeff 
                    
                    self.dds1.frequency = self.f0+P1*self.ls_coeff+P1**2*c_quadratic+P1**3*c_cubic
                    self.dds2.frequency = self.f0+P2*self.ls_coeff+P2**2*c_quadratic+P2**3*c_cubic
                    
                data = [time.time(),self.f0,self.f0+P1*self.ls_coeff,self.f0+P2*self.ls_coeff,self.f0-self.initial_dds_frequency,self.ls_coeff,self.mean_eps_A,self.mean_eps_B,self.mean_eps_C,self.mean_eps_D,eps_sum,eps_diff,mean_power_cpt, P1, P2] 
                self.buf.append(data)
                self.send_to_consumers(data)
            
            return int(0)       
        #---------------------------------------------------------------------------#

        #---------------------------------------------------------------------------#
        def DoneCallback(taskHandle, status, callbackData):
            self.clearTasks()
            return int(0)
        #---------------------------------------------------------------------------#
        
        #---------------------------------------------------------------------------#
        ##### Create task for clock timer #####
        self.COtaskHandle = TaskHandle()
        DAQmxCreateTask(None, ctypes.byref(self.COtaskHandle))

        ##### Create task for digital outputs #####     
        self.DOtaskHandle = TaskHandle()   
        DAQmxCreateTask(None, ctypes.byref(self.DOtaskHandle))  

        ##### Create task for analog outputs #####     
        self.AOtaskHandle = TaskHandle() 
        DAQmxCreateTask(None, ctypes.byref(self.AOtaskHandle))  

        ##### Create task for detection #####
        self.detectTaskHandle = TaskHandle()
        DAQmxCreateTask(None, ctypes.byref(self.detectTaskHandle))

        ##### Create chans #####
        DAQmxCreateCOPulseChanTime(self.COtaskHandle, self.device+self.gate_clock_in,None,DAQmx_Val_Seconds, DAQmx_Val_Low, 0., self.n_samp_per_cycle/(8*2*self.sampling_rate), self.n_samp_per_cycle/(8*2*self.sampling_rate)) # Genrerate a pulse each time the frequency or power changes
        DAQmxCreateDOChan(self.DOtaskHandle, self.device+self.switch_chan, None, DAQmx_Val_ChanPerLine)
        DAQmxCreateAOVoltageChan(self.AOtaskHandle, ",".join(self.AOChannels), None, self.outRange[0],self.outRange[1], DAQmx_Val_Volts, None)
        DAQmxCreateAIVoltageChan(self.detectTaskHandle, ",".join(self.AIChannels), None, DAQmx_Val_Cfg_Default, self.inRange[0],self.inRange[1], DAQmx_Val_Volts, None)
        
        ##### Config clock timing #####
        DAQmxCfgImplicitTiming(self.COtaskHandle,DAQmx_Val_ContSamps, self.n_samp_per_cycle) # buffer length set to cycle length
        DAQmxCfgSampClkTiming(self.DOtaskHandle,'/'+self.device+self.gate_clock_out, 8*self.sampling_rate/self.n_samp_per_cycle, DAQmx_Val_Rising, DAQmx_Val_ContSamps, 8)
        DAQmxCfgSampClkTiming(self.AOtaskHandle,'/'+self.device+self.gate_clock_out, 8*self.sampling_rate/self.n_samp_per_cycle, DAQmx_Val_Rising, DAQmx_Val_ContSamps, 8)
        DAQmxCfgSampClkTiming(self.detectTaskHandle,None, self.sampling_rate, DAQmx_Val_Rising, DAQmx_Val_ContSamps, self.n_samp_per_cycle)
       
        
        ##### Config start trigger #####
        DAQmxCfgDigEdgeStartTrig(self.AOtaskHandle,self.trigName,DAQmx_Val_Rising)
        DAQmxCfgDigEdgeStartTrig(self.COtaskHandle, self.trigName, DAQmx_Val_Rising)

        ##### Define write arrays #####
        DAQmxWriteDigitalLines(self.DOtaskHandle,8,0,0.0,DAQmx_Val_GroupByChannel,self.switch_data,None,None)
        DAQmxWriteAnalogF64(self.AOtaskHandle, 8, 0, 10, DAQmx_Val_GroupByChannel, 
                                np.hstack([
                                    self.rf_am_aom_out_data,
                                    self.lo_fm_out_data]),
                                None, None)

        
        DAQmxCfgInputBuffer(self.detectTaskHandle, ctypes.c_uint32(self.bufferLength*self._numAIChan))


        #DAQmxCfgInputBuffer(self.detectTaskHandle, ctypes.c_uint32(self.cycle_num_samp*self.numAIChan*80)) #10400*2*80 = 1.664e6

        #---------------------------------------------------------------------------#
              
        
        #---------------------------------------------------------------------------#
        if self.compatibility_mode == 0:
            EveryNCallbackCWRAPPER = ctypes.CFUNCTYPE(ctypes.c_int32,ctypes.c_void_p,ctypes.c_int32,ctypes.c_uint32,ctypes.c_void_p)
        else:
            EveryNCallbackCWRAPPER = ctypes.CFUNCTYPE(ctypes.c_int32,ctypes.c_ulong,ctypes.c_int32,ctypes.c_uint32,ctypes.c_void_p)
        self.everyNCallbackWrapped = EveryNCallbackCWRAPPER(EveryNCallback)
        DAQmxRegisterEveryNSamplesEvent(self.detectTaskHandle,DAQmx_Val_Acquired_Into_Buffer,self.n_samp_per_cycle,0,self.everyNCallbackWrapped,None)
       
        if self.compatibility_mode == 0:
            DoneCallbackCWRAPPER = ctypes.CFUNCTYPE(ctypes.c_int32,ctypes.c_void_p,ctypes.c_int32,ctypes.c_void_p)
        else:
            DoneCallbackCWRAPPER = ctypes.CFUNCTYPE(ctypes.c_int32,ctypes.c_ulong,ctypes.c_int32,ctypes.c_void_p)
        self.doneCallbackWrapped = DoneCallbackCWRAPPER(DoneCallback)
        DAQmxRegisterDoneEvent(self.detectTaskHandle,0,self.doneCallbackWrapped,None)
        #---------------------------------------------------------------------------#


        #---------------------------------------------------------------------------#
        # Gestion des exceptions
        # Charge à essayer en premier
        #try:
        #    DAQmxWriteAnalogF64(self.AOtaskHandle, self.n_samp_per_cycle, 0, 10, DAQmx_Val_GroupByChannel, np.hstack([self.rf_am_aom_out_data,self.lo_fm_out_data]), None, None)
        # Things to execute if error.
        #except PALResourceReservedError as e:
        #    print(e)
        #    self.running = False
        #    return False
        #---------------------------------------------------------------------------#

        #DAQmxWriteAnalogF64(self.AOtaskHandle, self.n_samp_per_cycle, 0, 10, DAQmx_Val_GroupByChannel, 
        #                        np.hstack([
        #                            self.rf_am_aom_out_data,
        #                            self.lo_fm_out_data]),
        #                        None, None)

        DAQmxStartTask(self.AOtaskHandle)
        DAQmxStartTask(self.DOtaskHandle)
        DAQmxStartTask(self.COtaskHandle)
        DAQmxStartTask(self.detectTaskHandle)
        print("Starting asserv")
        return True

    def stop(self):
        if self.running:
            self.clearTasks()
            self.setZero()
            self.running = False

    def clearTasks(self):
        if self.detectTaskHandle:
            DAQmxStopTask(self.detectTaskHandle)
            DAQmxClearTask(self.detectTaskHandle)
            self.detectTaskHandle = None
        if self.AOtaskHandle:
            DAQmxStopTask(self.AOtaskHandle)
            DAQmxClearTask(self.AOtaskHandle)
            self.AOtaskHandle = None
        if self.DOtaskHandle:
            DAQmxStopTask(self.DOtaskHandle)
            DAQmxClearTask(self.DOtaskHandle)
            self.DOtaskHandle = None
        if self.COtaskHandle:
            DAQmxStopTask(self.COtaskHandle)
            DAQmxClearTask(self.COtaskHandle)

    def setZero(self):
        print("Setting output to 0 V")
        clearAOtaskHandle = TaskHandle()
        DAQmxCreateTask("", ctypes.byref(clearAOtaskHandle))
        DAQmxCreateAOVoltageChan(clearAOtaskHandle, ",".join(self.AOChannels), None, self.outRange[0],self.outRange[1], DAQmx_Val_Volts, None)
        try:
            DAQmxWriteAnalogF64(clearAOtaskHandle,1,1,self.timeout,DAQmx_Val_GroupByChannel,np.array([0.]),None,None)
        except PALResourceReservedError as e:
            print(e)
            return       
        DAQmxStartTask(clearAOtaskHandle)
        DAQmxClearTask(clearAOtaskHandle)

        clearDOtaskHandle = TaskHandle()
        DAQmxCreateTask("", ctypes.byref(clearDOtaskHandle))
        DAQmxCreateDOChan(clearDOtaskHandle, self.device+self.switch_chan, None, DAQmx_Val_ChanPerLine)
        try:
            DAQmxStartTask(clearDOtaskHandle)
            DAQmxWriteDigitalLines(clearDOtaskHandle, 1, 1, 10, DAQmx_Val_GroupByChannel, np.array([0.], np.uint8), None, None)
        except PALResourceReservedError as e:
            print(e)
            return       
        DAQmxClearTask(clearDOtaskHandle)
    


    def __del__(self):
        self.stop()