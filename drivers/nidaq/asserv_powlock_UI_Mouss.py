from asserv_powlock_Mouss import AsservPowLock_Mouss
import pyqtgraph as pg
from pyqtgraph.Qt import QtGui, QtCore
import pyqtgraph.parametertree.parameterTypes as pTypes
from pyqtgraph.parametertree import Parameter, ParameterTree
from pyqtgraph.dockarea import *

class AsservPowLock_UI_Mouss(AsservPowLock_Mouss):

	timer = None
	param_group_name = "Asserv. CW"

	def __init__(self,**kwargs):
		super(AsservPowLock_UI_Mouss, self).__init__(**kwargs)
		self.param_group = pTypes.GroupParameter(name = self.param_group_name)
		self.freq_lock_param = Parameter.create(name='Freq. Lock',
											type='bool',
											value=self.freq_lock)
		self.rf_pow_lock_param = Parameter.create(name='RF Pow. Lock',
											type='bool',
											value=self.rf_pow_lock)
		self.freq_gain_param = Parameter.create(name='Freq. Gain',
											type='float',
											value=self.freq_gain,
											step= 10)
		self.rf_pow_gain_param = Parameter.create(name='RF. Pow. Gain',
											type='float',
											value=self.rf_pow_gain,
											step= 0.1)
		self.sampling_rate_param = Parameter.create(name='Sampling rate',
											type='int',
											value=self.sampling_rate,
											step= 100000,
											siPrefix=True,
											suffix='Samp/s')
		self.freq_mod_param = Parameter.create(name='Mod. frequency',
											type='float',
											value=self.freq_mod,
											step= 1,
											siPrefix=True,
											suffix='Hz')
		self.cycle_number_param = Parameter.create(name='N cycle',
											type='int',
											value=self.cycle_number,
											step=1)
		# self.discarded_samples_factor_param = Parameter.create(name='Ratio discarded samp.',
		# 									type='float',
		# 									value= self.discarded_samples_factor,
		# 									step=0.05)
		# Read only parameters
		self.n_samp_per_cycle_param = Parameter.create(name='N samp/cycle',
											type='int',
											value=self.n_samp_per_cycle,
											readonly=True)
		self.act_freq_mod_param = Parameter.create(name='Actual Mod frequency',
											type='float',
											value=self.freq_mod,
											siPrefix=True,
											suffix='Hz',
											readonly=True)
		self.update_interval_param = Parameter.create(name='Update interval',
											type='float',
											value=self.update_interval,
											siPrefix=True,
											suffix='s',
											readonly=True)
		self.param_group.addChildren([self.freq_lock_param,
									self.rf_pow_lock_param,
									self.freq_gain_param,
									self.rf_pow_gain_param,
									self.sampling_rate_param,
									self.freq_mod_param,
									self.cycle_number_param,
									# self.discarded_samples_factor_param,
									self.n_samp_per_cycle_param,
									self.act_freq_mod_param,
									self.update_interval_param])

		self.param_group.sigTreeStateChanged.connect(self.change)

	def change(self,param,changes):
		for param, change, data in changes:
			if param is self.freq_lock_param: self.freq_lock = param.value()
			elif param is self.rf_pow_lock_param: self.rf_pow_lock = param.value()
			elif param is self.freq_gain_param: self.freq_gain = param.value()
			elif param is self.rf_pow_gain_param: self.rf_pow_gain = param.value()
			elif param is self.sampling_rate_param:	self.sampling_rate = param.value()				
			elif param is self.freq_mod_param: self.freq_mod = param.value()
			elif param is self.cycle_number_param: self.cycle_number = param.value()
			# elif param is self.discarded_samples_factor_param: self.discarded_samples_factor = param.value()
			self.update_readonly_params()

	def update_readonly_params(self):
		self.act_freq_mod_param.setValue(self.freq_mod)
		self.n_samp_per_cycle_param.setValue(self.n_samp_per_cycle)
		self.update_interval_param.setValue(self.update_interval)

	def start(self):
		self.param_group.setName(self.param_group_name+" (Active)")
		if self.timer is not None: self.timer.start()
		super(AsservPowLock_UI_Mouss, self).start()

	def stop(self):
		self.param_group.setName(self.param_group_name)
		if self.timer is not None: self.timer.stop()
		super(AsservPowLock_UI_Mouss, self).stop()

	def makeDock(self):
		self.dock = Dock("Asserv signal", size=(800, 1))
		self.gw = pg.GraphicsLayoutWidget()
		self.p = self.gw.addPlot()
		self.curve = self.p.plot(pen=pg.mkPen((0, 255, 255,60)))
		self.avg_curve = self.p.plot(pen=pg.mkPen((0, 255, 60,255)))
		self.timer = QtCore.QTimer()
		self.timer.timeout.connect(self.update)
		self.dock.addWidget(self.gw)
		return self.dock

	def update(self):
		self.curve.setData(self.AIdata)
		self.avg_curve.setData(self.pow_mod_data)