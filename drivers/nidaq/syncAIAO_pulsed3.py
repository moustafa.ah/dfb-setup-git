from PyDAQmx import *
import numpy as np
import ctypes, time
from pyqtgraph.Qt import QtGui, QtCore
import pyqtgraph as pg
from collections import deque

class SyncAIAO_pulsed(object):

	# UI accessible settings
	Tbright = .0004 # s
	Tdark = .0002 # s
	Tdead = .003 # s
	Tdiscarded = .00001 # s
	Taverage = 	 .00001 # s

	compatibility_mode = 0          # Set this to 1 on some PC (Mouss)
	trigName = "ai/StartTrigger"
	timeout = 10.0
	# mean = 64
	sampling_rate = 5e5
	
	# ramp_frequency = 0.1 # Hz
	numfreqs=500
	vpp=1.
	offset = 0.
	n_cycle = 2

	# numSamp = None
	# nbSampCroppedFactor=0.5
	
	# queue =

	def __init__(self,device="Dev3",outChan="ao3", DOChan = 'port0/line0',inChan="ai0",auxChan='ai1',inRange=(-10.,10.),outRange=(-10.,10.)):
		self.device = device		
		self.outChan = outChan
		self.DOChan = DOChan
		self.inChan = inChan
		self.auxChan = auxChan
		self.inRange = inRange
		self.outRange = outRange
		self.running = False
		self.calculate_params()
		self.initialize()

	def calculate_params(self) :
		#Nb echantillons pour chaque partie de la sequence
		self.TbrightLength = int(self.Tbright*self.sampling_rate) #pulse
		self.TdarkLength = int(self.Tdark*self.sampling_rate) #dark time
		self.TdiscardedLength = int(self.Tdiscarded * self.sampling_rate) #delay before detection
		self.TaverageLength = int(self.Taverage *self.sampling_rate) # detection window for signal measurement
		self.TdeadLength = int(self.Tdead*self.sampling_rate) #dead time between each new Ramsey cycle to reset atomic state (useful for Ramsey spectroscopy)
		self.TdetectionLength = self.TdiscardedLength + self.TaverageLength # delay + detection window ensemble
		self.TworkLength = self.TbrightLength + self.TdarkLength + self.TdetectionLength # pukse + dark tile + delay before detection + detection window
		self.TcycleLength = self.TworkLength + self.TdeadLength # total sequence = Ramsey cycle + deadtime
		self.scan_duration = self.TcycleLength*self.n_cycle*self.numfreqs/self.sampling_rate  
		#print ("Tbrightlength =", self.Tbrightlength)
		#print ("Tdarklength =", self.Tdarklength)
		#print ("TdetectionLength =", self.TdetectionLength)
		#print ("TcycleLength =", self.TcycleLength)

	def initialize(self):
		# self.numSamp = self.numfreqs*self.n_cycle*self.TcycleLength
		# Tableau final correspondant a une rampe en frequence
		self.AImean = np.zeros(self.numfreqs,dtype=np.float64) 
		self.last_AImean = np.zeros(self.numfreqs,dtype=np.float64) 
		self.AIdata = np.zeros((2,self.TcycleLength*self.n_cycle))
		


		# Waveform envoyee pour pulser la lumiere 
		#---Vincent---#
		# self.DOdata = np.ones(int(self.TcycleLength/10), dtype = np.uint8)
		# self.DOdata[0:int(self.TbrightLength/10)] = 0
		# self.DOdata[int((self.TbrightLength+self.TdarkLength)/10) : int(self.TworkLength/10)] = 0
		self.DOdata = np.ones(int(self.TcycleLength), dtype = np.uint8)
		self.DOdata[0:int(self.TbrightLength)] = 0
		self.DOdata[int((self.TbrightLength+self.TdarkLength)) : int(self.TworkLength)] = 0
		# print (self.DOdata)
		
		#-------------#
		# Waveform moyennage
		self.average_mask = np.zeros(self.TcycleLength, dtype =bool)
		self.average_mask[self.TworkLength-self.TaverageLength : self.TworkLength] = 1
		self.average_mask = np.tile(self.average_mask, self.n_cycle)
		# Waveform DDS
		self.AOdata = np.linspace(-self.vpp/2.,self.vpp/2.,self.numfreqs)
		
		


		# # self.nbSampCropped = int(self.nbSampCroppedFactor * self.numSamp)
		# self.AImean = np.zeros(self.numSamp,dtype=np.float64) # Tableau final correspondant a une rampe en frequence
		# self.DOdata = np.zeros(self.TcycleLength, dtype = np.uint8)
		# self.DOdata[0:self.TbrightLength] = 1 # Waveform envoyee pour pulser la lumiere
		# self.AIdata_N_cycles = np.zeros(self.TcycleLength * self.N_cycles, dtype = np.float64)
		# self.mask = np.zeros(self.TcycleLength * self.N_cycles, dtype = bool)
		# for i in np.arange(1, N_cycles) :
		# 	self.mask[i*TcycleLength+TdiscardedLength : i*TcycleLength+TdiscardedLength + TaverageLength] = True
		# # self.deque = deque([],self.mean)
		# self.AOdata = self.offset + np.hstack([np.linspace(-self.vpp/2.,self.vpp/2.,self.numSamp/2,dtype=np.float64,endpoint=False),
		#     np.linspace(self.vpp/2.,-self.vpp/2.,self.numSamp/2,dtype=np.float64,endpoint=False)])
		# self.totalAI=0
		


		self.COtaskHandle = None
		self.AItaskHandle = None
		self.AOtaskHandle = None
		self.DOtaskHandle = None


	def makeInputStr(self):			
		return self.device+"/"+self.inChan+','+self.device+"/"+self.auxChan
	def makeOutputStr(self):
		return self.device+"/"+self.outChan
	def makeDOStr(self):
		return self.device+"/"+self.DOChan


	def start(self):	
		assert not self.running
		self.running = True
		self.initialize()
		self.ptr = 0
		readAI = ctypes.c_int32()
		def EveryNCallback(taskHandle, everyNsamplesEventType, nSamples, callbackData):
			# global AItaskHandle, totalAI, AIdata, ptr
			# readAI = ctypes.c_int32()
			# self.ptr=(self.ptr+1)%self.mean
			# self.deque.append(self.ptr)
			if self.ptr ==0 :
				self.last_AImean = self.AImean
			DAQmxReadAnalogF64(self.AItaskHandle,self.TcycleLength * self.n_cycle,self.timeout,DAQmx_Val_GroupByChannel,self.AIdata.ravel(),self.TcycleLength * self.n_cycle * 2,ctypes.byref(readAI),None)

			self.AImean[self.ptr] = np.mean(self.AIdata[0][self.average_mask])
			# if self.ptr == 0:
				# print(np.mean(self.AIdata[1]))
			# self.totalAI = self.totalAI + readAI.value
			# self.counter=self.counter+1
			# print self.totalAI
			self.ptr = (self.ptr+1)%self.numfreqs
			return int(0)
			

		def DoneCallback(taskHandle, status, callbackData):
			self.clearTasks()
			return int(0)

		self.COtaskHandle = TaskHandle()
		self.DOtaskHandle = TaskHandle()
		self.AItaskHandle = TaskHandle()
		self.AOtaskHandle = TaskHandle()
		# self.totalAI=0

		DAQmxCreateTask(None, ctypes.byref(self.DOtaskHandle))
		DAQmxCreateTask(None,ctypes.byref(self.AItaskHandle))
		DAQmxCreateTask(None, ctypes.byref(self.COtaskHandle))

		DAQmxCreateCOPulseChanTime(self.COtaskHandle, self.device+'/Ctr0',None,DAQmx_Val_Seconds, DAQmx_Val_High, 0, 5E-6,5E-6)
		DAQmxCreateDOChan(self.DOtaskHandle, self.makeDOStr(), None, DAQmx_Val_ChanPerLine)
		DAQmxCreateAIVoltageChan(self.AItaskHandle,self.makeInputStr(), None, DAQmx_Val_Cfg_Default, self.inRange[0],self.inRange[1], DAQmx_Val_Volts, None)
		DAQmxCfgInputBuffer(self.AItaskHandle, 10 * self.TcycleLength * self.n_cycle)
		DAQmxCfgSampClkTiming(self.AItaskHandle,None, self.sampling_rate, DAQmx_Val_Rising, DAQmx_Val_ContSamps, self.TcycleLength*self.n_cycle)
		DAQmxCreateTask(None,ctypes.byref(self.AOtaskHandle))
		DAQmxCreateAOVoltageChan(self.AOtaskHandle,self.makeOutputStr(),None,self.outRange[0],self.outRange[1],DAQmx_Val_Volts,None)
		DAQmxCfgSampClkTiming(self.DOtaskHandle, "ai/SampleClock", self.sampling_rate, DAQmx_Val_Rising, DAQmx_Val_ContSamps, self.TcycleLength)
		DAQmxCfgSampClkTiming(self.AOtaskHandle, None, self.sampling_rate/(self.TcycleLength*self.n_cycle),DAQmx_Val_Rising,DAQmx_Val_ContSamps,self.numfreqs)
		DAQmxCfgImplicitTiming(self.COtaskHandle,DAQmx_Val_ContSamps,self.TcycleLength)
		DAQmxCfgDigEdgeStartTrig(self.AOtaskHandle,self.trigName,DAQmx_Val_Rising)
		DAQmxCfgDigEdgeStartTrig(self.COtaskHandle,self.trigName,DAQmx_Val_Rising)

		if self.compatibility_mode == 0:
			EveryNCallbackCWRAPPER = ctypes.CFUNCTYPE(ctypes.c_int32,ctypes.c_void_p,ctypes.c_int32,ctypes.c_uint32,ctypes.c_void_p)
		else:
			EveryNCallbackCWRAPPER = ctypes.CFUNCTYPE(ctypes.c_int32,ctypes.c_ulong,ctypes.c_int32,ctypes.c_uint32,ctypes.c_void_p)
		self.everyNCallbackWrapped = EveryNCallbackCWRAPPER(EveryNCallback)
		DAQmxRegisterEveryNSamplesEvent(self.AItaskHandle,DAQmx_Val_Acquired_Into_Buffer,self.TcycleLength*self.n_cycle,0,self.everyNCallbackWrapped,None)

		if self.compatibility_mode == 0:
			DoneCallbackCWRAPPER = ctypes.CFUNCTYPE(ctypes.c_int32,ctypes.c_void_p,ctypes.c_int32,ctypes.c_void_p)
		else:
			DoneCallbackCWRAPPER = ctypes.CFUNCTYPE(ctypes.c_int32,ctypes.c_ulong,ctypes.c_int32,ctypes.c_void_p)
		self.doneCallbackWrapped = DoneCallbackCWRAPPER(DoneCallback)
		DAQmxRegisterDoneEvent(self.AItaskHandle,0,self.doneCallbackWrapped,None) 

		# DAQmxWriteDigitalLines(self.DOtaskHandle, self.n_cycle*self.TcycleLength, 0, self.timeout, DAQmx_Val_GroupByChannel, self.DOdata, None, None)
		#DAQmxWriteDigitalLines(self.DOtaskHandle,int(self.TcycleLength/10),0,0.0,DAQmx_Val_GroupByChannel,self.DOdata,None,None)
		DAQmxWriteDigitalLines(self.DOtaskHandle,int(self.TcycleLength),0,0.0,DAQmx_Val_GroupByChannel,self.DOdata,None,None)
		#print("Write error: ", DAQmxWriteDigitalLines(self.DOtaskHandle, 2,0,0,DAQmx_Val_GroupByChannel,np.array([1,1], dtype = np.uint8),None,None))
		DAQmxWriteAnalogF64(self.AOtaskHandle, self.numfreqs, 0, self.timeout, DAQmx_Val_GroupByChannel, self.AOdata, None, None)

		DAQmxStartTask(self.COtaskHandle)
		DAQmxStartTask(self.DOtaskHandle)
		DAQmxStartTask(self.AOtaskHandle)
		DAQmxStartTask(self.AItaskHandle)
		print("Starting acquisition")

	def clearTasks(self):
		if self.AItaskHandle:
			DAQmxStopTask(self.AItaskHandle)
			DAQmxClearTask(self.AItaskHandle)
			self.AItaskHandle = None
		if self.AOtaskHandle:
			DAQmxStopTask(self.AOtaskHandle)
			DAQmxClearTask(self.AOtaskHandle)
			self.AOtaskHandle = None
		if self.COtaskHandle:
			DAQmxStopTask(self.COtaskHandle)
			DAQmxClearTask(self.COtaskHandle)
			self.COtaskHandle = None
		if self.DOtaskHandle :
			DAQmxStopTask(self.DOtaskHandle)
			DAQmxClearTask(self.DOtaskHandle)
			self.DOtaskHandle = None

	def stop(self):
		if self.running:
			self.clearTasks()
			self.setZero()
			self.running = False

	def setZero(self):
		print("Setting output to 0 V")
		clearAOTaskHandle = TaskHandle()
		DAQmxCreateTask("", ctypes.byref(clearAOTaskHandle))
		DAQmxCreateAOVoltageChan(clearAOTaskHandle, self.makeOutputStr(), None, self.outRange[0],self.outRange[1], DAQmx_Val_Volts, None)
		DAQmxWriteAnalogF64(clearAOTaskHandle,1,1,self.timeout,DAQmx_Val_GroupByChannel,np.array([0.]),None,None)
		DAQmxStartTask(clearAOTaskHandle)
		DAQmxClearTask(clearAOTaskHandle)
		clearDOTaskHandle = TaskHandle()
		DAQmxCreateTask(None, ctypes.byref(clearDOTaskHandle))
		DAQmxCreateDOChan(clearDOTaskHandle, self.makeDOStr(), None, DAQmx_Val_ChanPerLine)
		DAQmxStartTask(clearDOTaskHandle)
		DAQmxWriteDigitalLines(clearDOTaskHandle, 1, 1, 10, DAQmx_Val_GroupByChannel, np.array([0.], np.uint8), None, None)
		DAQmxClearTask(clearDOTaskHandle)


	def __del__(self):
		self.stop()

if __name__=="__main__":

	app = QtGui.QApplication([])
	win = pg.GraphicsWindow()
	win.resize(1000,600)
	win.setWindowTitle('Pyqtgraph : Live NIDAQmx data')
	pg.setConfigOptions(antialias=True)
	outChan="ao3"
	inChan="ai7"
	syncAiAo = SyncAIAO_pulsed(device = "Dev1", inChan=inChan,outChan=outChan)
	p = win.addPlot(title="Live plot")
	p.addLegend()

	curve = p.plot(pen='c',name='None')

	def update():
		x, y = syncAiAo.AOdata, syncAiAo.AImean
		curve.setData(x=x, y=y)

	timer = QtCore.QTimer()
	timer.timeout.connect(update)
	timer.start(100)

	syncAiAo.start()

	import sys 
	if (sys.flags.interactive != 1) or not hasattr(QtCore, 'PYQT_VERSION'):
		ret = QtGui.QApplication.instance().exec_()
		print("Closing")
		syncAiAo.stop()
		sys.exit(ret)
