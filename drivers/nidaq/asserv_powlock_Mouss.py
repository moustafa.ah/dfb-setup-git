from PyDAQmx import *
import numpy as np
import ctypes, time, Queue
from utils.misc import Buffer

class AsservPowLock_Mouss(object):

    # UI accessible settings
    freq_lock = True
    rf_pow_lock = False
    freq_gain = 100
    rf_pow_gain = 0.0001
    cycle_number = 20
    cycle_number2 = 200
    default_freq_mod = 192
    n_samp_per_cycle = 5200
    sampling_rate = 1e6
    discarded_samples_factor = 0.35
    pow_discarded_samples_factor = 0.99
    # Fixed settings
    compatibility_mode = 0          # Set this to 1 on some PC (Mouss)
    trigName = "ai/StartTrigger"
    timeout = 10.0
    freq_mod_amplitude = 0.5
    pow_mod_amplitude = 0.5
    inRange = (-1.,1.)
    freq_mod_out_range = (-1.0,1.0)
    pow_mod_out_range = (-1.0,1.0)
    running = False
    header = ["Time","DAQ time (s)","Frequency (Hz)","Correction (Hz)","Error","Mean signal (V)","RF Pow Error","RF pow (dBm)"]

    def __init__(self, device="Dev1",rf_fm_out_chan="ao3",inChan="ai0",aom_am_out_chan="ao1",dds_device=None):
    
        self.device = device
        self.rf_fm_out_chan = rf_fm_out_chan
        self.aom_am_out_chan = aom_am_out_chan
        self.inChan = inChan
        self.dds = dds_device

        self.dataQueue = None

        self.freq_mod = self.default_freq_mod

    def initialize(self):
        
        x= 0.01
        self.AIdata = np.zeros(self.n_samp_per_cycle,dtype=np.float64)
        self.mean_buffer = Buffer(20)
        self.freq_mod_pattern = np.repeat([-1,1,-1,1], self.n_samp_per_cycle/4)
        self.pow_mod_pattern = np.repeat([-1,1,1,-1], self.n_samp_per_cycle/4)
        self.pow_mod_pattern_2 = np.ones(self.n_samp_per_cycle)
        self.pow_mod_pattern_2[0:0.01*self.n_samp_per_cycle/4] = -0.05
        self.pow_mod_pattern_2[0.01*self.n_samp_per_cycle/4:self.n_samp_per_cycle/4] = -0.2
        self.pow_mod_pattern_2[self.n_samp_per_cycle/4:self.n_samp_per_cycle/4 + 0.01*self.n_samp_per_cycle/4] = -0.05
        self.pow_mod_pattern_2[self.n_samp_per_cycle/4 + 0.01*self.n_samp_per_cycle/4:self.n_samp_per_cycle/2] = 0.2
        self.pow_mod_pattern_2[self.n_samp_per_cycle/2:self.n_samp_per_cycle/2 + 0.01*self.n_samp_per_cycle/4] = -0.05
        self.pow_mod_pattern_2[self.n_samp_per_cycle/2 + 0.01*self.n_samp_per_cycle/4:3*self.n_samp_per_cycle/4] = 0.2
        self.pow_mod_pattern_2[3*self.n_samp_per_cycle/4:3*self.n_samp_per_cycle/4 + 0.01*self.n_samp_per_cycle/4] = -0.05
        self.pow_mod_pattern_2[3*self.n_samp_per_cycle/4 + 0.01*self.n_samp_per_cycle/4:self.n_samp_per_cycle] = -0.2
        avg_win_pattern = np.ones(self.n_samp_per_cycle/4)
        pow_avg_win_pattern = np.ones(self.n_samp_per_cycle/4)
        avg_win_pattern[0:len(avg_win_pattern)*self.discarded_samples_factor] = 0.
        pow_avg_win_pattern[0:len(avg_win_pattern)*self.pow_discarded_samples_factor] = 0.
        self.averaging_window = np.tile(avg_win_pattern ,4)
        self.pow_averaging_window = np.tile(pow_avg_win_pattern ,4)
        # self.signal_mean_pattern = (1 - self.pow_mod_pattern)
        self.freq_detect_mask = self.averaging_window * self.freq_mod_pattern
        self.pow_detect_mask = self.pow_averaging_window * self.pow_mod_pattern * self.freq_mod_pattern

        # self.freq_mod_data = self.amplitude * np.hstack([ -1*np.ones(self.n_samp_per_cycle/2),
        #                              1*np.ones(self.n_samp_per_cycle/2)])
        self.freq_mod_data = self.freq_mod_amplitude * self.freq_mod_pattern
        self.pow_mod_data = self.pow_mod_amplitude * self.pow_mod_pattern_2
        self.AItaskHandle = None
        self.FreqModtaskHandle = None
        # self.PowModtaskHandle = None
        self.ptr = 0
        self.ptr2 = 0
        self.ctr = 0
        self.meanPowError = 0

        self.rf_pow = self.dds.power_amplitude
        self.dds_frequency = self.initial_dds_frequency = self.dds.frequency
        self.bufferLength = 2*self.cycle_number*self.n_samp_per_cycle*int(1e8/(2*self.cycle_number*self.n_samp_per_cycle))
        self.errorTab = np.zeros(self.cycle_number,dtype=np.float64)
        self.pow_errorTab = np.zeros(self.cycle_number2,dtype=np.float64)
        self.powerTab = np.zeros(self.cycle_number,dtype=np.float64)
        # self.nbSampCropped = int(self.n_samp_per_cycle/2*self.discarded_samples_factor)

    @property 
    def update_interval(self):
        return self.cycle_number/float(self.freq_mod)

    @property
    def freq_mod(self):
        return self.sampling_rate/float(self.n_samp_per_cycle)

    @freq_mod.setter
    def freq_mod(self,freq_mod):
        self.n_samp_per_cycle = 2*int(self.sampling_rate/(2*freq_mod))

    def start(self):

        assert not self.running
        self.running = True
        self.initialize()

        print ("Modulation frequency: {}".format(self.freq_mod)) 
        print("DDS Update Intervall: {}".format(self.update_interval))
        print("Number of samples per cycle: {}".format(self.n_samp_per_cycle))

        def EveryNCallback(taskHandle, everyNsamplesEventType, nSamples, callbackData):
            readAI = c_int32()
            DAQmxReadAnalogF64(self.AItaskHandle,self.n_samp_per_cycle,self.timeout,DAQmx_Val_GroupByChannel,self.AIdata,self.n_samp_per_cycle,byref(readAI),None)
            self.ptr = (self.ptr + 1) % self.cycle_number
            self.ptr2 = (self.ptr2 + 1) % self.cycle_number2
            self.AIdata = np.roll(self.AIdata, -1)
            error = np.sum(self.freq_detect_mask * self.AIdata)
            signal = np.mean(self.AIdata)
            pow_error = 100000*np.sum(self.pow_detect_mask * self.AIdata)
            # mean_over_resonance = np.mean(self.AIdata[self.nbSampCropped:self.n_samp_per_cycle/2])
            # mean_below_resonance = np.mean(self.AIdata[self.n_samp_per_cycle/2+self.nbSampCropped:self.n_samp_per_cycle])
            # error = mean_below_resonance - mean_over_resonance
            # signal = (mean_over_resonance + mean_below_resonance)/2
            self.errorTab[self.ptr] = error
            self.pow_errorTab[self.ptr2] = pow_error
            self.powerTab[self.ptr] = signal
            if self.ptr == 0:
                self.ctr += 1
                daqTime = self.ctr * self.cycle_number*self.n_samp_per_cycle/float(self.sampling_rate)
                meanError = np.mean(self.errorTab)
                # meanPowError = np.mean(self.pow_errorTab)
                self.meanPowError = np.mean(self.pow_errorTab)
                if self.freq_lock:
                    self.dds_frequency += self.freq_gain*meanError 
                    self.dds.frequency = self.dds_frequency
                else: self.dds_frequency = self.dds.frequency
                if self.rf_pow_lock:
                    self.rf_pow += self.rf_pow_gain*self.meanPowError
                    self.dds.power_amplitude = self.rf_pow
                else: self.rf_pow = self.dds.power_amplitude
                mean_power = np.mean(self.powerTab)
                self.mean_buffer.append(mean_power)
                if self.dataQueue:
                    self.dataQueue.put([time.time(),daqTime,self.dds_frequency,self.dds_frequency-self.initial_dds_frequency,meanError,mean_power,self.meanPowError,self.rf_pow])                
            return int(0)       

        def DoneCallback(taskHandle, status, callbackData):
            self.clearTasks()
            return int(0)

        self.AItaskHandle = TaskHandle()
        self.FreqModtaskHandle = TaskHandle()

        DAQmxCreateTask(None,byref(self.AItaskHandle))
        DAQmxCreateAIVoltageChan(self.AItaskHandle,self.device + '/' + self.inChan, None, DAQmx_Val_Cfg_Default, self.inRange[0],self.inRange[1], DAQmx_Val_Volts, None)
        DAQmxCfgSampClkTiming(self.AItaskHandle,None, self.sampling_rate, DAQmx_Val_Rising, DAQmx_Val_ContSamps, self.n_samp_per_cycle)

        DAQmxCreateTask(None,byref(self.FreqModtaskHandle))
        chans = ",".join([self.device + '/' + self.rf_fm_out_chan , self.device + '/' + self.aom_am_out_chan])
        DAQmxCreateAOVoltageChan(self.FreqModtaskHandle,chans,None,self.freq_mod_out_range[0],self.freq_mod_out_range[1],DAQmx_Val_Volts,None)
        DAQmxCfgSampClkTiming(self.FreqModtaskHandle,None,self.sampling_rate,DAQmx_Val_Rising,DAQmx_Val_ContSamps,self.n_samp_per_cycle)
        DAQmxCfgDigEdgeStartTrig(self.FreqModtaskHandle,self.trigName,DAQmx_Val_Rising)

        DAQmxCfgInputBuffer(self.AItaskHandle, c_uint32(self.bufferLength))

        if self.compatibility_mode == 0:
            EveryNCallbackCWRAPPER = ctypes.CFUNCTYPE(ctypes.c_int32,ctypes.c_void_p,ctypes.c_int32,ctypes.c_uint32,ctypes.c_void_p)
        else:
            EveryNCallbackCWRAPPER = ctypes.CFUNCTYPE(ctypes.c_int32,ctypes.c_ulong,ctypes.c_int32,ctypes.c_uint32,ctypes.c_void_p)
        self.everyNCallbackWrapped = EveryNCallbackCWRAPPER(EveryNCallback)
        DAQmxRegisterEveryNSamplesEvent(self.AItaskHandle,DAQmx_Val_Acquired_Into_Buffer,self.n_samp_per_cycle,0,self.everyNCallbackWrapped,None)
       
        if self.compatibility_mode == 0:
            DoneCallbackCWRAPPER = ctypes.CFUNCTYPE(ctypes.c_int32,ctypes.c_void_p,ctypes.c_int32,ctypes.c_void_p)
        else:
            DoneCallbackCWRAPPER = ctypes.CFUNCTYPE(ctypes.c_int32,ctypes.c_ulong,ctypes.c_int32,ctypes.c_void_p)
        self.doneCallbackWrapped = DoneCallbackCWRAPPER(DoneCallback)
        DAQmxRegisterDoneEvent(self.AItaskHandle,0,self.doneCallbackWrapped,None)

        DAQmxWriteAnalogF64(self.FreqModtaskHandle, self.n_samp_per_cycle, 0, self.timeout, DAQmx_Val_GroupByChannel, np.hstack([self.freq_mod_data,self.pow_mod_data]), None, None)

        DAQmxStartTask(self.FreqModtaskHandle)
        DAQmxStartTask(self.AItaskHandle)
        print "Starting asserv"

    def clearTasks(self):
        if self.AItaskHandle:
            DAQmxStopTask(self.AItaskHandle)
            DAQmxClearTask(self.AItaskHandle)
            self.AItaskHandle = None
        if self.FreqModtaskHandle:
            DAQmxStopTask(self.FreqModtaskHandle)
            DAQmxClearTask(self.FreqModtaskHandle)
            self.FreqModtaskHandle = None     

    def stop(self):
        if self.running:
            self.clearTasks()
            self.setZero(self.rf_fm_out_chan)
            self.setZero(self.aom_am_out_chan)
            self.running = False

    def setZero(self,chan):
        print "Setting output to 0 V"
        clearTaskHandle = TaskHandle()
        DAQmxCreateTask("", byref(clearTaskHandle))
        DAQmxCreateAOVoltageChan(clearTaskHandle, self.device + '/' + chan, None, -1.,1., DAQmx_Val_Volts, None)
        DAQmxWriteAnalogF64(clearTaskHandle,1,1,self.timeout,DAQmx_Val_GroupByChannel,np.array([0.]),None,None)
        DAQmxStartTask(clearTaskHandle)
        DAQmxClearTask(clearTaskHandle)

    def __del__(self):
        self.stop()