from PyDAQmx import *
import numpy as np
import ctypes, time, Queue
from utils.misc import Buffer

class Asserv_pulsed(object):

	Tbright = .0011 # s
	Tdark = .0027 # s
	Tdiscarded = .00002 # s
	Taverage = .000050 # s
	n_cycle = 6
	sampling_rate = 1e6
	compatibility_mode = 0          # Set this to 1 on some PC (Mouss)
	trigName = "ai/StartTrigger"
	timeout = 10.0
	initial_dds_frequency = 7358910
	get_init_freq_from_dds = True   # Set to False to use initial_dds_frequency as first frequency
	frequencyLock = True
	powerLock = False
	initial_CPT_transmission = 0
	gain = 40000
	powergain = 0.1
	log_ratio = 1
	amplitude = 0.5
	inRange = (-1.,1.)
	outRange = (-0.5,0.5)
	running = False
	header = ["Time","DAQ time (s)","Frequency (Hz)","Correction (Hz)","Error","Mean signal (V)"]
	waveform = 'square'
	phase = 0


	def __init__(self, device="Dev1",outChan="ao3",inChan="ai0",dds_device=None, power_lock_device = None):
		self.calculate_params()
		self.device = device
		self.outChan = outChan
		self.inChan = inChan
		self.dds = dds_device
		self.powerlockbox = power_lock_device

		self.dataQueue = None
		self.cycle_number = 1
		self.t0 = 0.

	def calculate_params(self) :
		self.TbrightLength = int(self.Tbright*self.sampling_rate)
		self.TdarkLength = int(self.Tdark*self.sampling_rate)
		self.TcycleLength = self.TbrightLength + self.TdarkLength
		self.TdiscardedLength = int(self.Tdiscarded * self.sampling_rate)
		self.TaverageLength = int(self.Taverage * self.sampling_rate)
		self.n_samp_per_period = self.TcycleLength*self.n_cycle
		self.mod_frequency = self.sampling_rate/self.n_samp_per_period
		self.update_interval = 1/self.mod_frequency

	def initialize(self):

		self.AIdata = np.zeros(self.n_samp_per_period,dtype=np.float64)
		self.mean_buffer = Buffer(20)
		self.average_mask = np.zeros(self.TcycleLength, dtype =bool)
		self.average_mask[self.TcycleLength-self.TaverageLength :] = 1
		self.average_mask = np.tile(self.average_mask, self.n_cycle)

		if self.waveform == 'sine' :
			self.AOdata = self.amplitude*np.sin(np.arange(self.n_samp_per_period)*2*np.pi/self.n_samp_per_period)
			self.demodulation_data = (np.sin(np.arange(self.n_samp_per_period)*2*np.pi/self.n_samp_per_period + self.phase*np.pi/180))[self.average_mask]
		else :
			self.AOdata = np.hstack([-self.amplitude *np.ones(self.n_samp_per_period/2),
			self.amplitude *np.ones(self.n_samp_per_period/2)])
			self.demodulation_data = (np.roll(self.AOdata, int(self.phase*self.n_samp_per_period/360))/self.amplitude)[self.average_mask]
		
		self.AItaskHandle = None
		self.AOtaskHandle = None
		self.COtaskHandle = None
		self.COtaskHandleTrig = None
		self.ptr = 0
		self.ctr = 0

		if self.get_init_freq_from_dds:         # Overide initial frequency
			self.initial_dds_frequency = self.dds.frequency
		self.dds_frequency = self.initial_dds_frequency
		self.power_correction = self.powerlockbox.correction
		self.bufferLength = 10* self.TcycleLength * self.n_cycle
		self.errorTab = np.zeros(self.cycle_number,dtype=np.float64)
		self.powerTab = np.zeros(self.cycle_number,dtype=np.float64)
		# self.nbSampCropped = int(self.n_samp_per_period/2*self.discarded_samples_factor)





	def start(self):

		assert not self.running
		self.running = True
		self.initialize()


		print ("Modulation frequency: {}".format(self.mod_frequency)) 
		print("DDS Update Intervall: {}".format(self.update_interval))
		print("Number of samples per cycle: {}".format(self.n_samp_per_period))

		def EveryNCallback(taskHandle, everyNsamplesEventType, nSamples, callbackData):
			readAI = c_int32()
			DAQmxReadAnalogF64(self.AItaskHandle,self.n_samp_per_period,self.timeout,DAQmx_Val_GroupByChannel,self.AIdata,self.n_samp_per_period,byref(readAI),None)
			self.ptr = (self.ptr + 1) % self.cycle_number
			self.AIavg = (np.roll(self.AIdata, -1))[self.average_mask]
			

			error = np.mean(self.AIavg*self.demodulation_data)
			self.errorTab[self.ptr] = error
			self.powerTab[self.ptr] = np.mean(self.AIdata)
            
			if self.ptr == 0:
				self.ctr += 1
				daqTime = self.ctr * self.cycle_number*self.n_samp_per_period/float(self.sampling_rate)
				meanError = np.mean(self.errorTab)
				correction = self.gain*meanError
				mean_power = np.mean(self.powerTab)
				# self.power_error = self.initial_CPT_transmission - mean_power
				# if self.powerLock:
				#     self.power_correction += self.powergain * self.power_error
				#     self.powerlockbox.correction = "%.9f" %self.power_correction
				if self.frequencyLock:
					self.dds_frequency += correction 
					self.dds.frequency = self.dds_frequency
				self.mean_buffer.append(mean_power)
				if self.dataQueue:
					if self.ctr % self.log_ratio == 0 :
						self.dataQueue.put([time.clock()-self.t0,daqTime,self.dds_frequency,self.dds_frequency-self.initial_dds_frequency,meanError,mean_power])                
			return int(0)       

		def DoneCallback(taskHandle, status, callbackData):
			self.clearTasks()
			return int(0)

		self.AItaskHandle = TaskHandle()
		self.AOtaskHandle = TaskHandle()
		self.COtaskHandle = TaskHandle()
		self.COtaskHandleTrig = TaskHandle()


		DAQmxCreateTask(None, byref(self.COtaskHandle))
		DAQmxCreateTask(None, byref(self.COtaskHandleTrig))
		DAQmxCreateCOPulseChanTime(self.COtaskHandle, 'Dev1/Ctr0',None,DAQmx_Val_Seconds, DAQmx_Val_High, 0, self.TbrightLength/self.sampling_rate,self.TdarkLength/self.sampling_rate)
		DAQmxCreateCOPulseChanTime(self.COtaskHandleTrig, 'Dev1/Ctr1', None, DAQmx_Val_Seconds, DAQmx_Val_High, 0, (self.TcycleLength-self.TaverageLength-self.TdiscardedLength)/self.sampling_rate, (self.TaverageLength+self.TdiscardedLength)/self.sampling_rate)
		DAQmxCreateTask(None,byref(self.AItaskHandle))
		DAQmxCreateAIVoltageChan(self.AItaskHandle,self.device + '/' + self.inChan, None, DAQmx_Val_Cfg_Default, self.inRange[0],self.inRange[1], DAQmx_Val_Volts, None)
		DAQmxCfgSampClkTiming(self.AItaskHandle,None, self.sampling_rate, DAQmx_Val_Rising, DAQmx_Val_ContSamps, self.n_samp_per_period)
		DAQmxCreateTask(None,byref(self.AOtaskHandle))
		DAQmxCreateAOVoltageChan(self.AOtaskHandle,self.device + '/' + self.outChan,None,self.outRange[0],self.outRange[1],DAQmx_Val_Volts,None)
		DAQmxCfgImplicitTiming(self.COtaskHandleTrig,DAQmx_Val_ContSamps, self.TcycleLength)
		DAQmxCfgImplicitTiming(self.COtaskHandle,DAQmx_Val_ContSamps, self.TcycleLength)
		DAQmxCfgSampClkTiming(self.AOtaskHandle,None,self.sampling_rate,DAQmx_Val_Rising,DAQmx_Val_ContSamps,self.n_samp_per_period)
		DAQmxCfgDigEdgeStartTrig(self.AOtaskHandle,self.trigName,DAQmx_Val_Rising)
		DAQmxCfgDigEdgeStartTrig(self.COtaskHandleTrig,self.trigName,DAQmx_Val_Rising)
		DAQmxCfgDigEdgeStartTrig(self.COtaskHandle, 'PFI13', DAQmx_Val_Rising)

		DAQmxCfgInputBuffer(self.AItaskHandle, c_uint32(self.bufferLength))

		if self.compatibility_mode == 0:
			EveryNCallbackCWRAPPER = ctypes.CFUNCTYPE(ctypes.c_int32,ctypes.c_void_p,ctypes.c_int32,ctypes.c_uint32,ctypes.c_void_p)
		else:
			EveryNCallbackCWRAPPER = ctypes.CFUNCTYPE(ctypes.c_int32,ctypes.c_ulong,ctypes.c_int32,ctypes.c_uint32,ctypes.c_void_p)
		self.everyNCallbackWrapped = EveryNCallbackCWRAPPER(EveryNCallback)
		DAQmxRegisterEveryNSamplesEvent(self.AItaskHandle,DAQmx_Val_Acquired_Into_Buffer,self.n_samp_per_period,0,self.everyNCallbackWrapped,None)

		if self.compatibility_mode == 0:
			DoneCallbackCWRAPPER = ctypes.CFUNCTYPE(ctypes.c_int32,ctypes.c_void_p,ctypes.c_int32,ctypes.c_void_p)
		else:
		    DoneCallbackCWRAPPER = ctypes.CFUNCTYPE(ctypes.c_int32,ctypes.c_ulong,ctypes.c_int32,ctypes.c_void_p)
		self.doneCallbackWrapped = DoneCallbackCWRAPPER(DoneCallback)
		DAQmxRegisterDoneEvent(self.AItaskHandle,0,self.doneCallbackWrapped,None)

		DAQmxWriteAnalogF64(self.AOtaskHandle, self.n_samp_per_period, 0, self.timeout, DAQmx_Val_GroupByChannel, self.AOdata, None, None)

		DAQmxStartTask(self.COtaskHandleTrig)
		DAQmxStartTask(self.COtaskHandle)
		DAQmxStartTask(self.AOtaskHandle)
		DAQmxStartTask(self.AItaskHandle)
		print "Starting asserv"

	def clearTasks(self):
		if self.AItaskHandle:
			DAQmxStopTask(self.AItaskHandle)
			DAQmxClearTask(self.AItaskHandle)
			self.AItaskHandle = None
		if self.AOtaskHandle:
			DAQmxStopTask(self.AOtaskHandle)
			DAQmxClearTask(self.AOtaskHandle)
			self.AOtaskHandle = None
		if self.COtaskHandle:
			DAQmxStopTask(self.COtaskHandle)
			DAQmxClearTask(self.COtaskHandle)
			self.COtaskHandle = None
		if self.COtaskHandleTrig:
			DAQmxStopTask(self.COtaskHandleTrig)
			DAQmxClearTask(self.COtaskHandleTrig)
			self.COtaskHandleTrig = None   

	def stop(self):
		if self.running:
			self.clearTasks()
			self.setZero()
			self.running = False

	def setZero(self):
		print "Setting output to 0 V"
		clearAOTaskHandle = TaskHandle()
		DAQmxCreateTask("", byref(clearAOTaskHandle))
		DAQmxCreateAOVoltageChan(clearAOTaskHandle, self.device + '/' + self.outChan, None, self.outRange[0],self.outRange[1], DAQmx_Val_Volts, None)
		DAQmxWriteAnalogF64(clearAOTaskHandle,1,1,self.timeout,DAQmx_Val_GroupByChannel,np.array([0.]),None,None)
		DAQmxStartTask(clearAOTaskHandle)
		DAQmxClearTask(clearAOTaskHandle)
		clearCOTaskHandle = TaskHandle()
		DAQmxCreateTask(None, byref(clearCOTaskHandle))
		DAQmxCreateDOChan(clearCOTaskHandle, 'Dev1/PFI12', None, DAQmx_Val_ChanPerLine)
		DAQmxStartTask(clearCOTaskHandle)
		DAQmxWriteDigitalLines(clearCOTaskHandle, 1, 1, 10, DAQmx_Val_GroupByChannel, np.array([0.], np.uint8), None, None)
		DAQmxClearTask(clearCOTaskHandle)

	def __del__(self):
		self.stop()