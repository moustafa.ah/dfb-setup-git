import numpy as np

class PulsedLOBase(object):

	def __init__(self):
		self._sampling_rate = 5e5
		self._t_bright = .0004 # s
		self._t_dark = .0002 # s
		self._t_detect = .0004
		self._scan_freq_num = 500
		# self._scan_freq_span = 1

	def generate_cycle_waveforms(self):

		n_bright = int(self._t_bright*self._sampling_rate)
		n_dark = int(self._t_dark*self._sampling_rate)
		n_detect = int(self._t_detect*self._sampling_rate)
		self._cycle_num_samp = num_samp = n_bright + n_dark + n_detect

		out_data = np.zeros(num_samp,dtype=[('ao_data','f8'),('do_data','b1')])
		in_data = np.zeros((2,num_samp),dtype='float64')
		out_data['do_data'][:n_bright] = 1
		out_data['do_data'][-n_detect:] = 1
		return num_samp, out_data, in_data

	def generate_scan_waveforms(self):

		num_samp, out_data, in_data = self.generate_cycle_waveforms()
		out_data = np.tile(out_data, self._scan_freq_num )
		out_data['ao_data'] = np.repeat(np.linspace(-.5,.5,self._scan_freq_num),num_samp)
		in_data = np.tile(in_data, self._scan_freq_num )		
		return num_samp*self._scan_freq_num, out_data, in_data


	def start_scan(self):

		nsamp, out_data, in_data = generate_scan_waveforms()

		self.ai_task_handle = TaskHandle()
		self.ao_task_handle = TaskHandle()
		self.do_task_handle = TaskHandle()
		DAQmxCreateTask(None, ctypes.byref(self.ai_task_handle))
		DAQmxCreateTask(None, ctypes.byref(self.ao_task_handle))
		DAQmxCreateTask(None, ctypes.byref(self.do_task_handle))
		DAQmxCreateAIVoltageChan(self.ai_task_handle, self.dev_id+'/'+'ai0', None, DAQmx_Val_Cfg_Default, -10., +10., DAQmx_Val_Volts, None)
		DAQmxCreateAOVoltageChan(self.ao_task_handle,self.dev_id + '/' +'ao3',None,-1.,1.,DAQmx_Val_Volts,None)
		DAQmxCreateDOChan(self.do_task_handle, 	self.dev_id+'/'+'port0/line0', None, DAQmx_Val_ChanPerLine)
		DAQmxCfgSampClkTiming(self.ai_task_handle, None, self._sampling_rate, DAQmx_Val_Rising, DAQmx_Val_ContSamps, nsamp)
		DAQmxCfgSampClkTiming(self.do_task_handle, "ai/SampleClock", self._sampling_rate, DAQmx_Val_Rising, DAQmx_Val_ContSamps, nsamp)
		DAQmxCfgSampClkTiming(self.ao_task_handle, "ai/SampleClock", self._sampling_rate, DAQmx_Val_Rising, DAQmx_Val_ContSamps, nsamp)

		EveryNCallbackCWRAPPER = ctypes.CFUNCTYPE(ctypes.c_int32,ctypes.c_void_p,ctypes.c_int32,ctypes.c_uint32,ctypes.c_void_p)
		self.everyNCallbackWrapped = EveryNCallbackCWRAPPER(EveryNCallback)
		DAQmxRegisterEveryNSamplesEvent(self.ai_task_handle,DAQmx_Val_Acquired_Into_Buffer,self._cycle_num_samp,0,self.everyNCallbackWrapped,None)

		DAQmxWriteDigitalLines(self.do_task_handle, nsamp,0, 10.0,DAQmx_Val_GroupByChannel, out_data['do_data'],None,None)
		DAQmxWriteAnalogF64(self.ao_task_handle, nsamp, 0, 10., DAQmx_Val_GroupByChannel, out_data['ao_data'], None, None)

		DAQmxStartTask(self.do_task_handle)
		DAQmxStartTask(self.ao_task_handle)
		DAQmxStartTask(self.ai_task_handle)


	def stop_scan(self):
		pass


# class PulsedLOScan(object):



# class PulsedLOServo(object):

