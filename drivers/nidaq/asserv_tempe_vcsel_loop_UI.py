from asserv_tempe_vcsel_loop import Asserv
import pyqtgraph.parametertree.parameterTypes as pTypes
from pyqtgraph.parametertree import Parameter, ParameterTree

class Asserv_tempe_vcsel_loop_UI(Asserv):

	param_group_name = "Asserv. CW"

	def __init__(self,**kwargs):
		super(Asserv_tempe_vcsel_loop_UI, self).__init__(**kwargs)
		self.param_group = pTypes.GroupParameter(name = self.param_group_name)
		self.freq_lock_param = Parameter.create(name='Lock',
											type='bool',
											value=self.lock)
		self.gain_param = Parameter.create(name='Gain',
											type='float',
											value=self.gain,
											step= 100)
		self.sampling_rate_param = Parameter.create(name='Sampling rate',
											type='int',
											value=self.sampling_rate,
											step= 100000,
											siPrefix=True,
											suffix='Samp/s')
		self.freq_mod_param = Parameter.create(name='Mod. frequency',
											type='float',
											value=self.freq_mod,
											step= 1,
											siPrefix=True,
											suffix='Hz')
		self.cycle_number_param = Parameter.create(name='N cycle',
											type='int',
											value=self.cycle_number,
											step=1)
		self.discarded_samples_factor_param = Parameter.create(name='Ratio discarded samp.',
											type='float',
											value= self.discarded_samples_factor,
											step=0.05)
		# Read only parameters
		self.n_samp_per_cycle_param = Parameter.create(name='N samp/cycle',
											type='int',
											value=self.n_samp_per_cycle,
											readonly=True)
		self.act_freq_mod_param = Parameter.create(name='Actual Mod frequency',
											type='float',
											value=self.freq_mod,
											siPrefix=True,
											suffix='Hz',
											readonly=True)
		self.update_interval_param = Parameter.create(name='Update interval',
											type='float',
											value=self.update_interval,
											siPrefix=True,
											suffix='s',
											readonly=True)
		self.tempe_vcsel_loop_status_param = Parameter.create(name='Lock VCSEL temperature',
											type='bool',
											value=False)
		self.tempe_vcsel_gain_param = Parameter.create(name='VCSEL temperature loop gain',
											type='float',
											value=self.tempe_vcsel_gain,
											step= 1)
		self.tempe_vcsel_average_window_length_param = Parameter.create(name='VCSEL temperature loop average window length',
											type='float',
											value=self.tempe_vcsel_average_window_length,
											step= 10)
		self.power_loop_status_param = Parameter.create(name='Lock optical power',
											type='bool',
											value=False)
		self.power_gain_param = Parameter.create(name='Power asserv loop gain',
											type='float',
											value=self.power_gain,
											step= 1)
		self.power_average_window_length_param = Parameter.create(name='Power loop average window length',
											type='float',
											value=self.power_average_window_length,
											step= 1)


		self.param_group.addChildren([self.freq_lock_param,
									self.gain_param,
									self.sampling_rate_param,
									self.freq_mod_param,
									self.cycle_number_param,
									self.discarded_samples_factor_param,
									self.n_samp_per_cycle_param,
									self.act_freq_mod_param,
									self.update_interval_param,
									self.tempe_vcsel_loop_status_param,
									self.tempe_vcsel_gain_param,
									self.tempe_vcsel_average_window_length_param,
									self.power_loop_status_param,
									self.power_gain_param,
									self.power_average_window_length_param
									])

		self.param_group.sigTreeStateChanged.connect(self.change)

	def change(self,param,changes):
		for param, change, data in changes:
			if param is self.freq_lock_param: self.lock = param.value()
			elif param is self.gain_param: self.gain = param.value()
			elif param is self.sampling_rate_param:	self.sampling_rate = param.value()				
			elif param is self.freq_mod_param: self.freq_mod = param.value()
			elif param is self.cycle_number_param: self.cycle_number = param.value()
			elif param is self.discarded_samples_factor_param: self.discarded_samples_factor = param.value()
			elif param is self.tempe_vcsel_loop_status_param :
				if param.value() == False :
					self.change_tempe_vcsel_setpoint(0.)
					self.tempe_vcsel_ctr = 0
					self.tempe_vcsel_first_round = True
				self.tempe_vcsel_loop_status = param.value()
			elif param is self.tempe_vcsel_gain_param : self.tempe_vcsel_gain = param.value()
			elif param is self.tempe_vcsel_average_window_length_param : self.tempe_vcsel_average_window_length = param.value()
			elif param is self.power_loop_status_param :
				if param.value() == False :
					self.change_power(0.)
					self.power_ctr = 0
					self.power_first_round = True
				self.power_loop_status = param.value()
			elif param is self.power_gain_param : self.power_gain = param.value()
			elif param is self.power_average_window_length_param : self.power_average_window_length = param.value()
			self.update_readonly_params()

	def update_readonly_params(self):
		self.act_freq_mod_param.setValue(self.freq_mod)
		self.n_samp_per_cycle_param.setValue(self.n_samp_per_cycle)
		self.update_interval_param.setValue(self.update_interval)

	def start(self):
		self.param_group.setName(self.param_group_name+" (Active)")
		super(Asserv_tempe_vcsel_loop_UI, self).start()

	def stop(self):
		self.param_group.setName(self.param_group_name)
		super(Asserv_tempe_vcsel_loop_UI, self).stop()