from .asserv_pulsed6 import Asserv_pulsed
import pyqtgraph.parametertree.parameterTypes as pTypes
from pyqtgraph.parametertree import Parameter, ParameterTree
from ui.toggle_button import ToggleButton
from pyqtgraph.graphicsItems.PlotItem import PlotItem
from pyqtgraph import LabelItem
import numpy as np

symbolBrushes = [(130, 45, 255), (255, 0, 133), (255, 226, 8), (20, 183, 204), "r"]

class Asserv_pulsed_UI(Asserv_pulsed):

	param_group_name = 'Asserv. Pulsed'

	def __init__(self,**kwargs):
		super(Asserv_pulsed_UI, self).__init__(**kwargs)

		self.param_group = pTypes.GroupParameter(name = self.param_group_name)
		self.freq_lock_param = Parameter.create(name='Lock',
											type='bool',
											value=self.frequencyLock)
		self.gain_param = Parameter.create(name='Gain',
											type='float',
											value=self.gain,
											step= 100)
		self.Tbright_param = Parameter.create(name = 'Tbright', 
											  type = 'float',
											  value = self.TbrightLength/self.sampling_rate, 
											  step = 0.0001,
											  siPrefix=True,
											  suffix= 's')
		self.Tdark_param = Parameter.create(name = 'Tdark',
											type = 'float',
											value = self.TdarkLength/self.sampling_rate,
											step = 0.0001,
											siPrefix=True,
											suffix= 's')
		self.Tdiscarded_param = Parameter.create(name = 'Dead time before averaging',
												type = 'float',
												value = self.TdiscardedLength/self.sampling_rate,
												step = 0.000001, 
												siPrefix=True,
												suffix= 's')
		self.Taverage_param = Parameter.create(name = 'Averaging time',
												type = 'float',
												value = self.TaverageLength/self.sampling_rate,
												step = 0.000001,
												siPrefix=True,
												suffix= 's')
		self.n_cycle_param = Parameter.create(name = 'Number of cycles per point',
												type = 'int',
												value = self.n_cycle,
												step = 1,
												limits = (1,1000))
		self.waveform_param = Parameter.create(name = 'Lock-in Waveform',
												type = 'list',
												values = self.waveforms,
												value = self.waveform)
		self.phase_param = Parameter.create(name = 'Phase',
											type = 'float',
											value = self.phase,
											step = 1,
											suffix = 'deg',
											limits = (-180, 180))
		
		# Read-only parameters
		self.Tcycle_param = Parameter.create(name = 'Tcycle',
											type = 'float',
											value = self.TcycleLength/self.sampling_rate,
											siPrefix = True,
											suffix = 's',
											readonly = True)
		self.n_samp_per_period_param = Parameter.create(name='N samp/period',
														type='int',
														value=self.n_samp_per_period,
														readonly=True)
		self.mod_frequency_param = Parameter.create(name='Modulation frequency',
													type='float',
													value=self.mod_frequency,
													siPrefix=True, 
													suffix='Hz',
													readonly=True)
		self.update_interval_param = Parameter.create(name='Update interval',
														type='float',
														value=self.update_interval,
														siPrefix=True,
														suffix='s',
														readonly=True)

		self.param_group.addChildren([self.freq_lock_param,
										self.gain_param,
										self.Tbright_param,
										self.Tdark_param,
										self.Tdiscarded_param,
										self.Taverage_param,
										self.n_cycle_param,
										self.waveform_param,
										self.phase_param,
										self.Tcycle_param,
										self.n_samp_per_period_param,
										self.mod_frequency_param,
										self.update_interval_param])

		self.param_group.sigTreeStateChanged.connect(self.change)


		self.startBtn = ToggleButton("Asserv Pulsed", self)

	def change(self,param,changes):
		for param, change, data in changes:
			if param is self.freq_lock_param: self.frequencyLock = param.value()
			elif param is self.gain_param: self.gain = param.value()
			elif param is self.Tbright_param: self.Tbright = param.value()
			elif param is self.Tdark_param: self.Tdark = param.value()
			elif param is self.Tdiscarded_param: self.Tdiscarded = param.value()
			elif param is self.Taverage_param: self.Taverage = param.value()
			elif param is self.n_cycle_param: self.n_cycle = param.value()
			self.update_params()

	def update_params(self):
		self.param_group.sigTreeStateChanged.disconnect(self.change)
		self.calculate_params()
		self.Tbright_param.setValue(self.TbrightLength/self.sampling_rate)
		self.Tdark_param.setValue(self.TdarkLength/self.sampling_rate)
		self.Tcycle_param.setValue(self.TcycleLength/self.sampling_rate)
		self.Tdiscarded_param.setValue(self.TdiscardedLength/self.sampling_rate)
		self.Taverage_param.setValue(self.TaverageLength/self.sampling_rate)
		self.n_samp_per_period_param.setValue(self.n_samp_per_period)
		self.mod_frequency_param.setValue(self.mod_frequency)
		self.update_interval_param.setValue(self.update_interval)
		self.param_group.sigTreeStateChanged.connect(self.change)

	def start(self):
		if super(Asserv_pulsed_UI, self).start():
			self.param_group.setName(self.param_group_name+" (Active)")
			# self.running=True
			return True
		return False

	def stop(self):
		self.param_group.setName(self.param_group_name)
		super(Asserv_pulsed_UI, self).stop()

	def make_plots(self, graph_layout=None,title_font_size=3):
		self.plots = []
		self.curves = []
		for idx, header in enumerate(self.header[1:]):
			l = LabelItem("<font size={:d}>{}</font>".format(title_font_size,header))
			# p = PlotItem(title = "<font size={:d}>{}</font>".format(title_font_size,header))
			p = PlotItem()
			c = p.plot( title = header, pen = symbolBrushes[idx % len(symbolBrushes)])
			self.plots.append(p)
			self.curves.append(c)
			if graph_layout is not None:
				graph_layout.addItem(l)
				graph_layout.addItem(p)
				graph_layout.nextRow()

	def update(self):
		if self.buf.index > 0:
			data = self.buf.get()
			t0 = data[0,0]
			for idx, curve in enumerate(self.curves):
				curve.setData(x = data[:,0] - t0 , y = data[:,idx+1])