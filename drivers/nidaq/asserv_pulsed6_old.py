from PyDAQmx import *
import numpy as np
import ctypes, time, queue
from utils.misc import Buffer
from base.dataproducer import DataProducer

class Asserv_pulsed(DataProducer):

	# UI accessible settings
	frequencyLock = True
	gain = 1000
	Tbright = .00004 # s
	Tdark = .00002 # s
	Tdiscarded = .00002 # s
	Taverage = .00001 # s
	n_cycle = 6
	waveform = 'square'
	phase = 0

	# Fixed settings
	waveforms = ['square', 'sine']
	sampling_rate = 1e6
	compatibility_mode = 0          # Set this to 1 on some PC (Mouss)
	trigName = "ai/StartTrigger"
	timeout = 10.0
	initial_dds_frequency = 7358910
	get_init_freq_from_dds = True   # Set to False to use initial_dds_frequency as first frequency
	
	log_ratio = 1  # Subsample written in log file
	amplitude = 0.5
	inRange = (-1.,1.)
	outRange = (-0.5,0.5)
	running = False
	header = ["Time","Frequency (Hz)","Correction (Hz)","Error","Mean signal (V)"]
	
	# powerLock = False
	# initial_CPT_transmission = 0
	# powergain = 0.1


	def __init__(self, device="Dev1",outChan="ao3",inChan="ai0", doChan = "port0/line6", dds_device=None, power_lock_device = None):
		DataProducer.__init__(self,maxlength=3000,numcol=len(self.header))
		self.calculate_params()
		self.device = device
		self.outChan = outChan
		self.doChan = doChan
		self.inChan = inChan
		self.dds = dds_device
		# self.powerlockbox = power_lock_device

		self.dataQueue = None
		self.cycle_number = 1

	def calculate_params(self) :
		self.TbrightLength = int(self.Tbright*self.sampling_rate)
		self.TdarkLength = int(self.Tdark*self.sampling_rate)
		self.TcycleLength = self.TbrightLength + self.TdarkLength
		self.TdiscardedLength = int(self.Tdiscarded * self.sampling_rate)
		self.TaverageLength = int(self.Taverage * self.sampling_rate)
		self.n_samp_per_period = self.TcycleLength*self.n_cycle
		self.mod_frequency = self.sampling_rate/self.n_samp_per_period
		self.update_interval = 1/self.mod_frequency
	
	def makeInputStr(self):			
		return self.device+"/"+self.inChan
	def makeOutputStr(self):
		return self.device+"/"+self.outChan
	def makeDOStr(self):
		return self.device+"/"+self.doChan

	def initialize(self):

		self.AIdata = np.zeros(self.n_samp_per_period,dtype=np.float64)
		self.mean_buffer = Buffer(20)
		self.average_mask = np.zeros(self.TcycleLength, dtype =bool)
		self.average_mask[self.TcycleLength-self.TaverageLength :] = 1
		self.average_mask = np.tile(self.average_mask, self.n_cycle)

		if self.waveform == 'sine' :
			self.AOdata = self.amplitude*np.sin(np.arange(self.n_samp_per_period)*2*np.pi/self.n_samp_per_period)
			self.demodulation_data = (np.sin(np.arange(self.n_samp_per_period)*2*np.pi/self.n_samp_per_period + self.phase*np.pi/180))[self.average_mask]
		else :
			self.AOdata = np.hstack([-self.amplitude *np.ones(self.n_samp_per_period//2),
			self.amplitude *np.ones(self.n_samp_per_period//2)])
			self.demodulation_data = (np.roll(self.AOdata, int(self.phase*self.n_samp_per_period/360))/self.amplitude)[self.average_mask]
		
		# Waveform envoyee pour pulser la lumiere
		self.DOdata = np.zeros(self.TcycleLength//10, dtype = np.uint8)
		self.DOdata[(self.TbrightLength - self.TdiscardedLength - self.TaverageLength)//10 : (self.TcycleLength - self.TdiscardedLength - self.TaverageLength)//10] = 1
		self.DOdata2 = np.ones(self.TcycleLength//10, dtype = np.uint8)
		self.DOdata2[(self.TbrightLength - self.TdiscardedLength - self.TaverageLength)//10 : (self.TcycleLength - self.TdiscardedLength - self.TaverageLength)//10] = 0

		self.AItaskHandle = None
		self.AOtaskHandle = None
		self.COtaskHandle = None
		self.DOtaskHandle = None
		# self.DO2taskHandle = None
		self.ptr = 0
		self.ctr = 0

		if self.get_init_freq_from_dds:         # Overide initial frequency
			self.initial_dds_frequency = self.dds.frequency
		self.dds_frequency = self.initial_dds_frequency
		# self.power_correction = self.powerlockbox.correction
		self.bufferLength = 10* self.TcycleLength * self.n_cycle
		self.errorTab = np.zeros(self.cycle_number,dtype=np.float64)
		self.powerTab = np.zeros(self.cycle_number,dtype=np.float64)
		# self.nbSampCropped = int(self.n_samp_per_period/2*self.discarded_samples_factor)


	def init5VEnabling(self):
		self.init5VtaskHandle = TaskHandle()
		DAQmxCreateTask(None, byref(self.init5VtaskHandle))
		DAQmxCreateDOChan(self.init5VtaskHandle, self.device+'/port0/line7', None, DAQmx_Val_ChanPerLine)
		DAQmxStartTask(self.init5VtaskHandle)
		DAQmxWriteDigitalLines(self.init5VtaskHandle, 1, 1, 10, DAQmx_Val_GroupByChannel, np.array([1.], np.uint8), None, None)
		DAQmxStopTask(self.init5VtaskHandle)
		DAQmxClearTask(self.init5VtaskHandle)

	def start(self):

		assert not self.running
		self.running = True
		self.initialize()


		print ("Modulation frequency: {}".format(self.mod_frequency)) 
		print("DDS Update Intervall: {}".format(self.update_interval))
		print("Number of samples per cycle: {}".format(self.n_samp_per_period))

		def EveryNCallback(taskHandle, everyNsamplesEventType, nSamples, callbackData):
			readAI = ctypes.c_int32()
			DAQmxReadAnalogF64(self.AItaskHandle,self.n_samp_per_period,self.timeout,DAQmx_Val_GroupByChannel,self.AIdata,self.n_samp_per_period,ctypes.byref(readAI),None)
			self.ptr = (self.ptr + 1) % self.cycle_number
			self.AIavg = (np.roll(self.AIdata, -1))[self.average_mask]
			

			error = np.mean(self.AIavg*self.demodulation_data)
			self.errorTab[self.ptr] = error
			self.powerTab[self.ptr] = np.mean(self.AIdata)
            
			if self.ptr == 0:
				self.ctr += 1
				daqTime = self.ctr * self.cycle_number*self.n_samp_per_period/float(self.sampling_rate)
				meanError = np.mean(self.errorTab)
				correction = self.gain*meanError
				mean_power = np.mean(self.powerTab)
				# self.power_error = self.initial_CPT_transmission - mean_power
				# if self.powerLock:
				#     self.power_correction += self.powergain * self.power_error
				#     self.powerlockbox.correction = "%.9f" %self.power_correction
				if self.frequencyLock:
					self.dds_frequency += correction 
					self.dds.frequency = self.dds_frequency
				self.mean_buffer.append(mean_power)
				data = [time.time(),self.dds_frequency,self.dds_frequency-self.initial_dds_frequency,meanError,mean_power]
				self.buf.append(data)
				self.send_to_consumers(data)
				# if self.dataQueue:
					# if self.ctr % self.log_ratio == 0 :
						# self.dataQueue.put([time.time(),daqTime,self.dds_frequency,self.dds_frequency-self.initial_dds_frequency,meanError,mean_power])                
			return int(0)       

		def DoneCallback(taskHandle, status, callbackData):
			self.clearTasks()
			return int(0)

		self.AItaskHandle = TaskHandle()
		self.AOtaskHandle = TaskHandle()
		self.COtaskHandle = TaskHandle()
		self.DOtaskHandle = TaskHandle()
		# self.DO2taskHandle = TaskHandle()


		DAQmxCreateTask(None, ctypes.byref(self.COtaskHandle))
		DAQmxCreateTask(None, ctypes.byref(self.DOtaskHandle))
		# DAQmxCreateTask(None, byref(self.DO2taskHandle))
		DAQmxCreateCOPulseChanTime(self.COtaskHandle, self.device+'/Ctr0',None,DAQmx_Val_Seconds, DAQmx_Val_High, 0, 5E-6, 5E-6)
		DAQmxCreateDOChan(self.DOtaskHandle, self.device+'/port0/line6:7', None, DAQmx_Val_ChanPerLine)
		# DAQmxCreateDOChan(self.DO2taskHandle, 'Dev1/port0/line7' , None, DAQmx_Val_ChanPerLine)
		DAQmxCreateTask(None,ctypes.byref(self.AItaskHandle))
		DAQmxCreateAIVoltageChan(self.AItaskHandle,self.device + '/' + self.inChan, None, DAQmx_Val_Cfg_Default, self.inRange[0],self.inRange[1], DAQmx_Val_Volts, None)
		DAQmxCfgSampClkTiming(self.AItaskHandle,None, self.sampling_rate, DAQmx_Val_Rising, DAQmx_Val_ContSamps, self.n_samp_per_period)
		DAQmxCreateTask(None,ctypes.byref(self.AOtaskHandle))
		DAQmxCreateAOVoltageChan(self.AOtaskHandle,self.device + '/' + self.outChan,None,self.outRange[0],self.outRange[1],DAQmx_Val_Volts,None)
		DAQmxCfgSampClkTiming(self.DOtaskHandle,'/'+self.device+'/PFI1', 100000., DAQmx_Val_Rising, DAQmx_Val_ContSamps, self.TcycleLength)
		# DAQmxCfgSampClkTiming(self.DO2taskHandle,'/Dev1/PFI1', 100000., DAQmx_Val_Rising, DAQmx_Val_ContSamps, self.TcycleLength)
		DAQmxCfgImplicitTiming(self.COtaskHandle,DAQmx_Val_ContSamps, self.TcycleLength)
		DAQmxCfgSampClkTiming(self.AOtaskHandle,None,self.sampling_rate,DAQmx_Val_Rising,DAQmx_Val_ContSamps,self.n_samp_per_period)
		DAQmxCfgDigEdgeStartTrig(self.AOtaskHandle,self.trigName,DAQmx_Val_Rising)
		DAQmxCfgDigEdgeStartTrig(self.COtaskHandle, self.trigName, DAQmx_Val_Rising)

		DAQmxCfgInputBuffer(self.AItaskHandle, ctypes.c_uint32(self.bufferLength))

		if self.compatibility_mode == 0:
			EveryNCallbackCWRAPPER = ctypes.CFUNCTYPE(ctypes.c_int32,ctypes.c_void_p,ctypes.c_int32,ctypes.c_uint32,ctypes.c_void_p)
		else:
			EveryNCallbackCWRAPPER = ctypes.CFUNCTYPE(ctypes.c_int32,ctypes.c_ulong,ctypes.c_int32,ctypes.c_uint32,ctypes.c_void_p)
		self.everyNCallbackWrapped = EveryNCallbackCWRAPPER(EveryNCallback)
		DAQmxRegisterEveryNSamplesEvent(self.AItaskHandle,DAQmx_Val_Acquired_Into_Buffer,self.n_samp_per_period,0,self.everyNCallbackWrapped,None)

		if self.compatibility_mode == 0:
			DoneCallbackCWRAPPER = ctypes.CFUNCTYPE(ctypes.c_int32,ctypes.c_void_p,ctypes.c_int32,ctypes.c_void_p)
		else:
		    DoneCallbackCWRAPPER = ctypes.CFUNCTYPE(ctypes.c_int32,ctypes.c_ulong,ctypes.c_int32,ctypes.c_void_p)
		self.doneCallbackWrapped = DoneCallbackCWRAPPER(DoneCallback)
		DAQmxRegisterDoneEvent(self.AItaskHandle,0,self.doneCallbackWrapped,None)
		
		DAQmxWriteDigitalLines(self.DOtaskHandle,self.TcycleLength//10,0,0.0,DAQmx_Val_GroupByChannel,np.hstack((self.DOdata, self.DOdata2)),None,None)
		# DAQmxWriteDigitalLines(self.DO2taskHandle,self.TcycleLength/10,0,0.0,DAQmx_Val_GroupByChannel,self.DOdata2 ,None,None)
		DAQmxWriteAnalogF64(self.AOtaskHandle, self.n_samp_per_period, 0, self.timeout, DAQmx_Val_GroupByChannel, self.AOdata, None, None)


		DAQmxStartTask(self.AOtaskHandle)
		DAQmxStartTask(self.DOtaskHandle)
		# DAQmxStartTask(self.DO2taskHandle)
		DAQmxStartTask(self.COtaskHandle)
		DAQmxStartTask(self.AItaskHandle)
		
		print("Starting asserv")

	def clearTasks(self):
		if self.AItaskHandle:
			DAQmxStopTask(self.AItaskHandle)
			DAQmxClearTask(self.AItaskHandle)
			self.AItaskHandle = None
		if self.AOtaskHandle:
			DAQmxStopTask(self.AOtaskHandle)
			DAQmxClearTask(self.AOtaskHandle)
			self.AOtaskHandle = None
		if self.COtaskHandle:
			DAQmxStopTask(self.COtaskHandle)
			DAQmxClearTask(self.COtaskHandle)
			self.COtaskHandle = None
		if self.DOtaskHandle:
			DAQmxStopTask(self.DOtaskHandle)
			DAQmxClearTask(self.DOtaskHandle)
			self.DOtaskHandle = None   
		# if self.DO2taskHandle:
		# 	DAQmxStopTask(self.DO2taskHandle)
		# 	DAQmxClearTask(self.DO2taskHandle)
		# 	self.DO2taskHandle = None   

	def stop(self):
		if self.running:
			self.clearTasks()
			self.setZero()
			self.running = False

	def setZero(self):
		print("Setting output to 0 V")
		clearAOTaskHandle = TaskHandle()
		DAQmxCreateTask("", ctypes.byref(clearAOTaskHandle))
		DAQmxCreateAOVoltageChan(clearAOTaskHandle, self.device + '/' + self.outChan, None, self.outRange[0],self.outRange[1], DAQmx_Val_Volts, None)
		DAQmxWriteAnalogF64(clearAOTaskHandle,1,1,self.timeout,DAQmx_Val_GroupByChannel,np.array([0.]),None,None)
		DAQmxStartTask(clearAOTaskHandle)
		DAQmxClearTask(clearAOTaskHandle)
		clearDOTaskHandle = TaskHandle()
		DAQmxCreateTask(None, ctypes.byref(clearDOTaskHandle))
		DAQmxCreateDOChan(clearDOTaskHandle, self.makeDOStr(), None, DAQmx_Val_ChanPerLine)
		DAQmxStartTask(clearDOTaskHandle)
		DAQmxWriteDigitalLines(clearDOTaskHandle, 1, 1, 10, DAQmx_Val_GroupByChannel, np.array([0.], np.uint8), None, None)
		DAQmxClearTask(clearDOTaskHandle)
		self.init5VEnabling()

	def __del__(self):
		self.stop()