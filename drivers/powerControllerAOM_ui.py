from .powerControllerAOM import PowerControllerAOM
import pyqtgraph.parametertree.parameterTypes as pTypes
from pyqtgraph.parametertree import Parameter, ParameterTree
from pyqtgraph.graphicsItems.PlotItem import PlotItem
from pyqtgraph import LabelItem
import numpy as np

symbolBrushes = [(130, 45, 255), (255, 0, 133), (255, 226, 8), (20, 183, 204), "r"]

class PowerControllerAOM_UI(PowerControllerAOM):

	def __init__(self,*args,**kwargs):
		super(PowerControllerAOM_UI, self).__init__(*args,**kwargs)
		self.param_group = pTypes.GroupParameter(name = 'AOM Power Controller')
		self.setpoint_param = Parameter.create(name='Power setpoint (uW)',
												type='float',
												value=self.setpoint)
		self.servo_state_param = Parameter.create(name='Power servo',
												type='bool',
												value=self.servo_state)
		self.signal_servo_state_param = Parameter.create(name='Signal servo',
												type='bool',
												value=self.signal_servo_state)
		self.gain_param = Parameter.create(name='Gain',
												type='float',
												value=self.gain)
		self.param_group.addChildren([self.setpoint_param,
									self.servo_state_param,
									self.signal_servo_state_param,
									self.gain_param,
									])
		self.param_group.sigTreeStateChanged.connect(self.change)


	def change(self,param,changes):
		for param, change, data in changes:
			if param is self.setpoint_param: self.setpoint = param.value()
			elif param is self.servo_state_param: self.servo_state = param.value()
			elif param is self.signal_servo_state_param: self.signal_servo_state = param.value()
			elif param is self.gain_param: self.gain = param.value()

	def make_plots(self, graph_layout=None,title_font_size=3):
		self.plots = []
		self.curves = []
		for idx, header in enumerate(self.header[1:]):
			l = LabelItem("<font size={:d}>{}</font>".format(title_font_size,header))
			p = PlotItem()
			c = p.plot( title = header, pen = symbolBrushes[idx % len(symbolBrushes)])
			self.plots.append(p)
			self.curves.append(c)
			if graph_layout is not None:
				graph_layout.addItem(l)
				graph_layout.addItem(p)
				graph_layout.nextRow()

	def update(self):
		if self.buf.index > 0:
			data = self.buf.get()
			t0 = data[0,0]
			for idx, curve in enumerate(self.curves):
				curve.setData(x = data[:,0] - t0 , y = data[:,idx+1])