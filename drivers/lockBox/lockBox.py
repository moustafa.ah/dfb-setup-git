import time
import serial
if __name__ == "__main__":
	import sys
	sys.path.append("../..")
from base.driver import Driver
from base.feat import Feat

class LockBox(Driver):

	dev_name = "LockBox"

	def __init__(self,comPort='com5',name="LockBox"):
		super(LockBox, self).__init__(name)
		try:
			self.ser = ser = serial.Serial()
			ser.port = comPort
			ser.timeout = 5
			ser.setDTR(False)
			ser.open()
		except serial.serialutil.SerialException:
			print("Could not connect")
			#no serial connection
			self.ser = None

	def __del__(self):
		if self.ser:
			self.ser.close()

	def send(self, command):
		self.ser.write((command+"\r\n").encode())

	def query(self, command):
		self.send(command)
		time.sleep(0.020)
		return self.ser.readline()[:-2]

	@Feat
	def lock(self):
		return bool(int(self.query("LOCK?")))

	@lock.setter
	def lock(self,state):
		if state:
			self.send("ON")
		else: self.send("OFF")
	
if __name__=='__main__':
	lockBox = LockBox(comPort='com5')