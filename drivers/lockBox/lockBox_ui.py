from .lockBox import LockBox
import pyqtgraph.parametertree.parameterTypes as pTypes
from pyqtgraph.parametertree import Parameter, ParameterTree

class LockBox_UI(LockBox):

	def __init__(self,**kwargs):
		super(LockBox_UI, self).__init__(**kwargs)
		self.param_group = pTypes.GroupParameter(name = 'Lock box')
		self.lock_param = Parameter.create(name='Lock',
											type='bool',
											value=self.lock)
		self.param_group.addChildren([self.lock_param])
		self.param_group.sigTreeStateChanged.connect(self.change)

	def change(self,param,changes):
		for param, change, data in changes:
			if param is self.lock_param: self.lock = param.value()
