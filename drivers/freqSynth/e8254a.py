import visa
from base.driver import Driver
from base.feat import Feat

class E8254A(Driver):

	def __init__(self,gpibAdress=19,name="E8254A",pow_amp_lim=28,raw_id=None):
		super(E8254A, self).__init__(name)
		freqSynthID = "GPIB::"+str(gpibAdress)+"::INSTR"
		if raw_id: freqSynthID = raw_id
		rm = visa.ResourceManager()
		self.inst = rm.open_resource(freqSynthID,read_termination='\n')
		self.pow_amp_lim = pow_amp_lim

	def query(self,command):
		self.inst.write(command)
		return self.inst.read()

	def send(self,command):
		self.inst.write(command)

	@Feat
	def fm_dev(self):
		return float(self.query("FM:DEV?"))

	@fm_dev.setter
	def fm_dev(self,fmDev):
		self.send("FM:DEV "+str(fmDev))
		
	@property
	def frequency(self):
		return float(self.query("FREQ:CW?"))
	
	@frequency.setter
	def frequency(self,freq):
		self.send("FREQ:CW "+str(freq))

	@Feat
	def power_amplitude(self):
		return float(self.query("POW:AMPL?"))

	@power_amplitude.setter
	def power_amplitude(self,pow):
		assert pow<=self.pow_amp_lim
		self.send("POW:AMPL "+str(pow))
		
	@Feat
	def rf_state(self):
		return bool(int(self.query("OUTP:STAT?")))

	@rf_state.setter
	def rf_state(self,state):
		self.send("OUTP:STAT "+str(int(bool(state))))

	@Feat
	def fm_state(self):
		return bool(int(self.query("FM:STAT?")))

	@fm_state.setter
	def fm_state(self,state):
		self.send("FM:STAT "+str(int(bool(state))))

	@Feat
	def mod_state(self):
		return bool(int(self.query("OUTP:MOD:STAT?")))
	
	@mod_state.setter
	def mod_state(self,state):
		self.send("OUTP:MOD:STAT "+str(int(bool(state))))

	@Feat
	def fm_ext1_coupling(self):
		return self.query("FM:EXT1:COUP?")
		
if __name__=='__main__':
	freqSynth = E8254A()