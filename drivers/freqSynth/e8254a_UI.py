from e8254a import E8254A
import pyqtgraph.parametertree.parameterTypes as pTypes
from pyqtgraph.parametertree import Parameter, ParameterTree

class E8254A_UI(E8254A):

	def __init__(self,**kwargs):
		super(E8254A_UI, self).__init__(**kwargs)
		self.param_group = pTypes.GroupParameter(name = 'Frequency synthesis')
		self.freq_param = Parameter.create(name='Frequency',
												type='float',
												value=self.frequency,
												step = 1000,
												suffix='Hz',
												decimals=12)
		self.pow_amp_param = Parameter.create(name='Power',
												type='float', 
												value=self.power_amplitude,
												step = 0.1,
												suffix='dBm')
		self.fm_dev_param = Parameter.create(name='FM dev',
												type='float',
												value=self.fm_dev,
												step = 100,
												suffix='Hz',
												siPrefix=True)
		self.freq_offset_action = Parameter.create(name='Frequency step',
												type='action')
		self.freq_offset_value = Parameter.create(name='Offset',
												type='float',
												value=20,
												step = 1,
												suffix='Hz')
		self.freq_offset_action.addChild(self.freq_offset_value)
		self.param_group.addChildren([self.freq_param,
									self.pow_amp_param,
									self.fm_dev_param,
									self.freq_offset_action])

		self.freq_offset_action.sigActivated.connect(self.freq_jump)
		self.param_group.sigTreeStateChanged.connect(self.change)

	def freq_jump(self):
		self.frequency = self.frequency + self.freq_offset_value.value()

	def change(self,param,changes):
		for param, change, data in changes:
			if param is self.pow_amp_param: self.power_amplitude = param.value()
			elif param is self.fm_dev_param: self.fm_dev = param.value()
			elif param is self.freq_param: self.frequency = param.value()
