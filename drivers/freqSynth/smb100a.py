import visa
from base.driver import Driver
from base.feat import Feat

class SMB100A(Driver):

	dev_name = 'SMB100A'
	pow_amp_lim = 10

	def __init__(self,gpibAdress=19,name="SMB100A",usbDev=None,pow_amp_lim=12):
		super(SMB100A, self).__init__(name)
		if usbDev:
			freqSynthID = usbDev
		else: freqSynthID = "GPIB::"+str(gpibAdress)+"::INSTR"
		rm = visa.ResourceManager()
		self.inst = rm.open_resource(freqSynthID,read_termination='\n')
		self.pow_amp_lim = pow_amp_lim
		self._frequency = None
		self._power_amplitude = None
		self._fm_dev = None

	def query(self,command):
		self.inst.write(command)
		return self.inst.read()

	def send(self,command):
		self.inst.write(command)

	@Feat
	def identity(self):
		return self.inst.query("*IDN?")
		
	@property
	def frequency(self):
		if self._frequency is None: self._frequency = float(self.query("FREQ:CW?"))
		return self._frequency
	
	@frequency.setter
	def frequency(self,freq):
		self._frequency = float(freq)
		self.send("FREQ:CW "+str(freq))

	@property
	def power_amplitude(self):
		if self._power_amplitude is None: self._power_amplitude = float(self.query("POW:AMPL?"))
		return self._power_amplitude

	@power_amplitude.setter
	def power_amplitude(self,power):
		if power>self.pow_amp_lim: print(f'{self.dev_name}: max amplitude reached ({power})!')
		power = min(power,self.pow_amp_lim)
		self._power_amplitude = float(power)
		self.send("POW:AMPL "+str(self._power_amplitude))
		
	@Feat
	def rf_state(self):
		return bool(int(self.query("OUTP:STAT?")))

	@rf_state.setter
	def rf_state(self,state):
		self.send("OUTP:STAT "+str(int(bool(state))))

	@Feat
	def mod_state(self):
		return bool(int(self.query("SOUR:MOD:STAT?")))
	
	@mod_state.setter
	def mod_state(self,state):
		self.send("SOUR:MOD:ALL "+str(int(bool(state))))

	# FM

	@Feat
	def fm_dev(self):
		if self._fm_dev is None: self._fm_dev = float(self.query("FM:DEV?"))
		return self._fm_dev

	@fm_dev.setter
	def fm_dev(self,fmDev):
		self._fm_dev = float(fmDev)
		self.send("FM:DEV "+str(self._fm_dev))

	@Feat
	def fm_source(self):
		return self.query("FM:SOUR?")

	@Feat
	def fm_mode(self):
		return self.query("FM:MODE?")

	@Feat
	def fm_state(self):
		return bool(int(self.query("FM:STAT?")))

	@fm_state.setter
	def fm_state(self,state):
		self.send("FM:STAT "+str(int(bool(state))))

	@Feat
	def fm_ext1_coupling(self):
		return self.query("FM:EXT1:COUP?")

	# AM

	@Feat
	def am_depth(self):
		return float(self.query("AM:DEPT?"))

	@am_depth.setter
	def am_depth(self,depth):
		self.send("AM:DEPT {:f}".format(depth))

	@Feat
	def am_ext_coupling(self):
		return self.query("AM:EXT:COUP?")

	@Feat
	def am_source(self):
		return self.query("AM:SOUR?")

	@Feat
	def am_state(self):
		return bool(int(self.query("AM:STAT?")))

	@am_state.setter
	def am_state(self,state):
		self.send("AM:STAT "+str(int(bool(state))))




		
if __name__=='__main__':
	freqSynth = SMB100A(usbDev="USB0::0x0AAD::0x0054::178641::INSTR")