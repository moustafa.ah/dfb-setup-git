import visa

class Marconi2042(object):

	def __init__(self,gpibAdress=20,name=None,pow_amp_lim=10):
		freqSynthID = "GPIB::"+str(gpibAdress)+"::INSTR"
		rm = visa.ResourceManager()
		self.name = name
		self.inst = rm.open_resource(freqSynthID,read_termination='\n')
		self.pow_amp_lim = pow_amp_lim

	def query(self,command):
		self.inst.write(command)
		return self.inst.read()

	def send(self,command):
		self.inst.write(command)

	@property
	def fm_dev(self):
		resp = self.query("FM1?")
		return float(resp.split(";")[0].split(" ")[1])

	@fm_dev.setter
	def fm_dev(self,fmDev):
		self.send("FM1:DEVN "+str(fmDev))
		
	@property
	def frequency(self):
		resp = self.query("CFRQ?")
		return float(resp.split(";")[0].split(" ")[1])
	
	@frequency.setter
	def frequency(self,freq):
		self.send("CFRQ "+str(freq))

	@property
	def power_amplitude(self):
		resp  = self.query("RFLV?")
		return float(resp.split(";")[1].split(" ")[1])
		# return float(self.query("RFLV?"))

	@power_amplitude.setter
	def power_amplitude(self,pow):
		assert pow<=self.pow_amp_lim
		self.send("RFLV "+str(pow))
		
	@property
	def rf_state(self):
		resp  = self.query("RFLV?")
		return resp.split(";")[3] == "ON"

	@rf_state.setter
	def rf_state(self,state):
		if bool(state):
			self.send("RFLV:ON")
		else:
			self.send("RFLV:OFF")

	@property
	def fm_state(self):
		resp = self.query("FM1?")
		return resp.split(";")[2] == "ON"

	@fm_state.setter
	def fm_state(self,state):
		if bool(state): self.send("FM1:ON")
		else: self.send("FM1:OFF")

	@property
	def mod_state(self):
		resp = self.query("MOD?")
		return resp.split(":")[2] == "ON"
	
	@mod_state.setter
	def mod_state(self,state):
		if bool(state): self.send("MOD:ON")
		else: self.send("MOD:OFF")

	# @property
	# def fm_ext1_coupling(self):
	# 	return self.query("FM:EXT1:COUP?")
	
	def getParams(self):
		params = dict()
		params.update({"RFstate":self.rf_state})
		params.update({"FMstate":self.fm_state})
		params.update({"Modstate":self.mod_state})
		params.update({"RFpow":self.power_amplitude})
		params.update({"fmDev":self.fm_dev})
		# params.update({"fmCoup":self.fm_ext1_coupling})
		return {self.name:params}
		
if __name__=='__main__':
	freqSynth = Marconi2042()