from .smb100a import SMB100A
import pyqtgraph.parametertree.parameterTypes as pTypes
from pyqtgraph.parametertree import Parameter, ParameterTree
# from ui.pyqtgraphaddon import MyFloatParameter

class SMB100A_UI(SMB100A):

	def __init__(self,**kwargs):
		super(SMB100A_UI, self).__init__(**kwargs)
		self.param_group = pTypes.GroupParameter(name = self._name+" (SMB100A)")
		self.rf_state_param = Parameter.create(name='RF state',
												type='bool',
												value=self.rf_state)
		self.freq_param = Parameter.create(name='Frequency (Hz)',
												type='float',# type='float2',
												value=self.frequency,
												step = 100,
												decimals = 10)
		self.pow_amp_param = Parameter.create(name='Power',
												type='float', 
												value=self.power_amplitude,
												step = 0.1,
												suffix='dBm')
		self.fm_state_param = Parameter.create(name='FM state',
												type='bool',
												value=self.fm_state)
		self.fm_dev_param = Parameter.create(name='FM dev',
												type='float',
												value=self.fm_dev,
												step = 100,
												suffix='Hz',
												siPrefix=True)
		self.freq_offset_action = Parameter.create(name='Frequency step',
												type='action')
		self.freq_offset_value = Parameter.create(name='Offset',
												type='float',
												value=20,
												step = 1,
												suffix='Hz')
		self.freq_offset_action.addChild(self.freq_offset_value)
		self.param_group.addChildren([self.rf_state_param,
									self.freq_param,
									self.pow_amp_param,
									self.fm_state_param,
									self.fm_dev_param,
									self.freq_offset_action])

		self.freq_offset_action.sigActivated.connect(self.freq_jump)
		self.param_group.sigTreeStateChanged.connect(self.change)

	def freq_jump(self):
		self.frequency = self.frequency + self.freq_offset_value.value()

	def change(self,param,changes):
		for param, change, data in changes:
			if param is self.rf_state_param: self.rf_state = param.value()
			elif param is self.pow_amp_param: SMB100A.power_amplitude.fset(self,param.value())
			elif param is self.fm_state_param: self.fm_state = param.value()
			elif param is self.fm_dev_param: self.fm_dev = param.value()
			elif param is self.freq_param: SMB100A.frequency.fset(self,param.value())

	# @property
	# def frequency(self):
	# 	return SMB100A.frequency.fget(self)

	# @frequency.setter
	# def frequency(self, value):
	# 	self.freq_param.setValue(value)

	# @property
	# def power_amplitude(self):
	# 	return SMB100A.power_amplitude.fget(self)

	# @power_amplitude.setter
	# def power_amplitude(self, value):
	# 	self.pow_amp_param.setValue(value)


class SMB100A_AM_UI(SMB100A):

	def __init__(self,**kwargs):
		super(SMB100A_AM_UI, self).__init__(**kwargs)
		self.param_group = pTypes.GroupParameter(name = self._name)
		self.rf_state_param = Parameter.create(name='RF state',
												type='bool',
												value=self.rf_state)
		self.freq_param = Parameter.create(name='Frequency',
												type='float',
												value=self.frequency,
												step = 1000,
												suffix='Hz')
		self.pow_amp_param = Parameter.create(name='Power',
												type='float', 
												value=self.power_amplitude,
												step = 0.1,
												suffix='dBm')
		self.am_depth_param = Parameter.create(name='AM depth',
												type='float',
												value=self.am_depth,
												step = 0.1,
												suffix='%')
		self.am_state_param = Parameter.create(name='AM state',
												type='bool',
												value=self.am_state)
		self.param_group.addChildren([self.rf_state_param,
									self.freq_param,
									self.pow_amp_param,
									self.am_depth_param,
									self.am_state_param])

		self.param_group.sigTreeStateChanged.connect(self.change)

	def change(self,param,changes):
		for param, change, data in changes:
			if param is self.rf_state_param: self.rf_state = param.value()
			elif param is self.freq_param: self.frequency = param.value()
			elif param is self.pow_amp_param: self.power_amplitude = param.value()
			elif param is self.am_depth_param: self.am_depth = param.value()
			elif param is self.am_state_param: self.am_state = param.value()
			