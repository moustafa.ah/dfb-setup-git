import time,sys, threading
from drivers.nidaq import *
from drivers.lockBox import *
from utils.misc import Buffer
import matplotlib.pyplot as plt
import numpy as np
from pyqtgraph.Qt import QtCore

class CarrierSignalGetter(threading.Thread):

	samp_num = 20

	def __init__(self,bias_ctrl):
			threading.Thread.__init__(self)
			self.daemon = True
			self.bias_ctrl = bias_ctrl
			self.buf = Buffer(self.samp_num)
			self.running = False

	def run(self):
		self.running = True
		while self.running:
			value = self.bias_ctrl.get_carrier()
			self.buf.append(value)
			self.bias_ctrl.newCarrier.emit(value)
			time.sleep(0.1)

	def get_last(self):
		return self.buf.get_data()[-1]

	def get_average(self):
		return np.mean(self.buf.get_data())

	def get_std(self):
		return np.std(self.buf.get_data())

class CarrierSignalCanceller(threading.Thread):

	def __init__(self,bias_ctrl):
		threading.Thread.__init__(self)
		self.daemon = True
		self.bias_ctrl = bias_ctrl
		self.running = False
		
	def run(self):
		print("Starting carrier canceller")
		self.running = True
		while self.running:
			targetVoltage = self.bias_ctrl.targetVoltage
			last = self.bias_ctrl.carrierSignalGetter.get_last()
			average = self.bias_ctrl.carrierSignalGetter.get_average()
			errorM = average - targetVoltage
			errorL = last - targetVoltage
			correction = errorL * self.bias_ctrl.canceller_p_gain + errorM * self.bias_ctrl.canceller_i_gain
			self.bias_ctrl.dc_bias += correction
			# print("Voltage: {}, Correction: {}".format(currentVoltage,correction))
			time.sleep(0.5)
		print("Carrier canceller stopped")

	def stop(self):
		self.running = False

class BiasController2(QtCore.QObject):

	newCarrier = QtCore.Signal(float)
	newBias = QtCore.Signal(float)
	canceller_p_gain = 10
	canceller_i_gain = 5
	targetVoltage = 0.0

	def __init__(self,daq,biasChan="ao1",signalChan="ai0",name=None):
		super(BiasController2, self).__init__() 
		self.daq = daq
		self.signalChan = signalChan
		self.biasChan = biasChan
		self._dc_bias = self.get_bias()
		self.name = name
		self.carrierSignalGetter = CarrierSignalGetter(self)
		self.carrierSignalGetter.start()
		self.carrierSignalCanceller = None

	def __del__(self):
		self.stop_canceller(block=True)
		self.carrierSignalGetter.running = False
		self.carrierSignalGetter.join()

	def get_carrier(self):
		return self.daq.getInputVoltage(channel=self.signalChan,range=(-1,1),samples=1)

	def get_bias(self):
		return np.round(self.daq.read_voltage_on_output(self.biasChan),3)

	@property
	def dc_bias(self):
		return self._dc_bias

	@dc_bias.setter
	def dc_bias(self,value):
		self._dc_bias = value
		self.daq.setOutputVoltage(self._dc_bias,self.biasChan,(-10,10))
		self.newBias.emit(value)

	@property
	def carrier_signal(self):
		if self.carrierSignalGetter.running:
			return self.carrierSignalGetter.get_last()
		return self.get_carrier()

	def start_canceller(self):
		self.carrierSignalCanceller = CarrierSignalCanceller(self)
		self.carrierSignalCanceller.start()

	def stop_canceller(self,block=False):
		if self.carrierSignalCanceller:
			self.carrierSignalCanceller.stop()
			if block: self.carrierSignalCanceller.join()