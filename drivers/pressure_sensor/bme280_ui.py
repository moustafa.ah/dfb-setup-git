# -*- coding: utf-8 -*-
from .bme280 import BME280
from pyqtgraph.graphicsItems.PlotItem import PlotItem
from pyqtgraph import LabelItem

symbolBrushes = [(130, 45, 255), (255, 0, 133), (255, 226, 8), (20, 183, 204), "r"]

class BME280_UI(BME280):

	def make_plots(self, graph_layout=None,title_font_size=3):
		self.plots = []
		self.curves = []
		for idx, header in enumerate(self.header[1:]):
			l = LabelItem("<font size={:d}>{}</font>".format(title_font_size,header))
			# p = PlotItem(title = "<font size={:d}>{}</font>".format(title_font_size,header))
			p = PlotItem()
			c = p.plot( title = header, pen = symbolBrushes[idx % len(symbolBrushes)])
			self.plots.append(p)
			self.curves.append(c)
			if graph_layout is not None:
				graph_layout.addItem(l)
				graph_layout.addItem(p)
				graph_layout.nextRow()

	def update(self):
		if self.buf.index > 0:
			data = self.buf.get()
			t0 = data[0,0]
			for idx, curve in enumerate(self.curves):
				curve.setData(x = data[:,0] - t0 , y = data[:,idx+1])