import serial, threading, time, sys
if __name__ == '__main__':
	sys.path.append('../..')
from base.dataproducer import DataProducer

class BME280(DataProducer):

	header = ["Time","Temperature (degC)","Pressure (Pa)","Humidity (%)"]

	def __init__(self, comPort='com6'):
		DataProducer.__init__(self,maxlength=1000,numcol=len(self.header))
		self.running = False
		self.ser = serial.Serial()
		self.ser.port = comPort
		self.ser.open()

	def __del__(self):
		self.running = False

	def data_poller(self):
		self.running = True
		while self.running:
			line = self.ser.readline().decode()
			if line:
				if line[0] == 'T':
					temperature, pressure, humidity = [float(fragment[2:]) for fragment in line.split(';')]
					data = [time.time(),temperature,pressure,humidity]
					self.buf.append(data)
					self.send_to_consumers(data)
				else: print(line[:-2]+"\n")
			time.sleep(0.1)

	def start(self):
		self.thread = threading.Thread(target=self.data_poller)
		self.thread.start()

	def stop(self):
		self.running = False

if __name__ == '__main__':
	sensor = BME280(comPort='com6')