import visa, threading

class HP34401A(object):

	mode_dc_voltage = "VOLT:DC"
	mode_resistance = "RES"

	def __init__(self,gpibAdress=2):
		gpibID = "GPIB::"+str(gpibAdress)+"::INSTR"
		rm = visa.ResourceManager()
		self.inst = rm.open_resource(gpibID,read_termination='\n')
		self.lock = threading.Lock()

	@property
	def identity(self):
		return self.inst.query('*IDN?')

	def configure(self,mode=mode_dc_voltage,range=1,resolution=1e-6):
		self.inst.write("CONF:{} {}, {}".format(str(mode),float(range),float(resolution)))
		self.set_trig_params()

	def set_trig_params(self):
		self.inst.write("TRIG:SOUR:IMM")
		self.inst.write("TRIG:COUN 1")
		self.init()

	def init(self):
		self.inst.write("INIT")

	def read(self):
		return float(self.inst.query("READ?"))

	def fetch(self):
		return float(self.inst.query("FETC?"))

	def fetch_and_trig(self):
		result = float('nan')
		self.lock.acquire()
		try:
			res_str = self.inst.query("FETC?")
			result = float(res_str)
			if result<-100000.:
				print("Multimeter - bad value detected :"+res_str)
		finally:
			self.init()
			self.lock.release()
		return result

	@property
	def resistance(self):
		return float(self.inst.query("MEAS:RES?"))
	
	@property	
	def dc_voltage(self):
		return float(self.inst.query("MEAS:VOLT:DC?"))


if __name__=='__main__':
	# multimeter = HP34401A()
	# multimeter2 = HP34401A(gpibAdress=16)
	# multimeter3 = HP34401A(gpibAdress=10)
	multimeter = HP34401A(gpibAdress=24)
	# multimeter5 = HP34401A(gpibAdress=13)
	# multimeter6 = HP34401A(gpibAdress=15)
	# multimeter7 = HP34401A(gpibAdress=23)