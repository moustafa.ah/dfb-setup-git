import visa

class LS330(object):

	def __init__(self,gpibAdress=12,name=None):
		gpibID = "GPIB::"+str(gpibAdress)+"::INSTR"
		rm = visa.ResourceManager()
		self.inst = rm.open_resource(gpibID,read_termination='\r')
		self.name = name
		
	def getTemperature(self,channel):
		self.inst.write("SCHN "+channel)	
		self.inst.write("SDAT?")
		return float(self.inst.read())

	# def setTemperature(self,temparature):
		# self.inst.write("FM:DEV "+str(fmDev))
		# return
	
		
if __name__=='__main__':
	tempCtrl = LS330()