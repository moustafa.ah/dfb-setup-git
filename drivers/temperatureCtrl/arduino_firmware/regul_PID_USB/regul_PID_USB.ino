
//#include <RotaryEncoder.h>;
#include <SoftwareSerial.h>   // We need this even if we're not using a SoftwareSerial object
                              // Due to the way the Arduino IDE compiles
#include <SerialCommand.h>
#include <LiquidCrystal.h>
#include <PID_v1.h>
#include <math.h>
//------------LTC2400------ 24 bit ADC
#define CLOCK 4
#define SDout 5
#define CS_ADC 6
//----------AD5541--------- 16 bit DAC
#define DATA_PIN A5
#define CLK_PIN A4
#define CS_DAC A3
#define VREF 2.5 //tension reference adc
//-----------codeur ------------------
#define CODER_KNOB_1_PIN A0
#define CODER_KNOB_2_PIN 2
#define CODER_SWITCH_PIN 3
//---------------------------------------
#define LOOP_RESOLUTION 16777215

SerialCommand SCmd;   // The demo SerialCommand object
LiquidCrystal lcd(13, 8, 9, 10, 11, 12);      //(rs,e,d4,d5,d6,d7) et pin Vo contrast=pin10
word val_init = 0;                          // valeur du dac pour 20° sur 11bits de dynamique (echelle 0 à 150°C maxi)
float T, R, i = 0.0001, epsilon;         // le courant dans la sonde i = 100µA
int mode_index = 0;
int parameter_index = 0;
float parameter_index_float = 0;
volatile long long debounce_timeout = 500;   // Debounce timer : 500 ms
volatile long long last_change_time = 0;

//Parameters
double t_setpoint = 20;
double p_gain = 96;
double i_gain = 10;
double d_gain = 50;
float average = 0;
float sign = 0;

//Temporary variables for parameters
double temp_t_setpoint = t_setpoint;
double temp_p_gain = p_gain;
double temp_i_gain = i_gain;
double temp_d_gain = d_gain;
float temp_average = average;
float temp_sign = sign;

double forced_in=-1;
double temperature, in, heater, d_setpoint;
unsigned int output;

PID myPID(&in, &heater, &d_setpoint, p_gain, i_gain, d_gain, REVERSE);

long int read_adc() {
  byte sig = 0;
  long int ltw = 0;
  byte b0;
  digitalWrite(CLOCK, LOW);
  digitalWrite(CS_ADC, LOW);
  b0 = shiftIn(SDout, CLOCK, MSBFIRST);             // read 4 bytes adc raw data with SPI
  if ((b0 & 0x20) ==0) sig=1;  // is input negative ?
  b0 &=0x1F;                   // discard bit 25..31
  ltw |= b0;
  ltw <<= 8;
  b0 = shiftIn(SDout, CLOCK, MSBFIRST);
  ltw |= b0;
  ltw <<= 8;
  b0 = shiftIn(SDout, CLOCK, MSBFIRST);
  ltw |= b0;
  ltw <<= 8;
  b0 = shiftIn(SDout, CLOCK, MSBFIRST);
  ltw |= b0;
  if (sig) ltw |= 0xf0000000;    // if input negative insert sign bit
  ltw=ltw/16;                    // scale result down , last 4 bits have no information
  return ltw;
}

void write_dac(int data) {
  digitalWrite(CS_DAC, HIGH);
  digitalWrite(CS_DAC, LOW);
  shiftOut(DATA_PIN, CLK_PIN, MSBFIRST, data >> 8); //changer MSB suivant protocole
  shiftOut(DATA_PIN, CLK_PIN, MSBFIRST, data);
  digitalWrite(CS_DAC, LOW);
  digitalWrite(CS_DAC, HIGH);
}

void affichage(float donnee_affichee, int n, int ligne) {
  lcd.setCursor(1, ligne);
  lcd.print(donnee_affichee, n); // n chiffre aprés la virgule
  lcd.print("       ");
}

void affichage_char(char *donnee_affichee, int ligne) {
  lcd.setCursor(1, ligne);
  lcd.print(donnee_affichee); // 1 chiffre aprés la virgule
  lcd.print("       ");
}

double D_to_temp(long int digital_temp) {
  R = (digital_temp * VREF / (16777215 * i));
  T = log(R);
  T = 1 / (0.0012146 + (0.00021922 * T) + (0.00000015244 * T * T * T));
  T = T - 273.15;
  return T;
}

long int temp_to_D(double T) {
  //-------------calcul Steinhart_Hart-----
  float x, y;
  T = T + 273.15;
  x = (0.0012146 - 1 / T) / 0.00000015244;
  y = pow((pow((0.00021922 / 3 / 0.00000015244), 3) + (pow(x, 2) / 4 )), 0.5);
  R = exp(pow((y - x / 2), 0.33333) - pow((y + x / 2), 0.33333));
  long int digital = R * i * 16777215 / VREF;
  return digital;
}

void read_encoder() {
  float val;
  int inc = 0;
  if (digitalRead(CODER_KNOB_2_PIN) == digitalRead(CODER_KNOB_1_PIN)) {
    inc++;
  }
  else {
    inc--;
  }
  val = float(inc) / 2;
  switch (mode_index) {
    case 0: // Parameter selection mode
      parameter_index_float = parameter_index_float + 0.1 * val;
      if (parameter_index_float >= 1){
        parameter_index = parameter_index + 1;
        parameter_index_float = 0;
      }
      if (parameter_index_float <= -1){
        parameter_index = parameter_index - 1;
        parameter_index_float = 0;
      }
      parameter_index  = constrain(parameter_index , 0, 6);
      break;
    case 1:  // Value edition mode

      switch (parameter_index) {
        case 0: //temperature monitoring
          break;
        case 1: //Setpoint
          temp_t_setpoint = temp_t_setpoint + 0.1 * val;
          temp_t_setpoint = constrain(temp_t_setpoint, 5, 50);
          break;
        case 2: //P gain
          temp_p_gain = temp_p_gain + 0.01 * val;
          temp_p_gain = constrain(temp_p_gain, 0.000001, 10);
          break;
        case 3: // I gain
          temp_i_gain = temp_i_gain + val * 0.001;
          temp_i_gain = constrain(temp_i_gain, 0.000000001, 10);
          break;
        case 4: // D gain
          temp_d_gain = temp_d_gain + val * 0.001;
          temp_d_gain = constrain(temp_d_gain, 0, 1000);
          break;
        case 5:  //Averaging
          temp_average = temp_average + val;
          temp_average = constrain(temp_average, 0, 4);
          break;
        case 6: //Error sign
          temp_sign = temp_sign + val;
          temp_sign = constrain(temp_sign, 0, -1);
          break;
      }
  }
}

void update_display() {

  switch (mode_index) {
    case 0:       // In parameter selection mode, add an arrow in front of the parameter name
      lcd.setCursor(0, 0);
      lcd.print(">");
      lcd.setCursor(0, 1);
      lcd.print(" ");
      break;
    case 1:       // In value edition mode, add the arrow before the value
      lcd.setCursor(0, 0);
      lcd.print(" ");
      lcd.setCursor(0, 1);
      lcd.print(">");
      break;
  }
  switch (parameter_index) {
    case 0:  //temperature monitoring
      affichage_char("Temperature", 0); //choix de la ligne (0 lignes en haut)
      affichage(temperature, 3, 1); // choix de la ligne 1 pour ligne du bas)
      break;
    case 1: //Setpoint
      affichage_char("Setpoint", 0);
      affichage(temp_t_setpoint, 0, 1);
      break;
    case 2: //P gain
      affichage_char("P gain", 0);
      affichage(temp_p_gain, 5, 1);
      break;
    case 3: // I gain
      affichage_char("I gain", 0);
      affichage(temp_i_gain, 5, 1);
      break;
    case 4: // D gain
      affichage_char("D gain", 0);
      affichage(temp_d_gain, 5, 1);
      break;
    case 5:  //Averanging
      affichage_char("Average", 0);
      affichage(temp_average, 0, 1);
      break;
    case 6: //Error sign
      affichage_char("Sign", 0);
      affichage(temp_sign, 0, 1);
      break;
  }
}

void change_mode() {
  int thisTime = millis();
  int difference = thisTime - last_change_time; // Get time since last switch push for debouncing

  if (difference > debounce_timeout) {  // This prevents unwanted switch bounces
    last_change_time = thisTime;
    if (parameter_index != 0) {         // You can not go to value edit mode from the temperature monitoring state
      if (mode_index == 1) {
        validation_value();             // Apply change before going back to parameter selection mode
        mode_index = 0;
      }
      else {
        mode_index = 1;                // Go to value edit mode
      }
    }
  }
}

void validation_value() {
  switch (parameter_index) {
    case 0: //temperature monitoring
      break;
    case 1: //Setpoint
      t_setpoint = temp_t_setpoint;
      d_setpoint = temp_to_D(t_setpoint);
      break;
    case 2: //P gain
      p_gain = temp_p_gain;
      break;
    case 3: // I gain
      i_gain = temp_i_gain;
      break;
    case 4: // D gain
      d_gain = temp_d_gain;
      break;
    case 5:  //Averaging
      average = temp_average;
      break;
    case 6: //Error sign
      sign = temp_sign;
      break;
  }
  myPID.SetTunings(p_gain, i_gain, d_gain);
}

void setup() {
  Serial.begin(9600);
  //Serial.print("0");
  pinMode(CODER_SWITCH_PIN, INPUT);
  pinMode(CLK_PIN, OUTPUT);
  pinMode(DATA_PIN, OUTPUT);
  pinMode(CS_DAC, OUTPUT);
  
  pinMode(CLOCK, OUTPUT);
  pinMode(SDout, INPUT);
  pinMode(CS_ADC, OUTPUT);

  attachInterrupt(digitalPinToInterrupt(CODER_KNOB_2_PIN), read_encoder, CHANGE);//interuption rotation encodeur
  attachInterrupt(digitalPinToInterrupt(CODER_SWITCH_PIN), change_mode, RISING); //interruption switch encodeur

  lcd.begin(4, 2);
  write_dac(val_init);
  d_setpoint = temp_to_D(t_setpoint);
  myPID.SetMode(AUTOMATIC);
  myPID.SetTunings(p_gain, i_gain, d_gain); //Set the PID gain constants and start running
  //myPID.SetOutputLimits(-LOOP_RESOLUTION / 2, LOOP_RESOLUTION / 2); // limiteur de courant logiciel
  myPID.SetOutputLimits(0, LOOP_RESOLUTION);

  SCmd.addCommand("T?",get_temp);
  SCmd.addCommand("IN?",get_input);
  SCmd.addCommand("FIN?",get_forced_input);
  SCmd.addCommand("FIN",set_forced_input);
  SCmd.addCommand("OUT?",get_output);
  SCmd.addCommand("C?",get_setpoint);
  SCmd.addCommand("C",set_setpoint); 
  SCmd.addCommand("P?",get_p_gain);
  SCmd.addCommand("P",set_p_gain);
  SCmd.addCommand("I?",get_i_gain);
  SCmd.addCommand("I",set_i_gain);
  SCmd.addCommand("D?",get_d_gain);
  SCmd.addCommand("D",set_d_gain);
  SCmd.addCommand("A?",get_averaging);
  SCmd.addCommand("A",set_averaging);
  SCmd.addCommand("S?",get_error_sign);
  SCmd.addCommand("S",set_error_sign);
  SCmd.addDefaultHandler(unrecognized);  // Handler for command that isn't matched  (says "What?") 
}

void loop() {
  SCmd.readSerial();
  in = read_adc();            // 2exp11 / 150°C = facteur 13.646667  pour une echelle affichée de 0 à 150.0°C
  in = constrain(in, 0, LOOP_RESOLUTION);
  if (forced_in >= 0){
    in = forced_in;
  }
  temperature = D_to_temp(in); // semble fonctionner !
  update_display();
  myPID.Compute();            //Calcul PID
  //output = (heater + LOOP_RESOLUTION / 2) / 256; //Remove the 8 least significant bits from the 24 bit ADC to feed the 16 bit DAC
  //Serial.print("in :");
  //Serial.println(in);
  //Serial.print("d_setpoint :");
  //Serial.println(d_setpoint);
  //Serial.print("heater :");
  //Serial.println(heater);
  output = heater / 256;
  //Serial.print("output :");
  //Serial.println(output);
  write_dac(output);
  delay(200);                 //METTRE SUR 200 minimum pour le temps de conversion ADC!
}

double read_float_arg(){
  char *arg;
  arg = SCmd.next(); 
  if (arg != NULL){
     return atof(arg);
  }
  return NULL;
}

void get_temp(){
  Serial.println(temperature,4);
}

void get_input(){
  Serial.println(in);
}

void get_forced_input(){
  Serial.println(forced_in);
}

void set_forced_input(){
  forced_in = read_float_arg();
}

void get_output(){
  Serial.println(output);
}

void get_setpoint(){
  Serial.println(t_setpoint,4);
}

void set_setpoint(){
  t_setpoint = read_float_arg();
  temp_t_setpoint = t_setpoint;
  d_setpoint = temp_to_D(t_setpoint);
}

void get_p_gain(){
  Serial.println(p_gain,5);
}

void set_p_gain(){
  p_gain = read_float_arg();
  temp_p_gain = p_gain;
  myPID.SetTunings(p_gain, i_gain, d_gain);
}

void get_i_gain(){
  Serial.println(i_gain,5);
}

void set_i_gain(){
  i_gain = read_float_arg();
  temp_i_gain = i_gain;
  myPID.SetTunings(p_gain, i_gain, d_gain);
}

void get_d_gain(){
  Serial.println(d_gain,5);
}

void set_d_gain(){
  d_gain = read_float_arg();
  temp_d_gain = d_gain;
  myPID.SetTunings(p_gain, i_gain, d_gain);
}

void get_averaging(){
  Serial.println(average);
}

void set_averaging(){
  average = read_float_arg();
  temp_average = average;
}

void get_error_sign(){
  Serial.println(sign);
}

void set_error_sign(){
  sign = read_float_arg();
  temp_sign = sign;
}

// This gets set as the default handler, and gets called when no other command matches. 
void unrecognized()
{
  Serial.println("What?"); 
}
