import time
import serial
if __name__=='__main__': 
	import sys
	sys.path.append('../..')
from base.driver import Driver
from base.feat import Feat

class CYPidReg(Driver):

	dev_name = "CYPidReg"
	
	def __init__(self,comPort='com4',name="CYPidReg"):
		super(CYPidReg, self).__init__(name)
		self.waiting = False
		try:
			self.ser = ser = serial.Serial()
			ser.port = comPort
			ser.timeout = 5
			ser.setDTR(False) # This prevents the arduino from rebooting on connect
			ser.open()
		except serial.serialutil.SerialException:
			#no serial connection
			print("The port is at use : reseting")
			ser.close()
			ser.open()
			#self.ser = None
		else:
			pass

	def __del__(self):
		if self.ser:
			self.ser.close()

	def send(self, command):
		self.ser.write((command+"\r\n").encode())

	def query(self, command):
		while self.waiting:
			pass
		self.waiting = True
		self.send(command)
		time.sleep(0.020)
		response = self.ser.readline()[:-2]
		self.waiting = False
		return response

	@property
	def temperature(self):
		return float(self.query("T?"))

	@property
	def output(self):
		return float(self.query("OUT?"))

	@property
	def input(self):
		return float(self.query("IN?"))

	@Feat
	def setpoint(self):
		return float(self.query("C?"))

	@setpoint.setter
	def setpoint(self,temperature):
		self.send("C "+str(temperature))

	@Feat
	def p_gain(self):
		return float(self.query("P?"))

	@p_gain.setter
	def p_gain(self,gain):
		self.send("P "+str(gain))

	@Feat
	def i_gain(self):
		return float(self.query("I?"))

	@i_gain.setter
	def i_gain(self,gain):
		self.send("I "+str(gain))

	@Feat
	def d_gain(self):
		return float(self.query("D?"))

	@d_gain.setter
	def d_gain(self,temperature):
		self.send("D "+str(temperature))

	@Feat
	def average(self):
		return float(self.query("A?"))

	@average.setter
	def average(self,temperature):
		self.send("A "+str(temperature))

	@property
	def error_sign(self):
		return float(self.query("S?"))

	@error_sign.setter
	def error_sign(self,temperature):
		self.send("S "+str(temperature))

	def configure(self,setpoint=None,p_gain=None,i_gain=None,d_gain=None):
		if setpoint: self.setpoint = setpoint
		if p_gain: self.p_gain = p_gain
		if i_gain: self.i_gain = i_gain
		if d_gain: self.d_gain = d_gain
		time.sleep(0.1)
	
if __name__=='__main__':
	regulation = CYPidReg(comPort='com4')