from .cypidreg import CYPidReg
import pyqtgraph.parametertree.parameterTypes as pTypes
from pyqtgraph.parametertree import Parameter, ParameterTree

class CYPidReg_UI(CYPidReg):

	def __init__(self,**kwargs):
		super(CYPidReg_UI, self).__init__(**kwargs)
		self.param_group = pTypes.GroupParameter(name = 'Temperature controller')
		self.t_setpoint_param = Parameter.create(name='T setpoint',
											type='float',
											value=self.setpoint,
											suffix='degC')
		self.p_gain_param = Parameter.create(name='P gain',
											type='float',
											value=self.p_gain)
		self.i_gain_param = Parameter.create(name='I gain',
											type='float',
											value=self.i_gain)
		self.d_gain_param = Parameter.create(name='D gain',
											type='float',
											value=self.d_gain)
		self.param_group.addChildren([self.t_setpoint_param,
									self.p_gain_param,
									self.i_gain_param,
									self.d_gain_param])
		self.param_group.sigTreeStateChanged.connect(self.change)

	def change(self,param,changes):
		for param, change, data in changes:
			if param is self.t_setpoint_param: self.setpoint = param.value()
			elif param is self.p_gain_param: self.p_gain = param.value()
			elif param is self.i_gain_param: self.i_gain = param.value()
			elif param is self.d_gain_param: self.d_gain = param.value()