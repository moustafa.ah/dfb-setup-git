import time,sys
import serial, threading
from cypidreg import CYPidReg
import numpy as np
import Queue
print "Loading pyqtgraph",
import pyqtgraph as pg
from pyqtgraph.dockarea import *
from pyqtgraph.Qt import QtGui, QtCore
import pyqtgraph.parametertree.parameterTypes as pTypes
from pyqtgraph.parametertree import Parameter, ParameterTree, ParameterItem, registerParameterType
print " [DONE]"

num_point_drawn = 50000
regulation = CYPidReg()
time.sleep(3)

class DataImu(object):
	def __init__(self,length,variables):
		self.queue = Queue.Queue()
		self.ptr = 0
		self.data = np.zeros((length,variables),dtype=np.float64)

class DataProducer(threading.Thread):

	header = ["Time","Temperature (degC)","Output"]

	def __init__(self,queue):
		threading.Thread.__init__(self)
		self.daemon = True
		self.running = True
		self.queue = queue

	def stop(self):
		self.running = False

	def run(self):
		print("Data producer starting")
		while self.running:
			self.queue.put([time.time(),regulation.temperature,regulation.output])
			time.sleep(0.2)

class DataConsumer(threading.Thread):

	def __init__(self,dataImu,logfile=None, fileheader=None):
		threading.Thread.__init__(self)
		self.daemon = True
		self.running = True
		self.dataImu = dataImu
		self.logfile = logfile
		if self.logfile:
			self.logfile.write(log_separator.join(fileheader))

	def run(self):
		print("DataConsumer waiting for data")
		while self.running:
			data = self.dataImu.queue.get()
			if self.logfile:
				self.logfile.write(log_separator.join(["%.6f"%(item) for item in data])+"\n")
			if self.dataImu.ptr>=num_point_drawn:
				self.dataImu.data = np.roll(self.dataImu.data,-1,0)
				self.dataImu.data[-1] = data
				
			else:
				self.dataImu.data[self.dataImu.ptr] = data
				self.dataImu.ptr += 1

	def stop(self):
		self.logfile = None
		self.running = False

class DataLogger(object):

	def __init__(self):

		self.consThread = None
		self.dataImu = DataImu(num_point_drawn,3)
		self.running = False

	def __del__(self):
		self.stop()

	def start(self):
		assert not self.running
		self.running = True
		self.dataImu = DataImu(num_point_drawn,3)
		self.prodThread = DataProducer(self.dataImu.queue)
		self.consThread = DataConsumer(self.dataImu,None,self.prodThread.header)
		self.consThread.start()
		self.prodThread.start()

	def stop(self):
		if self.running:
			self.running = False
			self.consThread.stop()
			self.prodThread.stop()

dl = DataLogger()

class ControlWidget(pg.LayoutWidget):
	def __init__(self, parent=None):
		super(ControlWidget, self).__init__(parent)
		self.par = parent
		self.startTempMonBtn = QtGui.QPushButton()
		self.startTempMonBtn.setText("Start Temperature Monitoring")
		self.startTempMonBtn.setCheckable(True)

		self.startTempMonBtn.clicked.connect(self.on_startTempMonBtn)

		gp1 = pTypes.GroupParameter(name='Temperature controller')
		self.t_setpoint_param = Parameter.create(name='T setpoint', type='float', value=regulation.setpoint)
		self.p_gain_param = Parameter.create(name='P gain', type='float', value=regulation.p_gain)
		self.i_gain_param = Parameter.create(name='I gain', type='float', value=regulation.i_gain)
		self.d_gain_param = Parameter.create(name='D gain', type='float', value=regulation.d_gain)
		gp1.addChildren([self.t_setpoint_param,self.p_gain_param,self.i_gain_param,self.d_gain_param])

		params = [gp1]
		self.p = Parameter.create(name='params', type='group', children=params)
		t = ParameterTree()
		t.setParameters(self.p, showTop=False)

		self.p.sigTreeStateChanged.connect(self.change)

		self.addWidget(self.startTempMonBtn,row=0, col=0)
		self.addWidget(t,row=1, col=0)

	def change(self,param,changes):
		for param, change, data in changes:
			if param is self.t_setpoint_param: regulation.setpoint = param.value()
			elif param is self.p_gain_param: regulation.p_gain = param.value()
			elif param is self.i_gain_param: regulation.i_gain = param.value()
			elif param is self.d_gain_param: regulation.d_gain = param.value()

	def on_startTempMonBtn(self):
		if self.startTempMonBtn.isChecked():
			dl.start()
			self.startTempMonBtn.setText("Stop Temperature Monitoring")
		else:
			dl.stop()
			self.startTempMonBtn.setText("Start Temperature Monitoring")


class LiveGraphWidget(pg.GraphicsLayoutWidget):
	def __init__(self, parent=None):
		super(LiveGraphWidget, self).__init__(parent)
		self.p = self.addPlot()
		self.p.showGrid(x=True, y=True, alpha=0.8)
		self.curve = self.p.plot(pen='r')
		# self.timer = QtCore.QTimer()
		# self.timer.timeout.connect(self.update)
		self.p.setLabel('left', "Temperature", units='degC')
		self.p.setLabel('bottom', "Time", units='s')

		self.nextRow()
		self.p2 = self.addPlot()
		self.p2.showGrid(x=True, y=True, alpha=0.8)
		self.curve2 = self.p2.plot(pen='c')
		self.p2.setLabel('left', "Output", units='degC')
		self.p2.setLabel('bottom', "Time", units='s')
		self.p2.setXLink(self.p)

		self.timer = QtCore.QTimer()
		self.timer.timeout.connect(self.update)
		self.timer.start(50)

	def update(self):
		currentDataPtr = dl.dataImu.ptr -1
		if currentDataPtr > 2:
			pcTimes = dl.dataImu.data[0:currentDataPtr,0]
			self.curve.setData(x=pcTimes, y=dl.dataImu.data[0:currentDataPtr,1])
			self.curve2.setData(x=pcTimes, y=dl.dataImu.data[0:currentDataPtr,2])


class MyWindow(QtGui.QMainWindow):
	def __init__(self):
		QtGui.QMainWindow.__init__(self)
		self.setWindowTitle("qtCyPidReg")
		self.resize(1400,900)

		self.control_widget = ControlWidget(self)
		self.data_graph_widget = LiveGraphWidget(self)

		area = DockArea()
		self.setCentralWidget(area)
		d1 = Dock("Controls", size=(400, 1))
		d2 = Dock("Graphs", size=(1000, 1))
		area.addDock(d1, 'left')
		area.addDock(d2, 'right')
		d1.addWidget(self.control_widget)
		d2.addWidget(self.data_graph_widget)

app = QtGui.QApplication([])
win = MyWindow()
win.show()

if (sys.flags.interactive != 1) or not hasattr(QtCore, 'PYQT_VERSION'):
	ret = QtGui.QApplication.instance().exec_()
	sys.exit(ret)