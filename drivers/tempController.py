from drivers.powerMeter import *
from drivers.stepper import *
import time,sys
sys.path.append("..")
from utils.calculs import *

convertors = {"BETHAT5K":betaTherm5kRes2Temp,"BETHAT10K":betaTherm10kRes2Temp,"ULMBETHA":resToTempBetaModel}

class TempController(object):

	def __init__(self,multimeter,name=None,char="BETHAT5K",convertorVtoRes = None):
		self.multimeter = multimeter
		self.name = name
		self.convertor = convertors[char]
		self.convertorVtoRes = convertorVtoRes
		# if convertorVtoRes is None: self.multimeter.configure(mode="RES",range=10000,resolution=1)

	@property
	def temperature(self):
		resistance = self.multimeter.fetch_and_trig()
		if self.convertorVtoRes:
			resistance = self.convertorVtoRes(resistance)
		return self.convertor(resistance)