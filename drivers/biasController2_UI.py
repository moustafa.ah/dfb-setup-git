from biasController2 import BiasController2
import pyqtgraph.parametertree.parameterTypes as pTypes
from pyqtgraph.parametertree import Parameter, ParameterTree

class BiasController2_UI(BiasController2):

	def __init__(self,*args,**kwargs):
		super(BiasController2_UI, self).__init__(*args,**kwargs)
		self.param_group = pTypes.GroupParameter(name = 'EOM Bias controller')
		self.eom_dc_bias_param = Parameter.create(name='DC bias',
												type='float',
												value=self.dc_bias,
												step= 0.01,
												suffix='V')
		self.carrier_target_param = Parameter.create(name='Carrier target',
													type='float',
													value= 0.0,
													step= 0.001,
													suffix='V')
		self.carrier_sig_param = Parameter.create(name='Carrier sig.',
													type='float',
													value=0.0,
													suffix='V',
													readonly=True)
		self.eom_dc_bias_lock_param = Parameter.create(name='Bias lock',
													type='bool',
													value=False)
		self.canceller_p_gain_param = Parameter.create(name='Canceller P gain',
													type='float',
													value=self.canceller_p_gain,
													step= 0.01)
		self.canceller_i_gain_param = Parameter.create(name='Canceller I gain',
													type='float',
													value=self.canceller_i_gain,
													step= 0.01)
		self.param_group.addChildren([self.eom_dc_bias_param,
										self.carrier_target_param,
										self.carrier_sig_param,
										self.eom_dc_bias_lock_param,
										self.canceller_p_gain_param,
										self.canceller_i_gain_param])
		self.param_group.sigTreeStateChanged.connect(self.change)
		self.eom_dc_bias_param.sigValueChanged.connect(self.set_bias_value)
		self.newCarrier.connect(self.update_carrier_value)
		self.newBias.connect(self.update_bias_value)


	def update_carrier_value(self,newValue):
		self.carrier_sig_param.setValue(newValue)

	def update_bias_value(self,newValue):
		self.eom_dc_bias_param.setValue(newValue)

	def set_bias_value(self,param):
		self.dc_bias = param.value()

	def change(self,param,changes):
		for param, change, data in changes:
			# if param is self.eom_dc_bias_param: self.dc_bias = param.value()
			if param is self.carrier_target_param: self.targetVoltage = param.value()
			elif param is self.eom_dc_bias_lock_param:
				if param.value():
					self.eom_dc_bias_param.sigValueChanged.disconnect(self.set_bias_value)
					self.eom_dc_bias_param.setWritable(False)
					self.start_canceller()
				else:
					self.stop_canceller()
					self.eom_dc_bias_param.setWritable(True)
					self.eom_dc_bias_param.sigValueChanged.connect(self.set_bias_value)
			elif param is self.canceller_p_gain_param: self.canceller_p_gain = param.value()
			elif param is self.canceller_i_gain_param: self.canceller_i_gain = param.value()