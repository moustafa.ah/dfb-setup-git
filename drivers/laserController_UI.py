from laserController import LaserController
import pyqtgraph.parametertree.parameterTypes as pTypes
from pyqtgraph.parametertree import Parameter, ParameterTree

class LaserController_UI(LaserController):

	def __init__(self,*args,**kwargs):
		super(LaserController_UI, self).__init__(*args,**kwargs)
		self.param_group = pTypes.GroupParameter(name = 'Laser controller')
		self.laser_dc_offset_param = Parameter.create(name='DC offset',
														type='float',
														value=self.dc_offset,
														step= 0.1,
														suffix='V')
		self.param_group.addChildren([self.laser_dc_offset_param])
		self.param_group.sigTreeStateChanged.connect(self.change)

	def change(self,param,changes):
		for param, change, data in changes:
			if param is self.laser_dc_offset_param: self.dc_offset = param.value()