import clr, sys
sys.path.append('C:\\Program Files\\IVI Foundation\\VISA\\VisaCom64\\Primary Interop Assemblies')
clr.AddReference("Thorlabs.TLPM_64.Interop")
# clr.AddReference("C:\\Program Files\\IVI Foundation\\VISA\\VisaCom64\\Primary Interop Assemblies\\Thorlabs.TLPM_64.Interop")
clr.AddReference("System.Runtime")
clr.AddReference("System.Runtime.InteropServices")
from Thorlabs.TLPM_64.Interop import TLPM
from System.Runtime.InteropServices import HandleRef, ExternalException
from System import String
from System.Text import StringBuilder
if __name__ == '__main__':
	import sys
	sys.path.append('../..')
from base.driver import Driver

def list_devices():
	instr_handle = HandleRef()
	searchDevice = TLPM(instr_handle.Handle)
	_, count = searchDevice.findRsrc(0)
	print(count)
	descr = StringBuilder(1024)
	for i in range(count):
		try:
			searchDevice.getRsrcName(i,descr)
			print(descr)
		except ExternalException: break
	searchDevice.Dispose()

if __name__=='__main__':
	list_devices()

class PM16(Driver):

	dev_name = 'PM16'

	def __init__(self,dev_ref='USB0::0x1313::0x807B::15110239::INSTR',name='Input power'):
		super(PM16, self).__init__(name)
		self.tlpm = TLPM(dev_ref, False, False)

	@property
	def identity(self):
		sb1 = StringBuilder(1024)
		sb2 = StringBuilder(1024)
		sb3 = StringBuilder(1024)
		sb4 = StringBuilder(1024)
		self.tlpm.identificationQuery(sb1,sb2,sb3,sb4)
		return ','.join([sb1.ToString(),sb2.ToString(),sb3.ToString(),sb4.ToString()])

	@property
	def power(self):
		return self.tlpm.measPower(0)[1]*1e6

	@property
	def auto_range(self):
		return self.tlpm.getPowerAutorange(0)[1]

if __name__=='__main__':
	power_meter = PM16()