import time,sys, threading
import numpy as np
from pyqtgraph.Qt import QtCore
from base.dataproducer import DataProducer
from PyDAQmx import *
from ctypes import byref

class PowerControllerAOM(DataProducer):

	def __init__(self,powmeter,aom_fs,aom_daqchan=None,beam_splitter_factor=1.,asserv=None,name="powerControllerAOM"):
		self.header = ["Time","Input power (uW)","AOM power (dBm)","AOM AM (V)"]
		DataProducer.__init__(self,maxlength=1000,numcol=len(self.header))
		self.powmeter = powmeter
		self.aom_fs = aom_fs
		self.asserv = asserv
		self.beam_splitter_factor = beam_splitter_factor
		self.name = name
		self.servo_state = False
		self.signal_servo_state = False
		self.signal_setpoint = None
		self.setpoint = 28.0
		self.gain = 0.005
		self.output_max = -2.
		self.output_min = -40.
		self.thread = None
		self.aom_daqchan = aom_daqchan

	def worker(self):
		self.running = True
		taskHandle = TaskHandle()
		DAQmxCreateTask("",byref(taskHandle))
		DAQmxCreateAOVoltageChan(taskHandle,self.aom_daqchan,"",-10,10,DAQmx_Val_Volts,None)
		DAQmxStartTask(taskHandle)
		voltage = 0
		DAQmxWriteAnalogF64(taskHandle,1,0,-1,DAQmx_Val_GroupByChannel, np.array(voltage, dtype=np.float64),None,None)
		aom_power = self.aom_fs.power_amplitude
		while self.running:
			real_power = self.beam_splitter_factor * self.powmeter.power
			if self.servo_state:
				if self.signal_servo_state:
					mean_signal = np.mean(self.asserv.powerTab)
					if self.signal_setpoint is None: self.signal_setpoint = mean_signal
					voltage += (self.signal_setpoint-mean_signal) * self.gain * 200.
				#aom_power += (self.setpoint-real_power) * self.gain
				#aom_power = min(max(aom_power,self.output_min),self.output_max)
				#self.aom_fs.power_amplitude = aom_power
				else:
					self.signal_setpoint = None
					voltage += (self.setpoint-real_power) * self.gain
				voltage = min(max(voltage,-10),10)
				DAQmxWriteAnalogF64(taskHandle,1,0,-1,DAQmx_Val_GroupByChannel, np.array(voltage, dtype=np.float64),None,None)
			data = [time.time(),real_power,self.aom_fs.power_amplitude,voltage]
			self.buf.append(data)
			self.send_to_consumers(data)
			time.sleep(0.1)
		DAQmxStopTask(taskHandle)
		DAQmxClearTask(taskHandle)

	def start(self):
		self.thread = threading.Thread(target=self.worker)
		self.thread.start()

	def stop(self,block=False):
		self.running = False
