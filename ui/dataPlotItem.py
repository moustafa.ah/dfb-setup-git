from pyqtgraph.graphicsItems.LabelItem import LabelItem
from pyqtgraph.graphicsItems.PlotItem import PlotItem
import numpy as np

class DataPlotItem(object):

	def __init__(self, label_text, cons_thread, index, color="w"):
		self.label = LabelItem(label_text)
		self.cons_thread = cons_thread
		self.index = index
		self.stats_label = LabelItem("Stats")
		self.pi = PlotItem()
		self.color = color
		self.curve = self.pi.plot(pen = self.color)
		self.pi.setDownsampling(mode='peak')
		self.pi.setClipToView(True)
		self.pi.showGrid(x=True, y=True, alpha=1)

	def addItems(self,graphic_layout,hideXAxis=True):
		graphic_layout.addItem(self.label,col=0)
		graphic_layout.addItem(self.stats_label,col=1)
		graphic_layout.nextRow()
		graphic_layout.addItem(self.pi,colspan=2)
		graphic_layout.nextRow()
		if hideXAxis:
			self.pi.hideAxis('bottom')

	def update_plot(self):
		currentDataPtr = self.cons_thread.data.ptr - 1
		pcTimes = self.cons_thread.data.array[0:currentDataPtr,0]
		self.curve.setData(x=pcTimes, y=self.cons_thread.data.array[0:currentDataPtr,self.index])

	def update_stats(self):
		currentDataPtr = self.cons_thread.data.ptr -1
		mean = np.mean(self.cons_thread.data.array[0:currentDataPtr,self.index])
		std_dev = np.std(self.cons_thread.data.array[0:currentDataPtr,self.index])
		self.stats_label.setText("Mean: {:.3f}   Std dev: {:.5f}".format(mean,std_dev))
