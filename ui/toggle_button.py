from pyqtgraph.Qt import QtGui, QtCore

class ToggleButton(QtGui.QPushButton):
	def __init__(self, label, startable_object):
		super(ToggleButton, self).__init__("Start "+label)
		self.label = label
		self.startable_object = startable_object
		self.setCheckable(True)
		self.clicked.connect(self.on_button_clicked)

	def on_button_clicked(self,object):
		if self.isChecked():
			if self.startable_object.start():
				self.setText("Stop "+self.label)
			else: self.setChecked(False)
		else:
			self.startable_object.stop()
			self.setText("Start "+self.label)