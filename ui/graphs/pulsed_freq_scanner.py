import pyqtgraph as pg
from pyqtgraph.dockarea import *
from pyqtgraph.Qt import QtGui, QtCore
import numpy as np
from ui.toggle_button import ToggleButton

class PulseFreqScanner(object):

	fm_dev = None
	running = False
	#param_group_name = "Freq. Scan Pulsed"

	def __init__(self,syncAiAoCPT_pulsed,freq_synth):
		self.syncAiAoCPT_pulsed = syncAiAoCPT_pulsed
		self.freq_synth = freq_synth


		self.gw = pg.GraphicsLayoutWidget()
		self.scan_plot = self.gw.addPlot()
		self.time_plot = self.gw.addPlot()
		self.scan_plot.showGrid(x=True, y=True, alpha=0.8)
		self.scan_plot.setLabel('left', "Signal", units='V')
		self.scan_plot.setLabel('bottom', "Detuning", units='Hz')
		self.point = self.scan_plot.plot(pen=(200,200,200), symbolBrush = (255,0,0), symbolPen ='w')
		self.curve = self.scan_plot.plot(pen="c")
		self.do_curve = self.time_plot.plot(pen="r")
		self.ai_curve = self.time_plot.plot(pen="w")
		self.aiaux_curve = self.time_plot.plot(pen="c")
		self.timer = QtCore.QTimer()
		self.timer.timeout.connect(self.update)

		self.startBtn = ToggleButton("Pulsed Frequency Scan", self)

	def update(self):
		x, y = self.syncAiAoCPT_pulsed.AOdata, self.syncAiAoCPT_pulsed.AImean
		self.curve.setData(x=x * self.fm_dev/self.syncAiAoCPT_pulsed.vpp,y=y)

		xpoint, ypoint = np.array([self.syncAiAoCPT_pulsed.AOdata[self.syncAiAoCPT_pulsed.ptr-1]]),\
						 np.array([self.syncAiAoCPT_pulsed.AImean[self.syncAiAoCPT_pulsed.ptr-1]])
		self.point.setData(x=xpoint* self.fm_dev/self.syncAiAoCPT_pulsed.vpp, y=ypoint)
		self.ai_curve.setData(self.syncAiAoCPT_pulsed.AIdata[0])
		self.aiaux_curve.setData(self.syncAiAoCPT_pulsed.AIdata[1])
		

	def update_label(self,object):
		self.frequency = self.freq_synth.frequency
		self.scan_plot.setLabel('bottom', "Frequency detuning from {:.2f} Hz".format(self.frequency), units='Hz')

	def update_fm_dev(self,object):
		# self.curve.clear()
		self.fm_dev = self.freq_synth.fm_dev

	def start(self):
		self.syncAiAoCPT_pulsed.start()
		self.fm_dev = self.freq_synth.fm_dev
		self.update_label(None)
		self.freq_synth.freq_param.sigValueChanged.connect(self.update_label)
		self.freq_synth.fm_dev_param.sigValueChanged.connect(self.update_fm_dev)
		self.timer.start(50)
		self.running = True
		try: self.dock.raiseDock()
		except:	pass
		return True

	def stop(self):
		self.syncAiAoCPT_pulsed.stop()
		self.timer.stop()
		self.freq_synth.freq_param.sigValueChanged.disconnect(self.update_label)
		self.freq_synth.fm_dev_param.sigValueChanged.disconnect(self.update_fm_dev)
		self.running = False