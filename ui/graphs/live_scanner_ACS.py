import pyqtgraph as pg
from pyqtgraph.Qt import QtGui, QtCore
import pyqtgraph.parametertree.parameterTypes as pTypes
from pyqtgraph.parametertree import Parameter, ParameterTree
import numpy as np
from tasks.fitter import *
import yaml
from ui.toggle_button import ToggleButton

class LiveScanner(object):

	running = False


	def __init__(self,asserv):
		self.asserv = asserv

		self.gw = pg.GraphicsLayoutWidget()
		labelStyle = {'color': '#FFF', 'size': '10pt'}
		self.meas_lbl = self.gw.addLabel("Live graph",**labelStyle)
		self.gw.nextRow()
		self.p = self.gw.addPlot()
		self.p.showGrid(x=True, y=True, alpha=0.8)
		self.p.setLabel('left', "Signals", units='V')
		self.p.setLabel('bottom', "Sample", units='')
		self.AOFreqCurve = self.p.plot(pen="m")
		self.AOPCurve = self.p.plot(pen="g")
		self.AICPT_Curve = self.p.plot(pen="r")
		self.AIP_Curve = self.p.plot(pen="y")
		self.DOCurve = self.p.plot(pen="d")
		self.epsCurve_A = self.p.plot(pen="d")
		self.epsCurve_B = self.p.plot(pen="g")
		self.epsCurve_C = self.p.plot(pen="r")
		self.epsCurve_D = self.p.plot(pen="m")
		


		self.legend = pg.LegendItem((100,60), offset=(70,30))
		self.legend.setVisible(False)
		self.legend.setParentItem(self.p.graphicsItem()) 
		self.timer = QtCore.QTimer()
		self.timer.timeout.connect(self.update)

		self.startBtn = ToggleButton("CW asserv", self)

	def update(self):

		
		
		y_AICPT_rolled = np.roll(self.asserv.pdcpt_data, -1)	
		y_AIP_rolled = np.roll(self.asserv.pdincell_data, -1)		
		self.AICPT_Curve.setData(y=y_AICPT_rolled)
		self.AIP_Curve.setData(y=y_AIP_rolled)
		
		self.epsCurve_A.setData(y=self.asserv.eps_A)
		self.epsCurve_B.setData(y=self.asserv.eps_B)
		self.epsCurve_C.setData(y=self.asserv.eps_C)
		self.epsCurve_D.setData(y=self.asserv.eps_D)


		if self.asserv.ptr == 1:
			self.p.enableAutoRange('xy', False)

	def start(self):
		if self.asserv.start():		
			self.timer.start(50)

			# plot once the output data that will not change with time
			y_AOFre = np.repeat(self.asserv.lo_fm_out_data, self.asserv.n_samp_per_cycle//8)
			y_AOP = np.repeat(self.asserv.rf_am_aom_out_data, self.asserv.n_samp_per_cycle//8)
			y_DO = np.repeat(self.asserv.switch_data, self.asserv.n_samp_per_cycle//8)
			self.AOFreqCurve.setData(y=y_AOFre)
			self.AOPCurve.setData(y=y_AOP)
			self.DOCurve.setData(y=y_DO)

			return True
		return False
		

	def stop(self):
		self.asserv.stop()
		self.timer.stop()	
		self.running = False 