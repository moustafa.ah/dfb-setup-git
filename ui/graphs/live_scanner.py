import pyqtgraph as pg
# from pyqtgraph.dockarea import *
from pyqtgraph.Qt import QtGui, QtCore
import pyqtgraph.parametertree.parameterTypes as pTypes
from pyqtgraph.parametertree import Parameter, ParameterTree
import numpy as np
from tasks.fitter import *
import yaml
from ui.toggle_button import ToggleButton

class LiveScanner(object):

	fm_dev = None
	running = False
	# param_group_name = "Freq. Scan CW"

	def __init__(self,asserv):
		self.asserv = asserv

		# self.dock = Dock("CW CPT signal", size=(800, 1))

		self.gw = pg.GraphicsLayoutWidget()
		labelStyle = {'color': '#FFF', 'size': '10pt'}
		self.meas_lbl = self.gw.addLabel("Live graph",**labelStyle)
		self.gw.nextRow()
		self.p = self.gw.addPlot()
		self.p.showGrid(x=True, y=True, alpha=0.8)
		self.p.setLabel('left', "Signals", units='V')
		self.p.setLabel('bottom', "Sample", units='')
		self.AOFreqCurve = self.p.plot(pen="m")
		self.AOPCurve = self.p.plot(pen="g")
		self.AICurve = self.p.plot(pen="r")
		self.AICurve_1 = self.p.plot(pen="d")
		self.AICurve_2 = self.p.plot(pen="d")
		self.AICurve_3 = self.p.plot(pen="d")
		self.AICurve_4 = self.p.plot(pen="d")
		self.AICurve_5 = self.p.plot(pen="d")
		self.AICurve_6 = self.p.plot(pen="d")
		self.AICurve_7 = self.p.plot(pen="d")
		self.AICurve_8 = self.p.plot(pen="d")
		self.epsCurve_A = self.p.plot(pen="d")
		self.epsCurve_B = self.p.plot(pen="g")
		self.epsCurve_C = self.p.plot(pen="r")
		self.epsCurve_D = self.p.plot(pen="m")
		self.diffCurve = self.p.plot(pen="b")



		self.legend = pg.LegendItem((100,60), offset=(70,30))
		self.legend.setVisible(False)
		self.legend.setParentItem(self.p.graphicsItem()) 
		self.timer = QtCore.QTimer()
		self.timer.timeout.connect(self.update)
		# self.dock.addWidget(self.gw)
		# self.make_param_group()

		self.startBtn = ToggleButton("CW asserv live scanner", self)

	# def make_param_group(self):
	# 	self.param_group = pTypes.GroupParameter(name=self.param_group_name)
	# 	self.param_group.addChild(self.syncAiAo.param_group)
	# 	self.fit_action = Parameter.create(name='Fit',
	# 										type='action')
	# 	self.show_data_cruve = Parameter.create(name='Show data curve',
	# 											type='bool',
	# 											value=True)
	# 	self.show_mean_cruve = Parameter.create(name='Show mean curve',
	# 											type='bool',
	# 											value=True)
	# 	self.show_fit_cruve = Parameter.create(name='Show fitting curve',
	# 											type='bool',
	# 											value=True)
	# 	self.param_group.addChildren([self.fit_action,
	# 									self.show_data_cruve,
	# 									self.show_mean_cruve,
	# 									self.show_fit_cruve])
	# 	self.param_group.sigTreeStateChanged.connect(self.change)
	# 	self.fit_action.sigActivated.connect(self.fit)

	# def change(self,param,changes):
	# 	for param, change, data in changes:
	# 		if param is self.show_data_cruve: self.curve.setVisible(param.value())
	# 		elif param is self.show_mean_cruve: self.meanCurve.setVisible(param.value())
	# 		elif param is self.show_fit_cruve:
	# 			self.fitCurve.setVisible(param.value())
	# 			self.legend.setVisible(param.value())

	def update(self):
		y_AOP = self.asserv.rf_am_aom_out_data
		y_AOFre = self.asserv.lo_fm_out_data
		y_AI = self.asserv.pdcpt_data
		y_AI_1 = self.asserv.pdcpt_data[self.asserv.nbSampCropped+1:(self.asserv.n_samp_per_cycle//8)+1]
		# print(y_AI_1)
		y_AI_2 = self.asserv.pdcpt_data[(self.asserv.n_samp_per_cycle//8)+self.asserv.nbSampCropped+1:(self.asserv.n_samp_per_cycle//4)+1]
		y_AI_3 = self.asserv.pdcpt_data[(self.asserv.n_samp_per_cycle//4)+self.asserv.nbSampCropped+1:(3*self.asserv.n_samp_per_cycle//8)+1]
		y_AI_4 = self.asserv.pdcpt_data[(3*self.asserv.n_samp_per_cycle//8)+self.asserv.nbSampCropped+1:self.asserv.n_samp_per_cycle//2+1]
		y_AI_5 = self.asserv.pdcpt_data[(self.asserv.n_samp_per_cycle//2)+self.asserv.nbSampCropped+1:(5*self.asserv.n_samp_per_cycle//8)+1]
		y_AI_6 = self.asserv.pdcpt_data[(5*self.asserv.n_samp_per_cycle//8)+self.asserv.nbSampCropped+1:(6*self.asserv.n_samp_per_cycle//8)+1]
		y_AI_7 = self.asserv.pdcpt_data[(6*self.asserv.n_samp_per_cycle//8)+self.asserv.nbSampCropped+1:(7*self.asserv.n_samp_per_cycle//8)+1]
		y_AI_8 = np.hstack((self.asserv.pdcpt_data[(7*self.asserv.n_samp_per_cycle//8)+self.asserv.nbSampCropped+1:self.asserv.n_samp_per_cycle], self.asserv.pdcpt_data[0]))

		eps_A = y_AI_2 - y_AI_1
		eps_B = y_AI_4 - y_AI_3
		eps_C = y_AI_5 - y_AI_6
		eps_D = y_AI_7 - y_AI_8

		diff = eps_A + eps_B + eps_C + eps_D

		x1 = np.arange(self.asserv.nbSampCropped+1, (self.asserv.n_samp_per_cycle//8)+1)
		x2 = np.arange((self.asserv.n_samp_per_cycle//8)+self.asserv.nbSampCropped+1, (self.asserv.n_samp_per_cycle//4)+1)
		x3 = np.arange((self.asserv.n_samp_per_cycle//4)+self.asserv.nbSampCropped+1, (3*self.asserv.n_samp_per_cycle//8)+1)
		x4 = np.arange((3*self.asserv.n_samp_per_cycle//8)+self.asserv.nbSampCropped+1, self.asserv.n_samp_per_cycle//2+1)
		x5 = np.arange((self.asserv.n_samp_per_cycle//2)+self.asserv.nbSampCropped+1, (5*self.asserv.n_samp_per_cycle//8)+1)
		x6 = np.arange((5*self.asserv.n_samp_per_cycle//8)+self.asserv.nbSampCropped+1, (6*self.asserv.n_samp_per_cycle//8)+1)
		x7 = np.arange((6*self.asserv.n_samp_per_cycle//8)+self.asserv.nbSampCropped+1, (7*self.asserv.n_samp_per_cycle//8)+1)
		x8 = np.hstack((np.arange((7*self.asserv.n_samp_per_cycle//8)+self.asserv.nbSampCropped+1, self.asserv.n_samp_per_cycle), np.array([0])))
		
		
		self.AICurve_1.setData(x=x1, y=y_AI_1)
		self.AICurve_2.setData(x=x2, y=y_AI_2)
		self.AICurve_3.setData(x=x3, y=y_AI_3)
		self.AICurve_4.setData(x=x4, y=y_AI_4)
		self.AICurve_5.setData(x=x5, y=y_AI_5)
		self.AICurve_6.setData(x=x6, y=y_AI_6)
		self.AICurve_7.setData(x=x7, y=y_AI_7)
		self.AICurve_8.setData(x=x8, y=y_AI_8)
		self.AOFreqCurve.setData(y=y_AOFre)
		self.AOPCurve.setData(y=y_AOP)
		self.AICurve.setData(y=y_AI)
		self.epsCurve_A.setData(y=eps_A)
		self.epsCurve_B.setData(y=eps_B)
		self.epsCurve_C.setData(y=eps_C)
		self.epsCurve_D.setData(y=eps_D)
		self.diffCurve.setData(y=diff)

		if self.asserv.ptr == 1:
			self.p.enableAutoRange('xy', False)

	# def update_label(self,object):
	# 	self.frequency = self.freq_synth.frequency
	# 	self.p.setLabel('bottom', "Frequency detuning from {:.2f} Hz".format(self.frequency), units='Hz')

	# def update_fm_dev(self,object):
	# 	# self.curve.clear()
	# 	self.fm_dev = self.freq_synth.fm_dev

	def start(self):
		if self.asserv.start():
			print("Toto")
			# self.param_group.setName(self.param_group_name+" (Active)")
			# self.fm_dev = self.freq_synth.fm_dev
			# self.update_label(None)
			# self.freq_synth.freq_param.sigValueChanged.connect(self.update_label)
			# self.freq_synth.fm_dev_param.sigValueChanged.connect(self.update_fm_dev)				
			self.timer.start(50)
			# self.running = True
			# try: self.dock.raiseDock()
			# except:	pass
			return True
		return False
		

	def stop(self):
		# self.param_group.setName(self.param_group_name)
		self.asserv.stop()
		self.timer.stop()
		# self.freq_synth.freq_param.sigValueChanged.disconnect(self.update_label)
		# self.freq_synth.fm_dev_param.sigValueChanged.disconnect(self.update_fm_dev)		
		self.running = False 