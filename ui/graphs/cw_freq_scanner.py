import pyqtgraph as pg
# from pyqtgraph.dockarea import *
from pyqtgraph.Qt import QtGui, QtCore
import pyqtgraph.parametertree.parameterTypes as pTypes
from pyqtgraph.parametertree import Parameter, ParameterTree
import numpy as np
from tasks.fitter import *
import yaml
from ui.toggle_button import ToggleButton

class CWFreqScanner(object):

	fm_dev = None
	running = False
	param_group_name = "Freq. Scan CW"

	def __init__(self,syncAiAo,freq_synth):
		self.syncAiAo = syncAiAo
		self.freq_synth = freq_synth

		# self.dock = Dock("CW CPT signal", size=(800, 1))

		self.gw = pg.GraphicsLayoutWidget()
		labelStyle = {'color': '#FFF', 'size': '10pt'}
		self.meas_lbl = self.gw.addLabel("Measurements",**labelStyle)
		self.gw.nextRow()
		self.p = self.gw.addPlot()
		self.p.showGrid(x=True, y=True, alpha=0.8)
		self.p.setLabel('left', "Signal", units='V')
		self.p.setLabel('bottom', "FM modulation signal", units='V')
		self.curve = self.p.plot(pen=pg.mkPen((0, 255, 255,60)))
		self.meanCurve = self.p.plot(pen=pg.mkPen((0, 255, 255,255)))
		self.fitCurve = self.p.plot(pen="r")
		self.error_curve = self.p.plot(pen="w")
		self.legend = pg.LegendItem((100,60), offset=(70,30))
		self.legend.setVisible(False)
		self.legend.setParentItem(self.p.graphicsItem()) 
		self.legend.addItem(self.fitCurve, "fit_parameter_text")
		self.timer = QtCore.QTimer()
		self.timer.timeout.connect(self.update)
		# self.dock.addWidget(self.gw)
		self.make_param_group()

		self.startBtn = ToggleButton("CW Frequency Scan", self)

	def make_param_group(self):
		self.param_group = pTypes.GroupParameter(name=self.param_group_name)
		self.param_group.addChild(self.syncAiAo.param_group)
		self.fit_action = Parameter.create(name='Fit',
											type='action')
		self.show_data_cruve = Parameter.create(name='Show data curve',
												type='bool',
												value=True)
		self.show_mean_cruve = Parameter.create(name='Show mean curve',
												type='bool',
												value=True)
		self.show_fit_cruve = Parameter.create(name='Show fitting curve',
												type='bool',
												value=True)
		self.param_group.addChildren([self.fit_action,
										self.show_data_cruve,
										self.show_mean_cruve,
										self.show_fit_cruve])
		self.param_group.sigTreeStateChanged.connect(self.change)
		self.fit_action.sigActivated.connect(self.fit)

	def change(self,param,changes):
		for param, change, data in changes:
			if param is self.show_data_cruve: self.curve.setVisible(param.value())
			elif param is self.show_mean_cruve: self.meanCurve.setVisible(param.value())
			elif param is self.show_fit_cruve:
				self.fitCurve.setVisible(param.value())
				self.legend.setVisible(param.value())

	def update(self):
		x, y = self.syncAiAo.getNthChanAIdata(0)
		_, y2 = self.syncAiAo.getNthChanAImean(0)
		x, y3 = self.syncAiAo.getNthChanAIdata(1)
		

		if self.fm_dev:
			x = x*self.fm_dev
		background = np.min(y2)
		signal = np.max(y2)-background
		contrast = signal/background
		self.meas_lbl.setText("Signal: {:.4f} mV    Background: {:.4f} mV  Contrast: {:.2f} %".format(signal*1000,background*1000,contrast*100))
		self.curve.setData(x=x,y=y)
		self.meanCurve.setData(x=x,y=y2)
		self.error_curve.setData(x=x,y=y3)
		if self.syncAiAo.counter == 1:
			self.p.enableAutoRange('xy', False)

	def update_label(self,object):
		self.frequency = self.freq_synth.frequency
		self.p.setLabel('bottom', "Frequency detuning from {:.2f} Hz".format(self.frequency), units='Hz')

	def update_fm_dev(self,object):
		# self.curve.clear()
		self.fm_dev = self.freq_synth.fm_dev

	def start(self):
		if self.syncAiAo.start():
			self.param_group.setName(self.param_group_name+" (Active)")
			self.fm_dev = self.freq_synth.fm_dev
			self.update_label(None)
			self.freq_synth.freq_param.sigValueChanged.connect(self.update_label)
			self.freq_synth.fm_dev_param.sigValueChanged.connect(self.update_fm_dev)				
			self.timer.start(50)
			self.running = True
			# try: self.dock.raiseDock()
			# except:	pass
			return True
		return False
		

	def stop(self):
		self.param_group.setName(self.param_group_name)
		self.syncAiAo.stop()
		self.timer.stop()
		self.freq_synth.freq_param.sigValueChanged.disconnect(self.update_label)
		self.freq_synth.fm_dev_param.sigValueChanged.disconnect(self.update_fm_dev)		
		self.running = False

	def fit(self,block=False):
		x, y = self.syncAiAo.getNthChanAImean(0)
		if self.fm_dev:
			x = x*self.fm_dev
		self.fitter = Fitter(x,y)
		self.fitter.newData.connect(self.update_fit_curve)
		self.fitter.start()
		if block:
			self.fitter.wait()	

	def update_fit_curve(self,x,fitData):
		self.last_fit_out = fitData
		self.fitCurve.setData(x=x,y=fitData.best_fit)
		fit_parameter_text = ("FWHM: {fwhm:.3g} Hz\n"
								"Amplitude: {signal:.3g} V\n"
								"Contrast: {contrast:.3g}").format(**fitData.best_values)
		self.legend.items[0][1].setText(fit_parameter_text)
		self.legend.setVisible(True)
		