import pyqtgraph as pg
# from pyqtgraph.dockarea import *
from pyqtgraph.Qt import QtGui, QtCore
import pyqtgraph.parametertree.parameterTypes as pTypes
from pyqtgraph.parametertree import Parameter, ParameterTree
import numpy as np
from tasks.fitter import *
from ui.toggle_button import ToggleButton

class LaserFreqScanner(object):

	running = False
	param_group_name = 'Laser Freq. Scan'

	def __init__(self,syncAiAo):
		self.syncAiAo = syncAiAo

		# self.dock = Dock("Absorption signal", size=(800, 1))

		self.gw = pg.GraphicsLayoutWidget()
		self.p = self.gw.addPlot()
		self.p.showGrid(x=True, y=True, alpha=0.8)
		self.p.setLabel('left', "Signal", units='V')
		self.p.setLabel('bottom', "Laser modulation signal", units='V')
		self.signalDirectCurve = self.p.plot(pen=pg.mkPen((0, 255, 100,50)))		#Modif EK 18/11/16
		self.signalMeanCurve = self.p.plot(pen="c")									#Modif EK 18/11/16
		self.errorDirectCurve = self.p.plot(pen=pg.mkPen((220, 60, 60,100)))		#Modif EK 18/11/16
		self.errorMeanCurve = self.p.plot(pen="m")									#Modif EK 18/11/16
		self.timer = QtCore.QTimer()
		self.timer.timeout.connect(self.update)
		# self.dock.addWidget(self.gw)
		self.make_param_group()
		self.startBtn = ToggleButton("Laser Scan", self)

	def make_param_group(self):
		self.param_group = pTypes.GroupParameter(name=self.param_group_name)
		self.param_group.addChild(self.syncAiAo.param_group)

	def update(self):
		x, y = self.syncAiAo.getNthChanAIdata(0)									#Modif EK 18/11/16
		_, y2 = self.syncAiAo.getNthChanAImean(0)									#Modif EK 18/11/16
		x, y3 = self.syncAiAo.getNthChanAIdata(1)									#Modif EK 18/11/16
		_, y4 = self.syncAiAo.getNthChanAImean(1)
		self.signalDirectCurve.setData(x=x,y=y)										#Modif EK 18/11/16
		self.signalMeanCurve.setData(x=x,y=y2)										#Modif EK 18/11/16
		self.errorDirectCurve.setData(x=x,y=y3)										#Modif EK 18/11/16
		self.errorMeanCurve.setData(x=x,y=y4)										#Modif EK 18/11/16
		if self.syncAiAo.counter == 1:
			self.p.enableAutoRange('xy', False)

	def start(self):
		if self.syncAiAo.start():
			self.param_group.setName(self.param_group_name+" (Active)")
			self.timer.start(50)
			self.running = True
			return True
		return False
		# try: self.dock.raiseDock()
		# except:	pass

	def stop(self):
		self.param_group.setName(self.param_group_name)
		self.syncAiAo.stop()
		self.timer.stop()
		self.running = False