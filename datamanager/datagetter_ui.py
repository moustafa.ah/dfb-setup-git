# -*- coding: utf-8 -*-
from .datagetter import DataGetter
import pyqtgraph.parametertree.parameterTypes as pTypes
from pyqtgraph.parametertree import Parameter
from pyqtgraph.graphicsItems.PlotItem import PlotItem
from pyqtgraph import LabelItem
import numpy as np

symbolBrushes = [(130, 45, 255), (255, 0, 133), (255, 226, 8), (20, 183, 204), "r"]

class DataGetter_UI(DataGetter):

	def __init__(self,columns,delay=0.6,maxlength=None,name='DataGetter'):
		DataGetter.__init__(self,columns,delay,maxlength)
		self.param_group = pTypes.GroupParameter(name = name)
		self.delay_param = Parameter.create(name='Delay',
												type='float',
												value=self.delay,
												suffix='s')
		self.param_group.addChildren([self.delay_param])

		self.param_group.sigTreeStateChanged.connect(self.change)

	def change(self,param,changes):
		for param, change, data in changes:
			if param is self.delay_param: self.delay = param.value()

	def make_plots(self, graph_layout=None,title_font_size=3):
		self.plots = []
		self.curves = []
		for idx, header in enumerate(self.header[1:]):
			l = LabelItem("<font size={:d}>{}</font>".format(title_font_size,header))
			# p = PlotItem(title = "<font size={:d}>{}</font>".format(title_font_size,header))
			p = PlotItem()
			c = p.plot( title = header, pen = symbolBrushes[idx % len(symbolBrushes)])
			self.plots.append(p)
			self.curves.append(c)
			if graph_layout is not None:
				graph_layout.addItem(l)
				graph_layout.addItem(p)
				graph_layout.nextRow()

	def update(self):
		if self.buf.index > 0:
			data = self.buf.get()
			t0 = data[0,0]
			for idx, curve in enumerate(self.curves):
				curve.setData(x = data[:,0] - t0 , y = data[:,idx+1])