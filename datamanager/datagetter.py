# import Queue as queue
import queue
import threading, time, logging
import numpy as np
from collections import deque
from base.dataproducer import DataProducer

class Worker(threading.Thread):

	def __init__(self,dataGetter):
		threading.Thread.__init__(self)
		self.dataGetter = dataGetter

	def run(self):
		# logger.info("Data producer starting")
		getters = [item[1] for item in self.dataGetter.columns]
		while self.dataGetter.running:
			data = []
			for getter in getters:
				if type(getter) is tuple:
					data.append(getattr(*getter))
				else:
					data.append(getter())
			# print(data)
			self.dataGetter.buf.append(data)
			self.dataGetter.send_to_consumers(data)
			# for consumer in self.consumers:
			# 	consumer.data_queue.put(data)
			time.sleep(self.dataGetter.delay)

class DataGetter(DataProducer):
	# consumers = []
	running = True

	def __init__(self,columns,delay=0.6,maxlength=1000):
		DataProducer.__init__(self,maxlength=maxlength,numcol=len(columns))
		self.columns = columns
		self.delay = delay
		self.header = [item[0] for item in self.columns]
		self.worker = None

	def start(self):
		self.running = True
		self.worker = Worker(self)
		self.buf.clear()
		self.worker.start()

	def stop(self):
		if self.worker is None: return
		self.running = False
		self.worker.join()
		self.worker = None
