import threading, time
try: import Queue as queue
except: import queue

column_separator = ","

class DataThread(threading.Thread):

	def __init__(self):
		threading.Thread.__init__(self)
		self.running = True

	def stop(self,block=True):
		self.running = False
		if block: self.join()

class LoggerDataConsumer(DataThread):

	def __init__(self,filename,producer,file_buffering=-1):
		DataThread.__init__(self)
		self.filename = filename
		self.data_queue = queue.Queue()
		self.producer = producer
		self.file_buffering = file_buffering
		self.producer.register_consumer(self)

	def make_new_file(self):
		# Make new file and write header
		self.log_file = open(self.filename,'w',self.file_buffering)
		header_line = column_separator.join(self.producer.header) + "\n"
		self.log_file.write(header_line)

	def close_file(self):
		self.log_file.close()
		# logger.info("Logfile is closed")

	def run(self):
		self.make_new_file()
		# logger.info("Data consumer waiting for data")
		retrying = False
		while self.running:
			try:
				if retrying:
					self.log_file = open(self.filename,'a',self.file_buffering)
					self.log_file.write( "# File break,,,,,,,,,,,,,,,,,,,,,,,,\n" )
				else:
					data_array = self.data_queue.get(block=True, timeout=2)
					line = column_separator.join( list(map(str,data_array)) )
				self.log_file.write( line + "\n" )
				retrying = False
			except queue.Empty:
				pass
			except IOError:
				retrying = True
				print("Got an IOError : it looks like the file cannot be written to right now")
				print("Waiting for 10 s")
				# self.close_file()
				time.sleep(10)
		self.close_file()
		self.producer.unregister_consumer(self)