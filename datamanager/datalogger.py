import pyqtgraph.parametertree.parameterTypes as pTypes
from pyqtgraph.parametertree import Parameter, ParameterTree
from .datafilewriter import LoggerDataConsumer
import time, os

class DataLogger(object):

	running = False
	start_time_str = None

	def __init__(self,device_set,data_folder='.'):
		self.device_set = device_set
		self.data_folder = data_folder
		self.loggable_dev_list = []
		# start/stop buttons
		self.start_stop_param = Parameter.create(name='Start', type='action')
		self.clear_param = Parameter.create(name='Clear', type='action')
		self.auto_config_param = Parameter.create(name='Auto save config', type='bool', value = False)
		# connect functions
		self.start_stop_param.sigActivated.connect(self.start_stop)
		self.clear_param.sigActivated.connect(self.clear_buffers)
		# create groups and set tree
		self.param_group = Parameter.create(name='Datalogger', type='group', children = [self.start_stop_param,
																						self.clear_param,
																						self.auto_config_param])

	def register_device(self,device,file_suffix,file_buffering=-1):
		if self.running:
			filename = "{}_{}.csv".format(self.start_time_str,file_suffix)
			logger = LoggerDataConsumer(os.path.join(self.data_folder,filename),device,file_buffering=file_buffering)
			self.loggable_dev_list.append([device,file_suffix,logger])
			logger.start()			
		else: self.loggable_dev_list.append([device,file_suffix,None])
		print(f'Number of dataproducers registered: {len(self.loggable_dev_list)}')

	def unregister_device(self,device):
		for _ , _ , logger in [x for x in self.loggable_dev_list if x[0] is device]:
			if logger: logger.stop()
		self.loggable_dev_list[:] = [x for x in self.loggable_dev_list if x[0] is not device]
		print(f'Number of dataproducers registered: {len(self.loggable_dev_list)}')

	def start_stop(self):
		if self.running == False:
			self.start()
			list(self.start_stop_param.items.keys())[0].button.setText("Stop")
		else:
			self.stop()
			list(self.start_stop_param.items.keys())[0].button.setText("Start")

	def start(self):
		self.running = True
		self.start_time_str = time.strftime("%Y-%m-%d %H%M%S", time.localtime())
		if self.auto_config_param.value():
			self.device_set.save_config(os.path.join(self.data_folder,self.start_time_str+"_config.yaml"))
		for item in self.loggable_dev_list:
			device,file_suffix, _ = item
			filename = "{}_{}.csv".format(self.start_time_str,file_suffix)
			logger = LoggerDataConsumer(os.path.join(self.data_folder,filename),device)
			item[2] = logger
			logger.start()

	def clear_buffers(self):
		for device, _ , logger in self.loggable_dev_list:
			device.buf.clear()

	def stop(self):
		for device, _ , logger in self.loggable_dev_list:
			if logger: logger.stop()
		self.running = False