from PyDAQmx import *
import numpy as np
import ctypes, time
from base.dataproducer import DataProducer

class PhaseJumps(DataProducer):
	header = ["Time","Frequency (Hz)","Frequency Error (V)"]

	##### Cooling light frequency and ON/OFF waveforms (VCO AOM) and magnetic field control #####
	_cooling_duration		= 0.22085 # s  kept at 0 V
	_molasse_duration		= 0.003 # s 
	_ramp_duration			= 0.002 # s  ramping from 0 to 5 V
	_mag_field_off_delay 	= 0.001 # s  Delay before ramp start

	_cpt_pump_duration 	= 0.003 # s
	_dark_duration 		= 0.016 # s
	_cpt_probe_duration = 50e-6 # s
	_padding_time 		= 100e-6 # s

	_pulse1_detect_duration = 0.0015 # [s] The duration of the detection window for pulse 1
	_pulse2_detect_duration = 0.0001 # [s] The duration of the detection window for pulse 2

	_phase_modulation_amp = 0.5

	##### Frequency scan parameters #####
	_center_frequency 	= 6834682610.904
	_initial_offset 	= 1

	##### Servo parameter #####
	_freq_lock 			= True
	_freq_p_gain 		= 500

	##### FP Trace parameters #####
	_get_FR_ratio 	= False
	_save_ratio_period = 500

	##### Channels #####
	CPT_light_switch_chan 		= "/Dev1/port0/line1"
	cooling_light_switch_chan 	= "/Dev1/port0/line2"
	repump_switch_chan 			= "/Dev1/port0/line3"
	cooling_light_freq_chan 	= "/Dev1/ao1"
	mag_field_switch_chan 		= "/Dev1/port0/line0"
	phase_modulation_chan 		= "/Dev1/ao0"
	signal_chan 				= "/Dev1/ai1"
	normal_chan 				= "/Dev1/ai3"

	DOChannels = [
				mag_field_switch_chan,
				CPT_light_switch_chan,
				cooling_light_switch_chan,
				repump_switch_chan
				]

	AOChannels = [
				phase_modulation_chan,
				cooling_light_freq_chan
				]

	AIChannels = [
				signal_chan,
				normal_chan
				]

	_numAIChan = len(AIChannels)

	##### DAQ reading parameters #####
	_samp_rate 	= 1e5 # Samp/s
	_chunk_size = 100 # [samples] - number of samples after which the data from the DAQ will be read
	_num_cycles = 2	  # The number of cycles per sequence (a cycle is cooling, two cpt pulses and dark time)

	##### Other stuff #####
	autorestart 	= True # Restart on setting change
	time_data 		= None # Time array in milliseconds
	running 		= False
	redraw_waveform = True
	flag 			= 0

	@property
	def phase_modulation_amp(self):
		return self._phase_modulation_amp

	@phase_modulation_amp.setter
	def phase_modulation_amp(self,value):
		self._phase_modulation_amp = value
		if self.autorestart: self.restart()

	@property
	def dark_duration(self):
		return self._dark_duration

	@dark_duration.setter
	def dark_duration(self,value):
		self._dark_duration = value
		if self.autorestart: self.restart()

	@property
	def cooling_duration(self):
		return self._cooling_duration

	@cooling_duration.setter
	def cooling_duration(self,value):
		self._cooling_duration = value
		if self.autorestart: self.restart()

	@property
	def initial_offset(self):
		return self._initial_offset

	@initial_offset.setter
	def initial_offset(self,value):
		self._initial_offset = value
		if self.autorestart: self.restart()

	def __init__(self,synthesizer=None,synthesizerAOM=None,oscilloscope=None):
		DataProducer.__init__(self,maxlength=1000,numcol=len(self.header))
		self.synthesizer = synthesizer
		self.synthesizerAOM = synthesizerAOM
		self.oscilloscope = oscilloscope
		self.generate_waveforms()
		self.set_up_synthesizer()
		print(10*"-"+"Phase Jumps" + 10*"-")

	def generate_waveforms(self):
		sequence_duration = self._cooling_duration + self._molasse_duration + self._cpt_pump_duration + self._dark_duration + self._cpt_probe_duration + self._padding_time
		self.num_samples = int(sequence_duration * self._samp_rate)

		##### Generate CPT_light_switch_data #####
		pulse1 = np.zeros(self.num_samples,dtype=np.uint8)
		pulse2 = np.zeros(self.num_samples,dtype=np.uint8)

		p1Start 			= int((self._cooling_duration + self._molasse_duration) * self._samp_rate) 
		self.p1End			= int((self._cooling_duration + self._molasse_duration + self._cpt_pump_duration) * self._samp_rate)
		self.p2Start 		= int((self._cooling_duration + self._molasse_duration + self._cpt_pump_duration + self._dark_duration) * self._samp_rate)
		p2End				= int((self._cooling_duration + self._molasse_duration + self._cpt_pump_duration + self._dark_duration + self._cpt_probe_duration) * self._samp_rate)
		self.p2Detect 		= int((self._pulse2_detect_duration) * self._samp_rate)
		self.dark_padding 	= int((0.00025) * self._samp_rate)

		pulse1[p1Start:self.p1End] = 1
		pulse2[self.p2Start:p2End] = 1

		self.CPT_light_switch_data = np.bitwise_or(pulse1,pulse2)

		##### Generate cooling_light_switch_data #####
		t0 = int((self._cooling_duration + self._molasse_duration) * self._samp_rate)
		self.cooling_light_switch_data = np.zeros(self.num_samples,dtype=np.uint8)
		self.cooling_light_switch_data[:t0] = 1

		##### Generate cooling_light_switch_data #####
		self.repump_switch_data = self.cooling_light_switch_data

		##### Generate cooling_light_freq_data #####
		t0 = int((self._cooling_duration + self._molasse_duration - self._ramp_duration) * self._samp_rate)
		t1 = int((self._cooling_duration + self._molasse_duration) * self._samp_rate)
		self.cooling_light_freq_data = np.zeros(self.num_samples,dtype=np.float64)
		self.cooling_light_freq_data[t0:t1] = np.linspace(0.,5.,t1-t0)
		self.cooling_light_freq_data[t1:] = 10.

		##### Generate mag_field_switch_data #####
		t0 = int((self._cooling_duration) * self._samp_rate)
		self.mag_field_switch_data = np.zeros(self.num_samples,dtype=np.uint8)
		self.mag_field_switch_data[:t0] = 1

		##### Generate LO phase modulation #####
		self.phase_modulation_data = np.zeros(self.num_samples,dtype=np.float64)
		flip = int((self._cooling_duration + self._molasse_duration + self._cpt_pump_duration + self._dark_duration/2) * self._samp_rate)

		self.phase_modulation_data[:flip] = self._phase_modulation_amp
		self.phase_modulation_data[flip:] = -self._phase_modulation_amp

		##### Tile data to get sequence of two cycles #####
		self.num_samples = self.num_samples*2
		self.CPT_light_switch_data = np.tile(self.CPT_light_switch_data,2)
		self.cooling_light_switch_data 	= np.tile(self.cooling_light_switch_data,2)
		self.repump_switch_data = np.tile(self.repump_switch_data,2)
		self.cooling_light_freq_data = np.tile(self.cooling_light_freq_data,2)
		self.mag_field_switch_data = np.tile(self.mag_field_switch_data,2)
		self.phase_modulation_data = np.hstack([self.phase_modulation_data,-1*self.phase_modulation_data])

		##### Generate time data #####
		self.time_data = np.arange(self.num_samples) / float(self._samp_rate) * 1000

		##### Generate data arrays for the read data #####
		self.AI_data 		= np.zeros(self._chunk_size*self._numAIChan,dtype=np.float64)	
		self.signal_data 	= np.zeros(self.num_samples,dtype=np.float64)
		self.normal_data 	= np.zeros(self.num_samples,dtype=np.float64)

	def set_up_synthesizer(self):
		print("Setting up synthesizer ({})".format(self.synthesizer.identity))
		if self.synthesizer.fm_state:
			self.synthesizer.fm_state = "OFF"
		if not self.synthesizer.pm_state1:
			self.synthesizer.pm_state1 = "ON"
		if self.synthesizer.pm_state2:
			self.synthesizer.pm_state2 = "OFF"

	def start(self):

		assert not self.running
		self.running = True

		self.generate_waveforms()

		##### Counters #####
		self.counter 			= 0
		self.cycle_counter 		= 1
		self.sequence_counter 	= 0

		self.samples_per_cycle = [self.num_samples/2,self.num_samples]

		print("Starting...")

		##### Create task for digital outputs #####
		self.DOtaskHandle = TaskHandle()
		DAQmxCreateTask(None,ctypes.byref(self.DOtaskHandle))
		DAQmxCreateDOChan(self.DOtaskHandle, ",".join(self.DOChannels), None, DAQmx_Val_ChanPerLine)
		DAQmxCfgSampClkTiming(self.DOtaskHandle,"ao/SampleClock", self._samp_rate, DAQmx_Val_Rising, DAQmx_Val_ContSamps, self.num_samples)
		DAQmxWriteDigitalLines(self.DOtaskHandle,self.num_samples,0,10,DAQmx_Val_GroupByChannel,
								np.hstack((
										self.mag_field_switch_data,
										self.CPT_light_switch_data,
										self.cooling_light_switch_data,
										self.repump_switch_data)),
								None,None)
		
		##### Create task for analog outputs #####
		self.AOtaskHandle = TaskHandle()
		DAQmxCreateTask(None,ctypes.byref(self.AOtaskHandle))
		DAQmxCreateAOVoltageChan(self.AOtaskHandle, ",".join(self.AOChannels), None, -10, +10, DAQmx_Val_Volts, None)
		DAQmxCfgSampClkTiming(self.AOtaskHandle,None, self._samp_rate, DAQmx_Val_Rising, DAQmx_Val_ContSamps, self.num_samples)
		DAQmxWriteAnalogF64(self.AOtaskHandle, self.num_samples, 0, 10, DAQmx_Val_GroupByChannel, 
								np.hstack([
									self.phase_modulation_data,
									self.cooling_light_freq_data]),
								None, None)

		##### Create task for detection #####
		self.detectTaskHandle = TaskHandle()
		DAQmxCreateTask(None,ctypes.byref(self.detectTaskHandle))
		DAQmxCreateAIVoltageChan(self.detectTaskHandle, ",".join(self.AIChannels), None, DAQmx_Val_Cfg_Default, -10, +10, DAQmx_Val_Volts, None)
		DAQmxCfgSampClkTiming(self.detectTaskHandle,"ao/SampleClock", self._samp_rate, DAQmx_Val_Rising, DAQmx_Val_ContSamps, self.num_samples)
		DAQmxCfgInputBuffer(self.detectTaskHandle, ctypes.c_uint32(self.num_samples*self._numAIChan*80))

		##### Register detector callback #####
		def EveryNCallback(taskHandle, everyNsamplesEventType, nSamples, callbackData):
			t1 = time.clock()
			readAI = ctypes.c_int32()
			DAQmxReadAnalogF64(self.detectTaskHandle,self._chunk_size,10,DAQmx_Val_GroupByChannel,self.AI_data ,self._chunk_size*self._numAIChan,ctypes.byref(readAI),None)

			# Update counter
			self.counter += self._chunk_size
			
			# Process signal_data
			self.signal_data[self.counter-self._chunk_size:self.counter] = self.AI_data[:self._chunk_size]
			self.normal_data[self.counter-self._chunk_size:self.counter] = self.AI_data[self._chunk_size:]

			if self.counter >= self.samples_per_cycle[self.cycle_counter-1]:
				if self.cycle_counter == 1:
					# print("Finished cycle 1")
					pass
				elif self.cycle_counter == 2:

					##### Process data #####
					pulse2_signal_cycle1 	= np.mean(self.signal_data[self.p2Start:self.p2Start+self.p2Detect])
					pulse2_normal_cycle1 	= np.mean(self.normal_data[self.p2Start:self.p2Start+self.p2Detect])
					dark_signal_cycle1 		= np.mean(self.signal_data[self.p1End+self.dark_padding:self.p2Start-self.dark_padding])
					dark_normal_cycle1 		= np.mean(self.normal_data[self.p1End+self.dark_padding:self.p2Start-self.dark_padding])

					pulse2_signal_cycle2 	= np.mean(self.signal_data[self.samples_per_cycle[0]+self.p2Start:self.samples_per_cycle[0]+self.p2Start+self.p2Detect])
					pulse2_normal_cycle2 	= np.mean(self.normal_data[self.samples_per_cycle[0]+self.p2Start:self.samples_per_cycle[0]+self.p2Start+self.p2Detect])
					dark_signal_cycle2 		= np.mean(self.signal_data[self.samples_per_cycle[0]+self.p1End+self.dark_padding:self.samples_per_cycle[0]+self.p2Start-self.dark_padding])
					dark_normal_cycle2 		= np.mean(self.normal_data[self.samples_per_cycle[0]+self.p1End+self.dark_padding:self.samples_per_cycle[0]+self.p2Start-self.dark_padding])
					
					pulse2_cycle1 = (pulse2_signal_cycle1-dark_signal_cycle1)/(pulse2_normal_cycle1-dark_normal_cycle1)
					pulse2_cycle2 = (pulse2_signal_cycle2-dark_signal_cycle2)/(pulse2_normal_cycle2-dark_normal_cycle2)

					# self.signal_display_data[self.p2Start:self.p2Start+self.p2Detect] = self.signal_data[self.p2Start:self.p2Start+self.p2Detect]
					# self.signal_display_data[self.samples_per_cycle[0]+self.p2Start:self.samples_per_cycle[0]+self.p2Start+self.p2Detect] = self.signal_data[self.samples_per_cycle[0]+self.p2Start:self.samples_per_cycle[0]+self.p2Start+self.p2Detect]
					
					# self.normal_display_data[self.p2Start:self.p2Start+self.p2Detect] = self.normal_data[self.p2Start:self.p2Start+self.p2Detect]
					# self.normal_display_data[self.samples_per_cycle[0]+self.p2Start:self.samples_per_cycle[0]+self.p2Start+self.p2Detect] = self.normal_data[self.samples_per_cycle[0]+self.p2Start:self.samples_per_cycle[0]+self.p2Start+self.p2Detect]
					
					frequency_error = pulse2_cycle2 - pulse2_cycle1

					frequency_correction = frequency_error * self._freq_p_gain

					if self._freq_lock:
						self._frequency += frequency_correction
						self.synthesizer.frequency = self._frequency
						# self.synthesizer.frequency = self._center_frequency

					##### Clock freq vs one-photon detuning #####
					# self.AOMFreq = 81700000 + 2000000*np.sin(self.sequence_counter*2*3.1415/4000)
					# self.synthesizerAOM.frequency = self.AOMFreq

					##### Save FP Trace #####
					# if self._get_FR_ratio:
						# if not (self.sequence_counter)%self._save_ratio_period:
					# ratio = self.oscilloscope.get_ratio()

					##### Clock Freq vs RF power #####
					# RFS = [-27,-23,-26,-22,-25,-21,-24,-20]
					# RFS = [-24, -23]
					# if not (self.sequence_counter)%2000:
					# 	self.synthesizer.power_amplitude = RFS[self.flag]
						# self.flag = (self.flag + 1)%len(RFS)

					##### Stability vs Gain #####
					# if not (self.sequence_counter)%8000:
					# 	if self._freq_p_gain >= 600:
					# 		print("Stopping...")
					# 		self.stop()
					# 	else:
					# 		self._freq_p_gain += 50
					# 		print("New Gain = {}".format(self._freq_p_gain))
					# 		self.restart()

					queued_data = [time.time(),self._frequency-self._center_frequency,frequency_error]
					self.buf.append(queued_data)
					self.send_to_consumers(queued_data)

					#### Update Counters #####
					self.sequence_counter += 1
					self.counter = 0


				self.cycle_counter = (self.cycle_counter)%self._num_cycles + 1
			
			return int(0)

		EveryNCallbackCWRAPPER = ctypes.CFUNCTYPE(ctypes.c_int32,ctypes.c_void_p,ctypes.c_int32,ctypes.c_uint32,ctypes.c_void_p)
		self.everyNCallbackWrapped = EveryNCallbackCWRAPPER(EveryNCallback)
		DAQmxRegisterEveryNSamplesEvent(self.detectTaskHandle, DAQmx_Val_Acquired_Into_Buffer, self._chunk_size, 0, self.everyNCallbackWrapped, None)

		self._frequency = self._center_frequency + self._initial_offset
		# self.synthesizer.frequency = self._frequency

		print("Starting tasks.")
		self.start_time  = time.time()
		DAQmxStartTask(self.detectTaskHandle)
		DAQmxStartTask(self.DOtaskHandle)
		DAQmxStartTask(self.AOtaskHandle)

	def stop(self):
		if self.running:
			DAQmxStopTask(self.detectTaskHandle)
			DAQmxStopTask(self.DOtaskHandle)
			DAQmxStopTask(self.AOtaskHandle)

			DAQmxClearTask(self.detectTaskHandle)
			DAQmxClearTask(self.DOtaskHandle)
			DAQmxClearTask(self.AOtaskHandle)
			self.zeroOutputs()
			self.running = False

	def restart(self):
		if self.running:
			self.stop()
			self.start()

	def zeroOutputs(self):
		clearDOtaskHandle = TaskHandle()
		DAQmxCreateTask("", ctypes.byref(clearDOtaskHandle))
		DAQmxCreateDOChan(clearDOtaskHandle, ",".join(self.DOChannels), None, DAQmx_Val_ChanPerLine)
		DAQmxWriteDigitalLines(clearDOtaskHandle,1,1,10,DAQmx_Val_GroupByChannel,np.zeros(len(self.DOChannels),dtype=np.uint8),None,None)
		DAQmxStartTask(clearDOtaskHandle)
		DAQmxClearTask(clearDOtaskHandle)

		clearAOtaskHandle = TaskHandle()
		DAQmxCreateTask(None,ctypes.byref(clearAOtaskHandle))
		DAQmxCreateAOVoltageChan(clearAOtaskHandle, ",".join(self.AOChannels), None, -10, +10, DAQmx_Val_Volts, None)
		DAQmxWriteAnalogF64(clearAOtaskHandle, 1, 1, 10, DAQmx_Val_GroupByChannel, np.zeros(len(self.AOChannels),dtype=np.float64), None, None)
		DAQmxStartTask(clearAOtaskHandle)
		DAQmxClearTask(clearAOtaskHandle)

	def get_config(self):
		feature_list = [
		'_cooling_duration',
		'_molasse_duration',
		'_ramp_duration',
		'_mag_field_off_delay',
		'_cpt_pump_duration',
		'_dark_duration',
		'_cpt_probe_duration',
		'_padding_time',
		'_pulse1_detect_duration',
		'_pulse2_detect_duration',
		'_phase_modulation_amp',
		'_center_frequency',
		'_initial_offset',
		'_freq_lock',
		'_freq_p_gain',
		'_samp_rate',
		'_chunk_size',
		'_num_cycles',
		'_get_FR_ratio',
		'_save_ratio_period'
		]

		return { 'Clock/Sequence Parameters' : { feature: getattr(self,feature) for feature in feature_list } }