import time, sys
from operator import attrgetter
import numpy as np
from storage.shelved_param_UI import Shelved_param_UI
from tasks.datalog import *
from tasks.autotaskpanel import AutoTaskPanel
from base.driver import Driver
print "Loading pyqtgraph",
import pyqtgraph as pg
from pyqtgraph.dockarea import *
from pyqtgraph.Qt import QtGui, QtCore
import pyqtgraph.parametertree.parameterTypes as pTypes
from pyqtgraph.parametertree import Parameter, ParameterTree
print " [DONE]"

print "Loading drivers",
from drivers import *
print " [DONE]"

from ui.graphs.pulsed_freq_scanner import PulseFreqScanner
from ui.graphs.cw_freq_scanner import CWFreqScanner
from ui.graphs.laser_freq_scanner import LaserFreqScanner
from ui.toggle_button import ToggleButton
from ui.dataPlotItem import DataPlotItem

import logging
logger = logging.getLogger('driver')
logger.setLevel(logging.DEBUG)
ch = logging.StreamHandler()
formatter = logging.Formatter("%(asctime)s - %(levelname)s - %(message)s")
ch.setFormatter(formatter)
logger.addHandler(ch)

data_folder_path = "G:\eric.kroemer\Desktop\Vincent\Pilotage Data"

class DeviceSet(object):
	def __init__(s):
		#Setup base equipments
		print "Connecting to devices"
		s.daq = NIUSB6259(devID='Dev3')
		# s.daq2 = NIUSB6259(devID='Dev2')
		s.regulation = CYPidReg_UI(comPort='com23')
		s.fs = E8254A_UI(raw_id="TCPIP0::192.168.254.45::inst1::INSTR")

		#Setup compound equipments
		s.asserv = Asserv_UI(device="Dev3",outChan="ao1",inChan="ai0",dds_device=s.fs)
		s.asserv_pulsed = Asserv_pulsed_UI(device="Dev3",outChan="ao1",inChan="ai0",dds_device=s.fs)
		# s.biasCtrl = BiasController2_UI(s.daq2,biasChan="ao2",signalChan="ai2")

		s.syncAiAo = SyncAIAO_UI(device="Dev3",inChanList=["ai0"],outChan="ao1",inRange=(-1.,1.),outRange=(-1.,1.))

		s.laserSyncAiAo = SyncAIAO_UI(device="Dev3",inChanList=["ai0","ai1","ai2"],outChan="ao0",inRange=(-10.,10.),outRange=(-10.,10.))
		s.laserSyncAiAo.vpp=3
		s.laserSyncAiAo.numSamp=4000

		s.syncAiAoCPT_pulsed = SyncAIAO_pulsed_UI(device = "Dev3", outChan="ao1", DOChan = 'port0/line6',inChan="ai0",inRange=(-10.,10.),outRange=(-10.,10.))
		s.asserv_pulsed.Tdark = 0.0004
		s.asserv_pulsed.Tbright = 0.0002
		s.asserv_pulsed.Tdiscarded = 0.0001
		s.asserv_pulsed.Taverage = 0.0001
		s.asserv_pulsed.n_cycle = 60

		# Rows must have the form:    [ "Label", function ]
		# 					       or [ "Label", (object,'parameter') ]
		# If the device directly provides a function to retrieve the parameter, use case 1
		# Otherwise, if it only provides a property that cannot be called, use case 2
		header_getters = [	["Time",time.time],
							["Cell temperature (degC)",(s.regulation,'temperature')]
							# ["EOM DC bias (V)",(s.biasCtrl,'dc_bias')],
							]
		s.prodThread = DataProducer(header_getters)
		s.consThread = DataConsumer(s.prodThread.header)
		s.asservConsThread = DataConsumer(s.asserv.header)

		s.dl = DataLogger()
		s.autotapa = AutoTaskPanel()

	def get_config(self):
		devices = [dev for key, dev in self.__dict__.items() if isinstance(dev,Driver)]
		configs = {}
		for dev in devices: configs.update(dev.get_config())
		return configs

class DataLogger(object):

	def __init__(self):
		self.param_group = pTypes.GroupParameter(name='Data logger')
		self.clear_plot_action = Parameter.create(name='Clear plot', type='action')
		self.param_group.addChildren([self.clear_plot_action])

		self.clear_plot_action.sigActivated.connect(self.empty)

		self.running = False

	def __del__(self):
		self.stop()

	def start(self,running_asserv):
		self.running_asserv = running_asserv
		assert not self.running
		self.running = True

		timeStr = time.strftime("%Y-%m-%d %H%M")
		self.f1_name = "{}\datalog {} A.txt".format(data_folder_path,timeStr)
		self.f2_name = "{}\datalog {} B.txt".format(data_folder_path,timeStr)

		a.consThread.start(self.f1_name)
		a.asservConsThread.start(self.f2_name)

		a.prodThread.start()
		self.running_asserv.dataQueue = a.asservConsThread.data.queue
		a.prodThread.queue = a.consThread.data.queue

	def stop(self):
		if self.running:
			self.running = False
			a.consThread.stop()
			a.asservConsThread.stop()
			a.prodThread.stop()
			if self.running_asserv: self.running_asserv.dataQueue = None

	def empty(self):
		a.consThread.empty()
		a.asservConsThread.empty()

class ControlWidget(pg.LayoutWidget):

	def __init__(self, window=None):
		super(ControlWidget, self).__init__(window)
		self.window = window
		self.startLaserScanBtn = ToggleButton("Laser Scan", self.window.laser_freq_scanner)
		self.startFreqScanBtn = ToggleButton("CW Freq. Scan", self.window.cw_freq_scanner)		
		self.startPulFreqScanBtn = ToggleButton("Pulsed Freq. Scan", self.window.pulse_freq_scanner)
		self.startAsservBtn = ToggleButton("CW Asserv", a.asserv)
		self.startPulAsservBtn = ToggleButton("Pulsed Asserv",a.asserv_pulsed)

		self.startDlBtn = QtGui.QPushButton("Start Data Logging")
		self.startDlBtn.setCheckable(True)
		self.startDlBtn.clicked.connect(self.on_startDlBtn)

		self.p = Parameter.create(name='params', type='group')

		constparams_gp = shelved_params.param_group

		equips_gp = pTypes.GroupParameter(name='Equipments')
		equips_gp.addChild(a.fs.param_group)
		equips_gp.addChild(a.regulation.param_group)
		# equips_gp.addChild(a.biasCtrl.param_group)
	
		cw_op_gp = pTypes.GroupParameter(name='CW Operation')
		cw_op_gp.addChild(a.asserv.param_group)
		cw_op_gp.addChild(self.window.cw_freq_scanner.param_group)

		pulse_op_gp = pTypes.GroupParameter(name='Pulsed Operation')
		pulse_op_gp.addChild(a.asserv_pulsed.param_group)
		pulse_op_gp.addChild(a.syncAiAoCPT_pulsed.param_group)

		laser_scan_gp = self.window.laser_freq_scanner.param_group

		datalog_gp = a.dl.param_group
		autota_gp = a.autotapa.param_group

		self.p.addChildren([constparams_gp,
							equips_gp,
							laser_scan_gp,
							cw_op_gp,
							pulse_op_gp,
							datalog_gp,
							autota_gp])
		
		t = ParameterTree()
		t.setParameters(self.p, showTop=False)
		
		self.addWidget(self.startLaserScanBtn,row=0, col=0,colspan=2)
		self.addWidget(self.startFreqScanBtn,row=1, col=0)
		self.addWidget(self.startAsservBtn,row=1, col=1)
		self.addWidget(self.startPulFreqScanBtn,row=2, col=0)
		self.addWidget(self.startPulAsservBtn,row=2, col=1)
		self.addWidget(self.startDlBtn,row=3, col=0,colspan=2)
		self.addWidget(t,row=4, col=0,colspan=2)

	def on_startDlBtn(self):
		if self.startDlBtn.isChecked():
			if self.startPulAsservBtn.isChecked(): a.dl.start(running_asserv = a.asserv_pulsed)
			else: a.dl.start(running_asserv = a.asserv)
			self.startDlBtn.setText("Stop Data Logging")
			try: self.window.data_graph_widget.dock.raiseDock()
			except: pass
		else:
			a.dl.stop()
			self.startDlBtn.setText("Start Data Logging")	

class DataGraphWidget(pg.GraphicsLayoutWidget):
	def __init__(self):
		super(DataGraphWidget, self).__init__()

		self.dock = Dock("Data logger", size=(800, 1))
		self.dock.addWidget(self)

		dpi1 = DataPlotItem("Frequency (Hz)", a.asservConsThread, 3, color='m')
		dpi2 = DataPlotItem("Error", a.asservConsThread, 4, color='c')
		dpi3 = DataPlotItem("Mean signal (V)", a.asservConsThread, 5, color='r')
		dpi4 = DataPlotItem("Cell temperature (degC)", a.consThread, 1, color='w')
		dpi5 = DataPlotItem("EOM DC bias (V)", a.consThread, 2, color='w')

		self.plot_items_set1 = [dpi1,dpi2,dpi3]
		self.plot_items_set2 = [dpi4,dpi5]

  		# self.run_task_action = Parameter.create(name='Start', type='action')

  		self.addGraphItems()

		self.timer = QtCore.QTimer()
		self.timer.timeout.connect(self.update1)
		self.timer.start(100)

		self.timer2 = QtCore.QTimer()
		self.timer2.timeout.connect(self.update2)
		self.timer2.start(100)

		self.timer3 = QtCore.QTimer()
		self.timer3.timeout.connect(self.update3)
		self.timer3.start(1000)

	def addGraphItems(self):
		self.clear()
		for dpi in self.plot_items_set1+self.plot_items_set2:
			dpi.addItems(self)

	def update1(self):
		if a.asservConsThread and  a.asservConsThread.data.ptr >= 1:
			for dpi in self.plot_items_set1:
				dpi.update_plot()

	def update2(self):
		if a.consThread and a.consThread.data.ptr >= 1:
			for dpi in self.plot_items_set2:
				dpi.update_plot()

	def update3(self):
		if a.asservConsThread and a.asservConsThread.data.ptr  >= 2:
			for dpi in self.plot_items_set1:
				dpi.update_stats()

class MyWindow(QtGui.QMainWindow):
	def __init__(self):
		QtGui.QMainWindow.__init__(self)
		self.setWindowTitle("qtMain")
		self.resize(1550,900)

		self.data_graph_widget = DataGraphWidget()
		self.cw_freq_scanner = CWFreqScanner(a.syncAiAo,a.fs)
		self.pulse_freq_scanner = PulseFreqScanner(a.syncAiAoCPT_pulsed,a.fs)
		self.laser_freq_scanner = LaserFreqScanner(a.laserSyncAiAo)
		self.control_widget = ControlWidget(self)

		area = DockArea()
		self.setCentralWidget(area)
		self.d1 = Dock("Controls", size=(650, 1))
		area.addDock(self.d1, 'left')
		area.addDock(self.data_graph_widget.dock, 'right')
		area.addDock(self.laser_freq_scanner.dock, 'above', self.data_graph_widget.dock)
		area.addDock(self.cw_freq_scanner.dock, 'left', self.data_graph_widget.dock)
		area.addDock(self.pulse_freq_scanner.dock, 'bottom', self.cw_freq_scanner.dock)
		self.d1.addWidget(self.control_widget)
		

shelved_params = Shelved_param_UI()
a = DeviceSet()	

app = QtGui.QApplication([])
win = MyWindow()
win.show()

# if __name__ == "__main__":
# 	if (sys.flags.interactive != 1) or not hasattr(QtCore, 'PYQT_VERSION'):
# 		ret = QtGui.QApplication.instance().exec_()
# 		sys.exit(ret)