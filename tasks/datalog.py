from timeout import escapableSleep
import sys, threading, Queue, time
import numpy as np

log_separator = ","

class DataImu(object):
	def __init__(self,length,variables):
		self.queue = Queue.Queue()
		self.ptr = 0
		self.array = np.zeros((length,variables),dtype=np.float64)

	def get_data(self):
		return self.array[0:self.ptr,:]


class DataProducerWorker(threading.Thread):
	def __init__(self,data_producer):
		threading.Thread.__init__(self)
		self.daemon = True
		self.dp = data_producer
		self.delay = self.dp.delay

	def run(self):
		print("Data producer starting")
		while self.dp.running:
			data = []
			# Loop through the parameters and read values
			for getter in self.dp.getters:
				# If the getter object is not a function (not callable),
				# for instance, (a.regulation,'temperature'), getattr is used
				# to retrieve the actual value a.regulation.temperature
				if callable(getter):
					data.append(getter())
				else:
					data.append(getattr(*getter))					
			time.sleep(self.delay)
			self.dp.queue.put(data)

class DataProducer(object):
	queue = None
	running = False
	delay = 0.1

	def __init__(self,header_getters):
		self.header = [item[0] for item in header_getters]
		self.getters = [item[1] for item in header_getters]

	def start(self):
		self.running = True
		self.worker = DataProducerWorker(self)
		self.worker.start()

	def stop(self):
		self.running = False
		self.worker.join()
		

class DataConsumerWorker(threading.Thread):
	def __init__(self,data_consumer,logfile):
		threading.Thread.__init__(self)
		self.daemon = True
		self.dc = data_consumer
		self.logfile = logfile

	def run(self):
		print("DataConsumer waiting for data")
		while self.dc.running:
			try:
				data = self.dc.data.queue.get(True,3) # Wait data to be put in the queue for 3 sec max.
				if self.logfile:
					self.logfile.write(log_separator.join(["%.6f"%(item) for item in data])+"\n")
				if self.dc.is_full():
					self.dc.data.array = np.roll(self.dc.data.array,-1,0)
					self.dc.data.array[-1] = data
				else:
					self.dc.data.array[self.dc.data.ptr] = data
					self.dc.data.ptr += 1
			except Queue.Empty:
				pass
				# print("DataConsumerWorker Waiting for data")
			except:
				raise

class DataConsumer(object):
	# num_point_drawn = 1000
	running = False

	def __init__(self,fileheader=None,buffer_length=1000):
		self.fileheader = fileheader
		self.num_point_drawn = buffer_length
		self.data = DataImu(self.num_point_drawn,len(self.fileheader))

	def is_full(self):
		return self.data.ptr >= self.num_point_drawn

	def empty(self):
		self.data.ptr = 0

	def start(self,logfilename):
		self.logfile = open(logfilename,'w')
		self.data = DataImu(self.num_point_drawn,len(self.fileheader))
		self.logfile.write(log_separator.join(self.fileheader)+"\n")
		self.running = True
		self.worker = DataConsumerWorker(self,self.logfile)
		self.worker.start()

	def stop(self):
		self.running = False
		self.worker.join()
		self.logfile.close()
		self.logfile = None
