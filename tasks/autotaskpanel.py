# import tasks.automation
import pyqtgraph.parametertree.parameterTypes as pTypes
from pyqtgraph.parametertree import Parameter, ParameterTree
import inspect, glob, os, importlib
from . import automatedtasks

class AutoTaskPanel(object):

	task = None
	window = None
	deviceset = None

	def __init__(self):        

		self.param_group = pTypes.GroupParameter(name = 'Automated tasks')
		
		self.refresh_task_list_action = Parameter.create(name='Refresh list',
														type='action')
		self.task_list = Parameter.create(name='Task',
											type='list',
											values= {"":None})
		self.task_list.setLimits(automatedtasks.tasks)
		self.task_doc_param = Parameter.create(name='Doc',
												type='text',
												readonly=True)
		self.run_task_action = Parameter.create(name='Start',
												type='action')
		self.stop_task_action = Parameter.create(name='Stop',
												type='action')
		self.param_group.addChildren([self.refresh_task_list_action,
										self.task_list,
										self.task_doc_param,
										self.run_task_action,
										self.stop_task_action])

		self.param_group.sigTreeStateChanged.connect(self.change)

		self.refresh_task_list_action.sigActivated.connect(self.on_refresh_task_list)
		self.run_task_action.sigActivated.connect(self.on_run_task)
		self.stop_task_action.sigActivated.connect(self.on_stop_task)

	def change(self,param,changes):
	 	for param, change, data in changes:
	 		if param is self.task_list: self.task_doc_param.setValue(str(param.value().__doc__))

	def on_refresh_task_list(self):
		print("Reloading automated task list")
		importlib.reload(automatedtasks)
		self.task_list.setLimits(automatedtasks.tasks)

	def on_run_task(self):
		if self.task:
			self.task.stop()
		selectedTaskClass = self.task_list.value()
		self.task = selectedTaskClass(self.deviceset,self.window)
		self.task.start()

	def on_stop_task(self):
		if self.task:
			self.task.stop()