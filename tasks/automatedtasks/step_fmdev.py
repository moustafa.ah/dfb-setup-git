import time
from .basetask import Task
import numpy as np
from base.dataproducer import DataProducer


class StepFMdev(Task,DataProducer):
	"""Step the temperature of the VCSEL."""

	def __init__(self,deviceSet, window):
		Task.__init__(self,deviceSet, window)
		self.header = ["Time","FM dev (Hz)"]
		DataProducer.__init__(self,maxlength=100,numcol=len(self.header))

	def worker(self):
		assert not self.running
		self.running = True
		print("Automated task running")

		self.ds.dl.register_device(self, "taskfmdev",file_buffering=1)

		fm_devs = [600,1800]

		while self.running:
			for fm_dev in fm_devs:
				print("Setting new fm deviation: {:.2f}".format(fm_dev))
				self.ds.fs.fm_dev = fm_dev

				step_start_time = time.time()
				data = [step_start_time,fm_dev]
				self.send_to_consumers(data)
				while self.running and time.time()-step_start_time < 200.:
					time.sleep(3)
				if not self.running: break

		self.ds.dl.unregister_device(self)

		print("Automated task done")
