import time
from .basetask import Task
import numpy as np

class StepCellTempRepeat(Task):
	"""Step cell temperature setpoint..."""

	def worker(self):
		assert not self.running
		self.running = True
		print("Automated task running")
		initial_setpoint = self.ds.regulation.setpoint
		setpoints = [80,84,88,90,86,82,78,74,70,72,76,80]
		print(setpoints)
		while self.running:
			for setpoint in setpoints:
				print("Setting new cell temperature setpoint: {:.2f}".format(setpoint))
				step_start_time = time.time()
				self.ds.regulation.setpoint = setpoint
				while self.running and time.time()-step_start_time < 1000.:
					time.sleep(1)
				if not self.running: break

		self.ds.regulation.setpoint = initial_setpoint
		print("Automated task done")