import time
from .basetask import Task
import numpy as np

class StepInputPower(Task):
	"""Step input power setpoint..."""

	def worker(self):
		assert not self.running
		self.running = True
		print("Automated task running")
		initial_setpoint = self.ds.pow_ctrl.setpoint
		#setpoints = [24,23,23.5,25.5,26,25.5]
		#♦setpoints = [10,15,20,25,30,35,40,45,50]
		setpoints = np.linspace(20,40,21)
		print(setpoints)
		while self.running:
			for setpoint in setpoints:
				print("Setting new input power setpoint: {:.2f}".format(setpoint))
				step_start_time = time.time()
				self.ds.pow_ctrl.setpoint = setpoint
				while self.running and time.time()-step_start_time < 120.:
					time.sleep(1)
				if not self.running: break

		self.ds.pow_ctrl.setpoint = initial_setpoint
		print("Automated task done")