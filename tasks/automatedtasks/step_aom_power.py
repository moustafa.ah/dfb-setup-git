import time
from .basetask import Task
import numpy as np

class StepAOMPower(Task):
	"""Step AOM power amplitude v2."""

	def worker(self):
		assert not self.running
		self.running = True
		print("Automated task running")
		initial_setpoint = self.ds.aom_fs.power_amplitude
		start = -14.
		end = -5.
		num = 5
		setpoints = np.hstack([np.linspace(start,end,num,endpoint=False),np.linspace(end,start,num,endpoint=False)])
		print(setpoints)
		while self.running:
			for setpoint in setpoints:
				print("Setting new AOM power amplitude: {:.2f}".format(setpoint))
				step_start_time = time.time()
				self.ds.aom_fs.power_amplitude = setpoint
				while self.running and time.time()-step_start_time < 100.:
					time.sleep(1)
				if not self.running: break
			# if not self.running: break

		self.ds.aom_fs.power_amplitude = initial_setpoint
		print("Automated task done")