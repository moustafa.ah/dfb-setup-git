import time
from .basetask import Task
import numpy as np
from base.dataproducer import DataProducer

class StepRFandInPower(Task,DataProducer):
	"""Step RF power and input power."""

	def __init__(self,deviceSet, window):
		Task.__init__(self,deviceSet, window)
		self.header = ["Time","InPowSP (uW)","RFPow (dBm)"]
		DataProducer.__init__(self,maxlength=100,numcol=len(self.header))

	def worker(self):
		assert not self.running
		self.running = True
		print("Automated task running")

		self.ds.dl.register_device(self, "taskRfInPow",file_buffering=1)

		rf_powers = [-2.0,-2.4,-2.8,-3.,-2.6,-2.2]
		input_powers = [24,23,23.5,25.5,26,25.5]
		initial_rf_power = self.ds.fs.power_amplitude
		initial_input_power = self.ds.pow_ctrl.setpoint
		while self.running:
			for rf_power in rf_powers:
				self.ds.fs.power_amplitude = rf_power
				time.sleep(3)
				for input_power in input_powers:
					print(f'Rf power : {rf_power}, Input power : {input_power}')
					self.ds.pow_ctrl.setpoint = input_power
					step_start_time = time.time()
					data = [step_start_time,input_power,rf_power]
					self.send_to_consumers(data)
					while self.running and time.time()-step_start_time < 120.:
						time.sleep(1)
					if not self.running: break
				if not self.running: break
		self.ds.fs.power_amplitude = initial_rf_power
		self.ds.pow_ctrl.setpoint = initial_input_power

		print("Automated task done")