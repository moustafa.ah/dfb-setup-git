import time
from .basetask import Task
import numpy as np
from base.dataproducer import DataProducer


class StepFMdevVCSELtempRFpower(Task,DataProducer):
	"""Step FM dev VCSEL temp RF pow."""

	def __init__(self,deviceSet, window):
		Task.__init__(self,deviceSet, window)
		self.header = ["Time","RFPow (dBm)","VCSEL temp tune (V)","FM dev (Hz)"]
		DataProducer.__init__(self,maxlength=100,numcol=len(self.header))

	def worker(self):
		assert not self.running
		self.running = True
		print("Automated task running")

		self.ds.dl.register_device(self, "taskfmrfvtemp",file_buffering=1)


		rf_powers = [-2.4,-2.8,-3.,-2.6,-2.2,-2.0]
		voltages = [-20e-4,-15e-4,-25e-4,-35e-4,-40e-4,-30e-4]
		fm_devs = [600,1200,1800]

		while self.running:
			for rf_power in rf_powers:
				print("Setting new rf power: {:f}".format(rf_power))
				self.ds.fs.power_amplitude = rf_power

				for voltage in voltages:
					print("Setting new voltage: {:f}".format(voltage))
					self.ds.analogOuts.setOutputVoltage(voltage,'ao0',(-1.,1.))

					for fm_dev in fm_devs:
						print("Setting new fm deviation: {:.2f}".format(fm_dev))
						self.ds.fs.fm_dev = fm_dev

						step_start_time = time.time()
						data = [step_start_time,rf_power,voltage,fm_dev]
						self.send_to_consumers(data)
						while self.running and time.time()-step_start_time < 120.:
							time.sleep(1)
						if not self.running: return
					if not self.running: return
				if not self.running: return

		self.ds.dl.unregister_device(self)

		print("Automated task done")
