import time
from .basetask import Task
import numpy as np

class StepRFPower(Task):
	"""Step RF power."""

	def worker(self):
		assert not self.running
		self.running = True
		print("Automated task running")
		initial_setpoint = self.ds.fs.power_amplitude
		powers = [-3.05,-3.10,-3.05,-3.,-2.95,-2.90,-2.95,-3.]
		print(powers)
		while self.running:
		# for i in xrange(10):
			for power in powers:
				print("Setting RF power: {:.2f}".format(power))
				step_start_time = time.time()
				self.ds.fs.power_amplitude = power
				while self.running and time.time()-step_start_time < 200.:
					time.sleep(1)
				if not self.running: break

		self.ds.fs.power_amplitude = initial_setpoint
		print("Automated task done")