import time
from .basetask import Task
import numpy as np
from base.dataproducer import DataProducer


class StepFMdevAOMpowerRFpower(Task,DataProducer):
	"""Step FM dev and AOM power."""

	def __init__(self,deviceSet, window):
		Task.__init__(self,deviceSet, window)
		self.header = ["Time","Step index","Sequence index","AOM power (dBm)","FM dev (Hz)","RF power (dBm)"]
		DataProducer.__init__(self,maxlength=100,numcol=len(self.header))

	def worker(self):
		assert not self.running
		self.running = True
		print("Automated task running")

		self.ds.dl.register_device(self, "task2",file_buffering=1)

		def ascend_descend(start,end,num):
			return np.hstack([np.linspace(start,end,num,endpoint=False),np.linspace(end,start,num,endpoint=False)])

		aom_powers = ascend_descend(-14,-5,3)
		fm_devs = [600,1000,1400]
		rf_powers = ascend_descend(-3,-4,5)
		
		self.sequence_index = 0

		def loop():
			step_index = 0
			for rf_power in rf_powers:
				print("Setting new RF power: {:.2f}".format(rf_power))
				self.ds.fs.power_amplitude = rf_power
				for fm_dev in fm_devs:
					print("Setting new fm deviation: {:.2f}".format(fm_dev))
					self.ds.fs.fm_dev = fm_dev
					for aom_power in aom_powers:
						print("Setting new AOM power amplitude: {:.2f}".format(aom_power))
						self.ds.aom_fs.power_amplitude = aom_power
						step_start_time = time.time()
						data = [step_start_time,step_index,self.sequence_index,aom_power,fm_dev,rf_power]
						self.send_to_consumers(data)
						while self.running and time.time()-step_start_time < 120.:
							time.sleep(1)
						if not self.running: return
						step_index += 1
			self.sequence_index += 1

		while self.running:
			loop()

		print("Automated task done")
