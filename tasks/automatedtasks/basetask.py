import threading

class Task(object):
	"""This is the abstract Task class that all other tasks should inherit"""
	def __init__(self, deviceSet, window):
		self.ds = deviceSet
		self.window = window
		self.running = False

	def worker(self):
		self.running = True
		print("Task started")
		#Do something
		# while self.running:
		print("Task done")
			# pass

	def start(self):
		self.thread = threading.Thread(target=self.worker)
		self.thread.start()

	def stop(self):
		self.running = False
		self.thread.join()
		print("Task stopped")