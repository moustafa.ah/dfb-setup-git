import time
from .basetask import Task
import numpy as np
from base.dataproducer import DataProducer


class StepVcselTempInPow(Task,DataProducer):
	"""Step the temperature of the VCSEL and the input power."""

	def __init__(self,deviceSet, window):
		Task.__init__(self,deviceSet, window)
		self.header = ["Time","VCSEL temp tune (V)","Input power SP (uW)"]
		DataProducer.__init__(self,maxlength=100,numcol=len(self.header))

	def worker(self):
		assert not self.running
		self.running = True
		print("Automated task running")

		self.ds.dl.register_device(self, "tasklateminpow",file_buffering=1)

		voltages = [-20e-4,-15e-4,-25e-4,-35e-4,-40e-4,-30e-4]
		powers = [24,23,22,22.5,23.5,25.5,26.5,27,26,24.5]

		while self.running:
			for voltage in voltages:
				print("Setting new voltage: {:f}".format(voltage))
				self.ds.analogOuts.setOutputVoltage(voltage,'ao0',(-1.,1.))

				for power in powers:
					print("Setting new input power setpoint: {:.2f}".format(power))
					self.ds.pow_ctrl.setpoint = power

					step_start_time = time.time()
					data = [step_start_time,voltage,power]
					self.send_to_consumers(data)
					while self.running and time.time()-step_start_time < 300.:
						time.sleep(3)
					if not self.running: break
				if not self.running: break

		self.ds.dl.unregister_device(self)

		print("Automated task done")
