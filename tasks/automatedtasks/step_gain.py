import time
from .basetask import Task
import numpy as np
from base.dataproducer import DataProducer


class StepGain(Task,DataProducer):
	"""Step freq lock gain."""

	def __init__(self,deviceSet, window):
		Task.__init__(self,deviceSet, window)
		self.header = ["Time","Gain"]
		DataProducer.__init__(self,maxlength=100,numcol=len(self.header))

	def worker(self):
		assert not self.running
		self.running = True
		print("Automated task running")

		self.ds.dl.register_device(self, "taskgain",file_buffering=1)

		gains = [8e4,1e5,4e4,2e4,1e4]

		while self.running:
			for gain in gains:
				print("Setting new gains: {:f}".format(gain))
				self.ds.asserv.gain = gain
				step_start_time = time.time()
				data = [step_start_time,gain]
				self.send_to_consumers(data)
				while self.running and time.time()-step_start_time < 1000.:
					time.sleep(3)
				if not self.running: break

		self.ds.dl.unregister_device(self)

		print("Automated task done")
