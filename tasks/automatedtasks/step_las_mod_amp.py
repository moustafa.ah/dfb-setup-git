import time
from .basetask import Task
import numpy as np
from base.dataproducer import DataProducer


class StepLasModAmp(Task,DataProducer):
	"""Step the laser modulation amplitude."""

	def __init__(self,deviceSet, window):
		Task.__init__(self,deviceSet, window)
		self.header = ["Time","Amplitude (V)"]
		DataProducer.__init__(self,maxlength=100,numcol=len(self.header))

	def worker(self):
		assert not self.running
		self.running = True
		print("Automated task running")

		self.ds.dl.register_device(self, "tasklasmodamp",file_buffering=1)

		amplitudes = [8e-3,16e-3,32e-3,64e-3,100e-3]

		initial_amplitude = self.ds.li_las.amplitude

		while self.running:
			for amplitude in amplitudes:
				print("Setting new amplitude: {:f}".format(amplitude))
				self.ds.li_las.amplitude = amplitude
				step_start_time = time.time()
				data = [step_start_time,self.ds.li_las.amplitude]
				self.send_to_consumers(data)
				while self.running and time.time()-step_start_time < 1800.:
					time.sleep(3)
				if not self.running: break

		self.ds.li_las.amplitude = initial_amplitude

		self.ds.dl.unregister_device(self)

		print("Automated task done")
