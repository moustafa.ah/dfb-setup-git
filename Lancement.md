# Démarrage d'une mesure de vieillissement

Dans le dossier Desktop\Pilotage\dfb-setup, ouvrir une invite de commande dans le dossier (clique droit + Majuscule)
Lancer : python new_vcsel_main.py

# Problèmes connus

La régulation thermique Cyrus "cypidreg" ne répond pas. Débrancher/rebrancher l'USB ou déactiver/activer le périphérique (com4) dans le gestionnaire de périphériques.

Idem pour LockBox (attendre plus longtemps après connexion?)

Problème Shelved parameters

Problèmes Source courant


# Démarrage de Jupyter Lab et traitement des fichiers

