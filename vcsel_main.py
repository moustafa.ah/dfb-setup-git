import time, sys
import numpy as np
from storage.shelved_param_UI import Shelved_param_UI
from tasks.datalog import *
from base.deviceset import DeviceSet
from tasks.autotaskpanel import AutoTaskPanel
print("Loading pyqtgraph")
import pyqtgraph as pg
from pyqtgraph.dockarea import *
from pyqtgraph.Qt import QtGui, QtCore
import pyqtgraph.parametertree.parameterTypes as pTypes
from pyqtgraph.parametertree import Parameter, ParameterTree

print("Loading drivers")
from drivers import *

from ui.graphs.pulsed_freq_scanner import PulseFreqScanner
from ui.graphs.cw_freq_scanner import CWFreqScanner
from ui.graphs.laser_freq_scanner import LaserFreqScanner
from ui.toggle_button import ToggleButton
from ui.dataPlotItem import DataPlotItem

# import logging
# logger = logging.getLogger('driver')
# logger.setLevel(logging.DEBUG)
# ch = logging.StreamHandler()
# formatter = logging.Formatter("%(asctime)s - %(levelname)s - %(message)s")
# ch.setFormatter(formatter)
# logger.addHandler(ch)

data_folder_path = "G:\Pilotage\Python Data"

ds = DeviceSet()
ds.shelved_params = Shelved_param_UI(filename='vcsel_params.shelve')
ds.regulation = CYPidReg_UI(comPort='com4')
ds.lock_box = LockBox_UI(comPort='com7')
ds.pow_meter = PM16()
ds.pow_ctrl = PowerControllerAOM_UI(ds.pow_meter)
ds.daq = NIUSB6259(devID='Dev1')
ds.fs = SMB100A_UI(pow_amp_lim=12,
							usbDev="USB0::0x0AAD::0x0054::178059::INSTR",
							name="RF Driver")
ds.aom_fs = SMB100A_AM_UI(pow_amp_lim = -2,
							usbDev= "USB0::0x0AAD::0x0054::176227::INSTR",
							name="AOM Driver")
ds.li_las = SR830_UI(gpibAdress=9,name="Laser DC lock-in")
ds.syncAiAo = SyncAIAO_UI(device="Dev1",
							inChanList=["ai0"],
							outChan="ao3",
							inRange=(-3.,3.),
							outRange=(-1.,1.))
ds.laserSyncAiAo = SyncAIAO_UI(device="Dev1",inChanList=["ai0","ai1"],outChan="ao2",inRange=(-10.,10.),outRange=(-10.,10.)) #(EK) ajout de la voie 3 pour lire cesium pur
ds.laserSyncAiAo.vpp=18
ds.laserSyncAiAo.numSamp=4000
ds.laserSyncAiAo.nbSampCropped=2000
ds.asserv = Asserv_UI(device="Dev1",rf_fm_out_chan="ao3",inChan="ai0",dds_device=ds.fs)
# ds.laserCtrl = LaserController_UI(ds.daq,laserModChan="ao2",signalChan="ai0",lockBox=ds.lock_box)
ds.analogOuts = AnalogOutputs(devID='Dev1',
							chanDic={"Laser DC offset":"ao2",
									"AOM offset":"ao1"})
ds.multimeter2 = HP34401A(gpibAdress=16)
ds.multimeter3 = HP34401A(gpibAdress=10)
ds.multimeter4 = HP34401A(gpibAdress=24)
ds.multimeter5 = HP34401A(gpibAdress=13)
ds.multimeter6 = HP34401A(gpibAdress=15)
ds.multimeter7 = HP34401A(gpibAdress=23)

def convertor(voltage):
	resistance = voltage/4.94287e-4
	return resistance

ds.tempCtrlVcsel = TempController(ds.multimeter6,name="VCSEL thermometer",char="ULMBETHA",convertorVtoRes=convertor)
ds.tempCtrlRoom = TempController(ds.multimeter3,name="Room thermometer",char="BETHAT5K")
# ds.heatcurrentCtrl = HeatCurrentController(s.multimeter3,name="Heater current")
ds.tempCtrlBox = TempController(ds.multimeter4,name="Box thermometer",char="BETHAT10K")


		# Rows must have the form:    [ "Label", function ]
		# 					       or [ "Label", (object,'parameter') ]
		# If the device directly provides a function to retrieve the parameter, use case 1
		# Otherwise, if it only provides a property that cannot be called, use case 2
header_getters = [["Time",time.time],
				["Power meter (uW)",(ds.pow_ctrl,"power")],
				["Cell temperature (degC)",(ds.regulation,'temperature')],
				["Room temperature (degC)",(ds.tempCtrlRoom,'temperature')],
				# ["Heater current (A)",(a.heatcurrentCtrl,'current')],
				["Box temperature (degC)",(ds.tempCtrlBox,'temperature')],
				["Heater current Image",(ds.regulation,'output')],
				["Laser current (V)",ds.multimeter5.read],
				# ["Optical Power (uW)",(a.powCtrl,"power")],
				# ["Optical Power Output Cell (V)",a.multimeter2.read],
				["Vcsel Temperature (degC)",(ds.tempCtrlVcsel,'temperature')],
				["Laser mod signal (V)",ds.multimeter7.read],
				]
ds.prodThread = DataProducer(header_getters)
ds.consThread = DataConsumer(ds.prodThread.header,buffer_length=60)
ds.asservConsThread = DataConsumer(ds.asserv.header,buffer_length=500)

ds.dl = DataLogger()
ds.autotapa = AutoTaskPanel()

class DataLogger(object):

	def __init__(self):
		self.param_group = pTypes.GroupParameter(name='Data logger')
		self.clear_plot_action = Parameter.create(name='Clear plot', type='action')
		self.param_group.addChildren([self.clear_plot_action])

		self.clear_plot_action.sigActivated.connect(self.empty)

		self.running = False

	def __del__(self):
		self.stop()

	def start(self,running_asserv):
		self.running_asserv = running_asserv
		assert not self.running
		self.running = True

		timeStr = time.strftime("%Y-%m-%d %H%M")
		self.f1_name = "{}\datalog {} A.txt".format(data_folder_path,timeStr)
		self.f2_name = "{}\datalog {} B.txt".format(data_folder_path,timeStr)

		a.consThread.start(self.f1_name)
		a.asservConsThread.start(self.f2_name)

		a.prodThread.start()
		self.running_asserv.dataQueue = a.asservConsThread.data.queue
		a.prodThread.queue = a.consThread.data.queue

	def stop(self):
		if self.running:
			self.running = False
			a.consThread.stop()
			a.asservConsThread.stop()
			a.prodThread.stop()
			if self.running_asserv: self.running_asserv.dataQueue = None

	def empty(self):
		a.consThread.empty()
		a.asservConsThread.empty()

class ControlWidget(pg.LayoutWidget):

	def __init__(self, window=None):
		super(ControlWidget, self).__init__(window)
		self.window = window
		self.startLaserScanBtn = ToggleButton("Laser Scan", self.window.laser_freq_scanner)
		self.startFreqScanBtn = ToggleButton("Frequency Scan", self.window.cw_freq_scanner)		
		self.startAsservBtn = ToggleButton("Asserv", a.asserv)

		self.startDlBtn = QtGui.QPushButton("Start Data Logging")
		self.startDlBtn.setCheckable(True)
		self.startDlBtn.clicked.connect(self.on_startDlBtn)

		self.p = Parameter.create(name='params', type='group')

		constparams_gp = a.shelved_params.param_group

		devices_gp = pTypes.GroupParameter(name='Devices')
		devices_gp.addChildren([a.lock_box.param_group,
							a.analogOuts.param_group,
							# a.laserCtrl.param_group,
							a.li_las.param_group,
							a.pow_ctrl.param_group,
							# a.li_magic.param_group,
							a.fs.param_group,
							a.aom_fs.param_group,
							a.regulation.param_group
							])
	
		cw_op_gp = pTypes.GroupParameter(name='CW Operation')
		cw_op_gp.addChild(a.asserv.param_group)
		cw_op_gp.addChild(self.window.cw_freq_scanner.param_group)

		laser_scan_gp = self.window.laser_freq_scanner.param_group

		datalog_gp = a.dl.param_group
		autota_gp = a.autotapa.param_group

		self.p.addChildren([constparams_gp,
							devices_gp,
							laser_scan_gp,
							cw_op_gp,
							datalog_gp,
							autota_gp])
		
		t = ParameterTree()
		t.setParameters(self.p, showTop=False)
		
		self.addWidget(self.startLaserScanBtn,row=0,col=0)
		self.addWidget(self.startFreqScanBtn,row=1,col=0)
		self.addWidget(self.startAsservBtn,row=2,col=0)
		self.addWidget(self.startDlBtn,row=3,col=0)
		self.addWidget(t,row=4,col=0)

	def on_startDlBtn(self):
		if self.startDlBtn.isChecked():
			a.dl.start(running_asserv = a.asserv)
			self.startDlBtn.setText("Stop Data Logging")
			try: self.window.data_graph_widget.dock.raiseDock()
			except: pass
		else:
			a.dl.stop()
			self.startDlBtn.setText("Start Data Logging")	

class DataGraphWidget(pg.GraphicsLayoutWidget):
	def __init__(self):
		super(DataGraphWidget, self).__init__()

		self.dock = Dock("Data logger", size=(800, 1))
		self.dock.addWidget(self)

		dpi1 = DataPlotItem("Frequency (Hz)", a.asservConsThread, 3, color='m')
		dpi2 = DataPlotItem("Error", a.asservConsThread, 4, color='c')
		dpi3 = DataPlotItem("Mean signal (V)", a.asservConsThread, 5, color='r')
		# dpi33 = DataPlotItem("Power Error", a.asservConsThread, 6, color='g')
		# dpi34 = DataPlotItem("RF power (dBm)", a.asservConsThread, 7, color='b')
		dpi41 = DataPlotItem("Power meter (uW)", a.consThread, 1, color='w')
		dpi42 = DataPlotItem("Cell temperature (degC)", a.consThread, 2, color='y')
		dpi5 = DataPlotItem("Room temperature (degC)", a.consThread, 3, color='b')
		dpi6 = DataPlotItem("Box temperature (degC)", a.consThread, 4, color='g')
		dpi7 = DataPlotItem("Heater current image", a.consThread, 5, color='w')
		dpi8 = DataPlotItem("Laser current (V)", a.consThread, 6, color='m')
		dpi9 = DataPlotItem("VCSEL Temperature (degC)", a.consThread, 7, color='c')
		dpi10 = DataPlotItem("VCSEL mod signal (V)", a.consThread, 8, color='r')

		self.plot_items_set1 = [dpi1,dpi2,dpi3]
		self.plot_items_set2 = [dpi41,dpi42,dpi5,dpi6,dpi7,dpi8,dpi9,dpi10]

  		# self.run_task_action = Parameter.create(name='Start', type='action')

  		self.addGraphItems()

		self.timer = QtCore.QTimer()
		self.timer.timeout.connect(self.update1)
		self.timer.start(100)

		self.timer2 = QtCore.QTimer()
		self.timer2.timeout.connect(self.update2)
		self.timer2.start(100)

	def addGraphItems(self):
		self.clear()
		for dpi in self.plot_items_set1+self.plot_items_set2:
			dpi.addItems(self)

	def update1(self):
		if a.asservConsThread and  a.asservConsThread.data.ptr >= 1:
			for dpi in self.plot_items_set1:
				dpi.update_plot()
				dpi.update_stats()

	def update2(self):
		if a.consThread and a.consThread.data.ptr >= 1:
			for dpi in self.plot_items_set2:
				dpi.update_plot()
				dpi.update_stats()

class MyWindow(QtGui.QMainWindow):
	def __init__(self):
		QtGui.QMainWindow.__init__(self)
		self.setWindowTitle("qtMain")
		self.resize(1550,900)

		saveConfigAction = QtGui.QAction('Save configuration', self)

		menubar = self.menuBar()
		dsMenu = menubar.addMenu('&Devices')
		dsMenu.addAction(saveConfigAction)
		saveConfigAction.triggered.connect(a.save_config)

		self.data_graph_widget = DataGraphWidget()
		self.cw_freq_scanner = CWFreqScanner(a.syncAiAo,a.fs)
		self.laser_freq_scanner = LaserFreqScanner(a.laserSyncAiAo)
		self.control_widget = ControlWidget(self)

		area = DockArea()
		self.setCentralWidget(area)
		self.d1 = Dock("Controls", size=(400, 1))
		area.addDock(self.d1, 'left')
		area.addDock(self.data_graph_widget.dock, 'right')
		area.addDock(self.laser_freq_scanner.dock, 'above', self.data_graph_widget.dock)
		area.addDock(self.cw_freq_scanner.dock, 'above', self.data_graph_widget.dock)
		self.d1.addWidget(self.control_widget)
		
	
app = QtGui.QApplication([])
win = MyWindow()
ds.autotapa.window = win
ds.autotapa.deviceset = ds
win.show()

if __name__ == "__main__":
	if (sys.flags.interactive != 1) or not hasattr(QtCore, 'PYQT_VERSION'):
		ret = QtGui.QApplication.instance().exec_()
		ds.pow_ctrl.stop()
		sys.exit(ret)
		